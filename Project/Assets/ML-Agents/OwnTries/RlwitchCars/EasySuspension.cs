﻿using UnityEngine;
//not our code- for easier control of wheels colliders
[ExecuteInEditMode]
public class EasySuspension : MonoBehaviour
{
    //option set for balancing for GameDesigner-later set only on Awake
    [Range(10, 1000)]
    [Tooltip("As name say-for wheel colither")]
    [SerializeField] private float _massOfWheel = 100;
    [Range(0.1f, 20f)]
    [Tooltip("Natural frequency of the suspension springs. Describes bounciness of the suspension.")]
    [SerializeField] private float _naturalFrequency = 10;

    [Range(0f, 3f)]
    [Tooltip("Damping ratio of the suspension springs. Describes how fast the spring returns back after a bounce. ")]
    [SerializeField] private float _dampingRatio = 0.8f;

    [Range(-1f, 1f)]
    [Tooltip("The distance along the Y axis the suspension forces application point is offset below the center of mass")]
    [SerializeField] private float _forceShift = 0.03f;

/*    [Range(0, 3f)]
    [Tooltip("Multiplayer for spring for back wheels")]
    [SerializeField] private float _backSpringmMultiplayer = 1;

    [Range(0f, 3f)]
    [Tooltip("Multiplayer for Damper for back wheels")]
    [SerializeField] private float _backDamperMultiplayer = 1;*/

    [Tooltip("Adjust the length of the suspension springs according to the natural frequency and damping ratio. When off, can cause unrealistic suspension bounce.")]
    [SerializeField] private bool _setSuspensionDistance = true;
/*    [Header("Forawrd Friction")]
    [Range(0f, 10f)]
    [Tooltip("Extremum Forward Slip")]
    [SerializeField] private float _extForwSlip = 0.4f;
    [Range(0f, 10f)]
    [Tooltip("Extremum Forward Value")]
    [SerializeField] private float _extForwValue = 1f;
    [Range(0f, 10f)]
    [Tooltip("Asymptote Forward Slip")]
    [SerializeField] private float _AsymForwSlip = 0.8f;
    [Range(0f, 10f)]
    [Tooltip("Asymptote Forward Value")]
    [SerializeField] private float _AsymForwValue = 0.5f;
    [Range(0f, 10f)]
    [Tooltip("Forward Stiffness")]
    [SerializeField] private float _forwStiffness = 1;
    [Header("Sideways Friction")]
    [Range(0f, 10f)]
    [Tooltip("Extremum Sideways Slip")]
    [SerializeField] private float _extSideSlip = 0.2f;
    [Range(0f, 10f)]
    [Tooltip("Extremum Sideways Value")]
    [SerializeField] private float _extSideValue = 1f;
    [Range(0f, 10f)]
    [Tooltip("Asymptote Sideways Slip")]
    [SerializeField] private float _AsymSideSlip = 0.5f;
    [Range(0f, 10f)]
    [Tooltip("Asymptote Sideways Value")]
    [SerializeField] private float _AsymSideValue = 0.75f;
    [Range(0f, 10f)]
    [Tooltip("Sideways Stiffness")]
    [SerializeField] private float _SideStiffness = 1;
    [Header("AntiRoll")]
    [SerializeField] private bool _antiRollActive = false;*/
    [Header("MoveCenterMassForward")]
    [SerializeField] private bool _activeMoveCenterMass = false;
    [Range(0f, 3f)]
    [SerializeField] private float _scaleMoveCenterMass = 1;
    private Rigidbody _mRigidbody;
    private float _sqrtWcSprungMass, _distance;
    private Vector3 _wheelRelativeBody;
    private JointSpring _spring;
    private WheelFrictionCurve _friction;
/*    private int k = 0;*/
    private WheelCollider _tempwc;
    void Start()
    {
        _mRigidbody = GetComponent<Rigidbody>();
        if (_activeMoveCenterMass)
        {
            _mRigidbody.centerOfMass += Vector3.forward * _scaleMoveCenterMass;
        }
    }

    void FixedUpdate()
    {
        // Work out the stiffness and damper parameters based on the better spring model.
        foreach (WheelCollider wc in GetComponentsInChildren<WheelCollider>())
        {
          /*  if (_antiRollActive)
            {
                k = k == 4 ? k = 0 : k++;
                if (k % 2 == 0)
                {
                    AntiRoll(_tempwc, wc);
                }
                else
                {
                    _tempwc = wc;
                }

            }*/
            wc.mass = _massOfWheel;
            _spring = wc.suspensionSpring;

            _sqrtWcSprungMass = Mathf.Sqrt(wc.sprungMass);
            _spring.spring = _sqrtWcSprungMass * _naturalFrequency * _sqrtWcSprungMass * _naturalFrequency;
            _spring.damper = 2f * _dampingRatio * Mathf.Sqrt(_spring.spring * wc.sprungMass); //co to jest to 2f? <- zrobić to do zmiennej

            wc.suspensionSpring = _spring;

            _wheelRelativeBody = transform.InverseTransformPoint(wc.transform.position);
            _distance = _mRigidbody.centerOfMass.y - _wheelRelativeBody.y + wc.radius;

            wc.forceAppPointDistance = _distance - _forceShift;

            // Make sure the spring force at maximum droop is exactly zero
            if (_spring.targetPosition > 0 && _setSuspensionDistance)
                wc.suspensionDistance = wc.sprungMass * Physics.gravity.magnitude / (_spring.targetPosition * _spring.spring);

            /*if (wc.gameObject.name == "a1r" || wc.gameObject.name == "a1l")
            {
                _spring = wc.suspensionSpring;
                _spring.spring *= _backSpringmMultiplayer;
                _spring.damper *= _backDamperMultiplayer;
                wc.suspensionSpring = _spring;

            }
            _friction = wc.forwardFriction;
            _friction.extremumSlip = _extForwSlip;
            _friction.extremumSlip = _extForwValue;
            _friction.asymptoteSlip = _AsymForwSlip;
            _friction.asymptoteValue = _AsymForwValue;
            wc.forwardFriction = _friction;
            _friction = wc.sidewaysFriction;
            _friction.extremumSlip = _extSideSlip;
            _friction.extremumSlip = _extSideValue;
            _friction.asymptoteSlip = _AsymSideSlip;
            _friction.asymptoteValue = _AsymSideValue;
            wc.sidewaysFriction = _friction;*/

        }
    }
    private void AntiRoll(WheelCollider WheelL, WheelCollider WheelR)
    {
        WheelHit hit;
        float travelL = 1.0f;
        float travelR = 1.0f;
        float _antiRoll = WheelL.suspensionSpring.spring;
        bool groundedL = WheelL.GetGroundHit(out hit);
        if (groundedL)
            travelL = (-WheelL.transform.InverseTransformPoint(hit.point).y - WheelL.radius) / WheelL.suspensionDistance;

        bool groundedR = WheelR.GetGroundHit(out hit);
        if (groundedR)
            travelR = (-WheelR.transform.InverseTransformPoint(hit.point).y - WheelR.radius) / WheelR.suspensionDistance;

        float antiRollForce = (travelL - travelR) * _antiRoll;

        if (groundedL)
            _mRigidbody.AddForceAtPosition(WheelL.transform.up * -antiRollForce,
                   WheelL.transform.position);
        if (groundedR)
            _mRigidbody.AddForceAtPosition(WheelR.transform.up * antiRollForce,
                   WheelR.transform.position);
    }
}
