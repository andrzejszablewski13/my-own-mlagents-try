﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class CarAgent : Agent
{
    private Rigidbody _rBody;
    private WheelDrive _driver;
    public Vector3 startPos=new Vector3(0,1,0);
    void Start()
    {
        _rBody = GetComponent<Rigidbody>();
        _driver = GetComponent<WheelDrive>();
        
    }

    private Transform target;
    public Transform targetsparent;
    public Transform[] targets;
    public Material _targetmat, _notarget;
    public override void OnEpisodeBegin()
    {
        if (this.transform.localPosition.y < -0.3 || Mathf.Abs(this.transform.localRotation.z) > 5 || Mathf.Abs(this.transform.localRotation.x) > 5)
        {
            // If the Agent fell, zero its momentum
            this._rBody.angularVelocity = Vector3.zero;
            this.transform.rotation = new Quaternion(0, 0, 0, 0);
            this._rBody.velocity = Vector3.zero;
            this.transform.localPosition = startPos;
        }
        olddistanceToTarget = 0;

        // Move the target to a new spot
        if (target!=null)
        {
            target.GetComponent<Renderer>().material = _notarget;
        }
        target = targets[Mathf.Abs(Random.Range(0,targets.Length))];
        target.GetComponent<Renderer>().material = _targetmat;
        dirtotarget = (target.position - this.transform.position).normalized;
        olddistanceToTarget = Vector3.Distance(this.transform.localPosition, target.localPosition);
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        Vector3 _pos, _vel;
        _pos = target.position;
        _vel = _rBody.velocity;
        // Target and Agent positions
        sensor.AddObservation(this.transform.InverseTransformPoint(_pos));
        sensor.AddObservation(this.transform.InverseTransformVector(_vel));
        sensor.AddObservation(this.transform.InverseTransformDirection(dirtotarget));

    }
    public float forceMultiplier = 10;
    private float distanceToTarget, olddistanceToTarget = 0, velalligment=0;
    Vector3 dirtotarget;
    public override void OnActionReceived(float[] vectorAction)
    {
        // Actions, size = 2
        float controlSignal = 0;
        controlSignal = vectorAction[0];
        _driver.angle = controlSignal*_driver.maxAngle;
        //controlSignal = vectorAction[1];
        _driver.torque = 1 * _driver.maxTorque;
        dirtotarget = (target.position - this.transform.position).normalized;
        velalligment = Vector3.Dot(dirtotarget, _rBody.velocity);
        // Rewards
        SetReward(-0.001f);
        float distanceToTarget = Vector3.Distance(this.transform.localPosition, target.localPosition);
        if (distanceToTarget<olddistanceToTarget)
        {
            SetReward(0.05f*velalligment);
        }
        // Reached target
        if (distanceToTarget < 3f)
        {
            SetReward(50.0f);
            target.GetComponent<Renderer>().material = _notarget;
            EndEpisode();
        }


        // Fell off platform
        if (this.transform.localPosition.y < -0.3 || Mathf.Abs( this.transform.localRotation.z)>5 || Mathf.Abs(this.transform.localRotation.x) >5)
        {
            SetReward(-5f);
            target.GetComponent<Renderer>().material = _notarget;
            EndEpisode();
        }
        olddistanceToTarget = distanceToTarget;
    }
    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = Input.GetAxis("Horizontal");
        //actionsOut[1] = Input.GetAxis("Vertical");
    }
}
