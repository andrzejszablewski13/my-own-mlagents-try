﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Reeversi
{
    public class CreateEnv : MonoBehaviour
    {
        // Start is called before the first frame update
        public GameObject _platePreFab, _darkPreFab, _lightPreFab, _assPreFab;
        public Material _posMove, _unPosMove;
        public int sizeOfMap = 8;
        public int[,] _dataMap;
        private GameObject[,] _visDataMap, _assDataMap;
        public Stack<GameObject> _darkStack, _lightStack;
        public UnityEvent happend;
        public bool notOnlyData = true;
        private void Awake()
        {
            CreateVisEnv();
            /* Debug.Log(PossibleMoveStack(true).Pop());
             MadeMove(PossibleMoveStack(true).Pop(), true);
             Debug.Log(PossibleMoveStack(false).Pop());*/
        }
        public void CreateVisEnv()
        {
            _darkStack = new Stack<GameObject>();
            _lightStack = new Stack<GameObject>();
            sizeOfMap = sizeOfMap % 2 == 0 ? sizeOfMap : sizeOfMap - 1;
            if (notOnlyData)
            {
                _visDataMap = new GameObject[sizeOfMap, sizeOfMap];
                _assDataMap = new GameObject[sizeOfMap, sizeOfMap];
                string _tempString;

                for (int i = 0; i < sizeOfMap; i++)
                {
                    for (int j = 0; j < sizeOfMap; j++)
                    {
                        _visDataMap[i, j] = Instantiate(_platePreFab, this.transform);

                        _tempString = i.ToString() + "i" + j.ToString();
                        _visDataMap[i, j].name = _tempString;
                        _visDataMap[i, j].AddComponent<Clicked>().happend = happend;
                        _visDataMap[i, j].transform.localPosition = new Vector3(_visDataMap[i, j].transform.localPosition.x + (i - sizeOfMap / 2),
                            0,
                             _visDataMap[i, j].transform.localPosition.z + (j - sizeOfMap / 2));
                        _assDataMap[i, j] = Instantiate(_assPreFab, this.transform);
                        _assDataMap[i, j].GetComponent<MeshRenderer>().material = _unPosMove;
                        _assDataMap[i, j].transform.localPosition = new Vector3(_visDataMap[i, j].transform.localPosition.x,
                            0.5f,
                             _visDataMap[i, j].transform.localPosition.z);
                    }
                }
            }
            /*        RestartEnv();*/
        }
        public void RestartEnv()
        {
            _dataMap = new int[sizeOfMap, sizeOfMap];
            for (int i = 0; i < sizeOfMap; i++)
            {
                for (int j = 0; j < sizeOfMap; j++)
                {
                    _dataMap[i, j] = 0;
                }
            }
            if (notOnlyData)
            {
                for (int i = 0; i < _darkStack.Count; i++)
                {
                    Destroy(_darkStack.Pop());
                }
                for (int i = 0; i < _lightStack.Count; i++)
                {
                    Destroy(_lightStack.Pop());
                }
            }
            InsertFigure(false, 3, 3);
            InsertFigure(false, 4, 4);
            InsertFigure(true, 4, 3);
            InsertFigure(true, 3, 4);
        }
        public bool InsertFigure(bool isBlack, int x, int y)
        {

            if (isBlack && _dataMap[x, y] != 1)
            {
                _dataMap[x, y] = 1;
                if (notOnlyData)
                {
                    for (int i = 0; i < _visDataMap[x, y].transform.childCount; i++)
                    {
                        Destroy(_visDataMap[x, y].transform.GetChild(0).gameObject);
                    }
                    GameObject _temp = Instantiate(_darkPreFab, _visDataMap[x, y].transform);
                    _darkStack.Push(_temp);
                }
            }
            else if (_dataMap[x, y] != -1)
            {
                _dataMap[x, y] = -1;
                if (notOnlyData)
                {
                    for (int i = 0; i < _visDataMap[x, y].transform.childCount; i++)
                    {
                        Destroy(_visDataMap[x, y].transform.GetChild(0).gameObject);
                    }

                    GameObject _temp = Instantiate(_lightPreFab, _visDataMap[x, y].transform);
                    _lightStack.Push(_temp);

                }
            }
            else
            {
                return false;
            }
            return true;
        }
        private Vector2Int _tempCord;
        private int startvalue, searchvalue, l;
        public void MadeMove(Vector2Int startPos, bool isBlack)
        {
            InsertFigure(isBlack, startPos.x, startPos.y);

            startvalue = isBlack ? 1 : -1;
            searchvalue = !isBlack ? 1 : -1;

            for (int k = 0; k < _directories.Length; k++)
            {
                _tempCord = RekuSearch(startPos + _directories[k], k, searchvalue, startvalue, true);
                if (_tempCord != _errorVecCord)
                {
                    l = k % 2 == 0 ? k + 1 : k - 1;
                    /*Debug.Log(_directories[k] + " " + _directories[j]+" "+_tempCord);*/
                    _tempCord = RekuSearch(_tempCord + _directories[l], l, searchvalue, startvalue, false, true);

                }
            }

        }
        public Stack<Vector2Int> PossibleMoveStack(bool isBlack)
        {
            if (notOnlyData)
            {
                for (int i = 0; i < sizeOfMap; i++)
                {
                    for (int j = 0; j < sizeOfMap; j++)
                    {
                        _assDataMap[i, j].GetComponent<MeshRenderer>().material = _unPosMove;
                    }
                }
            }
            Stack<Vector2Int> moveStak = new Stack<Vector2Int>();
            startvalue = isBlack ? 1 : -1;
            searchvalue = !isBlack ? 1 : -1;

            for (int i = 0; i < sizeOfMap; i++)
            {
                for (int j = 0; j < sizeOfMap; j++)
                {
                    if (_dataMap[i, j] == startvalue)
                    {
                        for (int k = 0; k < _directories.Length; k++)
                        {
                            _tempCord = RekuSearch(new Vector2Int(i, j) + _directories[k], k, searchvalue, 0, true);
                            if (_tempCord != _errorVecCord && !moveStak.Contains(_tempCord))
                            {
                                moveStak.Push(_tempCord);
                                if (notOnlyData)
                                {
                                    _assDataMap[_tempCord.x, _tempCord.y].GetComponent<MeshRenderer>().material = _posMove;
                                }
                            }
                        }
                    }
                }
            }


            return moveStak;
        }

        private Vector2Int _errorVecCord = new Vector2Int(-1, -1);
        private Vector2Int[] _directories = new Vector2Int[8] { new Vector2Int(0, 1), new Vector2Int(0, -1), new Vector2Int(1, 0),
        new Vector2Int(-1, 0), new Vector2Int(1, 1), new Vector2Int(-1, -1),new Vector2Int (1, -1),new Vector2Int (-1, 1) };
        private Vector2Int RekuSearch(Vector2Int _startPos, int _dir, int dontMind, int seachfor, bool dontmindfirst = false, bool changecolor = false)
        {
            if (_startPos.x >= 0 && _startPos.x < sizeOfMap && _startPos.y >= 0 && _startPos.y < sizeOfMap)
            {
                if (_dataMap[_startPos.x, _startPos.y] == seachfor && !dontmindfirst)
                {
                    return _startPos;
                }
                else if (_dataMap[_startPos.x, _startPos.y] == dontMind)
                {

                    if (!changecolor)
                    {
                        _startPos += _directories[_dir];
                        return RekuSearch(_startPos, _dir, dontMind, seachfor);
                    }
                    else
                    {
                        dontmindfirst = dontMind == -1 ? true : false;
                        InsertFigure(dontmindfirst, _startPos.x, _startPos.y);
                        _startPos += _directories[_dir];
                        return RekuSearch(_startPos, _dir, dontMind, seachfor, false, true);
                    }

                }
                else
                {
                    return _errorVecCord;
                }
            }
            else
            {
                return _errorVecCord;
            }
        }
    }
}