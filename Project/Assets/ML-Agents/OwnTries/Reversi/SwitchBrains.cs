﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.Barracuda;
using Unity.MLAgents.Policies;

public class SwitchBrains : MonoBehaviour
{
    public int firstBrainnumber = 0, secondBrainnumber = 0;
    public int numOfTestEpisodes = 25;
    public NNModel[] _brains;
}
