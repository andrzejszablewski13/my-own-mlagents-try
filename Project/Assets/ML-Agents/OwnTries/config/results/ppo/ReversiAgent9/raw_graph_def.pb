
C
global_step/initial_valueConst*
dtype0*
value	B : 
W
global_step
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
global_step/AssignAssignglobal_stepglobal_step/initial_value*
T0*
_class
loc:@global_step*
use_locking(*
validate_shape(
R
global_step/readIdentityglobal_step*
T0*
_class
loc:@global_step
;
steps_to_incrementPlaceholder*
dtype0*
shape: 
9
AddAddglobal_step/readsteps_to_increment*
T0
t
AssignAssignglobal_stepAdd*
T0*
_class
loc:@global_step*
use_locking(*
validate_shape(
L
vector_observationPlaceholder*
dtype0*
shape:���������@
5

batch_sizePlaceholder*
dtype0*
shape:
:
sequence_lengthPlaceholder*
dtype0*
shape:
;
masksPlaceholder*
dtype0*
shape:���������
A
epsilonPlaceholder*
dtype0*
shape:���������@
;
CastCastmasks*

DstT0*

SrcT0*
Truncate( 
M
#is_continuous_control/initial_valueConst*
dtype0*
value	B : 
a
is_continuous_control
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
is_continuous_control/AssignAssignis_continuous_control#is_continuous_control/initial_value*
T0*(
_class
loc:@is_continuous_control*
use_locking(*
validate_shape(
p
is_continuous_control/readIdentityis_continuous_control*
T0*(
_class
loc:@is_continuous_control
M
#trainer_major_version/initial_valueConst*
dtype0*
value	B : 
a
trainer_major_version
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
trainer_major_version/AssignAssigntrainer_major_version#trainer_major_version/initial_value*
T0*(
_class
loc:@trainer_major_version*
use_locking(*
validate_shape(
p
trainer_major_version/readIdentitytrainer_major_version*
T0*(
_class
loc:@trainer_major_version
M
#trainer_minor_version/initial_valueConst*
dtype0*
value	B :
a
trainer_minor_version
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
trainer_minor_version/AssignAssigntrainer_minor_version#trainer_minor_version/initial_value*
T0*(
_class
loc:@trainer_minor_version*
use_locking(*
validate_shape(
p
trainer_minor_version/readIdentitytrainer_minor_version*
T0*(
_class
loc:@trainer_minor_version
M
#trainer_patch_version/initial_valueConst*
dtype0*
value	B : 
a
trainer_patch_version
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
trainer_patch_version/AssignAssigntrainer_patch_version#trainer_patch_version/initial_value*
T0*(
_class
loc:@trainer_patch_version*
use_locking(*
validate_shape(
p
trainer_patch_version/readIdentitytrainer_patch_version*
T0*(
_class
loc:@trainer_patch_version
F
version_number/initial_valueConst*
dtype0*
value	B :
Z
version_number
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
version_number/AssignAssignversion_numberversion_number/initial_value*
T0*!
_class
loc:@version_number*
use_locking(*
validate_shape(
[
version_number/readIdentityversion_number*
T0*!
_class
loc:@version_number
C
memory_size/initial_valueConst*
dtype0*
value	B : 
W
memory_size
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
memory_size/AssignAssignmemory_sizememory_size/initial_value*
T0*
_class
loc:@memory_size*
use_locking(*
validate_shape(
R
memory_size/readIdentitymemory_size*
T0*
_class
loc:@memory_size
K
!action_output_shape/initial_valueConst*
dtype0*
value	B :@
_
action_output_shape
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
action_output_shape/AssignAssignaction_output_shape!action_output_shape/initial_value*
T0*&
_class
loc:@action_output_shape*
use_locking(*
validate_shape(
j
action_output_shape/readIdentityaction_output_shape*
T0*&
_class
loc:@action_output_shape
�
Fpolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/shapeConst*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Epolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/meanConst*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
dtype0*
valueB
 *    
�
Gpolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/stddevConst*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
dtype0*
valueB
 *6�>
�
Ppolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalFpolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/shape*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
dtype0*
seed�*
seed2 
�
Dpolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/mulMulPpolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalGpolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel
�
@policy/main_graph_0/hidden_0/kernel/Initializer/truncated_normalAddDpolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/mulEpolicy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel
�
#policy/main_graph_0/hidden_0/kernel
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
*policy/main_graph_0/hidden_0/kernel/AssignAssign#policy/main_graph_0/hidden_0/kernel@policy/main_graph_0/hidden_0/kernel/Initializer/truncated_normal*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
�
(policy/main_graph_0/hidden_0/kernel/readIdentity#policy/main_graph_0/hidden_0/kernel*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel
�
3policy/main_graph_0/hidden_0/bias/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
dtype0*
valueB@*    
�
!policy/main_graph_0/hidden_0/bias
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
(policy/main_graph_0/hidden_0/bias/AssignAssign!policy/main_graph_0/hidden_0/bias3policy/main_graph_0/hidden_0/bias/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
�
&policy/main_graph_0/hidden_0/bias/readIdentity!policy/main_graph_0/hidden_0/bias*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias
�
#policy/main_graph_0/hidden_0/MatMulMatMulvector_observation(policy/main_graph_0/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
$policy/main_graph_0/hidden_0/BiasAddBiasAdd#policy/main_graph_0/hidden_0/MatMul&policy/main_graph_0/hidden_0/bias/read*
T0*
data_formatNHWC
^
$policy/main_graph_0/hidden_0/SigmoidSigmoid$policy/main_graph_0/hidden_0/BiasAdd*
T0
|
 policy/main_graph_0/hidden_0/MulMul$policy/main_graph_0/hidden_0/BiasAdd$policy/main_graph_0/hidden_0/Sigmoid*
T0
�
Fpolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/shapeConst*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Epolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/meanConst*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
dtype0*
valueB
 *    
�
Gpolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/stddevConst*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
dtype0*
valueB
 *6�>
�
Ppolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalFpolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/shape*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
dtype0*
seed�*
seed2
�
Dpolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/mulMulPpolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalGpolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel
�
@policy/main_graph_0/hidden_1/kernel/Initializer/truncated_normalAddDpolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/mulEpolicy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel
�
#policy/main_graph_0/hidden_1/kernel
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
*policy/main_graph_0/hidden_1/kernel/AssignAssign#policy/main_graph_0/hidden_1/kernel@policy/main_graph_0/hidden_1/kernel/Initializer/truncated_normal*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
�
(policy/main_graph_0/hidden_1/kernel/readIdentity#policy/main_graph_0/hidden_1/kernel*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel
�
3policy/main_graph_0/hidden_1/bias/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
dtype0*
valueB@*    
�
!policy/main_graph_0/hidden_1/bias
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
(policy/main_graph_0/hidden_1/bias/AssignAssign!policy/main_graph_0/hidden_1/bias3policy/main_graph_0/hidden_1/bias/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
�
&policy/main_graph_0/hidden_1/bias/readIdentity!policy/main_graph_0/hidden_1/bias*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias
�
#policy/main_graph_0/hidden_1/MatMulMatMul policy/main_graph_0/hidden_0/Mul(policy/main_graph_0/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
$policy/main_graph_0/hidden_1/BiasAddBiasAdd#policy/main_graph_0/hidden_1/MatMul&policy/main_graph_0/hidden_1/bias/read*
T0*
data_formatNHWC
^
$policy/main_graph_0/hidden_1/SigmoidSigmoid$policy/main_graph_0/hidden_1/BiasAdd*
T0
|
 policy/main_graph_0/hidden_1/MulMul$policy/main_graph_0/hidden_1/BiasAdd$policy/main_graph_0/hidden_1/Sigmoid*
T0
�
Fpolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/shapeConst*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Epolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/meanConst*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
dtype0*
valueB
 *    
�
Gpolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/stddevConst*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
dtype0*
valueB
 *6�>
�
Ppolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalFpolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/shape*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
dtype0*
seed�*
seed2
�
Dpolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/mulMulPpolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalGpolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/stddev*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel
�
@policy/main_graph_0/hidden_2/kernel/Initializer/truncated_normalAddDpolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/mulEpolicy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal/mean*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel
�
#policy/main_graph_0/hidden_2/kernel
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
*policy/main_graph_0/hidden_2/kernel/AssignAssign#policy/main_graph_0/hidden_2/kernel@policy/main_graph_0/hidden_2/kernel/Initializer/truncated_normal*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
�
(policy/main_graph_0/hidden_2/kernel/readIdentity#policy/main_graph_0/hidden_2/kernel*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel
�
3policy/main_graph_0/hidden_2/bias/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
dtype0*
valueB@*    
�
!policy/main_graph_0/hidden_2/bias
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
(policy/main_graph_0/hidden_2/bias/AssignAssign!policy/main_graph_0/hidden_2/bias3policy/main_graph_0/hidden_2/bias/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
�
&policy/main_graph_0/hidden_2/bias/readIdentity!policy/main_graph_0/hidden_2/bias*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias
�
#policy/main_graph_0/hidden_2/MatMulMatMul policy/main_graph_0/hidden_1/Mul(policy/main_graph_0/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
$policy/main_graph_0/hidden_2/BiasAddBiasAdd#policy/main_graph_0/hidden_2/MatMul&policy/main_graph_0/hidden_2/bias/read*
T0*
data_formatNHWC
^
$policy/main_graph_0/hidden_2/SigmoidSigmoid$policy/main_graph_0/hidden_2/BiasAdd*
T0
|
 policy/main_graph_0/hidden_2/MulMul$policy/main_graph_0/hidden_2/BiasAdd$policy/main_graph_0/hidden_2/Sigmoid*
T0
�
Fpolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/shapeConst*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Epolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/meanConst*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
dtype0*
valueB
 *    
�
Gpolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/stddevConst*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
dtype0*
valueB
 *6�>
�
Ppolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalFpolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/shape*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
dtype0*
seed�*
seed2
�
Dpolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/mulMulPpolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalGpolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/stddev*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel
�
@policy/main_graph_0/hidden_3/kernel/Initializer/truncated_normalAddDpolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/mulEpolicy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal/mean*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel
�
#policy/main_graph_0/hidden_3/kernel
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
*policy/main_graph_0/hidden_3/kernel/AssignAssign#policy/main_graph_0/hidden_3/kernel@policy/main_graph_0/hidden_3/kernel/Initializer/truncated_normal*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
�
(policy/main_graph_0/hidden_3/kernel/readIdentity#policy/main_graph_0/hidden_3/kernel*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel
�
3policy/main_graph_0/hidden_3/bias/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
dtype0*
valueB@*    
�
!policy/main_graph_0/hidden_3/bias
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
(policy/main_graph_0/hidden_3/bias/AssignAssign!policy/main_graph_0/hidden_3/bias3policy/main_graph_0/hidden_3/bias/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
�
&policy/main_graph_0/hidden_3/bias/readIdentity!policy/main_graph_0/hidden_3/bias*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias
�
#policy/main_graph_0/hidden_3/MatMulMatMul policy/main_graph_0/hidden_2/Mul(policy/main_graph_0/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
$policy/main_graph_0/hidden_3/BiasAddBiasAdd#policy/main_graph_0/hidden_3/MatMul&policy/main_graph_0/hidden_3/bias/read*
T0*
data_formatNHWC
^
$policy/main_graph_0/hidden_3/SigmoidSigmoid$policy/main_graph_0/hidden_3/BiasAdd*
T0
|
 policy/main_graph_0/hidden_3/MulMul$policy/main_graph_0/hidden_3/BiasAdd$policy/main_graph_0/hidden_3/Sigmoid*
T0
F
action_masksPlaceholder*
dtype0*
shape:���������@
�
6policy/dense/kernel/Initializer/truncated_normal/shapeConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB"@   @   
�
5policy/dense/kernel/Initializer/truncated_normal/meanConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB
 *    
�
7policy/dense/kernel/Initializer/truncated_normal/stddevConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB
 *��h<
�
@policy/dense/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal6policy/dense/kernel/Initializer/truncated_normal/shape*
T0*&
_class
loc:@policy/dense/kernel*
dtype0*
seed�*
seed2
�
4policy/dense/kernel/Initializer/truncated_normal/mulMul@policy/dense/kernel/Initializer/truncated_normal/TruncatedNormal7policy/dense/kernel/Initializer/truncated_normal/stddev*
T0*&
_class
loc:@policy/dense/kernel
�
0policy/dense/kernel/Initializer/truncated_normalAdd4policy/dense/kernel/Initializer/truncated_normal/mul5policy/dense/kernel/Initializer/truncated_normal/mean*
T0*&
_class
loc:@policy/dense/kernel
�
policy/dense/kernel
VariableV2*&
_class
loc:@policy/dense/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
policy/dense/kernel/AssignAssignpolicy/dense/kernel0policy/dense/kernel/Initializer/truncated_normal*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
j
policy/dense/kernel/readIdentitypolicy/dense/kernel*
T0*&
_class
loc:@policy/dense/kernel
�
policy_1/dense/MatMulMatMul policy/main_graph_0/hidden_3/Mulpolicy/dense/kernel/read*
T0*
transpose_a( *
transpose_b( 
Q
policy_1/strided_slice/stackConst*
dtype0*
valueB"        
S
policy_1/strided_slice/stack_1Const*
dtype0*
valueB"    @   
S
policy_1/strided_slice/stack_2Const*
dtype0*
valueB"      
�
policy_1/strided_sliceStridedSliceaction_maskspolicy_1/strided_slice/stackpolicy_1/strided_slice/stack_1policy_1/strided_slice/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
;
policy_1/SoftmaxSoftmaxpolicy_1/dense/MatMul*
T0
;
policy_1/add/yConst*
dtype0*
valueB
 *���3
@
policy_1/addAddV2policy_1/Softmaxpolicy_1/add/y*
T0
B
policy_1/MulMulpolicy_1/addpolicy_1/strided_slice*
T0
H
policy_1/Sum/reduction_indicesConst*
dtype0*
value	B :
g
policy_1/SumSumpolicy_1/Mulpolicy_1/Sum/reduction_indices*
T0*

Tidx0*
	keep_dims(
@
policy_1/truedivRealDivpolicy_1/Mulpolicy_1/Sum*
T0
=
policy_1/add_1/yConst*
dtype0*
valueB
 *���3
D
policy_1/add_1AddV2policy_1/truedivpolicy_1/add_1/y*
T0
,
policy_1/LogLogpolicy_1/add_1*
T0
V
,policy_1/multinomial/Multinomial/num_samplesConst*
dtype0*
value	B :
�
 policy_1/multinomial/MultinomialMultinomialpolicy_1/Log,policy_1/multinomial/Multinomial/num_samples*
T0*
output_dtype0	*
seed�*
seed2
D
policy_1/concat/concat_dimConst*
dtype0*
value	B :
M
policy_1/concat/concatIdentity policy_1/multinomial/Multinomial*
T0	
F
policy_1/concat_1/concat_dimConst*
dtype0*
value	B :
?
policy_1/concat_1/concatIdentitypolicy_1/truediv*
T0
=
policy_1/add_2/yConst*
dtype0*
valueB
 *���3
D
policy_1/add_2AddV2policy_1/truedivpolicy_1/add_2/y*
T0
.
policy_1/Log_1Logpolicy_1/add_2*
T0
F
policy_1/concat_2/concat_dimConst*
dtype0*
value	B :
=
policy_1/concat_2/concatIdentitypolicy_1/Log_1*
T0
S
policy_1/strided_slice_1/stackConst*
dtype0*
valueB"        
U
 policy_1/strided_slice_1/stack_1Const*
dtype0*
valueB"       
U
 policy_1/strided_slice_1/stack_2Const*
dtype0*
valueB"      
�
policy_1/strided_slice_1StridedSlicepolicy_1/concat/concatpolicy_1/strided_slice_1/stack policy_1/strided_slice_1/stack_1 policy_1/strided_slice_1/stack_2*
Index0*
T0	*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask
F
policy_1/one_hot/on_valueConst*
dtype0*
valueB
 *  �?
G
policy_1/one_hot/off_valueConst*
dtype0*
valueB
 *    
@
policy_1/one_hot/depthConst*
dtype0*
value	B :@
�
policy_1/one_hotOneHotpolicy_1/strided_slice_1policy_1/one_hot/depthpolicy_1/one_hot/on_valuepolicy_1/one_hot/off_value*
T0*
TI0	*
axis���������
F
policy_1/concat_3/concat_dimConst*
dtype0*
value	B :
?
policy_1/concat_3/concatIdentitypolicy_1/one_hot*
T0
S
policy_1/strided_slice_2/stackConst*
dtype0*
valueB"        
U
 policy_1/strided_slice_2/stack_1Const*
dtype0*
valueB"    @   
U
 policy_1/strided_slice_2/stack_2Const*
dtype0*
valueB"      
�
policy_1/strided_slice_2StridedSlicepolicy_1/concat_2/concatpolicy_1/strided_slice_2/stack policy_1/strided_slice_2/stack_1 policy_1/strided_slice_2/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
@
policy_1/Softmax_1Softmaxpolicy_1/strided_slice_2*
T0
S
policy_1/strided_slice_3/stackConst*
dtype0*
valueB"        
U
 policy_1/strided_slice_3/stack_1Const*
dtype0*
valueB"    @   
U
 policy_1/strided_slice_3/stack_2Const*
dtype0*
valueB"      
�
policy_1/strided_slice_3StridedSlicepolicy_1/concat_2/concatpolicy_1/strided_slice_3/stack policy_1/strided_slice_3/stack_1 policy_1/strided_slice_3/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
Y
/policy_1/softmax_cross_entropy_with_logits/RankConst*
dtype0*
value	B :
l
0policy_1/softmax_cross_entropy_with_logits/ShapeShapepolicy_1/strided_slice_3*
T0*
out_type0
[
1policy_1/softmax_cross_entropy_with_logits/Rank_1Const*
dtype0*
value	B :
n
2policy_1/softmax_cross_entropy_with_logits/Shape_1Shapepolicy_1/strided_slice_3*
T0*
out_type0
Z
0policy_1/softmax_cross_entropy_with_logits/Sub/yConst*
dtype0*
value	B :
�
.policy_1/softmax_cross_entropy_with_logits/SubSub1policy_1/softmax_cross_entropy_with_logits/Rank_10policy_1/softmax_cross_entropy_with_logits/Sub/y*
T0
�
6policy_1/softmax_cross_entropy_with_logits/Slice/beginPack.policy_1/softmax_cross_entropy_with_logits/Sub*
N*
T0*

axis 
c
5policy_1/softmax_cross_entropy_with_logits/Slice/sizeConst*
dtype0*
valueB:
�
0policy_1/softmax_cross_entropy_with_logits/SliceSlice2policy_1/softmax_cross_entropy_with_logits/Shape_16policy_1/softmax_cross_entropy_with_logits/Slice/begin5policy_1/softmax_cross_entropy_with_logits/Slice/size*
Index0*
T0
q
:policy_1/softmax_cross_entropy_with_logits/concat/values_0Const*
dtype0*
valueB:
���������
`
6policy_1/softmax_cross_entropy_with_logits/concat/axisConst*
dtype0*
value	B : 
�
1policy_1/softmax_cross_entropy_with_logits/concatConcatV2:policy_1/softmax_cross_entropy_with_logits/concat/values_00policy_1/softmax_cross_entropy_with_logits/Slice6policy_1/softmax_cross_entropy_with_logits/concat/axis*
N*
T0*

Tidx0
�
2policy_1/softmax_cross_entropy_with_logits/ReshapeReshapepolicy_1/strided_slice_31policy_1/softmax_cross_entropy_with_logits/concat*
T0*
Tshape0
[
1policy_1/softmax_cross_entropy_with_logits/Rank_2Const*
dtype0*
value	B :
h
2policy_1/softmax_cross_entropy_with_logits/Shape_2Shapepolicy_1/Softmax_1*
T0*
out_type0
\
2policy_1/softmax_cross_entropy_with_logits/Sub_1/yConst*
dtype0*
value	B :
�
0policy_1/softmax_cross_entropy_with_logits/Sub_1Sub1policy_1/softmax_cross_entropy_with_logits/Rank_22policy_1/softmax_cross_entropy_with_logits/Sub_1/y*
T0
�
8policy_1/softmax_cross_entropy_with_logits/Slice_1/beginPack0policy_1/softmax_cross_entropy_with_logits/Sub_1*
N*
T0*

axis 
e
7policy_1/softmax_cross_entropy_with_logits/Slice_1/sizeConst*
dtype0*
valueB:
�
2policy_1/softmax_cross_entropy_with_logits/Slice_1Slice2policy_1/softmax_cross_entropy_with_logits/Shape_28policy_1/softmax_cross_entropy_with_logits/Slice_1/begin7policy_1/softmax_cross_entropy_with_logits/Slice_1/size*
Index0*
T0
s
<policy_1/softmax_cross_entropy_with_logits/concat_1/values_0Const*
dtype0*
valueB:
���������
b
8policy_1/softmax_cross_entropy_with_logits/concat_1/axisConst*
dtype0*
value	B : 
�
3policy_1/softmax_cross_entropy_with_logits/concat_1ConcatV2<policy_1/softmax_cross_entropy_with_logits/concat_1/values_02policy_1/softmax_cross_entropy_with_logits/Slice_18policy_1/softmax_cross_entropy_with_logits/concat_1/axis*
N*
T0*

Tidx0
�
4policy_1/softmax_cross_entropy_with_logits/Reshape_1Reshapepolicy_1/Softmax_13policy_1/softmax_cross_entropy_with_logits/concat_1*
T0*
Tshape0
�
*policy_1/softmax_cross_entropy_with_logitsSoftmaxCrossEntropyWithLogits2policy_1/softmax_cross_entropy_with_logits/Reshape4policy_1/softmax_cross_entropy_with_logits/Reshape_1*
T0
\
2policy_1/softmax_cross_entropy_with_logits/Sub_2/yConst*
dtype0*
value	B :
�
0policy_1/softmax_cross_entropy_with_logits/Sub_2Sub/policy_1/softmax_cross_entropy_with_logits/Rank2policy_1/softmax_cross_entropy_with_logits/Sub_2/y*
T0
f
8policy_1/softmax_cross_entropy_with_logits/Slice_2/beginConst*
dtype0*
valueB: 
�
7policy_1/softmax_cross_entropy_with_logits/Slice_2/sizePack0policy_1/softmax_cross_entropy_with_logits/Sub_2*
N*
T0*

axis 
�
2policy_1/softmax_cross_entropy_with_logits/Slice_2Slice0policy_1/softmax_cross_entropy_with_logits/Shape8policy_1/softmax_cross_entropy_with_logits/Slice_2/begin7policy_1/softmax_cross_entropy_with_logits/Slice_2/size*
Index0*
T0
�
4policy_1/softmax_cross_entropy_with_logits/Reshape_2Reshape*policy_1/softmax_cross_entropy_with_logits2policy_1/softmax_cross_entropy_with_logits/Slice_2*
T0*
Tshape0
j
policy_1/stackPack4policy_1/softmax_cross_entropy_with_logits/Reshape_2*
N*
T0*

axis
J
 policy_1/Sum_1/reduction_indicesConst*
dtype0*
value	B :
m
policy_1/Sum_1Sumpolicy_1/stack policy_1/Sum_1/reduction_indices*
T0*

Tidx0*
	keep_dims( 
S
policy_1/strided_slice_4/stackConst*
dtype0*
valueB"        
U
 policy_1/strided_slice_4/stack_1Const*
dtype0*
valueB"    @   
U
 policy_1/strided_slice_4/stack_2Const*
dtype0*
valueB"      
�
policy_1/strided_slice_4StridedSlicepolicy_1/concat_3/concatpolicy_1/strided_slice_4/stack policy_1/strided_slice_4/stack_1 policy_1/strided_slice_4/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
S
policy_1/strided_slice_5/stackConst*
dtype0*
valueB"        
U
 policy_1/strided_slice_5/stack_1Const*
dtype0*
valueB"    @   
U
 policy_1/strided_slice_5/stack_2Const*
dtype0*
valueB"      
�
policy_1/strided_slice_5StridedSlicepolicy_1/concat_2/concatpolicy_1/strided_slice_5/stack policy_1/strided_slice_5/stack_1 policy_1/strided_slice_5/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
[
1policy_1/softmax_cross_entropy_with_logits_1/RankConst*
dtype0*
value	B :
n
2policy_1/softmax_cross_entropy_with_logits_1/ShapeShapepolicy_1/strided_slice_5*
T0*
out_type0
]
3policy_1/softmax_cross_entropy_with_logits_1/Rank_1Const*
dtype0*
value	B :
p
4policy_1/softmax_cross_entropy_with_logits_1/Shape_1Shapepolicy_1/strided_slice_5*
T0*
out_type0
\
2policy_1/softmax_cross_entropy_with_logits_1/Sub/yConst*
dtype0*
value	B :
�
0policy_1/softmax_cross_entropy_with_logits_1/SubSub3policy_1/softmax_cross_entropy_with_logits_1/Rank_12policy_1/softmax_cross_entropy_with_logits_1/Sub/y*
T0
�
8policy_1/softmax_cross_entropy_with_logits_1/Slice/beginPack0policy_1/softmax_cross_entropy_with_logits_1/Sub*
N*
T0*

axis 
e
7policy_1/softmax_cross_entropy_with_logits_1/Slice/sizeConst*
dtype0*
valueB:
�
2policy_1/softmax_cross_entropy_with_logits_1/SliceSlice4policy_1/softmax_cross_entropy_with_logits_1/Shape_18policy_1/softmax_cross_entropy_with_logits_1/Slice/begin7policy_1/softmax_cross_entropy_with_logits_1/Slice/size*
Index0*
T0
s
<policy_1/softmax_cross_entropy_with_logits_1/concat/values_0Const*
dtype0*
valueB:
���������
b
8policy_1/softmax_cross_entropy_with_logits_1/concat/axisConst*
dtype0*
value	B : 
�
3policy_1/softmax_cross_entropy_with_logits_1/concatConcatV2<policy_1/softmax_cross_entropy_with_logits_1/concat/values_02policy_1/softmax_cross_entropy_with_logits_1/Slice8policy_1/softmax_cross_entropy_with_logits_1/concat/axis*
N*
T0*

Tidx0
�
4policy_1/softmax_cross_entropy_with_logits_1/ReshapeReshapepolicy_1/strided_slice_53policy_1/softmax_cross_entropy_with_logits_1/concat*
T0*
Tshape0
]
3policy_1/softmax_cross_entropy_with_logits_1/Rank_2Const*
dtype0*
value	B :
p
4policy_1/softmax_cross_entropy_with_logits_1/Shape_2Shapepolicy_1/strided_slice_4*
T0*
out_type0
^
4policy_1/softmax_cross_entropy_with_logits_1/Sub_1/yConst*
dtype0*
value	B :
�
2policy_1/softmax_cross_entropy_with_logits_1/Sub_1Sub3policy_1/softmax_cross_entropy_with_logits_1/Rank_24policy_1/softmax_cross_entropy_with_logits_1/Sub_1/y*
T0
�
:policy_1/softmax_cross_entropy_with_logits_1/Slice_1/beginPack2policy_1/softmax_cross_entropy_with_logits_1/Sub_1*
N*
T0*

axis 
g
9policy_1/softmax_cross_entropy_with_logits_1/Slice_1/sizeConst*
dtype0*
valueB:
�
4policy_1/softmax_cross_entropy_with_logits_1/Slice_1Slice4policy_1/softmax_cross_entropy_with_logits_1/Shape_2:policy_1/softmax_cross_entropy_with_logits_1/Slice_1/begin9policy_1/softmax_cross_entropy_with_logits_1/Slice_1/size*
Index0*
T0
u
>policy_1/softmax_cross_entropy_with_logits_1/concat_1/values_0Const*
dtype0*
valueB:
���������
d
:policy_1/softmax_cross_entropy_with_logits_1/concat_1/axisConst*
dtype0*
value	B : 
�
5policy_1/softmax_cross_entropy_with_logits_1/concat_1ConcatV2>policy_1/softmax_cross_entropy_with_logits_1/concat_1/values_04policy_1/softmax_cross_entropy_with_logits_1/Slice_1:policy_1/softmax_cross_entropy_with_logits_1/concat_1/axis*
N*
T0*

Tidx0
�
6policy_1/softmax_cross_entropy_with_logits_1/Reshape_1Reshapepolicy_1/strided_slice_45policy_1/softmax_cross_entropy_with_logits_1/concat_1*
T0*
Tshape0
�
,policy_1/softmax_cross_entropy_with_logits_1SoftmaxCrossEntropyWithLogits4policy_1/softmax_cross_entropy_with_logits_1/Reshape6policy_1/softmax_cross_entropy_with_logits_1/Reshape_1*
T0
^
4policy_1/softmax_cross_entropy_with_logits_1/Sub_2/yConst*
dtype0*
value	B :
�
2policy_1/softmax_cross_entropy_with_logits_1/Sub_2Sub1policy_1/softmax_cross_entropy_with_logits_1/Rank4policy_1/softmax_cross_entropy_with_logits_1/Sub_2/y*
T0
h
:policy_1/softmax_cross_entropy_with_logits_1/Slice_2/beginConst*
dtype0*
valueB: 
�
9policy_1/softmax_cross_entropy_with_logits_1/Slice_2/sizePack2policy_1/softmax_cross_entropy_with_logits_1/Sub_2*
N*
T0*

axis 
�
4policy_1/softmax_cross_entropy_with_logits_1/Slice_2Slice2policy_1/softmax_cross_entropy_with_logits_1/Shape:policy_1/softmax_cross_entropy_with_logits_1/Slice_2/begin9policy_1/softmax_cross_entropy_with_logits_1/Slice_2/size*
Index0*
T0
�
6policy_1/softmax_cross_entropy_with_logits_1/Reshape_2Reshape,policy_1/softmax_cross_entropy_with_logits_14policy_1/softmax_cross_entropy_with_logits_1/Slice_2*
T0*
Tshape0
T
policy_1/NegNeg6policy_1/softmax_cross_entropy_with_logits_1/Reshape_2*
T0
D
policy_1/stack_1Packpolicy_1/Neg*
N*
T0*

axis
J
 policy_1/Sum_2/reduction_indicesConst*
dtype0*
value	B :
o
policy_1/Sum_2Sumpolicy_1/stack_1 policy_1/Sum_2/reduction_indices*
T0*

Tidx0*
	keep_dims(
5
actionIdentitypolicy_1/concat_2/concat*
T0
?
StopGradientStopGradientpolicy_1/concat_3/concat*
T0
A
save/filename/inputConst*
dtype0*
valueB Bmodel
V
save/filenamePlaceholderWithDefaultsave/filename/input*
dtype0*
shape: 
M

save/ConstPlaceholderWithDefaultsave/filename*
dtype0*
shape: 
�
save/SaveV2/tensor_namesConst*
dtype0*�
value�B�Baction_output_shapeBglobal_stepBis_continuous_controlBmemory_sizeBpolicy/dense/kernelB!policy/main_graph_0/hidden_0/biasB#policy/main_graph_0/hidden_0/kernelB!policy/main_graph_0/hidden_1/biasB#policy/main_graph_0/hidden_1/kernelB!policy/main_graph_0/hidden_2/biasB#policy/main_graph_0/hidden_2/kernelB!policy/main_graph_0/hidden_3/biasB#policy/main_graph_0/hidden_3/kernelBtrainer_major_versionBtrainer_minor_versionBtrainer_patch_versionBversion_number
i
save/SaveV2/shape_and_slicesConst*
dtype0*5
value,B*B B B B B B B B B B B B B B B B B 
�
save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesaction_output_shapeglobal_stepis_continuous_controlmemory_sizepolicy/dense/kernel!policy/main_graph_0/hidden_0/bias#policy/main_graph_0/hidden_0/kernel!policy/main_graph_0/hidden_1/bias#policy/main_graph_0/hidden_1/kernel!policy/main_graph_0/hidden_2/bias#policy/main_graph_0/hidden_2/kernel!policy/main_graph_0/hidden_3/bias#policy/main_graph_0/hidden_3/kerneltrainer_major_versiontrainer_minor_versiontrainer_patch_versionversion_number*
dtypes
2
e
save/control_dependencyIdentity
save/Const^save/SaveV2*
T0*
_class
loc:@save/Const
�
save/RestoreV2/tensor_namesConst"/device:CPU:0*
dtype0*�
value�B�Baction_output_shapeBglobal_stepBis_continuous_controlBmemory_sizeBpolicy/dense/kernelB!policy/main_graph_0/hidden_0/biasB#policy/main_graph_0/hidden_0/kernelB!policy/main_graph_0/hidden_1/biasB#policy/main_graph_0/hidden_1/kernelB!policy/main_graph_0/hidden_2/biasB#policy/main_graph_0/hidden_2/kernelB!policy/main_graph_0/hidden_3/biasB#policy/main_graph_0/hidden_3/kernelBtrainer_major_versionBtrainer_minor_versionBtrainer_patch_versionBversion_number
{
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*
dtype0*5
value,B*B B B B B B B B B B B B B B B B B 
�
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*
dtypes
2
�
save/AssignAssignaction_output_shapesave/RestoreV2*
T0*&
_class
loc:@action_output_shape*
use_locking(*
validate_shape(
�
save/Assign_1Assignglobal_stepsave/RestoreV2:1*
T0*
_class
loc:@global_step*
use_locking(*
validate_shape(
�
save/Assign_2Assignis_continuous_controlsave/RestoreV2:2*
T0*(
_class
loc:@is_continuous_control*
use_locking(*
validate_shape(
�
save/Assign_3Assignmemory_sizesave/RestoreV2:3*
T0*
_class
loc:@memory_size*
use_locking(*
validate_shape(
�
save/Assign_4Assignpolicy/dense/kernelsave/RestoreV2:4*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
�
save/Assign_5Assign!policy/main_graph_0/hidden_0/biassave/RestoreV2:5*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
�
save/Assign_6Assign#policy/main_graph_0/hidden_0/kernelsave/RestoreV2:6*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
�
save/Assign_7Assign!policy/main_graph_0/hidden_1/biassave/RestoreV2:7*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
�
save/Assign_8Assign#policy/main_graph_0/hidden_1/kernelsave/RestoreV2:8*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
�
save/Assign_9Assign!policy/main_graph_0/hidden_2/biassave/RestoreV2:9*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
�
save/Assign_10Assign#policy/main_graph_0/hidden_2/kernelsave/RestoreV2:10*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
�
save/Assign_11Assign!policy/main_graph_0/hidden_3/biassave/RestoreV2:11*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
�
save/Assign_12Assign#policy/main_graph_0/hidden_3/kernelsave/RestoreV2:12*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
�
save/Assign_13Assigntrainer_major_versionsave/RestoreV2:13*
T0*(
_class
loc:@trainer_major_version*
use_locking(*
validate_shape(
�
save/Assign_14Assigntrainer_minor_versionsave/RestoreV2:14*
T0*(
_class
loc:@trainer_minor_version*
use_locking(*
validate_shape(
�
save/Assign_15Assigntrainer_patch_versionsave/RestoreV2:15*
T0*(
_class
loc:@trainer_patch_version*
use_locking(*
validate_shape(
�
save/Assign_16Assignversion_numbersave/RestoreV2:16*
T0*!
_class
loc:@version_number*
use_locking(*
validate_shape(
�
save/restore_allNoOp^save/Assign^save/Assign_1^save/Assign_10^save/Assign_11^save/Assign_12^save/Assign_13^save/Assign_14^save/Assign_15^save/Assign_16^save/Assign_2^save/Assign_3^save/Assign_4^save/Assign_5^save/Assign_6^save/Assign_7^save/Assign_8^save/Assign_9
�
initNoOp^action_output_shape/Assign^global_step/Assign^is_continuous_control/Assign^memory_size/Assign^policy/dense/kernel/Assign)^policy/main_graph_0/hidden_0/bias/Assign+^policy/main_graph_0/hidden_0/kernel/Assign)^policy/main_graph_0/hidden_1/bias/Assign+^policy/main_graph_0/hidden_1/kernel/Assign)^policy/main_graph_0/hidden_2/bias/Assign+^policy/main_graph_0/hidden_2/kernel/Assign)^policy/main_graph_0/hidden_3/bias/Assign+^policy/main_graph_0/hidden_3/kernel/Assign^trainer_major_version/Assign^trainer_minor_version/Assign^trainer_patch_version/Assign^version_number/Assign
[
!curiosity_next_vector_observationPlaceholder*
dtype0*
shape:���������@
�
Ocuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
dtype0*
valueB"@      
�
Ncuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Pcuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
dtype0*
valueB
 *6�>
�
Ycuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOcuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
dtype0*
seed�*
seed2
�
Mcuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/mulMulYcuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalPcuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel
�
Icuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normalAddMcuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/mulNcuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel
�
,curiosity_vector_obs_encoder/hidden_0/kernel
VariableV2*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
	container *
dtype0*
shape:	@�*
shared_name 
�
3curiosity_vector_obs_encoder/hidden_0/kernel/AssignAssign,curiosity_vector_obs_encoder/hidden_0/kernelIcuriosity_vector_obs_encoder/hidden_0/kernel/Initializer/truncated_normal*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
1curiosity_vector_obs_encoder/hidden_0/kernel/readIdentity,curiosity_vector_obs_encoder/hidden_0/kernel*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel
�
<curiosity_vector_obs_encoder/hidden_0/bias/Initializer/zerosConst*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
dtype0*
valueB�*    
�
*curiosity_vector_obs_encoder/hidden_0/bias
VariableV2*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
	container *
dtype0*
shape:�*
shared_name 
�
1curiosity_vector_obs_encoder/hidden_0/bias/AssignAssign*curiosity_vector_obs_encoder/hidden_0/bias<curiosity_vector_obs_encoder/hidden_0/bias/Initializer/zeros*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
/curiosity_vector_obs_encoder/hidden_0/bias/readIdentity*curiosity_vector_obs_encoder/hidden_0/bias*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias
�
,curiosity_vector_obs_encoder/hidden_0/MatMulMatMulvector_observation1curiosity_vector_obs_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
-curiosity_vector_obs_encoder/hidden_0/BiasAddBiasAdd,curiosity_vector_obs_encoder/hidden_0/MatMul/curiosity_vector_obs_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
p
-curiosity_vector_obs_encoder/hidden_0/SigmoidSigmoid-curiosity_vector_obs_encoder/hidden_0/BiasAdd*
T0
�
)curiosity_vector_obs_encoder/hidden_0/MulMul-curiosity_vector_obs_encoder/hidden_0/BiasAdd-curiosity_vector_obs_encoder/hidden_0/Sigmoid*
T0
�
Ocuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
dtype0*
valueB"      
�
Ncuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Pcuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
dtype0*
valueB
 *6��=
�
Ycuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOcuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
dtype0*
seed�*
seed2
�
Mcuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/mulMulYcuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalPcuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel
�
Icuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normalAddMcuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/mulNcuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel
�
,curiosity_vector_obs_encoder/hidden_1/kernel
VariableV2*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
3curiosity_vector_obs_encoder/hidden_1/kernel/AssignAssign,curiosity_vector_obs_encoder/hidden_1/kernelIcuriosity_vector_obs_encoder/hidden_1/kernel/Initializer/truncated_normal*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
1curiosity_vector_obs_encoder/hidden_1/kernel/readIdentity,curiosity_vector_obs_encoder/hidden_1/kernel*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel
�
<curiosity_vector_obs_encoder/hidden_1/bias/Initializer/zerosConst*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
dtype0*
valueB�*    
�
*curiosity_vector_obs_encoder/hidden_1/bias
VariableV2*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
	container *
dtype0*
shape:�*
shared_name 
�
1curiosity_vector_obs_encoder/hidden_1/bias/AssignAssign*curiosity_vector_obs_encoder/hidden_1/bias<curiosity_vector_obs_encoder/hidden_1/bias/Initializer/zeros*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
/curiosity_vector_obs_encoder/hidden_1/bias/readIdentity*curiosity_vector_obs_encoder/hidden_1/bias*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias
�
,curiosity_vector_obs_encoder/hidden_1/MatMulMatMul)curiosity_vector_obs_encoder/hidden_0/Mul1curiosity_vector_obs_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
-curiosity_vector_obs_encoder/hidden_1/BiasAddBiasAdd,curiosity_vector_obs_encoder/hidden_1/MatMul/curiosity_vector_obs_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
p
-curiosity_vector_obs_encoder/hidden_1/SigmoidSigmoid-curiosity_vector_obs_encoder/hidden_1/BiasAdd*
T0
�
)curiosity_vector_obs_encoder/hidden_1/MulMul-curiosity_vector_obs_encoder/hidden_1/BiasAdd-curiosity_vector_obs_encoder/hidden_1/Sigmoid*
T0
�
.curiosity_vector_obs_encoder_1/hidden_0/MatMulMatMul!curiosity_next_vector_observation1curiosity_vector_obs_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
/curiosity_vector_obs_encoder_1/hidden_0/BiasAddBiasAdd.curiosity_vector_obs_encoder_1/hidden_0/MatMul/curiosity_vector_obs_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
t
/curiosity_vector_obs_encoder_1/hidden_0/SigmoidSigmoid/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd*
T0
�
+curiosity_vector_obs_encoder_1/hidden_0/MulMul/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd/curiosity_vector_obs_encoder_1/hidden_0/Sigmoid*
T0
�
.curiosity_vector_obs_encoder_1/hidden_1/MatMulMatMul+curiosity_vector_obs_encoder_1/hidden_0/Mul1curiosity_vector_obs_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
/curiosity_vector_obs_encoder_1/hidden_1/BiasAddBiasAdd.curiosity_vector_obs_encoder_1/hidden_1/MatMul/curiosity_vector_obs_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
t
/curiosity_vector_obs_encoder_1/hidden_1/SigmoidSigmoid/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd*
T0
�
+curiosity_vector_obs_encoder_1/hidden_1/MulMul/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd/curiosity_vector_obs_encoder_1/hidden_1/Sigmoid*
T0
;
concat/concat_dimConst*
dtype0*
value	B :
M
concat/concatIdentity)curiosity_vector_obs_encoder/hidden_1/Mul*
T0
=
concat_1/concat_dimConst*
dtype0*
value	B :
Q
concat_1/concatIdentity+curiosity_vector_obs_encoder_1/hidden_1/Mul*
T0
7
concat_2/axisConst*
dtype0*
value	B :
a
concat_2ConcatV2concat/concatconcat_1/concatconcat_2/axis*
N*
T0*

Tidx0
�
-dense/kernel/Initializer/random_uniform/shapeConst*
_class
loc:@dense/kernel*
dtype0*
valueB"      
y
+dense/kernel/Initializer/random_uniform/minConst*
_class
loc:@dense/kernel*
dtype0*
valueB
 *���
y
+dense/kernel/Initializer/random_uniform/maxConst*
_class
loc:@dense/kernel*
dtype0*
valueB
 *��=
�
5dense/kernel/Initializer/random_uniform/RandomUniformRandomUniform-dense/kernel/Initializer/random_uniform/shape*
T0*
_class
loc:@dense/kernel*
dtype0*
seed�*
seed2
�
+dense/kernel/Initializer/random_uniform/subSub+dense/kernel/Initializer/random_uniform/max+dense/kernel/Initializer/random_uniform/min*
T0*
_class
loc:@dense/kernel
�
+dense/kernel/Initializer/random_uniform/mulMul5dense/kernel/Initializer/random_uniform/RandomUniform+dense/kernel/Initializer/random_uniform/sub*
T0*
_class
loc:@dense/kernel
�
'dense/kernel/Initializer/random_uniformAdd+dense/kernel/Initializer/random_uniform/mul+dense/kernel/Initializer/random_uniform/min*
T0*
_class
loc:@dense/kernel
�
dense/kernel
VariableV2*
_class
loc:@dense/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense/kernel/AssignAssigndense/kernel'dense/kernel/Initializer/random_uniform*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
U
dense/kernel/readIdentitydense/kernel*
T0*
_class
loc:@dense/kernel
m
dense/bias/Initializer/zerosConst*
_class
loc:@dense/bias*
dtype0*
valueB�*    
z

dense/bias
VariableV2*
_class
loc:@dense/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense/bias/AssignAssign
dense/biasdense/bias/Initializer/zeros*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
O
dense/bias/readIdentity
dense/bias*
T0*
_class
loc:@dense/bias
b
dense/MatMulMatMulconcat_2dense/kernel/read*
T0*
transpose_a( *
transpose_b( 
W
dense/BiasAddBiasAdddense/MatMuldense/bias/read*
T0*
data_formatNHWC
0
dense/SigmoidSigmoiddense/BiasAdd*
T0
7
	dense/MulMuldense/BiasAdddense/Sigmoid*
T0
�
/dense_1/kernel/Initializer/random_uniform/shapeConst*!
_class
loc:@dense_1/kernel*
dtype0*
valueB"   @   
}
-dense_1/kernel/Initializer/random_uniform/minConst*!
_class
loc:@dense_1/kernel*
dtype0*
valueB
 *�7�
}
-dense_1/kernel/Initializer/random_uniform/maxConst*!
_class
loc:@dense_1/kernel*
dtype0*
valueB
 *�7>
�
7dense_1/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_1/kernel/Initializer/random_uniform/shape*
T0*!
_class
loc:@dense_1/kernel*
dtype0*
seed�*
seed2	
�
-dense_1/kernel/Initializer/random_uniform/subSub-dense_1/kernel/Initializer/random_uniform/max-dense_1/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_1/kernel
�
-dense_1/kernel/Initializer/random_uniform/mulMul7dense_1/kernel/Initializer/random_uniform/RandomUniform-dense_1/kernel/Initializer/random_uniform/sub*
T0*!
_class
loc:@dense_1/kernel
�
)dense_1/kernel/Initializer/random_uniformAdd-dense_1/kernel/Initializer/random_uniform/mul-dense_1/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_1/kernel
�
dense_1/kernel
VariableV2*!
_class
loc:@dense_1/kernel*
	container *
dtype0*
shape:	�@*
shared_name 
�
dense_1/kernel/AssignAssigndense_1/kernel)dense_1/kernel/Initializer/random_uniform*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
[
dense_1/kernel/readIdentitydense_1/kernel*
T0*!
_class
loc:@dense_1/kernel
p
dense_1/bias/Initializer/zerosConst*
_class
loc:@dense_1/bias*
dtype0*
valueB@*    
}
dense_1/bias
VariableV2*
_class
loc:@dense_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
dense_1/bias/AssignAssigndense_1/biasdense_1/bias/Initializer/zeros*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
U
dense_1/bias/readIdentitydense_1/bias*
T0*
_class
loc:@dense_1/bias
g
dense_1/MatMulMatMul	dense/Muldense_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
]
dense_1/BiasAddBiasAdddense_1/MatMuldense_1/bias/read*
T0*
data_formatNHWC
4
dense_1/SoftmaxSoftmaxdense_1/BiasAdd*
T0
=
concat_3/concat_dimConst*
dtype0*
value	B :
5
concat_3/concatIdentitydense_1/Softmax*
T0
4
add_1/yConst*
dtype0*
valueB
 *���.
1
add_1AddV2concat_3/concatadd_1/y*
T0

LogLogadd_1*
T0

NegNegLog*
T0
&
mulMulNegStopGradient*
T0
?
Sum/reduction_indicesConst*
dtype0*
value	B :
L
SumSummulSum/reduction_indices*
T0*

Tidx0*
	keep_dims( 
N
DynamicPartitionDynamicPartitionSumCast*
T0*
num_partitions
3
ConstConst*
dtype0*
valueB: 
M
MeanMeanDynamicPartition:1Const*
T0*

Tidx0*
	keep_dims( 
7
concat_4/axisConst*
dtype0*
value	B :
^
concat_4ConcatV2concat/concatStopGradientconcat_4/axis*
N*
T0*

Tidx0
�
/dense_2/kernel/Initializer/random_uniform/shapeConst*!
_class
loc:@dense_2/kernel*
dtype0*
valueB"@     
}
-dense_2/kernel/Initializer/random_uniform/minConst*!
_class
loc:@dense_2/kernel*
dtype0*
valueB
 *�ѽ
}
-dense_2/kernel/Initializer/random_uniform/maxConst*!
_class
loc:@dense_2/kernel*
dtype0*
valueB
 *��=
�
7dense_2/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_2/kernel/Initializer/random_uniform/shape*
T0*!
_class
loc:@dense_2/kernel*
dtype0*
seed�*
seed2

�
-dense_2/kernel/Initializer/random_uniform/subSub-dense_2/kernel/Initializer/random_uniform/max-dense_2/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_2/kernel
�
-dense_2/kernel/Initializer/random_uniform/mulMul7dense_2/kernel/Initializer/random_uniform/RandomUniform-dense_2/kernel/Initializer/random_uniform/sub*
T0*!
_class
loc:@dense_2/kernel
�
)dense_2/kernel/Initializer/random_uniformAdd-dense_2/kernel/Initializer/random_uniform/mul-dense_2/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_2/kernel
�
dense_2/kernel
VariableV2*!
_class
loc:@dense_2/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense_2/kernel/AssignAssigndense_2/kernel)dense_2/kernel/Initializer/random_uniform*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
[
dense_2/kernel/readIdentitydense_2/kernel*
T0*!
_class
loc:@dense_2/kernel
q
dense_2/bias/Initializer/zerosConst*
_class
loc:@dense_2/bias*
dtype0*
valueB�*    
~
dense_2/bias
VariableV2*
_class
loc:@dense_2/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense_2/bias/AssignAssigndense_2/biasdense_2/bias/Initializer/zeros*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
U
dense_2/bias/readIdentitydense_2/bias*
T0*
_class
loc:@dense_2/bias
f
dense_2/MatMulMatMulconcat_4dense_2/kernel/read*
T0*
transpose_a( *
transpose_b( 
]
dense_2/BiasAddBiasAdddense_2/MatMuldense_2/bias/read*
T0*
data_formatNHWC
4
dense_2/SigmoidSigmoiddense_2/BiasAdd*
T0
=
dense_2/MulMuldense_2/BiasAdddense_2/Sigmoid*
T0
�
/dense_3/kernel/Initializer/random_uniform/shapeConst*!
_class
loc:@dense_3/kernel*
dtype0*
valueB"      
}
-dense_3/kernel/Initializer/random_uniform/minConst*!
_class
loc:@dense_3/kernel*
dtype0*
valueB
 *׳ݽ
}
-dense_3/kernel/Initializer/random_uniform/maxConst*!
_class
loc:@dense_3/kernel*
dtype0*
valueB
 *׳�=
�
7dense_3/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_3/kernel/Initializer/random_uniform/shape*
T0*!
_class
loc:@dense_3/kernel*
dtype0*
seed�*
seed2
�
-dense_3/kernel/Initializer/random_uniform/subSub-dense_3/kernel/Initializer/random_uniform/max-dense_3/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_3/kernel
�
-dense_3/kernel/Initializer/random_uniform/mulMul7dense_3/kernel/Initializer/random_uniform/RandomUniform-dense_3/kernel/Initializer/random_uniform/sub*
T0*!
_class
loc:@dense_3/kernel
�
)dense_3/kernel/Initializer/random_uniformAdd-dense_3/kernel/Initializer/random_uniform/mul-dense_3/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_3/kernel
�
dense_3/kernel
VariableV2*!
_class
loc:@dense_3/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense_3/kernel/AssignAssigndense_3/kernel)dense_3/kernel/Initializer/random_uniform*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
[
dense_3/kernel/readIdentitydense_3/kernel*
T0*!
_class
loc:@dense_3/kernel
q
dense_3/bias/Initializer/zerosConst*
_class
loc:@dense_3/bias*
dtype0*
valueB�*    
~
dense_3/bias
VariableV2*
_class
loc:@dense_3/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense_3/bias/AssignAssigndense_3/biasdense_3/bias/Initializer/zeros*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
U
dense_3/bias/readIdentitydense_3/bias*
T0*
_class
loc:@dense_3/bias
i
dense_3/MatMulMatMuldense_2/Muldense_3/kernel/read*
T0*
transpose_a( *
transpose_b( 
]
dense_3/BiasAddBiasAdddense_3/MatMuldense_3/bias/read*
T0*
data_formatNHWC
Q
SquaredDifferenceSquaredDifferencedense_3/BiasAddconcat_1/concat*
T0
A
Sum_1/reduction_indicesConst*
dtype0*
value	B :
^
Sum_1SumSquaredDifferenceSum_1/reduction_indices*
T0*

Tidx0*
	keep_dims( 
4
mul_1/xConst*
dtype0*
valueB
 *   ?
%
mul_1Mulmul_1/xSum_1*
T0
R
DynamicPartition_1DynamicPartitionmul_1Cast*
T0*
num_partitions
5
Const_1Const*
dtype0*
valueB: 
S
Mean_1MeanDynamicPartition_1:1Const_1*
T0*

Tidx0*
	keep_dims( 
4
mul_2/xConst*
dtype0*
valueB
 *��L>
&
mul_2Mulmul_2/xMean_1*
T0
4
mul_3/xConst*
dtype0*
valueB
 *��L?
$
mul_3Mulmul_3/xMean*
T0
%
add_2AddV2mul_2mul_3*
T0
4
mul_4/xConst*
dtype0*
valueB
 *   A
%
mul_4Mulmul_4/xadd_2*
T0
8
gradients/ShapeConst*
dtype0*
valueB 
@
gradients/grad_ys_0Const*
dtype0*
valueB
 *  �?
W
gradients/FillFillgradients/Shapegradients/grad_ys_0*
T0*

index_type0
?
gradients/mul_4_grad/MulMulgradients/Filladd_2*
T0
C
gradients/mul_4_grad/Mul_1Mulgradients/Fillmul_4/x*
T0
e
%gradients/mul_4_grad/tuple/group_depsNoOp^gradients/mul_4_grad/Mul^gradients/mul_4_grad/Mul_1
�
-gradients/mul_4_grad/tuple/control_dependencyIdentitygradients/mul_4_grad/Mul&^gradients/mul_4_grad/tuple/group_deps*
T0*+
_class!
loc:@gradients/mul_4_grad/Mul
�
/gradients/mul_4_grad/tuple/control_dependency_1Identitygradients/mul_4_grad/Mul_1&^gradients/mul_4_grad/tuple/group_deps*
T0*-
_class#
!loc:@gradients/mul_4_grad/Mul_1
_
%gradients/add_2_grad/tuple/group_depsNoOp0^gradients/mul_4_grad/tuple/control_dependency_1
�
-gradients/add_2_grad/tuple/control_dependencyIdentity/gradients/mul_4_grad/tuple/control_dependency_1&^gradients/add_2_grad/tuple/group_deps*
T0*-
_class#
!loc:@gradients/mul_4_grad/Mul_1
�
/gradients/add_2_grad/tuple/control_dependency_1Identity/gradients/mul_4_grad/tuple/control_dependency_1&^gradients/add_2_grad/tuple/group_deps*
T0*-
_class#
!loc:@gradients/mul_4_grad/Mul_1
_
gradients/mul_2_grad/MulMul-gradients/add_2_grad/tuple/control_dependencyMean_1*
T0
b
gradients/mul_2_grad/Mul_1Mul-gradients/add_2_grad/tuple/control_dependencymul_2/x*
T0
e
%gradients/mul_2_grad/tuple/group_depsNoOp^gradients/mul_2_grad/Mul^gradients/mul_2_grad/Mul_1
�
-gradients/mul_2_grad/tuple/control_dependencyIdentitygradients/mul_2_grad/Mul&^gradients/mul_2_grad/tuple/group_deps*
T0*+
_class!
loc:@gradients/mul_2_grad/Mul
�
/gradients/mul_2_grad/tuple/control_dependency_1Identitygradients/mul_2_grad/Mul_1&^gradients/mul_2_grad/tuple/group_deps*
T0*-
_class#
!loc:@gradients/mul_2_grad/Mul_1
_
gradients/mul_3_grad/MulMul/gradients/add_2_grad/tuple/control_dependency_1Mean*
T0
d
gradients/mul_3_grad/Mul_1Mul/gradients/add_2_grad/tuple/control_dependency_1mul_3/x*
T0
e
%gradients/mul_3_grad/tuple/group_depsNoOp^gradients/mul_3_grad/Mul^gradients/mul_3_grad/Mul_1
�
-gradients/mul_3_grad/tuple/control_dependencyIdentitygradients/mul_3_grad/Mul&^gradients/mul_3_grad/tuple/group_deps*
T0*+
_class!
loc:@gradients/mul_3_grad/Mul
�
/gradients/mul_3_grad/tuple/control_dependency_1Identitygradients/mul_3_grad/Mul_1&^gradients/mul_3_grad/tuple/group_deps*
T0*-
_class#
!loc:@gradients/mul_3_grad/Mul_1
Q
#gradients/Mean_1_grad/Reshape/shapeConst*
dtype0*
valueB:
�
gradients/Mean_1_grad/ReshapeReshape/gradients/mul_2_grad/tuple/control_dependency_1#gradients/Mean_1_grad/Reshape/shape*
T0*
Tshape0
S
gradients/Mean_1_grad/ShapeShapeDynamicPartition_1:1*
T0*
out_type0
y
gradients/Mean_1_grad/TileTilegradients/Mean_1_grad/Reshapegradients/Mean_1_grad/Shape*
T0*

Tmultiples0
U
gradients/Mean_1_grad/Shape_1ShapeDynamicPartition_1:1*
T0*
out_type0
F
gradients/Mean_1_grad/Shape_2Const*
dtype0*
valueB 
I
gradients/Mean_1_grad/ConstConst*
dtype0*
valueB: 
�
gradients/Mean_1_grad/ProdProdgradients/Mean_1_grad/Shape_1gradients/Mean_1_grad/Const*
T0*

Tidx0*
	keep_dims( 
K
gradients/Mean_1_grad/Const_1Const*
dtype0*
valueB: 
�
gradients/Mean_1_grad/Prod_1Prodgradients/Mean_1_grad/Shape_2gradients/Mean_1_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
I
gradients/Mean_1_grad/Maximum/yConst*
dtype0*
value	B :
p
gradients/Mean_1_grad/MaximumMaximumgradients/Mean_1_grad/Prod_1gradients/Mean_1_grad/Maximum/y*
T0
n
gradients/Mean_1_grad/floordivFloorDivgradients/Mean_1_grad/Prodgradients/Mean_1_grad/Maximum*
T0
j
gradients/Mean_1_grad/CastCastgradients/Mean_1_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
i
gradients/Mean_1_grad/truedivRealDivgradients/Mean_1_grad/Tilegradients/Mean_1_grad/Cast*
T0
O
!gradients/Mean_grad/Reshape/shapeConst*
dtype0*
valueB:
�
gradients/Mean_grad/ReshapeReshape/gradients/mul_3_grad/tuple/control_dependency_1!gradients/Mean_grad/Reshape/shape*
T0*
Tshape0
O
gradients/Mean_grad/ShapeShapeDynamicPartition:1*
T0*
out_type0
s
gradients/Mean_grad/TileTilegradients/Mean_grad/Reshapegradients/Mean_grad/Shape*
T0*

Tmultiples0
Q
gradients/Mean_grad/Shape_1ShapeDynamicPartition:1*
T0*
out_type0
D
gradients/Mean_grad/Shape_2Const*
dtype0*
valueB 
G
gradients/Mean_grad/ConstConst*
dtype0*
valueB: 
~
gradients/Mean_grad/ProdProdgradients/Mean_grad/Shape_1gradients/Mean_grad/Const*
T0*

Tidx0*
	keep_dims( 
I
gradients/Mean_grad/Const_1Const*
dtype0*
valueB: 
�
gradients/Mean_grad/Prod_1Prodgradients/Mean_grad/Shape_2gradients/Mean_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
G
gradients/Mean_grad/Maximum/yConst*
dtype0*
value	B :
j
gradients/Mean_grad/MaximumMaximumgradients/Mean_grad/Prod_1gradients/Mean_grad/Maximum/y*
T0
h
gradients/Mean_grad/floordivFloorDivgradients/Mean_grad/Prodgradients/Mean_grad/Maximum*
T0
f
gradients/Mean_grad/CastCastgradients/Mean_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
c
gradients/Mean_grad/truedivRealDivgradients/Mean_grad/Tilegradients/Mean_grad/Cast*
T0
>
gradients/zeros_like	ZerosLikeDynamicPartition_1*
T0
O
'gradients/DynamicPartition_1_grad/ShapeShapeCast*
T0*
out_type0
U
'gradients/DynamicPartition_1_grad/ConstConst*
dtype0*
valueB: 
�
&gradients/DynamicPartition_1_grad/ProdProd'gradients/DynamicPartition_1_grad/Shape'gradients/DynamicPartition_1_grad/Const*
T0*

Tidx0*
	keep_dims( 
W
-gradients/DynamicPartition_1_grad/range/startConst*
dtype0*
value	B : 
W
-gradients/DynamicPartition_1_grad/range/deltaConst*
dtype0*
value	B :
�
'gradients/DynamicPartition_1_grad/rangeRange-gradients/DynamicPartition_1_grad/range/start&gradients/DynamicPartition_1_grad/Prod-gradients/DynamicPartition_1_grad/range/delta*

Tidx0
�
)gradients/DynamicPartition_1_grad/ReshapeReshape'gradients/DynamicPartition_1_grad/range'gradients/DynamicPartition_1_grad/Shape*
T0*
Tshape0
�
2gradients/DynamicPartition_1_grad/DynamicPartitionDynamicPartition)gradients/DynamicPartition_1_grad/ReshapeCast*
T0*
num_partitions
�
7gradients/DynamicPartition_1_grad/ParallelDynamicStitchParallelDynamicStitch2gradients/DynamicPartition_1_grad/DynamicPartition4gradients/DynamicPartition_1_grad/DynamicPartition:1gradients/zeros_likegradients/Mean_1_grad/truediv*
N*
T0
R
)gradients/DynamicPartition_1_grad/Shape_1Shapemul_1*
T0*
out_type0
�
+gradients/DynamicPartition_1_grad/Reshape_1Reshape7gradients/DynamicPartition_1_grad/ParallelDynamicStitch)gradients/DynamicPartition_1_grad/Shape_1*
T0*
Tshape0
>
gradients/zeros_like_1	ZerosLikeDynamicPartition*
T0
M
%gradients/DynamicPartition_grad/ShapeShapeCast*
T0*
out_type0
S
%gradients/DynamicPartition_grad/ConstConst*
dtype0*
valueB: 
�
$gradients/DynamicPartition_grad/ProdProd%gradients/DynamicPartition_grad/Shape%gradients/DynamicPartition_grad/Const*
T0*

Tidx0*
	keep_dims( 
U
+gradients/DynamicPartition_grad/range/startConst*
dtype0*
value	B : 
U
+gradients/DynamicPartition_grad/range/deltaConst*
dtype0*
value	B :
�
%gradients/DynamicPartition_grad/rangeRange+gradients/DynamicPartition_grad/range/start$gradients/DynamicPartition_grad/Prod+gradients/DynamicPartition_grad/range/delta*

Tidx0
�
'gradients/DynamicPartition_grad/ReshapeReshape%gradients/DynamicPartition_grad/range%gradients/DynamicPartition_grad/Shape*
T0*
Tshape0
�
0gradients/DynamicPartition_grad/DynamicPartitionDynamicPartition'gradients/DynamicPartition_grad/ReshapeCast*
T0*
num_partitions
�
5gradients/DynamicPartition_grad/ParallelDynamicStitchParallelDynamicStitch0gradients/DynamicPartition_grad/DynamicPartition2gradients/DynamicPartition_grad/DynamicPartition:1gradients/zeros_like_1gradients/Mean_grad/truediv*
N*
T0
N
'gradients/DynamicPartition_grad/Shape_1ShapeSum*
T0*
out_type0
�
)gradients/DynamicPartition_grad/Reshape_1Reshape5gradients/DynamicPartition_grad/ParallelDynamicStitch'gradients/DynamicPartition_grad/Shape_1*
T0*
Tshape0
E
gradients/mul_1_grad/ShapeShapemul_1/x*
T0*
out_type0
E
gradients/mul_1_grad/Shape_1ShapeSum_1*
T0*
out_type0
�
*gradients/mul_1_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/mul_1_grad/Shapegradients/mul_1_grad/Shape_1*
T0
\
gradients/mul_1_grad/MulMul+gradients/DynamicPartition_1_grad/Reshape_1Sum_1*
T0
�
gradients/mul_1_grad/SumSumgradients/mul_1_grad/Mul*gradients/mul_1_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
t
gradients/mul_1_grad/ReshapeReshapegradients/mul_1_grad/Sumgradients/mul_1_grad/Shape*
T0*
Tshape0
`
gradients/mul_1_grad/Mul_1Mulmul_1/x+gradients/DynamicPartition_1_grad/Reshape_1*
T0
�
gradients/mul_1_grad/Sum_1Sumgradients/mul_1_grad/Mul_1,gradients/mul_1_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
z
gradients/mul_1_grad/Reshape_1Reshapegradients/mul_1_grad/Sum_1gradients/mul_1_grad/Shape_1*
T0*
Tshape0
m
%gradients/mul_1_grad/tuple/group_depsNoOp^gradients/mul_1_grad/Reshape^gradients/mul_1_grad/Reshape_1
�
-gradients/mul_1_grad/tuple/control_dependencyIdentitygradients/mul_1_grad/Reshape&^gradients/mul_1_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/mul_1_grad/Reshape
�
/gradients/mul_1_grad/tuple/control_dependency_1Identitygradients/mul_1_grad/Reshape_1&^gradients/mul_1_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/mul_1_grad/Reshape_1
?
gradients/Sum_grad/ShapeShapemul*
T0*
out_type0
n
gradients/Sum_grad/SizeConst*+
_class!
loc:@gradients/Sum_grad/Shape*
dtype0*
value	B :
�
gradients/Sum_grad/addAddV2Sum/reduction_indicesgradients/Sum_grad/Size*
T0*+
_class!
loc:@gradients/Sum_grad/Shape
�
gradients/Sum_grad/modFloorModgradients/Sum_grad/addgradients/Sum_grad/Size*
T0*+
_class!
loc:@gradients/Sum_grad/Shape
p
gradients/Sum_grad/Shape_1Const*+
_class!
loc:@gradients/Sum_grad/Shape*
dtype0*
valueB 
u
gradients/Sum_grad/range/startConst*+
_class!
loc:@gradients/Sum_grad/Shape*
dtype0*
value	B : 
u
gradients/Sum_grad/range/deltaConst*+
_class!
loc:@gradients/Sum_grad/Shape*
dtype0*
value	B :
�
gradients/Sum_grad/rangeRangegradients/Sum_grad/range/startgradients/Sum_grad/Sizegradients/Sum_grad/range/delta*

Tidx0*+
_class!
loc:@gradients/Sum_grad/Shape
t
gradients/Sum_grad/Fill/valueConst*+
_class!
loc:@gradients/Sum_grad/Shape*
dtype0*
value	B :
�
gradients/Sum_grad/FillFillgradients/Sum_grad/Shape_1gradients/Sum_grad/Fill/value*
T0*+
_class!
loc:@gradients/Sum_grad/Shape*

index_type0
�
 gradients/Sum_grad/DynamicStitchDynamicStitchgradients/Sum_grad/rangegradients/Sum_grad/modgradients/Sum_grad/Shapegradients/Sum_grad/Fill*
N*
T0*+
_class!
loc:@gradients/Sum_grad/Shape
�
gradients/Sum_grad/ReshapeReshape)gradients/DynamicPartition_grad/Reshape_1 gradients/Sum_grad/DynamicStitch*
T0*
Tshape0
x
gradients/Sum_grad/BroadcastToBroadcastTogradients/Sum_grad/Reshapegradients/Sum_grad/Shape*
T0*

Tidx0
O
gradients/Sum_1_grad/ShapeShapeSquaredDifference*
T0*
out_type0
r
gradients/Sum_1_grad/SizeConst*-
_class#
!loc:@gradients/Sum_1_grad/Shape*
dtype0*
value	B :
�
gradients/Sum_1_grad/addAddV2Sum_1/reduction_indicesgradients/Sum_1_grad/Size*
T0*-
_class#
!loc:@gradients/Sum_1_grad/Shape
�
gradients/Sum_1_grad/modFloorModgradients/Sum_1_grad/addgradients/Sum_1_grad/Size*
T0*-
_class#
!loc:@gradients/Sum_1_grad/Shape
t
gradients/Sum_1_grad/Shape_1Const*-
_class#
!loc:@gradients/Sum_1_grad/Shape*
dtype0*
valueB 
y
 gradients/Sum_1_grad/range/startConst*-
_class#
!loc:@gradients/Sum_1_grad/Shape*
dtype0*
value	B : 
y
 gradients/Sum_1_grad/range/deltaConst*-
_class#
!loc:@gradients/Sum_1_grad/Shape*
dtype0*
value	B :
�
gradients/Sum_1_grad/rangeRange gradients/Sum_1_grad/range/startgradients/Sum_1_grad/Size gradients/Sum_1_grad/range/delta*

Tidx0*-
_class#
!loc:@gradients/Sum_1_grad/Shape
x
gradients/Sum_1_grad/Fill/valueConst*-
_class#
!loc:@gradients/Sum_1_grad/Shape*
dtype0*
value	B :
�
gradients/Sum_1_grad/FillFillgradients/Sum_1_grad/Shape_1gradients/Sum_1_grad/Fill/value*
T0*-
_class#
!loc:@gradients/Sum_1_grad/Shape*

index_type0
�
"gradients/Sum_1_grad/DynamicStitchDynamicStitchgradients/Sum_1_grad/rangegradients/Sum_1_grad/modgradients/Sum_1_grad/Shapegradients/Sum_1_grad/Fill*
N*
T0*-
_class#
!loc:@gradients/Sum_1_grad/Shape
�
gradients/Sum_1_grad/ReshapeReshape/gradients/mul_1_grad/tuple/control_dependency_1"gradients/Sum_1_grad/DynamicStitch*
T0*
Tshape0
~
 gradients/Sum_1_grad/BroadcastToBroadcastTogradients/Sum_1_grad/Reshapegradients/Sum_1_grad/Shape*
T0*

Tidx0
?
gradients/mul_grad/ShapeShapeNeg*
T0*
out_type0
J
gradients/mul_grad/Shape_1ShapeStopGradient*
T0*
out_type0
�
(gradients/mul_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/mul_grad/Shapegradients/mul_grad/Shape_1*
T0
T
gradients/mul_grad/MulMulgradients/Sum_grad/BroadcastToStopGradient*
T0
�
gradients/mul_grad/SumSumgradients/mul_grad/Mul(gradients/mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
n
gradients/mul_grad/ReshapeReshapegradients/mul_grad/Sumgradients/mul_grad/Shape*
T0*
Tshape0
M
gradients/mul_grad/Mul_1MulNeggradients/Sum_grad/BroadcastTo*
T0
�
gradients/mul_grad/Sum_1Sumgradients/mul_grad/Mul_1*gradients/mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
t
gradients/mul_grad/Reshape_1Reshapegradients/mul_grad/Sum_1gradients/mul_grad/Shape_1*
T0*
Tshape0
g
#gradients/mul_grad/tuple/group_depsNoOp^gradients/mul_grad/Reshape^gradients/mul_grad/Reshape_1
�
+gradients/mul_grad/tuple/control_dependencyIdentitygradients/mul_grad/Reshape$^gradients/mul_grad/tuple/group_deps*
T0*-
_class#
!loc:@gradients/mul_grad/Reshape
�
-gradients/mul_grad/tuple/control_dependency_1Identitygradients/mul_grad/Reshape_1$^gradients/mul_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/mul_grad/Reshape_1
w
'gradients/SquaredDifference_grad/scalarConst!^gradients/Sum_1_grad/BroadcastTo*
dtype0*
valueB
 *   @

$gradients/SquaredDifference_grad/MulMul'gradients/SquaredDifference_grad/scalar gradients/Sum_1_grad/BroadcastTo*
T0
y
$gradients/SquaredDifference_grad/subSubdense_3/BiasAddconcat_1/concat!^gradients/Sum_1_grad/BroadcastTo*
T0
�
&gradients/SquaredDifference_grad/mul_1Mul$gradients/SquaredDifference_grad/Mul$gradients/SquaredDifference_grad/sub*
T0
Y
&gradients/SquaredDifference_grad/ShapeShapedense_3/BiasAdd*
T0*
out_type0
[
(gradients/SquaredDifference_grad/Shape_1Shapeconcat_1/concat*
T0*
out_type0
�
6gradients/SquaredDifference_grad/BroadcastGradientArgsBroadcastGradientArgs&gradients/SquaredDifference_grad/Shape(gradients/SquaredDifference_grad/Shape_1*
T0
�
$gradients/SquaredDifference_grad/SumSum&gradients/SquaredDifference_grad/mul_16gradients/SquaredDifference_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
(gradients/SquaredDifference_grad/ReshapeReshape$gradients/SquaredDifference_grad/Sum&gradients/SquaredDifference_grad/Shape*
T0*
Tshape0
�
&gradients/SquaredDifference_grad/Sum_1Sum&gradients/SquaredDifference_grad/mul_18gradients/SquaredDifference_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
*gradients/SquaredDifference_grad/Reshape_1Reshape&gradients/SquaredDifference_grad/Sum_1(gradients/SquaredDifference_grad/Shape_1*
T0*
Tshape0
`
$gradients/SquaredDifference_grad/NegNeg*gradients/SquaredDifference_grad/Reshape_1*
T0
�
1gradients/SquaredDifference_grad/tuple/group_depsNoOp%^gradients/SquaredDifference_grad/Neg)^gradients/SquaredDifference_grad/Reshape
�
9gradients/SquaredDifference_grad/tuple/control_dependencyIdentity(gradients/SquaredDifference_grad/Reshape2^gradients/SquaredDifference_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/SquaredDifference_grad/Reshape
�
;gradients/SquaredDifference_grad/tuple/control_dependency_1Identity$gradients/SquaredDifference_grad/Neg2^gradients/SquaredDifference_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/SquaredDifference_grad/Neg
S
gradients/Neg_grad/NegNeg+gradients/mul_grad/tuple/control_dependency*
T0
�
*gradients/dense_3/BiasAdd_grad/BiasAddGradBiasAddGrad9gradients/SquaredDifference_grad/tuple/control_dependency*
T0*
data_formatNHWC
�
/gradients/dense_3/BiasAdd_grad/tuple/group_depsNoOp:^gradients/SquaredDifference_grad/tuple/control_dependency+^gradients/dense_3/BiasAdd_grad/BiasAddGrad
�
7gradients/dense_3/BiasAdd_grad/tuple/control_dependencyIdentity9gradients/SquaredDifference_grad/tuple/control_dependency0^gradients/dense_3/BiasAdd_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/SquaredDifference_grad/Reshape
�
9gradients/dense_3/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_3/BiasAdd_grad/BiasAddGrad0^gradients/dense_3/BiasAdd_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/dense_3/BiasAdd_grad/BiasAddGrad
T
gradients/Log_grad/Reciprocal
Reciprocaladd_1^gradients/Neg_grad/Neg*
T0
]
gradients/Log_grad/mulMulgradients/Neg_grad/Neggradients/Log_grad/Reciprocal*
T0
�
$gradients/dense_3/MatMul_grad/MatMulMatMul7gradients/dense_3/BiasAdd_grad/tuple/control_dependencydense_3/kernel/read*
T0*
transpose_a( *
transpose_b(
�
&gradients/dense_3/MatMul_grad/MatMul_1MatMuldense_2/Mul7gradients/dense_3/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
.gradients/dense_3/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_3/MatMul_grad/MatMul'^gradients/dense_3/MatMul_grad/MatMul_1
�
6gradients/dense_3/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_3/MatMul_grad/MatMul/^gradients/dense_3/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_3/MatMul_grad/MatMul
�
8gradients/dense_3/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_3/MatMul_grad/MatMul_1/^gradients/dense_3/MatMul_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/dense_3/MatMul_grad/MatMul_1
M
gradients/add_1_grad/ShapeShapeconcat_3/concat*
T0*
out_type0
G
gradients/add_1_grad/Shape_1Shapeadd_1/y*
T0*
out_type0
�
*gradients/add_1_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/add_1_grad/Shapegradients/add_1_grad/Shape_1*
T0
�
gradients/add_1_grad/SumSumgradients/Log_grad/mul*gradients/add_1_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
t
gradients/add_1_grad/ReshapeReshapegradients/add_1_grad/Sumgradients/add_1_grad/Shape*
T0*
Tshape0
�
gradients/add_1_grad/Sum_1Sumgradients/Log_grad/mul,gradients/add_1_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
z
gradients/add_1_grad/Reshape_1Reshapegradients/add_1_grad/Sum_1gradients/add_1_grad/Shape_1*
T0*
Tshape0
m
%gradients/add_1_grad/tuple/group_depsNoOp^gradients/add_1_grad/Reshape^gradients/add_1_grad/Reshape_1
�
-gradients/add_1_grad/tuple/control_dependencyIdentitygradients/add_1_grad/Reshape&^gradients/add_1_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/add_1_grad/Reshape
�
/gradients/add_1_grad/tuple/control_dependency_1Identitygradients/add_1_grad/Reshape_1&^gradients/add_1_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/add_1_grad/Reshape_1
S
 gradients/dense_2/Mul_grad/ShapeShapedense_2/BiasAdd*
T0*
out_type0
U
"gradients/dense_2/Mul_grad/Shape_1Shapedense_2/Sigmoid*
T0*
out_type0
�
0gradients/dense_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs gradients/dense_2/Mul_grad/Shape"gradients/dense_2/Mul_grad/Shape_1*
T0
w
gradients/dense_2/Mul_grad/MulMul6gradients/dense_3/MatMul_grad/tuple/control_dependencydense_2/Sigmoid*
T0
�
gradients/dense_2/Mul_grad/SumSumgradients/dense_2/Mul_grad/Mul0gradients/dense_2/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
"gradients/dense_2/Mul_grad/ReshapeReshapegradients/dense_2/Mul_grad/Sum gradients/dense_2/Mul_grad/Shape*
T0*
Tshape0
y
 gradients/dense_2/Mul_grad/Mul_1Muldense_2/BiasAdd6gradients/dense_3/MatMul_grad/tuple/control_dependency*
T0
�
 gradients/dense_2/Mul_grad/Sum_1Sum gradients/dense_2/Mul_grad/Mul_12gradients/dense_2/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
$gradients/dense_2/Mul_grad/Reshape_1Reshape gradients/dense_2/Mul_grad/Sum_1"gradients/dense_2/Mul_grad/Shape_1*
T0*
Tshape0

+gradients/dense_2/Mul_grad/tuple/group_depsNoOp#^gradients/dense_2/Mul_grad/Reshape%^gradients/dense_2/Mul_grad/Reshape_1
�
3gradients/dense_2/Mul_grad/tuple/control_dependencyIdentity"gradients/dense_2/Mul_grad/Reshape,^gradients/dense_2/Mul_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/dense_2/Mul_grad/Reshape
�
5gradients/dense_2/Mul_grad/tuple/control_dependency_1Identity$gradients/dense_2/Mul_grad/Reshape_1,^gradients/dense_2/Mul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_2/Mul_grad/Reshape_1
�
*gradients/dense_2/Sigmoid_grad/SigmoidGradSigmoidGraddense_2/Sigmoid5gradients/dense_2/Mul_grad/tuple/control_dependency_1*
T0
r
"gradients/dense_1/Softmax_grad/mulMul-gradients/add_1_grad/tuple/control_dependencydense_1/Softmax*
T0
g
4gradients/dense_1/Softmax_grad/Sum/reduction_indicesConst*
dtype0*
valueB :
���������
�
"gradients/dense_1/Softmax_grad/SumSum"gradients/dense_1/Softmax_grad/mul4gradients/dense_1/Softmax_grad/Sum/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
"gradients/dense_1/Softmax_grad/subSub-gradients/add_1_grad/tuple/control_dependency"gradients/dense_1/Softmax_grad/Sum*
T0
i
$gradients/dense_1/Softmax_grad/mul_1Mul"gradients/dense_1/Softmax_grad/subdense_1/Softmax*
T0
�
gradients/AddNAddN3gradients/dense_2/Mul_grad/tuple/control_dependency*gradients/dense_2/Sigmoid_grad/SigmoidGrad*
N*
T0*5
_class+
)'loc:@gradients/dense_2/Mul_grad/Reshape
i
*gradients/dense_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN*
T0*
data_formatNHWC
u
/gradients/dense_2/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN+^gradients/dense_2/BiasAdd_grad/BiasAddGrad
�
7gradients/dense_2/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN0^gradients/dense_2/BiasAdd_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/dense_2/Mul_grad/Reshape
�
9gradients/dense_2/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_2/BiasAdd_grad/BiasAddGrad0^gradients/dense_2/BiasAdd_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/dense_2/BiasAdd_grad/BiasAddGrad

*gradients/dense_1/BiasAdd_grad/BiasAddGradBiasAddGrad$gradients/dense_1/Softmax_grad/mul_1*
T0*
data_formatNHWC
�
/gradients/dense_1/BiasAdd_grad/tuple/group_depsNoOp+^gradients/dense_1/BiasAdd_grad/BiasAddGrad%^gradients/dense_1/Softmax_grad/mul_1
�
7gradients/dense_1/BiasAdd_grad/tuple/control_dependencyIdentity$gradients/dense_1/Softmax_grad/mul_10^gradients/dense_1/BiasAdd_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_1/Softmax_grad/mul_1
�
9gradients/dense_1/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_1/BiasAdd_grad/BiasAddGrad0^gradients/dense_1/BiasAdd_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/dense_1/BiasAdd_grad/BiasAddGrad
�
$gradients/dense_2/MatMul_grad/MatMulMatMul7gradients/dense_2/BiasAdd_grad/tuple/control_dependencydense_2/kernel/read*
T0*
transpose_a( *
transpose_b(
�
&gradients/dense_2/MatMul_grad/MatMul_1MatMulconcat_47gradients/dense_2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
.gradients/dense_2/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_2/MatMul_grad/MatMul'^gradients/dense_2/MatMul_grad/MatMul_1
�
6gradients/dense_2/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_2/MatMul_grad/MatMul/^gradients/dense_2/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_2/MatMul_grad/MatMul
�
8gradients/dense_2/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_2/MatMul_grad/MatMul_1/^gradients/dense_2/MatMul_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/dense_2/MatMul_grad/MatMul_1
�
$gradients/dense_1/MatMul_grad/MatMulMatMul7gradients/dense_1/BiasAdd_grad/tuple/control_dependencydense_1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
&gradients/dense_1/MatMul_grad/MatMul_1MatMul	dense/Mul7gradients/dense_1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
.gradients/dense_1/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_1/MatMul_grad/MatMul'^gradients/dense_1/MatMul_grad/MatMul_1
�
6gradients/dense_1/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_1/MatMul_grad/MatMul/^gradients/dense_1/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_1/MatMul_grad/MatMul
�
8gradients/dense_1/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_1/MatMul_grad/MatMul_1/^gradients/dense_1/MatMul_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/dense_1/MatMul_grad/MatMul_1
F
gradients/concat_4_grad/RankConst*
dtype0*
value	B :
]
gradients/concat_4_grad/modFloorModconcat_4/axisgradients/concat_4_grad/Rank*
T0
N
gradients/concat_4_grad/ShapeShapeconcat/concat*
T0*
out_type0
g
gradients/concat_4_grad/ShapeNShapeNconcat/concatStopGradient*
N*
T0*
out_type0
�
$gradients/concat_4_grad/ConcatOffsetConcatOffsetgradients/concat_4_grad/modgradients/concat_4_grad/ShapeN gradients/concat_4_grad/ShapeN:1*
N
�
gradients/concat_4_grad/SliceSlice6gradients/dense_2/MatMul_grad/tuple/control_dependency$gradients/concat_4_grad/ConcatOffsetgradients/concat_4_grad/ShapeN*
Index0*
T0
�
gradients/concat_4_grad/Slice_1Slice6gradients/dense_2/MatMul_grad/tuple/control_dependency&gradients/concat_4_grad/ConcatOffset:1 gradients/concat_4_grad/ShapeN:1*
Index0*
T0
r
(gradients/concat_4_grad/tuple/group_depsNoOp^gradients/concat_4_grad/Slice ^gradients/concat_4_grad/Slice_1
�
0gradients/concat_4_grad/tuple/control_dependencyIdentitygradients/concat_4_grad/Slice)^gradients/concat_4_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients/concat_4_grad/Slice
�
2gradients/concat_4_grad/tuple/control_dependency_1Identitygradients/concat_4_grad/Slice_1)^gradients/concat_4_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients/concat_4_grad/Slice_1
O
gradients/dense/Mul_grad/ShapeShapedense/BiasAdd*
T0*
out_type0
Q
 gradients/dense/Mul_grad/Shape_1Shapedense/Sigmoid*
T0*
out_type0
�
.gradients/dense/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/dense/Mul_grad/Shape gradients/dense/Mul_grad/Shape_1*
T0
s
gradients/dense/Mul_grad/MulMul6gradients/dense_1/MatMul_grad/tuple/control_dependencydense/Sigmoid*
T0
�
gradients/dense/Mul_grad/SumSumgradients/dense/Mul_grad/Mul.gradients/dense/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
 gradients/dense/Mul_grad/ReshapeReshapegradients/dense/Mul_grad/Sumgradients/dense/Mul_grad/Shape*
T0*
Tshape0
u
gradients/dense/Mul_grad/Mul_1Muldense/BiasAdd6gradients/dense_1/MatMul_grad/tuple/control_dependency*
T0
�
gradients/dense/Mul_grad/Sum_1Sumgradients/dense/Mul_grad/Mul_10gradients/dense/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
"gradients/dense/Mul_grad/Reshape_1Reshapegradients/dense/Mul_grad/Sum_1 gradients/dense/Mul_grad/Shape_1*
T0*
Tshape0
y
)gradients/dense/Mul_grad/tuple/group_depsNoOp!^gradients/dense/Mul_grad/Reshape#^gradients/dense/Mul_grad/Reshape_1
�
1gradients/dense/Mul_grad/tuple/control_dependencyIdentity gradients/dense/Mul_grad/Reshape*^gradients/dense/Mul_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients/dense/Mul_grad/Reshape
�
3gradients/dense/Mul_grad/tuple/control_dependency_1Identity"gradients/dense/Mul_grad/Reshape_1*^gradients/dense/Mul_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/dense/Mul_grad/Reshape_1
�
(gradients/dense/Sigmoid_grad/SigmoidGradSigmoidGraddense/Sigmoid3gradients/dense/Mul_grad/tuple/control_dependency_1*
T0
�
gradients/AddN_1AddN1gradients/dense/Mul_grad/tuple/control_dependency(gradients/dense/Sigmoid_grad/SigmoidGrad*
N*
T0*3
_class)
'%loc:@gradients/dense/Mul_grad/Reshape
i
(gradients/dense/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN_1*
T0*
data_formatNHWC
s
-gradients/dense/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN_1)^gradients/dense/BiasAdd_grad/BiasAddGrad
�
5gradients/dense/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN_1.^gradients/dense/BiasAdd_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients/dense/Mul_grad/Reshape
�
7gradients/dense/BiasAdd_grad/tuple/control_dependency_1Identity(gradients/dense/BiasAdd_grad/BiasAddGrad.^gradients/dense/BiasAdd_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/dense/BiasAdd_grad/BiasAddGrad
�
"gradients/dense/MatMul_grad/MatMulMatMul5gradients/dense/BiasAdd_grad/tuple/control_dependencydense/kernel/read*
T0*
transpose_a( *
transpose_b(
�
$gradients/dense/MatMul_grad/MatMul_1MatMulconcat_25gradients/dense/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
,gradients/dense/MatMul_grad/tuple/group_depsNoOp#^gradients/dense/MatMul_grad/MatMul%^gradients/dense/MatMul_grad/MatMul_1
�
4gradients/dense/MatMul_grad/tuple/control_dependencyIdentity"gradients/dense/MatMul_grad/MatMul-^gradients/dense/MatMul_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/dense/MatMul_grad/MatMul
�
6gradients/dense/MatMul_grad/tuple/control_dependency_1Identity$gradients/dense/MatMul_grad/MatMul_1-^gradients/dense/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense/MatMul_grad/MatMul_1
F
gradients/concat_2_grad/RankConst*
dtype0*
value	B :
]
gradients/concat_2_grad/modFloorModconcat_2/axisgradients/concat_2_grad/Rank*
T0
N
gradients/concat_2_grad/ShapeShapeconcat/concat*
T0*
out_type0
j
gradients/concat_2_grad/ShapeNShapeNconcat/concatconcat_1/concat*
N*
T0*
out_type0
�
$gradients/concat_2_grad/ConcatOffsetConcatOffsetgradients/concat_2_grad/modgradients/concat_2_grad/ShapeN gradients/concat_2_grad/ShapeN:1*
N
�
gradients/concat_2_grad/SliceSlice4gradients/dense/MatMul_grad/tuple/control_dependency$gradients/concat_2_grad/ConcatOffsetgradients/concat_2_grad/ShapeN*
Index0*
T0
�
gradients/concat_2_grad/Slice_1Slice4gradients/dense/MatMul_grad/tuple/control_dependency&gradients/concat_2_grad/ConcatOffset:1 gradients/concat_2_grad/ShapeN:1*
Index0*
T0
r
(gradients/concat_2_grad/tuple/group_depsNoOp^gradients/concat_2_grad/Slice ^gradients/concat_2_grad/Slice_1
�
0gradients/concat_2_grad/tuple/control_dependencyIdentitygradients/concat_2_grad/Slice)^gradients/concat_2_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients/concat_2_grad/Slice
�
2gradients/concat_2_grad/tuple/control_dependency_1Identitygradients/concat_2_grad/Slice_1)^gradients/concat_2_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients/concat_2_grad/Slice_1
�
gradients/AddN_2AddN0gradients/concat_4_grad/tuple/control_dependency0gradients/concat_2_grad/tuple/control_dependency*
N*
T0*0
_class&
$"loc:@gradients/concat_4_grad/Slice
�
gradients/AddN_3AddN;gradients/SquaredDifference_grad/tuple/control_dependency_12gradients/concat_2_grad/tuple/control_dependency_1*
N*
T0*7
_class-
+)loc:@gradients/SquaredDifference_grad/Neg
�
>gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/ShapeShape-curiosity_vector_obs_encoder/hidden_1/BiasAdd*
T0*
out_type0
�
@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Shape_1Shape-curiosity_vector_obs_encoder/hidden_1/Sigmoid*
T0*
out_type0
�
Ngradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs>gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Shape@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Shape_1*
T0
�
<gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/MulMulgradients/AddN_2-curiosity_vector_obs_encoder/hidden_1/Sigmoid*
T0
�
<gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/SumSum<gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/MulNgradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/ReshapeReshape<gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Sum>gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Shape*
T0*
Tshape0
�
>gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Mul_1Mul-curiosity_vector_obs_encoder/hidden_1/BiasAddgradients/AddN_2*
T0
�
>gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Sum_1Sum>gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Mul_1Pgradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Bgradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Reshape_1Reshape>gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Sum_1@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
�
Igradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/tuple/group_depsNoOpA^gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/ReshapeC^gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Reshape_1
�
Qgradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/tuple/control_dependencyIdentity@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/ReshapeJ^gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Reshape
�
Sgradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/tuple/control_dependency_1IdentityBgradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Reshape_1J^gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Reshape_1
�
@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/ShapeShape/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd*
T0*
out_type0
�
Bgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Shape_1Shape/curiosity_vector_obs_encoder_1/hidden_1/Sigmoid*
T0*
out_type0
�
Pgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/ShapeBgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Shape_1*
T0
�
>gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/MulMulgradients/AddN_3/curiosity_vector_obs_encoder_1/hidden_1/Sigmoid*
T0
�
>gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/SumSum>gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/MulPgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Bgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/ReshapeReshape>gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Sum@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Shape*
T0*
Tshape0
�
@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Mul_1Mul/curiosity_vector_obs_encoder_1/hidden_1/BiasAddgradients/AddN_3*
T0
�
@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Sum_1Sum@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Mul_1Rgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Dgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Reshape_1Reshape@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Sum_1Bgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
�
Kgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/tuple/group_depsNoOpC^gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/ReshapeE^gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Reshape_1
�
Sgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/tuple/control_dependencyIdentityBgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/ReshapeL^gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Reshape
�
Ugradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/tuple/control_dependency_1IdentityDgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Reshape_1L^gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/tuple/group_deps*
T0*W
_classM
KIloc:@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Reshape_1
�
Hgradients/curiosity_vector_obs_encoder/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad-curiosity_vector_obs_encoder/hidden_1/SigmoidSgradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
�
Jgradients/curiosity_vector_obs_encoder_1/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad/curiosity_vector_obs_encoder_1/hidden_1/SigmoidUgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
�
gradients/AddN_4AddNQgradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/tuple/control_dependencyHgradients/curiosity_vector_obs_encoder/hidden_1/Sigmoid_grad/SigmoidGrad*
N*
T0*S
_classI
GEloc:@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Reshape
�
Hgradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN_4*
T0*
data_formatNHWC
�
Mgradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN_4I^gradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/BiasAddGrad
�
Ugradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN_4N^gradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/curiosity_vector_obs_encoder/hidden_1/Mul_grad/Reshape
�
Wgradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityHgradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/BiasAddGradN^gradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*[
_classQ
OMloc:@gradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/BiasAddGrad
�
gradients/AddN_5AddNSgradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/tuple/control_dependencyJgradients/curiosity_vector_obs_encoder_1/hidden_1/Sigmoid_grad/SigmoidGrad*
N*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Reshape
�
Jgradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN_5*
T0*
data_formatNHWC
�
Ogradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN_5K^gradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/BiasAddGrad
�
Wgradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN_5P^gradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder_1/hidden_1/Mul_grad/Reshape
�
Ygradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityJgradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/BiasAddGradP^gradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*]
_classS
QOloc:@gradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/BiasAddGrad
�
Bgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMulMatMulUgradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency1curiosity_vector_obs_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Dgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMul_1MatMul)curiosity_vector_obs_encoder/hidden_0/MulUgradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Lgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/tuple/group_depsNoOpC^gradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMulE^gradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMul_1
�
Tgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/tuple/control_dependencyIdentityBgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMulM^gradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMul
�
Vgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1IdentityDgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMul_1M^gradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*W
_classM
KIloc:@gradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMul_1
�
Dgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/MatMulMatMulWgradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/tuple/control_dependency1curiosity_vector_obs_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Fgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/MatMul_1MatMul+curiosity_vector_obs_encoder_1/hidden_0/MulWgradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Ngradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/tuple/group_depsNoOpE^gradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/MatMulG^gradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/MatMul_1
�
Vgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/tuple/control_dependencyIdentityDgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/MatMulO^gradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/tuple/group_deps*
T0*W
_classM
KIloc:@gradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/MatMul
�
Xgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/tuple/control_dependency_1IdentityFgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/MatMul_1O^gradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/tuple/group_deps*
T0*Y
_classO
MKloc:@gradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/MatMul_1
�
gradients/AddN_6AddNWgradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1Ygradients/curiosity_vector_obs_encoder_1/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
N*
T0*[
_classQ
OMloc:@gradients/curiosity_vector_obs_encoder/hidden_1/BiasAdd_grad/BiasAddGrad
�
>gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/ShapeShape-curiosity_vector_obs_encoder/hidden_0/BiasAdd*
T0*
out_type0
�
@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Shape_1Shape-curiosity_vector_obs_encoder/hidden_0/Sigmoid*
T0*
out_type0
�
Ngradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs>gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Shape@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Shape_1*
T0
�
<gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/MulMulTgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/tuple/control_dependency-curiosity_vector_obs_encoder/hidden_0/Sigmoid*
T0
�
<gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/SumSum<gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/MulNgradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/ReshapeReshape<gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Sum>gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Shape*
T0*
Tshape0
�
>gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Mul_1Mul-curiosity_vector_obs_encoder/hidden_0/BiasAddTgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/tuple/control_dependency*
T0
�
>gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Sum_1Sum>gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Mul_1Pgradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Bgradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Reshape_1Reshape>gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Sum_1@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
�
Igradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/tuple/group_depsNoOpA^gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/ReshapeC^gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Reshape_1
�
Qgradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/tuple/control_dependencyIdentity@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/ReshapeJ^gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Reshape
�
Sgradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/tuple/control_dependency_1IdentityBgradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Reshape_1J^gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Reshape_1
�
@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/ShapeShape/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd*
T0*
out_type0
�
Bgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Shape_1Shape/curiosity_vector_obs_encoder_1/hidden_0/Sigmoid*
T0*
out_type0
�
Pgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/ShapeBgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Shape_1*
T0
�
>gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/MulMulVgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/tuple/control_dependency/curiosity_vector_obs_encoder_1/hidden_0/Sigmoid*
T0
�
>gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/SumSum>gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/MulPgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Bgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/ReshapeReshape>gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Sum@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Shape*
T0*
Tshape0
�
@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Mul_1Mul/curiosity_vector_obs_encoder_1/hidden_0/BiasAddVgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/tuple/control_dependency*
T0
�
@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Sum_1Sum@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Mul_1Rgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Dgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Reshape_1Reshape@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Sum_1Bgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
�
Kgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/tuple/group_depsNoOpC^gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/ReshapeE^gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Reshape_1
�
Sgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/tuple/control_dependencyIdentityBgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/ReshapeL^gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Reshape
�
Ugradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/tuple/control_dependency_1IdentityDgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Reshape_1L^gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/tuple/group_deps*
T0*W
_classM
KIloc:@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Reshape_1
�
gradients/AddN_7AddNVgradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1Xgradients/curiosity_vector_obs_encoder_1/hidden_1/MatMul_grad/tuple/control_dependency_1*
N*
T0*W
_classM
KIloc:@gradients/curiosity_vector_obs_encoder/hidden_1/MatMul_grad/MatMul_1
�
Hgradients/curiosity_vector_obs_encoder/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad-curiosity_vector_obs_encoder/hidden_0/SigmoidSgradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
�
Jgradients/curiosity_vector_obs_encoder_1/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad/curiosity_vector_obs_encoder_1/hidden_0/SigmoidUgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
�
gradients/AddN_8AddNQgradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/tuple/control_dependencyHgradients/curiosity_vector_obs_encoder/hidden_0/Sigmoid_grad/SigmoidGrad*
N*
T0*S
_classI
GEloc:@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Reshape
�
Hgradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN_8*
T0*
data_formatNHWC
�
Mgradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN_8I^gradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/BiasAddGrad
�
Ugradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN_8N^gradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/curiosity_vector_obs_encoder/hidden_0/Mul_grad/Reshape
�
Wgradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityHgradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/BiasAddGradN^gradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*[
_classQ
OMloc:@gradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/BiasAddGrad
�
gradients/AddN_9AddNSgradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/tuple/control_dependencyJgradients/curiosity_vector_obs_encoder_1/hidden_0/Sigmoid_grad/SigmoidGrad*
N*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Reshape
�
Jgradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN_9*
T0*
data_formatNHWC
�
Ogradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN_9K^gradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/BiasAddGrad
�
Wgradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN_9P^gradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder_1/hidden_0/Mul_grad/Reshape
�
Ygradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityJgradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/BiasAddGradP^gradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*]
_classS
QOloc:@gradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/BiasAddGrad
�
Bgradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMulMatMulUgradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency1curiosity_vector_obs_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Dgradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationUgradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Lgradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/tuple/group_depsNoOpC^gradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMulE^gradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMul_1
�
Tgradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/tuple/control_dependencyIdentityBgradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMulM^gradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMul
�
Vgradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1IdentityDgradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMul_1M^gradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*W
_classM
KIloc:@gradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMul_1
�
Dgradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/MatMulMatMulWgradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/tuple/control_dependency1curiosity_vector_obs_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Fgradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/MatMul_1MatMul!curiosity_next_vector_observationWgradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Ngradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/tuple/group_depsNoOpE^gradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/MatMulG^gradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/MatMul_1
�
Vgradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/tuple/control_dependencyIdentityDgradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/MatMulO^gradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/tuple/group_deps*
T0*W
_classM
KIloc:@gradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/MatMul
�
Xgradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/tuple/control_dependency_1IdentityFgradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/MatMul_1O^gradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/tuple/group_deps*
T0*Y
_classO
MKloc:@gradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/MatMul_1
�
gradients/AddN_10AddNWgradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1Ygradients/curiosity_vector_obs_encoder_1/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
N*
T0*[
_classQ
OMloc:@gradients/curiosity_vector_obs_encoder/hidden_0/BiasAdd_grad/BiasAddGrad
�
gradients/AddN_11AddNVgradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1Xgradients/curiosity_vector_obs_encoder_1/hidden_0/MatMul_grad/tuple/control_dependency_1*
N*
T0*W
_classM
KIloc:@gradients/curiosity_vector_obs_encoder/hidden_0/MatMul_grad/MatMul_1
�
beta1_power/initial_valueConst*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
dtype0*
valueB
 *fff?
�
beta1_power
VariableV2*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
	container *
dtype0*
shape: *
shared_name 
�
beta1_power/AssignAssignbeta1_powerbeta1_power/initial_value*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
q
beta1_power/readIdentitybeta1_power*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias
�
beta2_power/initial_valueConst*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
dtype0*
valueB
 *w�?
�
beta2_power
VariableV2*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
	container *
dtype0*
shape: *
shared_name 
�
beta2_power/AssignAssignbeta2_powerbeta2_power/initial_value*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
q
beta2_power/readIdentitybeta2_power*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias
�
Scuriosity_vector_obs_encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
dtype0*
valueB"@      
�
Icuriosity_vector_obs_encoder/hidden_0/kernel/Adam/Initializer/zeros/ConstConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Ccuriosity_vector_obs_encoder/hidden_0/kernel/Adam/Initializer/zerosFillScuriosity_vector_obs_encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorIcuriosity_vector_obs_encoder/hidden_0/kernel/Adam/Initializer/zeros/Const*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*

index_type0
�
1curiosity_vector_obs_encoder/hidden_0/kernel/Adam
VariableV2*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
	container *
dtype0*
shape:	@�*
shared_name 
�
8curiosity_vector_obs_encoder/hidden_0/kernel/Adam/AssignAssign1curiosity_vector_obs_encoder/hidden_0/kernel/AdamCcuriosity_vector_obs_encoder/hidden_0/kernel/Adam/Initializer/zeros*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
6curiosity_vector_obs_encoder/hidden_0/kernel/Adam/readIdentity1curiosity_vector_obs_encoder/hidden_0/kernel/Adam*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel
�
Ucuriosity_vector_obs_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
dtype0*
valueB"@      
�
Kcuriosity_vector_obs_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/ConstConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Ecuriosity_vector_obs_encoder/hidden_0/kernel/Adam_1/Initializer/zerosFillUcuriosity_vector_obs_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorKcuriosity_vector_obs_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/Const*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*

index_type0
�
3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1
VariableV2*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
	container *
dtype0*
shape:	@�*
shared_name 
�
:curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1/AssignAssign3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1Ecuriosity_vector_obs_encoder/hidden_0/kernel/Adam_1/Initializer/zeros*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
8curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1/readIdentity3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel
�
Acuriosity_vector_obs_encoder/hidden_0/bias/Adam/Initializer/zerosConst*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
dtype0*
valueB�*    
�
/curiosity_vector_obs_encoder/hidden_0/bias/Adam
VariableV2*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
	container *
dtype0*
shape:�*
shared_name 
�
6curiosity_vector_obs_encoder/hidden_0/bias/Adam/AssignAssign/curiosity_vector_obs_encoder/hidden_0/bias/AdamAcuriosity_vector_obs_encoder/hidden_0/bias/Adam/Initializer/zeros*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
4curiosity_vector_obs_encoder/hidden_0/bias/Adam/readIdentity/curiosity_vector_obs_encoder/hidden_0/bias/Adam*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias
�
Ccuriosity_vector_obs_encoder/hidden_0/bias/Adam_1/Initializer/zerosConst*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
dtype0*
valueB�*    
�
1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1
VariableV2*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
	container *
dtype0*
shape:�*
shared_name 
�
8curiosity_vector_obs_encoder/hidden_0/bias/Adam_1/AssignAssign1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1Ccuriosity_vector_obs_encoder/hidden_0/bias/Adam_1/Initializer/zeros*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
6curiosity_vector_obs_encoder/hidden_0/bias/Adam_1/readIdentity1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias
�
Scuriosity_vector_obs_encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
dtype0*
valueB"      
�
Icuriosity_vector_obs_encoder/hidden_1/kernel/Adam/Initializer/zeros/ConstConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Ccuriosity_vector_obs_encoder/hidden_1/kernel/Adam/Initializer/zerosFillScuriosity_vector_obs_encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorIcuriosity_vector_obs_encoder/hidden_1/kernel/Adam/Initializer/zeros/Const*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*

index_type0
�
1curiosity_vector_obs_encoder/hidden_1/kernel/Adam
VariableV2*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
8curiosity_vector_obs_encoder/hidden_1/kernel/Adam/AssignAssign1curiosity_vector_obs_encoder/hidden_1/kernel/AdamCcuriosity_vector_obs_encoder/hidden_1/kernel/Adam/Initializer/zeros*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
6curiosity_vector_obs_encoder/hidden_1/kernel/Adam/readIdentity1curiosity_vector_obs_encoder/hidden_1/kernel/Adam*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel
�
Ucuriosity_vector_obs_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
dtype0*
valueB"      
�
Kcuriosity_vector_obs_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/ConstConst*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Ecuriosity_vector_obs_encoder/hidden_1/kernel/Adam_1/Initializer/zerosFillUcuriosity_vector_obs_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorKcuriosity_vector_obs_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/Const*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*

index_type0
�
3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1
VariableV2*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
:curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1/AssignAssign3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1Ecuriosity_vector_obs_encoder/hidden_1/kernel/Adam_1/Initializer/zeros*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
8curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1/readIdentity3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel
�
Acuriosity_vector_obs_encoder/hidden_1/bias/Adam/Initializer/zerosConst*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
dtype0*
valueB�*    
�
/curiosity_vector_obs_encoder/hidden_1/bias/Adam
VariableV2*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
	container *
dtype0*
shape:�*
shared_name 
�
6curiosity_vector_obs_encoder/hidden_1/bias/Adam/AssignAssign/curiosity_vector_obs_encoder/hidden_1/bias/AdamAcuriosity_vector_obs_encoder/hidden_1/bias/Adam/Initializer/zeros*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
4curiosity_vector_obs_encoder/hidden_1/bias/Adam/readIdentity/curiosity_vector_obs_encoder/hidden_1/bias/Adam*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias
�
Ccuriosity_vector_obs_encoder/hidden_1/bias/Adam_1/Initializer/zerosConst*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
dtype0*
valueB�*    
�
1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1
VariableV2*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
	container *
dtype0*
shape:�*
shared_name 
�
8curiosity_vector_obs_encoder/hidden_1/bias/Adam_1/AssignAssign1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1Ccuriosity_vector_obs_encoder/hidden_1/bias/Adam_1/Initializer/zeros*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
6curiosity_vector_obs_encoder/hidden_1/bias/Adam_1/readIdentity1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias
�
3dense/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
_class
loc:@dense/kernel*
dtype0*
valueB"      
w
)dense/kernel/Adam/Initializer/zeros/ConstConst*
_class
loc:@dense/kernel*
dtype0*
valueB
 *    
�
#dense/kernel/Adam/Initializer/zerosFill3dense/kernel/Adam/Initializer/zeros/shape_as_tensor)dense/kernel/Adam/Initializer/zeros/Const*
T0*
_class
loc:@dense/kernel*

index_type0
�
dense/kernel/Adam
VariableV2*
_class
loc:@dense/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense/kernel/Adam/AssignAssigndense/kernel/Adam#dense/kernel/Adam/Initializer/zeros*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
_
dense/kernel/Adam/readIdentitydense/kernel/Adam*
T0*
_class
loc:@dense/kernel
�
5dense/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
_class
loc:@dense/kernel*
dtype0*
valueB"      
y
+dense/kernel/Adam_1/Initializer/zeros/ConstConst*
_class
loc:@dense/kernel*
dtype0*
valueB
 *    
�
%dense/kernel/Adam_1/Initializer/zerosFill5dense/kernel/Adam_1/Initializer/zeros/shape_as_tensor+dense/kernel/Adam_1/Initializer/zeros/Const*
T0*
_class
loc:@dense/kernel*

index_type0
�
dense/kernel/Adam_1
VariableV2*
_class
loc:@dense/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense/kernel/Adam_1/AssignAssigndense/kernel/Adam_1%dense/kernel/Adam_1/Initializer/zeros*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
c
dense/kernel/Adam_1/readIdentitydense/kernel/Adam_1*
T0*
_class
loc:@dense/kernel
r
!dense/bias/Adam/Initializer/zerosConst*
_class
loc:@dense/bias*
dtype0*
valueB�*    

dense/bias/Adam
VariableV2*
_class
loc:@dense/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense/bias/Adam/AssignAssigndense/bias/Adam!dense/bias/Adam/Initializer/zeros*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
Y
dense/bias/Adam/readIdentitydense/bias/Adam*
T0*
_class
loc:@dense/bias
t
#dense/bias/Adam_1/Initializer/zerosConst*
_class
loc:@dense/bias*
dtype0*
valueB�*    
�
dense/bias/Adam_1
VariableV2*
_class
loc:@dense/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense/bias/Adam_1/AssignAssigndense/bias/Adam_1#dense/bias/Adam_1/Initializer/zeros*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
]
dense/bias/Adam_1/readIdentitydense/bias/Adam_1*
T0*
_class
loc:@dense/bias
�
5dense_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*!
_class
loc:@dense_1/kernel*
dtype0*
valueB"   @   
{
+dense_1/kernel/Adam/Initializer/zeros/ConstConst*!
_class
loc:@dense_1/kernel*
dtype0*
valueB
 *    
�
%dense_1/kernel/Adam/Initializer/zerosFill5dense_1/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_1/kernel/Adam/Initializer/zeros/Const*
T0*!
_class
loc:@dense_1/kernel*

index_type0
�
dense_1/kernel/Adam
VariableV2*!
_class
loc:@dense_1/kernel*
	container *
dtype0*
shape:	�@*
shared_name 
�
dense_1/kernel/Adam/AssignAssigndense_1/kernel/Adam%dense_1/kernel/Adam/Initializer/zeros*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
e
dense_1/kernel/Adam/readIdentitydense_1/kernel/Adam*
T0*!
_class
loc:@dense_1/kernel
�
7dense_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*!
_class
loc:@dense_1/kernel*
dtype0*
valueB"   @   
}
-dense_1/kernel/Adam_1/Initializer/zeros/ConstConst*!
_class
loc:@dense_1/kernel*
dtype0*
valueB
 *    
�
'dense_1/kernel/Adam_1/Initializer/zerosFill7dense_1/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_1/kernel/Adam_1/Initializer/zeros/Const*
T0*!
_class
loc:@dense_1/kernel*

index_type0
�
dense_1/kernel/Adam_1
VariableV2*!
_class
loc:@dense_1/kernel*
	container *
dtype0*
shape:	�@*
shared_name 
�
dense_1/kernel/Adam_1/AssignAssigndense_1/kernel/Adam_1'dense_1/kernel/Adam_1/Initializer/zeros*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
i
dense_1/kernel/Adam_1/readIdentitydense_1/kernel/Adam_1*
T0*!
_class
loc:@dense_1/kernel
u
#dense_1/bias/Adam/Initializer/zerosConst*
_class
loc:@dense_1/bias*
dtype0*
valueB@*    
�
dense_1/bias/Adam
VariableV2*
_class
loc:@dense_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
dense_1/bias/Adam/AssignAssigndense_1/bias/Adam#dense_1/bias/Adam/Initializer/zeros*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
_
dense_1/bias/Adam/readIdentitydense_1/bias/Adam*
T0*
_class
loc:@dense_1/bias
w
%dense_1/bias/Adam_1/Initializer/zerosConst*
_class
loc:@dense_1/bias*
dtype0*
valueB@*    
�
dense_1/bias/Adam_1
VariableV2*
_class
loc:@dense_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
dense_1/bias/Adam_1/AssignAssigndense_1/bias/Adam_1%dense_1/bias/Adam_1/Initializer/zeros*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
c
dense_1/bias/Adam_1/readIdentitydense_1/bias/Adam_1*
T0*
_class
loc:@dense_1/bias
�
5dense_2/kernel/Adam/Initializer/zeros/shape_as_tensorConst*!
_class
loc:@dense_2/kernel*
dtype0*
valueB"@     
{
+dense_2/kernel/Adam/Initializer/zeros/ConstConst*!
_class
loc:@dense_2/kernel*
dtype0*
valueB
 *    
�
%dense_2/kernel/Adam/Initializer/zerosFill5dense_2/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_2/kernel/Adam/Initializer/zeros/Const*
T0*!
_class
loc:@dense_2/kernel*

index_type0
�
dense_2/kernel/Adam
VariableV2*!
_class
loc:@dense_2/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense_2/kernel/Adam/AssignAssigndense_2/kernel/Adam%dense_2/kernel/Adam/Initializer/zeros*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
e
dense_2/kernel/Adam/readIdentitydense_2/kernel/Adam*
T0*!
_class
loc:@dense_2/kernel
�
7dense_2/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*!
_class
loc:@dense_2/kernel*
dtype0*
valueB"@     
}
-dense_2/kernel/Adam_1/Initializer/zeros/ConstConst*!
_class
loc:@dense_2/kernel*
dtype0*
valueB
 *    
�
'dense_2/kernel/Adam_1/Initializer/zerosFill7dense_2/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_2/kernel/Adam_1/Initializer/zeros/Const*
T0*!
_class
loc:@dense_2/kernel*

index_type0
�
dense_2/kernel/Adam_1
VariableV2*!
_class
loc:@dense_2/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense_2/kernel/Adam_1/AssignAssigndense_2/kernel/Adam_1'dense_2/kernel/Adam_1/Initializer/zeros*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
i
dense_2/kernel/Adam_1/readIdentitydense_2/kernel/Adam_1*
T0*!
_class
loc:@dense_2/kernel
v
#dense_2/bias/Adam/Initializer/zerosConst*
_class
loc:@dense_2/bias*
dtype0*
valueB�*    
�
dense_2/bias/Adam
VariableV2*
_class
loc:@dense_2/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense_2/bias/Adam/AssignAssigndense_2/bias/Adam#dense_2/bias/Adam/Initializer/zeros*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
_
dense_2/bias/Adam/readIdentitydense_2/bias/Adam*
T0*
_class
loc:@dense_2/bias
x
%dense_2/bias/Adam_1/Initializer/zerosConst*
_class
loc:@dense_2/bias*
dtype0*
valueB�*    
�
dense_2/bias/Adam_1
VariableV2*
_class
loc:@dense_2/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense_2/bias/Adam_1/AssignAssigndense_2/bias/Adam_1%dense_2/bias/Adam_1/Initializer/zeros*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
c
dense_2/bias/Adam_1/readIdentitydense_2/bias/Adam_1*
T0*
_class
loc:@dense_2/bias
�
5dense_3/kernel/Adam/Initializer/zeros/shape_as_tensorConst*!
_class
loc:@dense_3/kernel*
dtype0*
valueB"      
{
+dense_3/kernel/Adam/Initializer/zeros/ConstConst*!
_class
loc:@dense_3/kernel*
dtype0*
valueB
 *    
�
%dense_3/kernel/Adam/Initializer/zerosFill5dense_3/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_3/kernel/Adam/Initializer/zeros/Const*
T0*!
_class
loc:@dense_3/kernel*

index_type0
�
dense_3/kernel/Adam
VariableV2*!
_class
loc:@dense_3/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense_3/kernel/Adam/AssignAssigndense_3/kernel/Adam%dense_3/kernel/Adam/Initializer/zeros*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
e
dense_3/kernel/Adam/readIdentitydense_3/kernel/Adam*
T0*!
_class
loc:@dense_3/kernel
�
7dense_3/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*!
_class
loc:@dense_3/kernel*
dtype0*
valueB"      
}
-dense_3/kernel/Adam_1/Initializer/zeros/ConstConst*!
_class
loc:@dense_3/kernel*
dtype0*
valueB
 *    
�
'dense_3/kernel/Adam_1/Initializer/zerosFill7dense_3/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_3/kernel/Adam_1/Initializer/zeros/Const*
T0*!
_class
loc:@dense_3/kernel*

index_type0
�
dense_3/kernel/Adam_1
VariableV2*!
_class
loc:@dense_3/kernel*
	container *
dtype0*
shape:
��*
shared_name 
�
dense_3/kernel/Adam_1/AssignAssigndense_3/kernel/Adam_1'dense_3/kernel/Adam_1/Initializer/zeros*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
i
dense_3/kernel/Adam_1/readIdentitydense_3/kernel/Adam_1*
T0*!
_class
loc:@dense_3/kernel
v
#dense_3/bias/Adam/Initializer/zerosConst*
_class
loc:@dense_3/bias*
dtype0*
valueB�*    
�
dense_3/bias/Adam
VariableV2*
_class
loc:@dense_3/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense_3/bias/Adam/AssignAssigndense_3/bias/Adam#dense_3/bias/Adam/Initializer/zeros*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
_
dense_3/bias/Adam/readIdentitydense_3/bias/Adam*
T0*
_class
loc:@dense_3/bias
x
%dense_3/bias/Adam_1/Initializer/zerosConst*
_class
loc:@dense_3/bias*
dtype0*
valueB�*    
�
dense_3/bias/Adam_1
VariableV2*
_class
loc:@dense_3/bias*
	container *
dtype0*
shape:�*
shared_name 
�
dense_3/bias/Adam_1/AssignAssigndense_3/bias/Adam_1%dense_3/bias/Adam_1/Initializer/zeros*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
c
dense_3/bias/Adam_1/readIdentitydense_3/bias/Adam_1*
T0*
_class
loc:@dense_3/bias
?
Adam/learning_rateConst*
dtype0*
valueB
 *RI�9
7

Adam/beta1Const*
dtype0*
valueB
 *fff?
7

Adam/beta2Const*
dtype0*
valueB
 *w�?
9
Adam/epsilonConst*
dtype0*
valueB
 *w�+2
�
BAdam/update_curiosity_vector_obs_encoder/hidden_0/kernel/ApplyAdam	ApplyAdam,curiosity_vector_obs_encoder/hidden_0/kernel1curiosity_vector_obs_encoder/hidden_0/kernel/Adam3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN_11*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking( *
use_nesterov( 
�
@Adam/update_curiosity_vector_obs_encoder/hidden_0/bias/ApplyAdam	ApplyAdam*curiosity_vector_obs_encoder/hidden_0/bias/curiosity_vector_obs_encoder/hidden_0/bias/Adam1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN_10*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking( *
use_nesterov( 
�
BAdam/update_curiosity_vector_obs_encoder/hidden_1/kernel/ApplyAdam	ApplyAdam,curiosity_vector_obs_encoder/hidden_1/kernel1curiosity_vector_obs_encoder/hidden_1/kernel/Adam3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN_7*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking( *
use_nesterov( 
�
@Adam/update_curiosity_vector_obs_encoder/hidden_1/bias/ApplyAdam	ApplyAdam*curiosity_vector_obs_encoder/hidden_1/bias/curiosity_vector_obs_encoder/hidden_1/bias/Adam1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN_6*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking( *
use_nesterov( 
�
"Adam/update_dense/kernel/ApplyAdam	ApplyAdamdense/kerneldense/kernel/Adamdense/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon6gradients/dense/MatMul_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense/kernel*
use_locking( *
use_nesterov( 
�
 Adam/update_dense/bias/ApplyAdam	ApplyAdam
dense/biasdense/bias/Adamdense/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon7gradients/dense/BiasAdd_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense/bias*
use_locking( *
use_nesterov( 
�
$Adam/update_dense_1/kernel/ApplyAdam	ApplyAdamdense_1/kerneldense_1/kernel/Adamdense_1/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_1/MatMul_grad/tuple/control_dependency_1*
T0*!
_class
loc:@dense_1/kernel*
use_locking( *
use_nesterov( 
�
"Adam/update_dense_1/bias/ApplyAdam	ApplyAdamdense_1/biasdense_1/bias/Adamdense_1/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_1/BiasAdd_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense_1/bias*
use_locking( *
use_nesterov( 
�
$Adam/update_dense_2/kernel/ApplyAdam	ApplyAdamdense_2/kerneldense_2/kernel/Adamdense_2/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_2/MatMul_grad/tuple/control_dependency_1*
T0*!
_class
loc:@dense_2/kernel*
use_locking( *
use_nesterov( 
�
"Adam/update_dense_2/bias/ApplyAdam	ApplyAdamdense_2/biasdense_2/bias/Adamdense_2/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_2/BiasAdd_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense_2/bias*
use_locking( *
use_nesterov( 
�
$Adam/update_dense_3/kernel/ApplyAdam	ApplyAdamdense_3/kerneldense_3/kernel/Adamdense_3/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_3/MatMul_grad/tuple/control_dependency_1*
T0*!
_class
loc:@dense_3/kernel*
use_locking( *
use_nesterov( 
�
"Adam/update_dense_3/bias/ApplyAdam	ApplyAdamdense_3/biasdense_3/bias/Adamdense_3/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_3/BiasAdd_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense_3/bias*
use_locking( *
use_nesterov( 
�
Adam/mulMulbeta1_power/read
Adam/beta1A^Adam/update_curiosity_vector_obs_encoder/hidden_0/bias/ApplyAdamC^Adam/update_curiosity_vector_obs_encoder/hidden_0/kernel/ApplyAdamA^Adam/update_curiosity_vector_obs_encoder/hidden_1/bias/ApplyAdamC^Adam/update_curiosity_vector_obs_encoder/hidden_1/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam#^Adam/update_dense/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias
�
Adam/AssignAssignbeta1_powerAdam/mul*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking( *
validate_shape(
�

Adam/mul_1Mulbeta2_power/read
Adam/beta2A^Adam/update_curiosity_vector_obs_encoder/hidden_0/bias/ApplyAdamC^Adam/update_curiosity_vector_obs_encoder/hidden_0/kernel/ApplyAdamA^Adam/update_curiosity_vector_obs_encoder/hidden_1/bias/ApplyAdamC^Adam/update_curiosity_vector_obs_encoder/hidden_1/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam#^Adam/update_dense/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias
�
Adam/Assign_1Assignbeta2_power
Adam/mul_1*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking( *
validate_shape(
�
AdamNoOp^Adam/Assign^Adam/Assign_1A^Adam/update_curiosity_vector_obs_encoder/hidden_0/bias/ApplyAdamC^Adam/update_curiosity_vector_obs_encoder/hidden_0/kernel/ApplyAdamA^Adam/update_curiosity_vector_obs_encoder/hidden_1/bias/ApplyAdamC^Adam/update_curiosity_vector_obs_encoder/hidden_1/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam#^Adam/update_dense/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam
C
Variable/initial_valueConst*
dtype0*
valueB
 *  �?
T
Variable
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
Variable/AssignAssignVariableVariable/initial_value*
T0*
_class
loc:@Variable*
use_locking(*
validate_shape(
I
Variable/readIdentityVariable*
T0*
_class
loc:@Variable
E
Variable_1/initial_valueConst*
dtype0*
valueB
 *  �?
V

Variable_1
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
Variable_1/AssignAssign
Variable_1Variable_1/initial_value*
T0*
_class
loc:@Variable_1*
use_locking(*
validate_shape(
O
Variable_1/readIdentity
Variable_1*
T0*
_class
loc:@Variable_1
;
Assign_1/valueConst*
dtype0*
valueB
 *    
{
Assign_1AssignVariableAssign_1/value*
T0*
_class
loc:@Variable*
use_locking( *
validate_shape(
;
Assign_2/valueConst*
dtype0*
valueB
 *    

Assign_2Assign
Variable_1Assign_2/value*
T0*
_class
loc:@Variable_1*
use_locking( *
validate_shape(
>
sac_sequence_lengthPlaceholder*
dtype0*
shape:
�
Gcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Fcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Hcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
valueB
 *6�>
�
Qcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalGcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
seed�*
seed2
�
Ecritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mulMulQcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalHcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
�
Acritic/value/encoder/hidden_0/kernel/Initializer/truncated_normalAddEcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mulFcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
�
$critic/value/encoder/hidden_0/kernel
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
+critic/value/encoder/hidden_0/kernel/AssignAssign$critic/value/encoder/hidden_0/kernelAcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
)critic/value/encoder/hidden_0/kernel/readIdentity$critic/value/encoder/hidden_0/kernel*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
�
4critic/value/encoder/hidden_0/bias/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
dtype0*
valueB@*    
�
"critic/value/encoder/hidden_0/bias
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
)critic/value/encoder/hidden_0/bias/AssignAssign"critic/value/encoder/hidden_0/bias4critic/value/encoder/hidden_0/bias/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
'critic/value/encoder/hidden_0/bias/readIdentity"critic/value/encoder/hidden_0/bias*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias
�
$critic/value/encoder/hidden_0/MatMulMatMulvector_observation)critic/value/encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
%critic/value/encoder/hidden_0/BiasAddBiasAdd$critic/value/encoder/hidden_0/MatMul'critic/value/encoder/hidden_0/bias/read*
T0*
data_formatNHWC
`
%critic/value/encoder/hidden_0/SigmoidSigmoid%critic/value/encoder/hidden_0/BiasAdd*
T0

!critic/value/encoder/hidden_0/MulMul%critic/value/encoder/hidden_0/BiasAdd%critic/value/encoder/hidden_0/Sigmoid*
T0
�
Gcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Fcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Hcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
valueB
 *6�>
�
Qcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalGcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
seed�*
seed2
�
Ecritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mulMulQcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalHcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
�
Acritic/value/encoder/hidden_1/kernel/Initializer/truncated_normalAddEcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mulFcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
�
$critic/value/encoder/hidden_1/kernel
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
+critic/value/encoder/hidden_1/kernel/AssignAssign$critic/value/encoder/hidden_1/kernelAcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
)critic/value/encoder/hidden_1/kernel/readIdentity$critic/value/encoder/hidden_1/kernel*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
�
4critic/value/encoder/hidden_1/bias/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
dtype0*
valueB@*    
�
"critic/value/encoder/hidden_1/bias
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
)critic/value/encoder/hidden_1/bias/AssignAssign"critic/value/encoder/hidden_1/bias4critic/value/encoder/hidden_1/bias/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
'critic/value/encoder/hidden_1/bias/readIdentity"critic/value/encoder/hidden_1/bias*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias
�
$critic/value/encoder/hidden_1/MatMulMatMul!critic/value/encoder/hidden_0/Mul)critic/value/encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
%critic/value/encoder/hidden_1/BiasAddBiasAdd$critic/value/encoder/hidden_1/MatMul'critic/value/encoder/hidden_1/bias/read*
T0*
data_formatNHWC
`
%critic/value/encoder/hidden_1/SigmoidSigmoid%critic/value/encoder/hidden_1/BiasAdd*
T0

!critic/value/encoder/hidden_1/MulMul%critic/value/encoder/hidden_1/BiasAdd%critic/value/encoder/hidden_1/Sigmoid*
T0
�
Gcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/shapeConst*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Fcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/meanConst*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Hcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/stddevConst*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
dtype0*
valueB
 *6�>
�
Qcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalGcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/shape*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
dtype0*
seed�*
seed2
�
Ecritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/mulMulQcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalHcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/stddev*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel
�
Acritic/value/encoder/hidden_2/kernel/Initializer/truncated_normalAddEcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/mulFcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/mean*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel
�
$critic/value/encoder/hidden_2/kernel
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
+critic/value/encoder/hidden_2/kernel/AssignAssign$critic/value/encoder/hidden_2/kernelAcritic/value/encoder/hidden_2/kernel/Initializer/truncated_normal*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
)critic/value/encoder/hidden_2/kernel/readIdentity$critic/value/encoder/hidden_2/kernel*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel
�
4critic/value/encoder/hidden_2/bias/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
dtype0*
valueB@*    
�
"critic/value/encoder/hidden_2/bias
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
)critic/value/encoder/hidden_2/bias/AssignAssign"critic/value/encoder/hidden_2/bias4critic/value/encoder/hidden_2/bias/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
'critic/value/encoder/hidden_2/bias/readIdentity"critic/value/encoder/hidden_2/bias*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias
�
$critic/value/encoder/hidden_2/MatMulMatMul!critic/value/encoder/hidden_1/Mul)critic/value/encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
%critic/value/encoder/hidden_2/BiasAddBiasAdd$critic/value/encoder/hidden_2/MatMul'critic/value/encoder/hidden_2/bias/read*
T0*
data_formatNHWC
`
%critic/value/encoder/hidden_2/SigmoidSigmoid%critic/value/encoder/hidden_2/BiasAdd*
T0

!critic/value/encoder/hidden_2/MulMul%critic/value/encoder/hidden_2/BiasAdd%critic/value/encoder/hidden_2/Sigmoid*
T0
�
Gcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/shapeConst*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Fcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/meanConst*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Hcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/stddevConst*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
dtype0*
valueB
 *6�>
�
Qcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalGcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/shape*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
dtype0*
seed�*
seed2
�
Ecritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/mulMulQcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalHcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/stddev*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel
�
Acritic/value/encoder/hidden_3/kernel/Initializer/truncated_normalAddEcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/mulFcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/mean*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel
�
$critic/value/encoder/hidden_3/kernel
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
+critic/value/encoder/hidden_3/kernel/AssignAssign$critic/value/encoder/hidden_3/kernelAcritic/value/encoder/hidden_3/kernel/Initializer/truncated_normal*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
)critic/value/encoder/hidden_3/kernel/readIdentity$critic/value/encoder/hidden_3/kernel*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel
�
4critic/value/encoder/hidden_3/bias/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
dtype0*
valueB@*    
�
"critic/value/encoder/hidden_3/bias
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
)critic/value/encoder/hidden_3/bias/AssignAssign"critic/value/encoder/hidden_3/bias4critic/value/encoder/hidden_3/bias/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
'critic/value/encoder/hidden_3/bias/readIdentity"critic/value/encoder/hidden_3/bias*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias
�
$critic/value/encoder/hidden_3/MatMulMatMul!critic/value/encoder/hidden_2/Mul)critic/value/encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
%critic/value/encoder/hidden_3/BiasAddBiasAdd$critic/value/encoder/hidden_3/MatMul'critic/value/encoder/hidden_3/bias/read*
T0*
data_formatNHWC
`
%critic/value/encoder/hidden_3/SigmoidSigmoid%critic/value/encoder/hidden_3/BiasAdd*
T0

!critic/value/encoder/hidden_3/MulMul%critic/value/encoder/hidden_3/BiasAdd%critic/value/encoder/hidden_3/Sigmoid*
T0
�
Dcritic/value/extrinsic_value/kernel/Initializer/random_uniform/shapeConst*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
valueB"@      
�
Bcritic/value/extrinsic_value/kernel/Initializer/random_uniform/minConst*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
valueB
 *����
�
Bcritic/value/extrinsic_value/kernel/Initializer/random_uniform/maxConst*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
valueB
 *���>
�
Lcritic/value/extrinsic_value/kernel/Initializer/random_uniform/RandomUniformRandomUniformDcritic/value/extrinsic_value/kernel/Initializer/random_uniform/shape*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
seed�*
seed2
�
Bcritic/value/extrinsic_value/kernel/Initializer/random_uniform/subSubBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/maxBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel
�
Bcritic/value/extrinsic_value/kernel/Initializer/random_uniform/mulMulLcritic/value/extrinsic_value/kernel/Initializer/random_uniform/RandomUniformBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/sub*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel
�
>critic/value/extrinsic_value/kernel/Initializer/random_uniformAddBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/mulBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel
�
#critic/value/extrinsic_value/kernel
VariableV2*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
	container *
dtype0*
shape
:@*
shared_name 
�
*critic/value/extrinsic_value/kernel/AssignAssign#critic/value/extrinsic_value/kernel>critic/value/extrinsic_value/kernel/Initializer/random_uniform*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
(critic/value/extrinsic_value/kernel/readIdentity#critic/value/extrinsic_value/kernel*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel
�
3critic/value/extrinsic_value/bias/Initializer/zerosConst*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
dtype0*
valueB*    
�
!critic/value/extrinsic_value/bias
VariableV2*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
	container *
dtype0*
shape:*
shared_name 
�
(critic/value/extrinsic_value/bias/AssignAssign!critic/value/extrinsic_value/bias3critic/value/extrinsic_value/bias/Initializer/zeros*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
&critic/value/extrinsic_value/bias/readIdentity!critic/value/extrinsic_value/bias*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias
�
#critic/value/extrinsic_value/MatMulMatMul!critic/value/encoder/hidden_3/Mul(critic/value/extrinsic_value/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
$critic/value/extrinsic_value/BiasAddBiasAdd#critic/value/extrinsic_value/MatMul&critic/value/extrinsic_value/bias/read*
T0*
data_formatNHWC
�
Dcritic/value/curiosity_value/kernel/Initializer/random_uniform/shapeConst*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
dtype0*
valueB"@      
�
Bcritic/value/curiosity_value/kernel/Initializer/random_uniform/minConst*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
dtype0*
valueB
 *����
�
Bcritic/value/curiosity_value/kernel/Initializer/random_uniform/maxConst*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
dtype0*
valueB
 *���>
�
Lcritic/value/curiosity_value/kernel/Initializer/random_uniform/RandomUniformRandomUniformDcritic/value/curiosity_value/kernel/Initializer/random_uniform/shape*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
dtype0*
seed�*
seed2
�
Bcritic/value/curiosity_value/kernel/Initializer/random_uniform/subSubBcritic/value/curiosity_value/kernel/Initializer/random_uniform/maxBcritic/value/curiosity_value/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel
�
Bcritic/value/curiosity_value/kernel/Initializer/random_uniform/mulMulLcritic/value/curiosity_value/kernel/Initializer/random_uniform/RandomUniformBcritic/value/curiosity_value/kernel/Initializer/random_uniform/sub*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel
�
>critic/value/curiosity_value/kernel/Initializer/random_uniformAddBcritic/value/curiosity_value/kernel/Initializer/random_uniform/mulBcritic/value/curiosity_value/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel
�
#critic/value/curiosity_value/kernel
VariableV2*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
	container *
dtype0*
shape
:@*
shared_name 
�
*critic/value/curiosity_value/kernel/AssignAssign#critic/value/curiosity_value/kernel>critic/value/curiosity_value/kernel/Initializer/random_uniform*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
(critic/value/curiosity_value/kernel/readIdentity#critic/value/curiosity_value/kernel*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel
�
3critic/value/curiosity_value/bias/Initializer/zerosConst*4
_class*
(&loc:@critic/value/curiosity_value/bias*
dtype0*
valueB*    
�
!critic/value/curiosity_value/bias
VariableV2*4
_class*
(&loc:@critic/value/curiosity_value/bias*
	container *
dtype0*
shape:*
shared_name 
�
(critic/value/curiosity_value/bias/AssignAssign!critic/value/curiosity_value/bias3critic/value/curiosity_value/bias/Initializer/zeros*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
&critic/value/curiosity_value/bias/readIdentity!critic/value/curiosity_value/bias*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias
�
#critic/value/curiosity_value/MatMulMatMul!critic/value/encoder/hidden_3/Mul(critic/value/curiosity_value/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
$critic/value/curiosity_value/BiasAddBiasAdd#critic/value/curiosity_value/MatMul&critic/value/curiosity_value/bias/read*
T0*
data_formatNHWC
�
critic/value/Mean/inputPack$critic/value/extrinsic_value/BiasAdd$critic/value/curiosity_value/BiasAdd*
N*
T0*

axis 
M
#critic/value/Mean/reduction_indicesConst*
dtype0*
value	B : 
}
critic/value/MeanMeancritic/value/Mean/input#critic/value/Mean/reduction_indices*
T0*

Tidx0*
	keep_dims( 
�
Rcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Qcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Scritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
valueB
 *6�>
�
\critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
seed�*
seed2
�
Pcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/mulMul\critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
�
Lcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normalAddPcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/mulQcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
�
/critic/q/q1_encoding/q1_encoder/hidden_0/kernel
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/AssignAssign/critic/q/q1_encoding/q1_encoder/hidden_0/kernelLcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/readIdentity/critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
�
?critic/q/q1_encoding/q1_encoder/hidden_0/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
dtype0*
valueB@*    
�
-critic/q/q1_encoding/q1_encoder/hidden_0/bias
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
4critic/q/q1_encoding/q1_encoder/hidden_0/bias/AssignAssign-critic/q/q1_encoding/q1_encoder/hidden_0/bias?critic/q/q1_encoding/q1_encoder/hidden_0/bias/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
2critic/q/q1_encoding/q1_encoder/hidden_0/bias/readIdentity-critic/q/q1_encoding/q1_encoder/hidden_0/bias*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias
�
/critic/q/q1_encoding/q1_encoder/hidden_0/MatMulMatMulvector_observation4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAddBiasAdd/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul2critic/q/q1_encoding/q1_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
v
0critic/q/q1_encoding/q1_encoder/hidden_0/SigmoidSigmoid0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd*
T0
�
,critic/q/q1_encoding/q1_encoder/hidden_0/MulMul0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd0critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid*
T0
�
Rcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Qcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Scritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
valueB
 *6�>
�
\critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
seed�*
seed2
�
Pcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/mulMul\critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
�
Lcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normalAddPcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/mulQcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
�
/critic/q/q1_encoding/q1_encoder/hidden_1/kernel
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/AssignAssign/critic/q/q1_encoding/q1_encoder/hidden_1/kernelLcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/readIdentity/critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
�
?critic/q/q1_encoding/q1_encoder/hidden_1/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
dtype0*
valueB@*    
�
-critic/q/q1_encoding/q1_encoder/hidden_1/bias
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
4critic/q/q1_encoding/q1_encoder/hidden_1/bias/AssignAssign-critic/q/q1_encoding/q1_encoder/hidden_1/bias?critic/q/q1_encoding/q1_encoder/hidden_1/bias/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
2critic/q/q1_encoding/q1_encoder/hidden_1/bias/readIdentity-critic/q/q1_encoding/q1_encoder/hidden_1/bias*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias
�
/critic/q/q1_encoding/q1_encoder/hidden_1/MatMulMatMul,critic/q/q1_encoding/q1_encoder/hidden_0/Mul4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAddBiasAdd/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul2critic/q/q1_encoding/q1_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
v
0critic/q/q1_encoding/q1_encoder/hidden_1/SigmoidSigmoid0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd*
T0
�
,critic/q/q1_encoding/q1_encoder/hidden_1/MulMul0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd0critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid*
T0
�
Rcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Qcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Scritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
dtype0*
valueB
 *6�>
�
\critic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/shape*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
dtype0*
seed�*
seed2
�
Pcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/mulMul\critic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel
�
Lcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normalAddPcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/mulQcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel
�
/critic/q/q1_encoding/q1_encoder/hidden_2/kernel
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
6critic/q/q1_encoding/q1_encoder/hidden_2/kernel/AssignAssign/critic/q/q1_encoding/q1_encoder/hidden_2/kernelLcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/Initializer/truncated_normal*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
4critic/q/q1_encoding/q1_encoder/hidden_2/kernel/readIdentity/critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel
�
?critic/q/q1_encoding/q1_encoder/hidden_2/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
dtype0*
valueB@*    
�
-critic/q/q1_encoding/q1_encoder/hidden_2/bias
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
4critic/q/q1_encoding/q1_encoder/hidden_2/bias/AssignAssign-critic/q/q1_encoding/q1_encoder/hidden_2/bias?critic/q/q1_encoding/q1_encoder/hidden_2/bias/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
2critic/q/q1_encoding/q1_encoder/hidden_2/bias/readIdentity-critic/q/q1_encoding/q1_encoder/hidden_2/bias*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias
�
/critic/q/q1_encoding/q1_encoder/hidden_2/MatMulMatMul,critic/q/q1_encoding/q1_encoder/hidden_1/Mul4critic/q/q1_encoding/q1_encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
0critic/q/q1_encoding/q1_encoder/hidden_2/BiasAddBiasAdd/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul2critic/q/q1_encoding/q1_encoder/hidden_2/bias/read*
T0*
data_formatNHWC
v
0critic/q/q1_encoding/q1_encoder/hidden_2/SigmoidSigmoid0critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd*
T0
�
,critic/q/q1_encoding/q1_encoder/hidden_2/MulMul0critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd0critic/q/q1_encoding/q1_encoder/hidden_2/Sigmoid*
T0
�
Rcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Qcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Scritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
dtype0*
valueB
 *6�>
�
\critic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/shape*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
dtype0*
seed�*
seed2
�
Pcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/mulMul\critic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel
�
Lcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normalAddPcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/mulQcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel
�
/critic/q/q1_encoding/q1_encoder/hidden_3/kernel
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
6critic/q/q1_encoding/q1_encoder/hidden_3/kernel/AssignAssign/critic/q/q1_encoding/q1_encoder/hidden_3/kernelLcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/Initializer/truncated_normal*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
4critic/q/q1_encoding/q1_encoder/hidden_3/kernel/readIdentity/critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel
�
?critic/q/q1_encoding/q1_encoder/hidden_3/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
dtype0*
valueB@*    
�
-critic/q/q1_encoding/q1_encoder/hidden_3/bias
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
4critic/q/q1_encoding/q1_encoder/hidden_3/bias/AssignAssign-critic/q/q1_encoding/q1_encoder/hidden_3/bias?critic/q/q1_encoding/q1_encoder/hidden_3/bias/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
2critic/q/q1_encoding/q1_encoder/hidden_3/bias/readIdentity-critic/q/q1_encoding/q1_encoder/hidden_3/bias*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias
�
/critic/q/q1_encoding/q1_encoder/hidden_3/MatMulMatMul,critic/q/q1_encoding/q1_encoder/hidden_2/Mul4critic/q/q1_encoding/q1_encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
0critic/q/q1_encoding/q1_encoder/hidden_3/BiasAddBiasAdd/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul2critic/q/q1_encoding/q1_encoder/hidden_3/bias/read*
T0*
data_formatNHWC
v
0critic/q/q1_encoding/q1_encoder/hidden_3/SigmoidSigmoid0critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd*
T0
�
,critic/q/q1_encoding/q1_encoder/hidden_3/MulMul0critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd0critic/q/q1_encoding/q1_encoder/hidden_3/Sigmoid*
T0
�
Icritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/shapeConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
valueB"@   @   
�
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/minConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
valueB
 *׳]�
�
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/maxConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
valueB
 *׳]>
�
Qcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/RandomUniformRandomUniformIcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/shape*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
seed�*
seed2
�
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/subSubGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/maxGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
�
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/mulMulQcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/RandomUniformGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/sub*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
�
Ccritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniformAddGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/mulGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
�
(critic/q/q1_encoding/extrinsic_q1/kernel
VariableV2*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
/critic/q/q1_encoding/extrinsic_q1/kernel/AssignAssign(critic/q/q1_encoding/extrinsic_q1/kernelCcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
�
-critic/q/q1_encoding/extrinsic_q1/kernel/readIdentity(critic/q/q1_encoding/extrinsic_q1/kernel*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
�
8critic/q/q1_encoding/extrinsic_q1/bias/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0*
valueB@*    
�
&critic/q/q1_encoding/extrinsic_q1/bias
VariableV2*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
-critic/q/q1_encoding/extrinsic_q1/bias/AssignAssign&critic/q/q1_encoding/extrinsic_q1/bias8critic/q/q1_encoding/extrinsic_q1/bias/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
�
+critic/q/q1_encoding/extrinsic_q1/bias/readIdentity&critic/q/q1_encoding/extrinsic_q1/bias*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias
�
(critic/q/q1_encoding/extrinsic_q1/MatMulMatMul,critic/q/q1_encoding/q1_encoder/hidden_3/Mul-critic/q/q1_encoding/extrinsic_q1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
)critic/q/q1_encoding/extrinsic_q1/BiasAddBiasAdd(critic/q/q1_encoding/extrinsic_q1/MatMul+critic/q/q1_encoding/extrinsic_q1/bias/read*
T0*
data_formatNHWC
�
Icritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/shapeConst*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
dtype0*
valueB"@   @   
�
Gcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/minConst*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
dtype0*
valueB
 *׳]�
�
Gcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/maxConst*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
dtype0*
valueB
 *׳]>
�
Qcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/RandomUniformRandomUniformIcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/shape*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
dtype0*
seed�*
seed2
�
Gcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/subSubGcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/maxGcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel
�
Gcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/mulMulQcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/RandomUniformGcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/sub*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel
�
Ccritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniformAddGcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/mulGcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel
�
(critic/q/q1_encoding/curiosity_q1/kernel
VariableV2*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
/critic/q/q1_encoding/curiosity_q1/kernel/AssignAssign(critic/q/q1_encoding/curiosity_q1/kernelCcritic/q/q1_encoding/curiosity_q1/kernel/Initializer/random_uniform*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
�
-critic/q/q1_encoding/curiosity_q1/kernel/readIdentity(critic/q/q1_encoding/curiosity_q1/kernel*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel
�
8critic/q/q1_encoding/curiosity_q1/bias/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
dtype0*
valueB@*    
�
&critic/q/q1_encoding/curiosity_q1/bias
VariableV2*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
-critic/q/q1_encoding/curiosity_q1/bias/AssignAssign&critic/q/q1_encoding/curiosity_q1/bias8critic/q/q1_encoding/curiosity_q1/bias/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
�
+critic/q/q1_encoding/curiosity_q1/bias/readIdentity&critic/q/q1_encoding/curiosity_q1/bias*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias
�
(critic/q/q1_encoding/curiosity_q1/MatMulMatMul,critic/q/q1_encoding/q1_encoder/hidden_3/Mul-critic/q/q1_encoding/curiosity_q1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
)critic/q/q1_encoding/curiosity_q1/BiasAddBiasAdd(critic/q/q1_encoding/curiosity_q1/MatMul+critic/q/q1_encoding/curiosity_q1/bias/read*
T0*
data_formatNHWC
�
critic/q/q1_encoding/Mean/inputPack)critic/q/q1_encoding/extrinsic_q1/BiasAdd)critic/q/q1_encoding/curiosity_q1/BiasAdd*
N*
T0*

axis 
U
+critic/q/q1_encoding/Mean/reduction_indicesConst*
dtype0*
value	B : 
�
critic/q/q1_encoding/MeanMeancritic/q/q1_encoding/Mean/input+critic/q/q1_encoding/Mean/reduction_indices*
T0*

Tidx0*
	keep_dims( 
�
Rcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Qcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Scritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
valueB
 *6�>
�
\critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
seed�*
seed2
�
Pcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/mulMul\critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
�
Lcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normalAddPcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/mulQcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
�
/critic/q/q2_encoding/q2_encoder/hidden_0/kernel
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/AssignAssign/critic/q/q2_encoding/q2_encoder/hidden_0/kernelLcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/readIdentity/critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
�
?critic/q/q2_encoding/q2_encoder/hidden_0/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
dtype0*
valueB@*    
�
-critic/q/q2_encoding/q2_encoder/hidden_0/bias
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
4critic/q/q2_encoding/q2_encoder/hidden_0/bias/AssignAssign-critic/q/q2_encoding/q2_encoder/hidden_0/bias?critic/q/q2_encoding/q2_encoder/hidden_0/bias/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
2critic/q/q2_encoding/q2_encoder/hidden_0/bias/readIdentity-critic/q/q2_encoding/q2_encoder/hidden_0/bias*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias
�
/critic/q/q2_encoding/q2_encoder/hidden_0/MatMulMatMulvector_observation4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAddBiasAdd/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul2critic/q/q2_encoding/q2_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
v
0critic/q/q2_encoding/q2_encoder/hidden_0/SigmoidSigmoid0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd*
T0
�
,critic/q/q2_encoding/q2_encoder/hidden_0/MulMul0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd0critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid*
T0
�
Rcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Qcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Scritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
valueB
 *6�>
�
\critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
seed�*
seed2
�
Pcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/mulMul\critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
�
Lcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normalAddPcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/mulQcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
�
/critic/q/q2_encoding/q2_encoder/hidden_1/kernel
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/AssignAssign/critic/q/q2_encoding/q2_encoder/hidden_1/kernelLcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/readIdentity/critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
�
?critic/q/q2_encoding/q2_encoder/hidden_1/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
dtype0*
valueB@*    
�
-critic/q/q2_encoding/q2_encoder/hidden_1/bias
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
4critic/q/q2_encoding/q2_encoder/hidden_1/bias/AssignAssign-critic/q/q2_encoding/q2_encoder/hidden_1/bias?critic/q/q2_encoding/q2_encoder/hidden_1/bias/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
2critic/q/q2_encoding/q2_encoder/hidden_1/bias/readIdentity-critic/q/q2_encoding/q2_encoder/hidden_1/bias*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias
�
/critic/q/q2_encoding/q2_encoder/hidden_1/MatMulMatMul,critic/q/q2_encoding/q2_encoder/hidden_0/Mul4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAddBiasAdd/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul2critic/q/q2_encoding/q2_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
v
0critic/q/q2_encoding/q2_encoder/hidden_1/SigmoidSigmoid0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd*
T0
�
,critic/q/q2_encoding/q2_encoder/hidden_1/MulMul0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd0critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid*
T0
�
Rcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Qcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Scritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
dtype0*
valueB
 *6�>
�
\critic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/shape*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
dtype0*
seed�*
seed2
�
Pcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/mulMul\critic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel
�
Lcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normalAddPcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/mulQcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel
�
/critic/q/q2_encoding/q2_encoder/hidden_2/kernel
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
6critic/q/q2_encoding/q2_encoder/hidden_2/kernel/AssignAssign/critic/q/q2_encoding/q2_encoder/hidden_2/kernelLcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/Initializer/truncated_normal*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
4critic/q/q2_encoding/q2_encoder/hidden_2/kernel/readIdentity/critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel
�
?critic/q/q2_encoding/q2_encoder/hidden_2/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
dtype0*
valueB@*    
�
-critic/q/q2_encoding/q2_encoder/hidden_2/bias
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
4critic/q/q2_encoding/q2_encoder/hidden_2/bias/AssignAssign-critic/q/q2_encoding/q2_encoder/hidden_2/bias?critic/q/q2_encoding/q2_encoder/hidden_2/bias/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
2critic/q/q2_encoding/q2_encoder/hidden_2/bias/readIdentity-critic/q/q2_encoding/q2_encoder/hidden_2/bias*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias
�
/critic/q/q2_encoding/q2_encoder/hidden_2/MatMulMatMul,critic/q/q2_encoding/q2_encoder/hidden_1/Mul4critic/q/q2_encoding/q2_encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
0critic/q/q2_encoding/q2_encoder/hidden_2/BiasAddBiasAdd/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul2critic/q/q2_encoding/q2_encoder/hidden_2/bias/read*
T0*
data_formatNHWC
v
0critic/q/q2_encoding/q2_encoder/hidden_2/SigmoidSigmoid0critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd*
T0
�
,critic/q/q2_encoding/q2_encoder/hidden_2/MulMul0critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd0critic/q/q2_encoding/q2_encoder/hidden_2/Sigmoid*
T0
�
Rcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Qcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Scritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
dtype0*
valueB
 *6�>
�
\critic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/shape*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
dtype0*
seed�*
seed2
�
Pcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/mulMul\critic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel
�
Lcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normalAddPcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/mulQcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel
�
/critic/q/q2_encoding/q2_encoder/hidden_3/kernel
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
6critic/q/q2_encoding/q2_encoder/hidden_3/kernel/AssignAssign/critic/q/q2_encoding/q2_encoder/hidden_3/kernelLcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/Initializer/truncated_normal*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
4critic/q/q2_encoding/q2_encoder/hidden_3/kernel/readIdentity/critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel
�
?critic/q/q2_encoding/q2_encoder/hidden_3/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
dtype0*
valueB@*    
�
-critic/q/q2_encoding/q2_encoder/hidden_3/bias
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
4critic/q/q2_encoding/q2_encoder/hidden_3/bias/AssignAssign-critic/q/q2_encoding/q2_encoder/hidden_3/bias?critic/q/q2_encoding/q2_encoder/hidden_3/bias/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
2critic/q/q2_encoding/q2_encoder/hidden_3/bias/readIdentity-critic/q/q2_encoding/q2_encoder/hidden_3/bias*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias
�
/critic/q/q2_encoding/q2_encoder/hidden_3/MatMulMatMul,critic/q/q2_encoding/q2_encoder/hidden_2/Mul4critic/q/q2_encoding/q2_encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
0critic/q/q2_encoding/q2_encoder/hidden_3/BiasAddBiasAdd/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul2critic/q/q2_encoding/q2_encoder/hidden_3/bias/read*
T0*
data_formatNHWC
v
0critic/q/q2_encoding/q2_encoder/hidden_3/SigmoidSigmoid0critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd*
T0
�
,critic/q/q2_encoding/q2_encoder/hidden_3/MulMul0critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd0critic/q/q2_encoding/q2_encoder/hidden_3/Sigmoid*
T0
�
Icritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/shapeConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
valueB"@   @   
�
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/minConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
valueB
 *׳]�
�
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/maxConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
valueB
 *׳]>
�
Qcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/RandomUniformRandomUniformIcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/shape*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
seed�*
seed2
�
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/subSubGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/maxGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
�
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/mulMulQcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/RandomUniformGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/sub*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
�
Ccritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniformAddGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/mulGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
�
(critic/q/q2_encoding/extrinsic_q2/kernel
VariableV2*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
/critic/q/q2_encoding/extrinsic_q2/kernel/AssignAssign(critic/q/q2_encoding/extrinsic_q2/kernelCcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
�
-critic/q/q2_encoding/extrinsic_q2/kernel/readIdentity(critic/q/q2_encoding/extrinsic_q2/kernel*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
�
8critic/q/q2_encoding/extrinsic_q2/bias/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
dtype0*
valueB@*    
�
&critic/q/q2_encoding/extrinsic_q2/bias
VariableV2*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
-critic/q/q2_encoding/extrinsic_q2/bias/AssignAssign&critic/q/q2_encoding/extrinsic_q2/bias8critic/q/q2_encoding/extrinsic_q2/bias/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
�
+critic/q/q2_encoding/extrinsic_q2/bias/readIdentity&critic/q/q2_encoding/extrinsic_q2/bias*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias
�
(critic/q/q2_encoding/extrinsic_q2/MatMulMatMul,critic/q/q2_encoding/q2_encoder/hidden_3/Mul-critic/q/q2_encoding/extrinsic_q2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
)critic/q/q2_encoding/extrinsic_q2/BiasAddBiasAdd(critic/q/q2_encoding/extrinsic_q2/MatMul+critic/q/q2_encoding/extrinsic_q2/bias/read*
T0*
data_formatNHWC
�
Icritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/shapeConst*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
dtype0*
valueB"@   @   
�
Gcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/minConst*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
dtype0*
valueB
 *׳]�
�
Gcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/maxConst*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
dtype0*
valueB
 *׳]>
�
Qcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/RandomUniformRandomUniformIcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/shape*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
dtype0*
seed�*
seed2
�
Gcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/subSubGcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/maxGcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel
�
Gcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/mulMulQcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/RandomUniformGcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/sub*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel
�
Ccritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniformAddGcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/mulGcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel
�
(critic/q/q2_encoding/curiosity_q2/kernel
VariableV2*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
/critic/q/q2_encoding/curiosity_q2/kernel/AssignAssign(critic/q/q2_encoding/curiosity_q2/kernelCcritic/q/q2_encoding/curiosity_q2/kernel/Initializer/random_uniform*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
�
-critic/q/q2_encoding/curiosity_q2/kernel/readIdentity(critic/q/q2_encoding/curiosity_q2/kernel*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel
�
8critic/q/q2_encoding/curiosity_q2/bias/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
dtype0*
valueB@*    
�
&critic/q/q2_encoding/curiosity_q2/bias
VariableV2*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
-critic/q/q2_encoding/curiosity_q2/bias/AssignAssign&critic/q/q2_encoding/curiosity_q2/bias8critic/q/q2_encoding/curiosity_q2/bias/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
�
+critic/q/q2_encoding/curiosity_q2/bias/readIdentity&critic/q/q2_encoding/curiosity_q2/bias*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias
�
(critic/q/q2_encoding/curiosity_q2/MatMulMatMul,critic/q/q2_encoding/q2_encoder/hidden_3/Mul-critic/q/q2_encoding/curiosity_q2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
)critic/q/q2_encoding/curiosity_q2/BiasAddBiasAdd(critic/q/q2_encoding/curiosity_q2/MatMul+critic/q/q2_encoding/curiosity_q2/bias/read*
T0*
data_formatNHWC
�
critic/q/q2_encoding/Mean/inputPack)critic/q/q2_encoding/extrinsic_q2/BiasAdd)critic/q/q2_encoding/curiosity_q2/BiasAdd*
N*
T0*

axis 
U
+critic/q/q2_encoding/Mean/reduction_indicesConst*
dtype0*
value	B : 
�
critic/q/q2_encoding/MeanMeancritic/q/q2_encoding/Mean/input+critic/q/q2_encoding/Mean/reduction_indices*
T0*

Tidx0*
	keep_dims( 
�
1critic/q/q1_encoding_1/q1_encoder/hidden_0/MatMulMatMulvector_observation4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
2critic/q/q1_encoding_1/q1_encoder/hidden_0/BiasAddBiasAdd1critic/q/q1_encoding_1/q1_encoder/hidden_0/MatMul2critic/q/q1_encoding/q1_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
z
2critic/q/q1_encoding_1/q1_encoder/hidden_0/SigmoidSigmoid2critic/q/q1_encoding_1/q1_encoder/hidden_0/BiasAdd*
T0
�
.critic/q/q1_encoding_1/q1_encoder/hidden_0/MulMul2critic/q/q1_encoding_1/q1_encoder/hidden_0/BiasAdd2critic/q/q1_encoding_1/q1_encoder/hidden_0/Sigmoid*
T0
�
1critic/q/q1_encoding_1/q1_encoder/hidden_1/MatMulMatMul.critic/q/q1_encoding_1/q1_encoder/hidden_0/Mul4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
2critic/q/q1_encoding_1/q1_encoder/hidden_1/BiasAddBiasAdd1critic/q/q1_encoding_1/q1_encoder/hidden_1/MatMul2critic/q/q1_encoding/q1_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
z
2critic/q/q1_encoding_1/q1_encoder/hidden_1/SigmoidSigmoid2critic/q/q1_encoding_1/q1_encoder/hidden_1/BiasAdd*
T0
�
.critic/q/q1_encoding_1/q1_encoder/hidden_1/MulMul2critic/q/q1_encoding_1/q1_encoder/hidden_1/BiasAdd2critic/q/q1_encoding_1/q1_encoder/hidden_1/Sigmoid*
T0
�
1critic/q/q1_encoding_1/q1_encoder/hidden_2/MatMulMatMul.critic/q/q1_encoding_1/q1_encoder/hidden_1/Mul4critic/q/q1_encoding/q1_encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
2critic/q/q1_encoding_1/q1_encoder/hidden_2/BiasAddBiasAdd1critic/q/q1_encoding_1/q1_encoder/hidden_2/MatMul2critic/q/q1_encoding/q1_encoder/hidden_2/bias/read*
T0*
data_formatNHWC
z
2critic/q/q1_encoding_1/q1_encoder/hidden_2/SigmoidSigmoid2critic/q/q1_encoding_1/q1_encoder/hidden_2/BiasAdd*
T0
�
.critic/q/q1_encoding_1/q1_encoder/hidden_2/MulMul2critic/q/q1_encoding_1/q1_encoder/hidden_2/BiasAdd2critic/q/q1_encoding_1/q1_encoder/hidden_2/Sigmoid*
T0
�
1critic/q/q1_encoding_1/q1_encoder/hidden_3/MatMulMatMul.critic/q/q1_encoding_1/q1_encoder/hidden_2/Mul4critic/q/q1_encoding/q1_encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
2critic/q/q1_encoding_1/q1_encoder/hidden_3/BiasAddBiasAdd1critic/q/q1_encoding_1/q1_encoder/hidden_3/MatMul2critic/q/q1_encoding/q1_encoder/hidden_3/bias/read*
T0*
data_formatNHWC
z
2critic/q/q1_encoding_1/q1_encoder/hidden_3/SigmoidSigmoid2critic/q/q1_encoding_1/q1_encoder/hidden_3/BiasAdd*
T0
�
.critic/q/q1_encoding_1/q1_encoder/hidden_3/MulMul2critic/q/q1_encoding_1/q1_encoder/hidden_3/BiasAdd2critic/q/q1_encoding_1/q1_encoder/hidden_3/Sigmoid*
T0
�
*critic/q/q1_encoding_1/extrinsic_q1/MatMulMatMul.critic/q/q1_encoding_1/q1_encoder/hidden_3/Mul-critic/q/q1_encoding/extrinsic_q1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
+critic/q/q1_encoding_1/extrinsic_q1/BiasAddBiasAdd*critic/q/q1_encoding_1/extrinsic_q1/MatMul+critic/q/q1_encoding/extrinsic_q1/bias/read*
T0*
data_formatNHWC
�
*critic/q/q1_encoding_1/curiosity_q1/MatMulMatMul.critic/q/q1_encoding_1/q1_encoder/hidden_3/Mul-critic/q/q1_encoding/curiosity_q1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
+critic/q/q1_encoding_1/curiosity_q1/BiasAddBiasAdd*critic/q/q1_encoding_1/curiosity_q1/MatMul+critic/q/q1_encoding/curiosity_q1/bias/read*
T0*
data_formatNHWC
�
!critic/q/q1_encoding_1/Mean/inputPack+critic/q/q1_encoding_1/extrinsic_q1/BiasAdd+critic/q/q1_encoding_1/curiosity_q1/BiasAdd*
N*
T0*

axis 
W
-critic/q/q1_encoding_1/Mean/reduction_indicesConst*
dtype0*
value	B : 
�
critic/q/q1_encoding_1/MeanMean!critic/q/q1_encoding_1/Mean/input-critic/q/q1_encoding_1/Mean/reduction_indices*
T0*

Tidx0*
	keep_dims( 
�
1critic/q/q2_encoding_1/q2_encoder/hidden_0/MatMulMatMulvector_observation4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
2critic/q/q2_encoding_1/q2_encoder/hidden_0/BiasAddBiasAdd1critic/q/q2_encoding_1/q2_encoder/hidden_0/MatMul2critic/q/q2_encoding/q2_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
z
2critic/q/q2_encoding_1/q2_encoder/hidden_0/SigmoidSigmoid2critic/q/q2_encoding_1/q2_encoder/hidden_0/BiasAdd*
T0
�
.critic/q/q2_encoding_1/q2_encoder/hidden_0/MulMul2critic/q/q2_encoding_1/q2_encoder/hidden_0/BiasAdd2critic/q/q2_encoding_1/q2_encoder/hidden_0/Sigmoid*
T0
�
1critic/q/q2_encoding_1/q2_encoder/hidden_1/MatMulMatMul.critic/q/q2_encoding_1/q2_encoder/hidden_0/Mul4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
2critic/q/q2_encoding_1/q2_encoder/hidden_1/BiasAddBiasAdd1critic/q/q2_encoding_1/q2_encoder/hidden_1/MatMul2critic/q/q2_encoding/q2_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
z
2critic/q/q2_encoding_1/q2_encoder/hidden_1/SigmoidSigmoid2critic/q/q2_encoding_1/q2_encoder/hidden_1/BiasAdd*
T0
�
.critic/q/q2_encoding_1/q2_encoder/hidden_1/MulMul2critic/q/q2_encoding_1/q2_encoder/hidden_1/BiasAdd2critic/q/q2_encoding_1/q2_encoder/hidden_1/Sigmoid*
T0
�
1critic/q/q2_encoding_1/q2_encoder/hidden_2/MatMulMatMul.critic/q/q2_encoding_1/q2_encoder/hidden_1/Mul4critic/q/q2_encoding/q2_encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
2critic/q/q2_encoding_1/q2_encoder/hidden_2/BiasAddBiasAdd1critic/q/q2_encoding_1/q2_encoder/hidden_2/MatMul2critic/q/q2_encoding/q2_encoder/hidden_2/bias/read*
T0*
data_formatNHWC
z
2critic/q/q2_encoding_1/q2_encoder/hidden_2/SigmoidSigmoid2critic/q/q2_encoding_1/q2_encoder/hidden_2/BiasAdd*
T0
�
.critic/q/q2_encoding_1/q2_encoder/hidden_2/MulMul2critic/q/q2_encoding_1/q2_encoder/hidden_2/BiasAdd2critic/q/q2_encoding_1/q2_encoder/hidden_2/Sigmoid*
T0
�
1critic/q/q2_encoding_1/q2_encoder/hidden_3/MatMulMatMul.critic/q/q2_encoding_1/q2_encoder/hidden_2/Mul4critic/q/q2_encoding/q2_encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
2critic/q/q2_encoding_1/q2_encoder/hidden_3/BiasAddBiasAdd1critic/q/q2_encoding_1/q2_encoder/hidden_3/MatMul2critic/q/q2_encoding/q2_encoder/hidden_3/bias/read*
T0*
data_formatNHWC
z
2critic/q/q2_encoding_1/q2_encoder/hidden_3/SigmoidSigmoid2critic/q/q2_encoding_1/q2_encoder/hidden_3/BiasAdd*
T0
�
.critic/q/q2_encoding_1/q2_encoder/hidden_3/MulMul2critic/q/q2_encoding_1/q2_encoder/hidden_3/BiasAdd2critic/q/q2_encoding_1/q2_encoder/hidden_3/Sigmoid*
T0
�
*critic/q/q2_encoding_1/extrinsic_q2/MatMulMatMul.critic/q/q2_encoding_1/q2_encoder/hidden_3/Mul-critic/q/q2_encoding/extrinsic_q2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
+critic/q/q2_encoding_1/extrinsic_q2/BiasAddBiasAdd*critic/q/q2_encoding_1/extrinsic_q2/MatMul+critic/q/q2_encoding/extrinsic_q2/bias/read*
T0*
data_formatNHWC
�
*critic/q/q2_encoding_1/curiosity_q2/MatMulMatMul.critic/q/q2_encoding_1/q2_encoder/hidden_3/Mul-critic/q/q2_encoding/curiosity_q2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
+critic/q/q2_encoding_1/curiosity_q2/BiasAddBiasAdd*critic/q/q2_encoding_1/curiosity_q2/MatMul+critic/q/q2_encoding/curiosity_q2/bias/read*
T0*
data_formatNHWC
�
!critic/q/q2_encoding_1/Mean/inputPack+critic/q/q2_encoding_1/extrinsic_q2/BiasAdd+critic/q/q2_encoding_1/curiosity_q2/BiasAdd*
N*
T0*

axis 
W
-critic/q/q2_encoding_1/Mean/reduction_indicesConst*
dtype0*
value	B : 
�
critic/q/q2_encoding_1/MeanMean!critic/q/q2_encoding_1/Mean/input-critic/q/q2_encoding_1/Mean/reduction_indices*
T0*

Tidx0*
	keep_dims( 
@
sac_sequence_length_1Placeholder*
dtype0*
shape:
[
!target_network/vector_observationPlaceholder*
dtype0*
shape:���������@
�
Vtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Utarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Wtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
dtype0*
valueB
 *6�>
�
`target_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalVtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
dtype0*
seed�*
seed2
�
Ttarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mulMul`target_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalWtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel
�
Ptarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normalAddTtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mulUtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel
�
3target_network/critic/value/encoder/hidden_0/kernel
VariableV2*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
:target_network/critic/value/encoder/hidden_0/kernel/AssignAssign3target_network/critic/value/encoder/hidden_0/kernelPtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
8target_network/critic/value/encoder/hidden_0/kernel/readIdentity3target_network/critic/value/encoder/hidden_0/kernel*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel
�
Ctarget_network/critic/value/encoder/hidden_0/bias/Initializer/zerosConst*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
dtype0*
valueB@*    
�
1target_network/critic/value/encoder/hidden_0/bias
VariableV2*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
8target_network/critic/value/encoder/hidden_0/bias/AssignAssign1target_network/critic/value/encoder/hidden_0/biasCtarget_network/critic/value/encoder/hidden_0/bias/Initializer/zeros*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
6target_network/critic/value/encoder/hidden_0/bias/readIdentity1target_network/critic/value/encoder/hidden_0/bias*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias
�
3target_network/critic/value/encoder/hidden_0/MatMulMatMul!target_network/vector_observation8target_network/critic/value/encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
4target_network/critic/value/encoder/hidden_0/BiasAddBiasAdd3target_network/critic/value/encoder/hidden_0/MatMul6target_network/critic/value/encoder/hidden_0/bias/read*
T0*
data_formatNHWC
~
4target_network/critic/value/encoder/hidden_0/SigmoidSigmoid4target_network/critic/value/encoder/hidden_0/BiasAdd*
T0
�
0target_network/critic/value/encoder/hidden_0/MulMul4target_network/critic/value/encoder/hidden_0/BiasAdd4target_network/critic/value/encoder/hidden_0/Sigmoid*
T0
�
Vtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Utarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Wtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
dtype0*
valueB
 *6�>
�
`target_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalVtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
dtype0*
seed�*
seed2
�
Ttarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mulMul`target_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalWtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel
�
Ptarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normalAddTtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mulUtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel
�
3target_network/critic/value/encoder/hidden_1/kernel
VariableV2*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
:target_network/critic/value/encoder/hidden_1/kernel/AssignAssign3target_network/critic/value/encoder/hidden_1/kernelPtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
8target_network/critic/value/encoder/hidden_1/kernel/readIdentity3target_network/critic/value/encoder/hidden_1/kernel*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel
�
Ctarget_network/critic/value/encoder/hidden_1/bias/Initializer/zerosConst*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
dtype0*
valueB@*    
�
1target_network/critic/value/encoder/hidden_1/bias
VariableV2*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
8target_network/critic/value/encoder/hidden_1/bias/AssignAssign1target_network/critic/value/encoder/hidden_1/biasCtarget_network/critic/value/encoder/hidden_1/bias/Initializer/zeros*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
6target_network/critic/value/encoder/hidden_1/bias/readIdentity1target_network/critic/value/encoder/hidden_1/bias*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias
�
3target_network/critic/value/encoder/hidden_1/MatMulMatMul0target_network/critic/value/encoder/hidden_0/Mul8target_network/critic/value/encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
4target_network/critic/value/encoder/hidden_1/BiasAddBiasAdd3target_network/critic/value/encoder/hidden_1/MatMul6target_network/critic/value/encoder/hidden_1/bias/read*
T0*
data_formatNHWC
~
4target_network/critic/value/encoder/hidden_1/SigmoidSigmoid4target_network/critic/value/encoder/hidden_1/BiasAdd*
T0
�
0target_network/critic/value/encoder/hidden_1/MulMul4target_network/critic/value/encoder/hidden_1/BiasAdd4target_network/critic/value/encoder/hidden_1/Sigmoid*
T0
�
Vtarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/shapeConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Utarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/meanConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Wtarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/stddevConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
dtype0*
valueB
 *6�>
�
`target_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalVtarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/shape*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
dtype0*
seed�*
seed2 
�
Ttarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/mulMul`target_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/TruncatedNormalWtarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/stddev*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel
�
Ptarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normalAddTtarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/mulUtarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal/mean*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel
�
3target_network/critic/value/encoder/hidden_2/kernel
VariableV2*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
:target_network/critic/value/encoder/hidden_2/kernel/AssignAssign3target_network/critic/value/encoder/hidden_2/kernelPtarget_network/critic/value/encoder/hidden_2/kernel/Initializer/truncated_normal*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
8target_network/critic/value/encoder/hidden_2/kernel/readIdentity3target_network/critic/value/encoder/hidden_2/kernel*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel
�
Ctarget_network/critic/value/encoder/hidden_2/bias/Initializer/zerosConst*D
_class:
86loc:@target_network/critic/value/encoder/hidden_2/bias*
dtype0*
valueB@*    
�
1target_network/critic/value/encoder/hidden_2/bias
VariableV2*D
_class:
86loc:@target_network/critic/value/encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
8target_network/critic/value/encoder/hidden_2/bias/AssignAssign1target_network/critic/value/encoder/hidden_2/biasCtarget_network/critic/value/encoder/hidden_2/bias/Initializer/zeros*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
6target_network/critic/value/encoder/hidden_2/bias/readIdentity1target_network/critic/value/encoder/hidden_2/bias*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_2/bias
�
3target_network/critic/value/encoder/hidden_2/MatMulMatMul0target_network/critic/value/encoder/hidden_1/Mul8target_network/critic/value/encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
4target_network/critic/value/encoder/hidden_2/BiasAddBiasAdd3target_network/critic/value/encoder/hidden_2/MatMul6target_network/critic/value/encoder/hidden_2/bias/read*
T0*
data_formatNHWC
~
4target_network/critic/value/encoder/hidden_2/SigmoidSigmoid4target_network/critic/value/encoder/hidden_2/BiasAdd*
T0
�
0target_network/critic/value/encoder/hidden_2/MulMul4target_network/critic/value/encoder/hidden_2/BiasAdd4target_network/critic/value/encoder/hidden_2/Sigmoid*
T0
�
Vtarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/shapeConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Utarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/meanConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Wtarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/stddevConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
dtype0*
valueB
 *6�>
�
`target_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalVtarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/shape*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
dtype0*
seed�*
seed2!
�
Ttarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/mulMul`target_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/TruncatedNormalWtarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/stddev*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel
�
Ptarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normalAddTtarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/mulUtarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal/mean*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel
�
3target_network/critic/value/encoder/hidden_3/kernel
VariableV2*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
:target_network/critic/value/encoder/hidden_3/kernel/AssignAssign3target_network/critic/value/encoder/hidden_3/kernelPtarget_network/critic/value/encoder/hidden_3/kernel/Initializer/truncated_normal*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
8target_network/critic/value/encoder/hidden_3/kernel/readIdentity3target_network/critic/value/encoder/hidden_3/kernel*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel
�
Ctarget_network/critic/value/encoder/hidden_3/bias/Initializer/zerosConst*D
_class:
86loc:@target_network/critic/value/encoder/hidden_3/bias*
dtype0*
valueB@*    
�
1target_network/critic/value/encoder/hidden_3/bias
VariableV2*D
_class:
86loc:@target_network/critic/value/encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
8target_network/critic/value/encoder/hidden_3/bias/AssignAssign1target_network/critic/value/encoder/hidden_3/biasCtarget_network/critic/value/encoder/hidden_3/bias/Initializer/zeros*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
6target_network/critic/value/encoder/hidden_3/bias/readIdentity1target_network/critic/value/encoder/hidden_3/bias*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_3/bias
�
3target_network/critic/value/encoder/hidden_3/MatMulMatMul0target_network/critic/value/encoder/hidden_2/Mul8target_network/critic/value/encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
4target_network/critic/value/encoder/hidden_3/BiasAddBiasAdd3target_network/critic/value/encoder/hidden_3/MatMul6target_network/critic/value/encoder/hidden_3/bias/read*
T0*
data_formatNHWC
~
4target_network/critic/value/encoder/hidden_3/SigmoidSigmoid4target_network/critic/value/encoder/hidden_3/BiasAdd*
T0
�
0target_network/critic/value/encoder/hidden_3/MulMul4target_network/critic/value/encoder/hidden_3/BiasAdd4target_network/critic/value/encoder/hidden_3/Sigmoid*
T0
�
Starget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/shapeConst*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
dtype0*
valueB"@      
�
Qtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/minConst*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
dtype0*
valueB
 *����
�
Qtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/maxConst*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
dtype0*
valueB
 *���>
�
[target_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/RandomUniformRandomUniformStarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/shape*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
dtype0*
seed�*
seed2"
�
Qtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/subSubQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/maxQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/min*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel
�
Qtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/mulMul[target_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/RandomUniformQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/sub*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel
�
Mtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniformAddQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/mulQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/min*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel
�
2target_network/critic/value/extrinsic_value/kernel
VariableV2*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
	container *
dtype0*
shape
:@*
shared_name 
�
9target_network/critic/value/extrinsic_value/kernel/AssignAssign2target_network/critic/value/extrinsic_value/kernelMtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
7target_network/critic/value/extrinsic_value/kernel/readIdentity2target_network/critic/value/extrinsic_value/kernel*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel
�
Btarget_network/critic/value/extrinsic_value/bias/Initializer/zerosConst*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
dtype0*
valueB*    
�
0target_network/critic/value/extrinsic_value/bias
VariableV2*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
	container *
dtype0*
shape:*
shared_name 
�
7target_network/critic/value/extrinsic_value/bias/AssignAssign0target_network/critic/value/extrinsic_value/biasBtarget_network/critic/value/extrinsic_value/bias/Initializer/zeros*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
5target_network/critic/value/extrinsic_value/bias/readIdentity0target_network/critic/value/extrinsic_value/bias*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias
�
2target_network/critic/value/extrinsic_value/MatMulMatMul0target_network/critic/value/encoder/hidden_3/Mul7target_network/critic/value/extrinsic_value/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
3target_network/critic/value/extrinsic_value/BiasAddBiasAdd2target_network/critic/value/extrinsic_value/MatMul5target_network/critic/value/extrinsic_value/bias/read*
T0*
data_formatNHWC
�
Starget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/shapeConst*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
dtype0*
valueB"@      
�
Qtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/minConst*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
dtype0*
valueB
 *����
�
Qtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/maxConst*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
dtype0*
valueB
 *���>
�
[target_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/RandomUniformRandomUniformStarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/shape*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
dtype0*
seed�*
seed2#
�
Qtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/subSubQtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/maxQtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/min*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel
�
Qtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/mulMul[target_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/RandomUniformQtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/sub*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel
�
Mtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniformAddQtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/mulQtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform/min*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel
�
2target_network/critic/value/curiosity_value/kernel
VariableV2*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
	container *
dtype0*
shape
:@*
shared_name 
�
9target_network/critic/value/curiosity_value/kernel/AssignAssign2target_network/critic/value/curiosity_value/kernelMtarget_network/critic/value/curiosity_value/kernel/Initializer/random_uniform*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
7target_network/critic/value/curiosity_value/kernel/readIdentity2target_network/critic/value/curiosity_value/kernel*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel
�
Btarget_network/critic/value/curiosity_value/bias/Initializer/zerosConst*C
_class9
75loc:@target_network/critic/value/curiosity_value/bias*
dtype0*
valueB*    
�
0target_network/critic/value/curiosity_value/bias
VariableV2*C
_class9
75loc:@target_network/critic/value/curiosity_value/bias*
	container *
dtype0*
shape:*
shared_name 
�
7target_network/critic/value/curiosity_value/bias/AssignAssign0target_network/critic/value/curiosity_value/biasBtarget_network/critic/value/curiosity_value/bias/Initializer/zeros*
T0*C
_class9
75loc:@target_network/critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
5target_network/critic/value/curiosity_value/bias/readIdentity0target_network/critic/value/curiosity_value/bias*
T0*C
_class9
75loc:@target_network/critic/value/curiosity_value/bias
�
2target_network/critic/value/curiosity_value/MatMulMatMul0target_network/critic/value/encoder/hidden_3/Mul7target_network/critic/value/curiosity_value/kernel/read*
T0*
transpose_a( *
transpose_b( 
�
3target_network/critic/value/curiosity_value/BiasAddBiasAdd2target_network/critic/value/curiosity_value/MatMul5target_network/critic/value/curiosity_value/bias/read*
T0*
data_formatNHWC
�
&target_network/critic/value/Mean/inputPack3target_network/critic/value/extrinsic_value/BiasAdd3target_network/critic/value/curiosity_value/BiasAdd*
N*
T0*

axis 
\
2target_network/critic/value/Mean/reduction_indicesConst*
dtype0*
value	B : 
�
 target_network/critic/value/MeanMean&target_network/critic/value/Mean/input2target_network/critic/value/Mean/reduction_indices*
T0*

Tidx0*
	keep_dims( 
=
value_estimate_unusedIdentitycritic/value/Mean*
T0
B
dones_holderPlaceholder*
dtype0*
shape:���������
E
Variable_2/initial_valueConst*
dtype0*
valueB
 *RI�9
V

Variable_2
VariableV2*
	container *
dtype0*
shape: *
shared_name 
�
Variable_2/AssignAssign
Variable_2Variable_2/initial_value*
T0*
_class
loc:@Variable_2*
use_locking(*
validate_shape(
O
Variable_2/readIdentity
Variable_2*
T0*
_class
loc:@Variable_2

ExpExpaction*
T0
"
mul_5MulExpaction*
T0
G
mul_6Mul+critic/q/q1_encoding_1/extrinsic_q1/BiasAddExp*
T0
H
strided_slice/stackConst*
dtype0*
valueB"        
J
strided_slice/stack_1Const*
dtype0*
valueB"    @   
J
strided_slice/stack_2Const*
dtype0*
valueB"      
�
strided_sliceStridedSlicemul_6strided_slice/stackstrided_slice/stack_1strided_slice/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
A
Sum_2/reduction_indicesConst*
dtype0*
value	B :
Z
Sum_2Sumstrided_sliceSum_2/reduction_indices*
T0*

Tidx0*
	keep_dims(
2
stackPackSum_2*
N*
T0*

axis 
B
Mean_2/reduction_indicesConst*
dtype0*
value	B : 
U
Mean_2MeanstackMean_2/reduction_indices*
T0*

Tidx0*
	keep_dims( 
G
mul_7Mul+critic/q/q2_encoding_1/extrinsic_q2/BiasAddExp*
T0
J
strided_slice_1/stackConst*
dtype0*
valueB"        
L
strided_slice_1/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_1/stack_2Const*
dtype0*
valueB"      
�
strided_slice_1StridedSlicemul_7strided_slice_1/stackstrided_slice_1/stack_1strided_slice_1/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
A
Sum_3/reduction_indicesConst*
dtype0*
value	B :
\
Sum_3Sumstrided_slice_1Sum_3/reduction_indices*
T0*

Tidx0*
	keep_dims(
4
stack_1PackSum_3*
N*
T0*

axis 
B
Mean_3/reduction_indicesConst*
dtype0*
value	B : 
W
Mean_3Meanstack_1Mean_3/reduction_indices*
T0*

Tidx0*
	keep_dims( 
+
MinimumMinimumMean_2Mean_3*
T0
G
extrinsic_rewardsPlaceholder*
dtype0*
shape:���������
G
mul_8Mul+critic/q/q1_encoding_1/curiosity_q1/BiasAddExp*
T0
J
strided_slice_2/stackConst*
dtype0*
valueB"        
L
strided_slice_2/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_2/stack_2Const*
dtype0*
valueB"      
�
strided_slice_2StridedSlicemul_8strided_slice_2/stackstrided_slice_2/stack_1strided_slice_2/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
A
Sum_4/reduction_indicesConst*
dtype0*
value	B :
\
Sum_4Sumstrided_slice_2Sum_4/reduction_indices*
T0*

Tidx0*
	keep_dims(
4
stack_2PackSum_4*
N*
T0*

axis 
B
Mean_4/reduction_indicesConst*
dtype0*
value	B : 
W
Mean_4Meanstack_2Mean_4/reduction_indices*
T0*

Tidx0*
	keep_dims( 
G
mul_9Mul+critic/q/q2_encoding_1/curiosity_q2/BiasAddExp*
T0
J
strided_slice_3/stackConst*
dtype0*
valueB"        
L
strided_slice_3/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_3/stack_2Const*
dtype0*
valueB"      
�
strided_slice_3StridedSlicemul_9strided_slice_3/stackstrided_slice_3/stack_1strided_slice_3/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
A
Sum_5/reduction_indicesConst*
dtype0*
value	B :
\
Sum_5Sumstrided_slice_3Sum_5/reduction_indices*
T0*

Tidx0*
	keep_dims(
4
stack_3PackSum_5*
N*
T0*

axis 
B
Mean_5/reduction_indicesConst*
dtype0*
value	B : 
W
Mean_5Meanstack_3Mean_5/reduction_indices*
T0*

Tidx0*
	keep_dims( 
-
	Minimum_1MinimumMean_4Mean_5*
T0
G
curiosity_rewardsPlaceholder*
dtype0*
shape:���������
A
ExpandDims/dimConst*
dtype0*
valueB :
���������
K

ExpandDims
ExpandDimsdones_holderExpandDims/dim*
T0*

Tdim0
C
ExpandDims_1/dimConst*
dtype0*
valueB :
���������
T
ExpandDims_1
ExpandDimsextrinsic_rewardsExpandDims_1/dim*
T0*

Tdim0
1
mul_10MulVariable/read
ExpandDims*
T0
2
sub/xConst*
dtype0*
valueB
 *  �?
"
subSubsub/xmul_10*
T0
5
mul_11/yConst*
dtype0*
valueB
 *�p}?
%
mul_11Mulsubmul_11/y*
T0
S
mul_12Mulmul_113target_network/critic/value/extrinsic_value/BiasAdd*
T0
-
add_3AddV2ExpandDims_1mul_12*
T0
.
StopGradient_1StopGradientadd_3*
T0
O
mul_13MulStopGradient)critic/q/q1_encoding/extrinsic_q1/BiasAdd*
T0
J
strided_slice_4/stackConst*
dtype0*
valueB"        
L
strided_slice_4/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_4/stack_2Const*
dtype0*
valueB"      
�
strided_slice_4StridedSlicemul_13strided_slice_4/stackstrided_slice_4/stack_1strided_slice_4/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
O
mul_14MulStopGradient)critic/q/q2_encoding/extrinsic_q2/BiasAdd*
T0
J
strided_slice_5/stackConst*
dtype0*
valueB"        
L
strided_slice_5/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_5/stack_2Const*
dtype0*
valueB"      
�
strided_slice_5StridedSlicemul_14strided_slice_5/stackstrided_slice_5/stack_1strided_slice_5/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
A
Sum_6/reduction_indicesConst*
dtype0*
value	B :
\
Sum_6Sumstrided_slice_4Sum_6/reduction_indices*
T0*

Tidx0*
	keep_dims(
A
Sum_7/reduction_indicesConst*
dtype0*
value	B :
\
Sum_7Sumstrided_slice_5Sum_7/reduction_indices*
T0*

Tidx0*
	keep_dims(
9
Mean_6/inputPackSum_6*
N*
T0*

axis 
B
Mean_6/reduction_indicesConst*
dtype0*
value	B : 
\
Mean_6MeanMean_6/inputMean_6/reduction_indices*
T0*

Tidx0*
	keep_dims( 
9
Mean_7/inputPackSum_7*
N*
T0*

axis 
B
Mean_7/reduction_indicesConst*
dtype0*
value	B : 
\
Mean_7MeanMean_7/inputMean_7/reduction_indices*
T0*

Tidx0*
	keep_dims( 
=
ToFloatCastCast*

DstT0*

SrcT0*
Truncate( 
I
SquaredDifference_1SquaredDifferenceStopGradient_1Mean_6*
T0
4
mul_15MulToFloatSquaredDifference_1*
T0
<
Const_2Const*
dtype0*
valueB"       
E
Mean_8Meanmul_15Const_2*
T0*

Tidx0*
	keep_dims( 
5
mul_16/xConst*
dtype0*
valueB
 *   ?
(
mul_16Mulmul_16/xMean_8*
T0
?
	ToFloat_1CastCast*

DstT0*

SrcT0*
Truncate( 
I
SquaredDifference_2SquaredDifferenceStopGradient_1Mean_7*
T0
6
mul_17Mul	ToFloat_1SquaredDifference_2*
T0
<
Const_3Const*
dtype0*
valueB"       
E
Mean_9Meanmul_17Const_3*
T0*

Tidx0*
	keep_dims( 
5
mul_18/xConst*
dtype0*
valueB
 *   ?
(
mul_18Mulmul_18/xMean_9*
T0
C
ExpandDims_2/dimConst*
dtype0*
valueB :
���������
T
ExpandDims_2
ExpandDimscuriosity_rewardsExpandDims_2/dim*
T0*

Tdim0
3
mul_19MulVariable_1/read
ExpandDims*
T0
4
sub_1/xConst*
dtype0*
valueB
 *  �?
&
sub_1Subsub_1/xmul_19*
T0
5
mul_20/yConst*
dtype0*
valueB
 *�p}?
'
mul_20Mulsub_1mul_20/y*
T0
S
mul_21Mulmul_203target_network/critic/value/curiosity_value/BiasAdd*
T0
-
add_4AddV2ExpandDims_2mul_21*
T0
.
StopGradient_2StopGradientadd_4*
T0
O
mul_22MulStopGradient)critic/q/q1_encoding/curiosity_q1/BiasAdd*
T0
J
strided_slice_6/stackConst*
dtype0*
valueB"        
L
strided_slice_6/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_6/stack_2Const*
dtype0*
valueB"      
�
strided_slice_6StridedSlicemul_22strided_slice_6/stackstrided_slice_6/stack_1strided_slice_6/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
O
mul_23MulStopGradient)critic/q/q2_encoding/curiosity_q2/BiasAdd*
T0
J
strided_slice_7/stackConst*
dtype0*
valueB"        
L
strided_slice_7/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_7/stack_2Const*
dtype0*
valueB"      
�
strided_slice_7StridedSlicemul_23strided_slice_7/stackstrided_slice_7/stack_1strided_slice_7/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
A
Sum_8/reduction_indicesConst*
dtype0*
value	B :
\
Sum_8Sumstrided_slice_6Sum_8/reduction_indices*
T0*

Tidx0*
	keep_dims(
A
Sum_9/reduction_indicesConst*
dtype0*
value	B :
\
Sum_9Sumstrided_slice_7Sum_9/reduction_indices*
T0*

Tidx0*
	keep_dims(
:
Mean_10/inputPackSum_8*
N*
T0*

axis 
C
Mean_10/reduction_indicesConst*
dtype0*
value	B : 
_
Mean_10MeanMean_10/inputMean_10/reduction_indices*
T0*

Tidx0*
	keep_dims( 
:
Mean_11/inputPackSum_9*
N*
T0*

axis 
C
Mean_11/reduction_indicesConst*
dtype0*
value	B : 
_
Mean_11MeanMean_11/inputMean_11/reduction_indices*
T0*

Tidx0*
	keep_dims( 
?
	ToFloat_2CastCast*

DstT0*

SrcT0*
Truncate( 
J
SquaredDifference_3SquaredDifferenceStopGradient_2Mean_10*
T0
6
mul_24Mul	ToFloat_2SquaredDifference_3*
T0
<
Const_4Const*
dtype0*
valueB"       
F
Mean_12Meanmul_24Const_4*
T0*

Tidx0*
	keep_dims( 
5
mul_25/xConst*
dtype0*
valueB
 *   ?
)
mul_25Mulmul_25/xMean_12*
T0
?
	ToFloat_3CastCast*

DstT0*

SrcT0*
Truncate( 
J
SquaredDifference_4SquaredDifferenceStopGradient_2Mean_11*
T0
6
mul_26Mul	ToFloat_3SquaredDifference_4*
T0
<
Const_5Const*
dtype0*
valueB"       
F
Mean_13Meanmul_26Const_5*
T0*

Tidx0*
	keep_dims( 
5
mul_27/xConst*
dtype0*
valueB
 *   ?
)
mul_27Mulmul_27/xMean_13*
T0
A
Rank/packedPackmul_16mul_25*
N*
T0*

axis 
.
RankConst*
dtype0*
value	B :
5
range/startConst*
dtype0*
value	B : 
5
range/deltaConst*
dtype0*
value	B :
:
rangeRangerange/startRankrange/delta*

Tidx0
C
Mean_14/inputPackmul_16mul_25*
N*
T0*

axis 
K
Mean_14MeanMean_14/inputrange*
T0*

Tidx0*
	keep_dims( 
C
Rank_1/packedPackmul_18mul_27*
N*
T0*

axis 
0
Rank_1Const*
dtype0*
value	B :
7
range_1/startConst*
dtype0*
value	B : 
7
range_1/deltaConst*
dtype0*
value	B :
B
range_1Rangerange_1/startRank_1range_1/delta*

Tidx0
C
Mean_15/inputPackmul_18mul_27*
N*
T0*

axis 
M
Mean_15MeanMean_15/inputrange_1*
T0*

Tidx0*
	keep_dims( 
8
Const_6Const*
dtype0*
valueB*�j�
K
log_ent_coef/initial_valueConst*
dtype0*
valueB*�j�
\
log_ent_coef
VariableV2*
	container *
dtype0*
shape:*
shared_name 
�
log_ent_coef/AssignAssignlog_ent_coeflog_ent_coef/initial_value*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
U
log_ent_coef/readIdentitylog_ent_coef*
T0*
_class
loc:@log_ent_coef
(
Exp_1Explog_ent_coef/read*
T0
J
strided_slice_8/stackConst*
dtype0*
valueB"        
L
strided_slice_8/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_8/stack_2Const*
dtype0*
valueB"      
�
strided_slice_8StridedSlicemul_5strided_slice_8/stackstrided_slice_8/stack_1strided_slice_8/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
B
Sum_10/reduction_indicesConst*
dtype0*
value	B :
^
Sum_10Sumstrided_slice_8Sum_10/reduction_indices*
T0*

Tidx0*
	keep_dims(
4
add_5/yConst*
dtype0*
valueB
 *P�T?
(
add_5AddV2Sum_10add_5/y*
T0
4
stack_4Packadd_5*
N*
T0*

axis
?
	ToFloat_4CastCast*

DstT0*

SrcT0*
Truncate( 
0
StopGradient_3StopGradientstack_4*
T0
B
SqueezeSqueezeStopGradient_3*
T0*
squeeze_dims

2
mul_28Mullog_ent_coef/readSqueeze*
T0
C
Mean_16/reduction_indicesConst*
dtype0*
value	B :
X
Mean_16Meanmul_28Mean_16/reduction_indices*
T0*

Tidx0*
	keep_dims( 
*
mul_29Mul	ToFloat_4Mean_16*
T0
5
Const_7Const*
dtype0*
valueB: 
F
Mean_17Meanmul_29Const_7*
T0*

Tidx0*
	keep_dims( 

Neg_1NegMean_17*
T0
8
mul_30MulExpcritic/q/q1_encoding_1/Mean*
T0
J
strided_slice_9/stackConst*
dtype0*
valueB"        
L
strided_slice_9/stack_1Const*
dtype0*
valueB"    @   
L
strided_slice_9/stack_2Const*
dtype0*
valueB"      
�
strided_slice_9StridedSlicemul_30strided_slice_9/stackstrided_slice_9/stack_1strided_slice_9/stack_2*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
D
strided_slice_10/stackConst*
dtype0*
valueB: 
F
strided_slice_10/stack_1Const*
dtype0*
valueB:
F
strided_slice_10/stack_2Const*
dtype0*
valueB:
�
strided_slice_10StridedSliceExp_1strided_slice_10/stackstrided_slice_10/stack_1strided_slice_10/stack_2*
Index0*
T0*

begin_mask *
ellipsis_mask *
end_mask *
new_axis_mask *
shrink_axis_mask
9
mul_31Mulstrided_slice_10strided_slice_8*
T0
.
sub_2Submul_31strided_slice_9*
T0
B
Sum_11/reduction_indicesConst*
dtype0*
value	B :
T
Sum_11Sumsub_2Sum_11/reduction_indices*
T0*

Tidx0*
	keep_dims(
5
stack_5PackSum_11*
N*
T0*

axis 
?
	ToFloat_5CastCast*

DstT0*

SrcT0*
Truncate( 
:
	Squeeze_1Squeezestack_5*
T0*
squeeze_dims
 
,
mul_32Mul	ToFloat_5	Squeeze_1*
T0

Rank_2Rankmul_32*
T0
7
range_2/startConst*
dtype0*
value	B : 
7
range_2/deltaConst*
dtype0*
value	B :
B
range_2Rangerange_2/startRank_2range_2/delta*

Tidx0
F
Mean_18Meanmul_32range_2*
T0*

Tidx0*
	keep_dims( 
D
strided_slice_11/stackConst*
dtype0*
valueB: 
F
strided_slice_11/stack_1Const*
dtype0*
valueB:
F
strided_slice_11/stack_2Const*
dtype0*
valueB:
�
strided_slice_11StridedSliceExp_1strided_slice_11/stackstrided_slice_11/stack_1strided_slice_11/stack_2*
Index0*
T0*

begin_mask *
ellipsis_mask *
end_mask *
new_axis_mask *
shrink_axis_mask
9
mul_33Mulstrided_slice_11strided_slice_8*
T0
B
Sum_12/reduction_indicesConst*
dtype0*
value	B :
U
Sum_12Summul_33Sum_12/reduction_indices*
T0*

Tidx0*
	keep_dims(
5
stack_6PackSum_12*
N*
T0*

axis 
C
Mean_19/reduction_indicesConst*
dtype0*
value	B : 
Y
Mean_19Meanstack_6Mean_19/reduction_indices*
T0*

Tidx0*
	keep_dims( 
'
sub_3SubMinimumMean_19*
T0
.
StopGradient_4StopGradientsub_3*
T0
?
	ToFloat_6CastCast*

DstT0*

SrcT0*
Truncate( 
g
SquaredDifference_5SquaredDifference$critic/value/extrinsic_value/BiasAddStopGradient_4*
T0
6
mul_34Mul	ToFloat_6SquaredDifference_5*
T0
<
Const_8Const*
dtype0*
valueB"       
F
Mean_20Meanmul_34Const_8*
T0*

Tidx0*
	keep_dims( 
5
mul_35/xConst*
dtype0*
valueB
 *   ?
)
mul_35Mulmul_35/xMean_20*
T0
C
Mean_21/reduction_indicesConst*
dtype0*
value	B : 
Y
Mean_21Meanstack_6Mean_21/reduction_indices*
T0*

Tidx0*
	keep_dims( 
)
sub_4Sub	Minimum_1Mean_21*
T0
.
StopGradient_5StopGradientsub_4*
T0
?
	ToFloat_7CastCast*

DstT0*

SrcT0*
Truncate( 
g
SquaredDifference_6SquaredDifference$critic/value/curiosity_value/BiasAddStopGradient_5*
T0
6
mul_36Mul	ToFloat_7SquaredDifference_6*
T0
<
Const_9Const*
dtype0*
valueB"       
F
Mean_22Meanmul_36Const_9*
T0*

Tidx0*
	keep_dims( 
5
mul_37/xConst*
dtype0*
valueB
 *   ?
)
mul_37Mulmul_37/xMean_22*
T0
C
Rank_3/packedPackmul_35mul_37*
N*
T0*

axis 
0
Rank_3Const*
dtype0*
value	B :
7
range_3/startConst*
dtype0*
value	B : 
7
range_3/deltaConst*
dtype0*
value	B :
B
range_3Rangerange_3/startRank_3range_3/delta*

Tidx0
C
Mean_23/inputPackmul_35mul_37*
N*
T0*

axis 
M
Mean_23MeanMean_23/inputrange_3*
T0*

Tidx0*
	keep_dims( 
)
add_6AddV2Mean_14Mean_15*
T0
'
add_7AddV2add_6Mean_23*
T0
5
mul_38/xConst*
dtype0*
valueB
 *R�~?
Z
mul_38Mulmul_38/x8target_network/critic/value/encoder/hidden_0/kernel/read*
T0
5
mul_39/xConst*
dtype0*
valueB
 *
ף;
K
mul_39Mulmul_39/x)critic/value/encoder/hidden_0/kernel/read*
T0
'
add_8AddV2mul_38mul_39*
T0
�
Assign_3Assign3target_network/critic/value/encoder/hidden_0/kerneladd_8*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
5
mul_40/xConst*
dtype0*
valueB
 *R�~?
X
mul_40Mulmul_40/x6target_network/critic/value/encoder/hidden_0/bias/read*
T0
5
mul_41/xConst*
dtype0*
valueB
 *
ף;
I
mul_41Mulmul_41/x'critic/value/encoder/hidden_0/bias/read*
T0
'
add_9AddV2mul_40mul_41*
T0
�
Assign_4Assign1target_network/critic/value/encoder/hidden_0/biasadd_9*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
5
mul_42/xConst*
dtype0*
valueB
 *R�~?
Z
mul_42Mulmul_42/x8target_network/critic/value/encoder/hidden_1/kernel/read*
T0
5
mul_43/xConst*
dtype0*
valueB
 *
ף;
K
mul_43Mulmul_43/x)critic/value/encoder/hidden_1/kernel/read*
T0
(
add_10AddV2mul_42mul_43*
T0
�
Assign_5Assign3target_network/critic/value/encoder/hidden_1/kerneladd_10*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
5
mul_44/xConst*
dtype0*
valueB
 *R�~?
X
mul_44Mulmul_44/x6target_network/critic/value/encoder/hidden_1/bias/read*
T0
5
mul_45/xConst*
dtype0*
valueB
 *
ף;
I
mul_45Mulmul_45/x'critic/value/encoder/hidden_1/bias/read*
T0
(
add_11AddV2mul_44mul_45*
T0
�
Assign_6Assign1target_network/critic/value/encoder/hidden_1/biasadd_11*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
5
mul_46/xConst*
dtype0*
valueB
 *R�~?
Z
mul_46Mulmul_46/x8target_network/critic/value/encoder/hidden_2/kernel/read*
T0
5
mul_47/xConst*
dtype0*
valueB
 *
ף;
K
mul_47Mulmul_47/x)critic/value/encoder/hidden_2/kernel/read*
T0
(
add_12AddV2mul_46mul_47*
T0
�
Assign_7Assign3target_network/critic/value/encoder/hidden_2/kerneladd_12*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
5
mul_48/xConst*
dtype0*
valueB
 *R�~?
X
mul_48Mulmul_48/x6target_network/critic/value/encoder/hidden_2/bias/read*
T0
5
mul_49/xConst*
dtype0*
valueB
 *
ף;
I
mul_49Mulmul_49/x'critic/value/encoder/hidden_2/bias/read*
T0
(
add_13AddV2mul_48mul_49*
T0
�
Assign_8Assign1target_network/critic/value/encoder/hidden_2/biasadd_13*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
5
mul_50/xConst*
dtype0*
valueB
 *R�~?
Z
mul_50Mulmul_50/x8target_network/critic/value/encoder/hidden_3/kernel/read*
T0
5
mul_51/xConst*
dtype0*
valueB
 *
ף;
K
mul_51Mulmul_51/x)critic/value/encoder/hidden_3/kernel/read*
T0
(
add_14AddV2mul_50mul_51*
T0
�
Assign_9Assign3target_network/critic/value/encoder/hidden_3/kerneladd_14*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
5
mul_52/xConst*
dtype0*
valueB
 *R�~?
X
mul_52Mulmul_52/x6target_network/critic/value/encoder/hidden_3/bias/read*
T0
5
mul_53/xConst*
dtype0*
valueB
 *
ף;
I
mul_53Mulmul_53/x'critic/value/encoder/hidden_3/bias/read*
T0
(
add_15AddV2mul_52mul_53*
T0
�
	Assign_10Assign1target_network/critic/value/encoder/hidden_3/biasadd_15*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
5
mul_54/xConst*
dtype0*
valueB
 *R�~?
Y
mul_54Mulmul_54/x7target_network/critic/value/extrinsic_value/kernel/read*
T0
5
mul_55/xConst*
dtype0*
valueB
 *
ף;
J
mul_55Mulmul_55/x(critic/value/extrinsic_value/kernel/read*
T0
(
add_16AddV2mul_54mul_55*
T0
�
	Assign_11Assign2target_network/critic/value/extrinsic_value/kerneladd_16*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
5
mul_56/xConst*
dtype0*
valueB
 *R�~?
W
mul_56Mulmul_56/x5target_network/critic/value/extrinsic_value/bias/read*
T0
5
mul_57/xConst*
dtype0*
valueB
 *
ף;
H
mul_57Mulmul_57/x&critic/value/extrinsic_value/bias/read*
T0
(
add_17AddV2mul_56mul_57*
T0
�
	Assign_12Assign0target_network/critic/value/extrinsic_value/biasadd_17*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
5
mul_58/xConst*
dtype0*
valueB
 *R�~?
Y
mul_58Mulmul_58/x7target_network/critic/value/curiosity_value/kernel/read*
T0
5
mul_59/xConst*
dtype0*
valueB
 *
ף;
J
mul_59Mulmul_59/x(critic/value/curiosity_value/kernel/read*
T0
(
add_18AddV2mul_58mul_59*
T0
�
	Assign_13Assign2target_network/critic/value/curiosity_value/kerneladd_18*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
5
mul_60/xConst*
dtype0*
valueB
 *R�~?
W
mul_60Mulmul_60/x5target_network/critic/value/curiosity_value/bias/read*
T0
5
mul_61/xConst*
dtype0*
valueB
 *
ף;
H
mul_61Mulmul_61/x&critic/value/curiosity_value/bias/read*
T0
(
add_19AddV2mul_60mul_61*
T0
�
	Assign_14Assign0target_network/critic/value/curiosity_value/biasadd_19*
T0*C
_class9
75loc:@target_network/critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
	Assign_15Assign3target_network/critic/value/encoder/hidden_0/kernel)critic/value/encoder/hidden_0/kernel/read*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
	Assign_16Assign1target_network/critic/value/encoder/hidden_0/bias'critic/value/encoder/hidden_0/bias/read*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
	Assign_17Assign3target_network/critic/value/encoder/hidden_1/kernel)critic/value/encoder/hidden_1/kernel/read*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
	Assign_18Assign1target_network/critic/value/encoder/hidden_1/bias'critic/value/encoder/hidden_1/bias/read*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
	Assign_19Assign3target_network/critic/value/encoder/hidden_2/kernel)critic/value/encoder/hidden_2/kernel/read*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
	Assign_20Assign1target_network/critic/value/encoder/hidden_2/bias'critic/value/encoder/hidden_2/bias/read*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
	Assign_21Assign3target_network/critic/value/encoder/hidden_3/kernel)critic/value/encoder/hidden_3/kernel/read*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
	Assign_22Assign1target_network/critic/value/encoder/hidden_3/bias'critic/value/encoder/hidden_3/bias/read*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
	Assign_23Assign2target_network/critic/value/extrinsic_value/kernel(critic/value/extrinsic_value/kernel/read*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
	Assign_24Assign0target_network/critic/value/extrinsic_value/bias&critic/value/extrinsic_value/bias/read*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
	Assign_25Assign2target_network/critic/value/curiosity_value/kernel(critic/value/curiosity_value/kernel/read*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
	Assign_26Assign0target_network/critic/value/curiosity_value/bias&critic/value/curiosity_value/bias/read*
T0*C
_class9
75loc:@target_network/critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
:
gradients_1/ShapeConst*
dtype0*
valueB 
B
gradients_1/grad_ys_0Const*
dtype0*
valueB
 *  �?
]
gradients_1/FillFillgradients_1/Shapegradients_1/grad_ys_0*
T0*

index_type0
H
gradients_1/Mean_18_grad/ShapeShapemul_32*
T0*
out_type0
�
gradients_1/Mean_18_grad/SizeSizegradients_1/Mean_18_grad/Shape*
T0*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape*
out_type0
�
gradients_1/Mean_18_grad/addAddV2range_2gradients_1/Mean_18_grad/Size*
T0*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape
�
gradients_1/Mean_18_grad/modFloorModgradients_1/Mean_18_grad/addgradients_1/Mean_18_grad/Size*
T0*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape
�
 gradients_1/Mean_18_grad/Shape_1Shapegradients_1/Mean_18_grad/mod*
T0*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape*
out_type0
�
$gradients_1/Mean_18_grad/range/startConst*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape*
dtype0*
value	B : 
�
$gradients_1/Mean_18_grad/range/deltaConst*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape*
dtype0*
value	B :
�
gradients_1/Mean_18_grad/rangeRange$gradients_1/Mean_18_grad/range/startgradients_1/Mean_18_grad/Size$gradients_1/Mean_18_grad/range/delta*

Tidx0*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape
�
#gradients_1/Mean_18_grad/Fill/valueConst*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape*
dtype0*
value	B :
�
gradients_1/Mean_18_grad/FillFill gradients_1/Mean_18_grad/Shape_1#gradients_1/Mean_18_grad/Fill/value*
T0*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape*

index_type0
�
&gradients_1/Mean_18_grad/DynamicStitchDynamicStitchgradients_1/Mean_18_grad/rangegradients_1/Mean_18_grad/modgradients_1/Mean_18_grad/Shapegradients_1/Mean_18_grad/Fill*
N*
T0*1
_class'
%#loc:@gradients_1/Mean_18_grad/Shape
|
 gradients_1/Mean_18_grad/ReshapeReshapegradients_1/Fill&gradients_1/Mean_18_grad/DynamicStitch*
T0*
Tshape0
�
$gradients_1/Mean_18_grad/BroadcastToBroadcastTo gradients_1/Mean_18_grad/Reshapegradients_1/Mean_18_grad/Shape*
T0*

Tidx0
J
 gradients_1/Mean_18_grad/Shape_2Shapemul_32*
T0*
out_type0
I
 gradients_1/Mean_18_grad/Shape_3Const*
dtype0*
valueB 
L
gradients_1/Mean_18_grad/ConstConst*
dtype0*
valueB: 
�
gradients_1/Mean_18_grad/ProdProd gradients_1/Mean_18_grad/Shape_2gradients_1/Mean_18_grad/Const*
T0*

Tidx0*
	keep_dims( 
N
 gradients_1/Mean_18_grad/Const_1Const*
dtype0*
valueB: 
�
gradients_1/Mean_18_grad/Prod_1Prod gradients_1/Mean_18_grad/Shape_3 gradients_1/Mean_18_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
L
"gradients_1/Mean_18_grad/Maximum/yConst*
dtype0*
value	B :
y
 gradients_1/Mean_18_grad/MaximumMaximumgradients_1/Mean_18_grad/Prod_1"gradients_1/Mean_18_grad/Maximum/y*
T0
w
!gradients_1/Mean_18_grad/floordivFloorDivgradients_1/Mean_18_grad/Prod gradients_1/Mean_18_grad/Maximum*
T0
p
gradients_1/Mean_18_grad/CastCast!gradients_1/Mean_18_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
y
 gradients_1/Mean_18_grad/truedivRealDiv$gradients_1/Mean_18_grad/BroadcastTogradients_1/Mean_18_grad/Cast*
T0
J
gradients_1/mul_32_grad/ShapeShape	ToFloat_5*
T0*
out_type0
L
gradients_1/mul_32_grad/Shape_1Shape	Squeeze_1*
T0*
out_type0
�
-gradients_1/mul_32_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_32_grad/Shapegradients_1/mul_32_grad/Shape_1*
T0
X
gradients_1/mul_32_grad/MulMul gradients_1/Mean_18_grad/truediv	Squeeze_1*
T0
�
gradients_1/mul_32_grad/SumSumgradients_1/mul_32_grad/Mul-gradients_1/mul_32_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_1/mul_32_grad/ReshapeReshapegradients_1/mul_32_grad/Sumgradients_1/mul_32_grad/Shape*
T0*
Tshape0
Z
gradients_1/mul_32_grad/Mul_1Mul	ToFloat_5 gradients_1/Mean_18_grad/truediv*
T0
�
gradients_1/mul_32_grad/Sum_1Sumgradients_1/mul_32_grad/Mul_1/gradients_1/mul_32_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_1/mul_32_grad/Reshape_1Reshapegradients_1/mul_32_grad/Sum_1gradients_1/mul_32_grad/Shape_1*
T0*
Tshape0
v
(gradients_1/mul_32_grad/tuple/group_depsNoOp ^gradients_1/mul_32_grad/Reshape"^gradients_1/mul_32_grad/Reshape_1
�
0gradients_1/mul_32_grad/tuple/control_dependencyIdentitygradients_1/mul_32_grad/Reshape)^gradients_1/mul_32_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_32_grad/Reshape
�
2gradients_1/mul_32_grad/tuple/control_dependency_1Identity!gradients_1/mul_32_grad/Reshape_1)^gradients_1/mul_32_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_32_grad/Reshape_1
K
 gradients_1/Squeeze_1_grad/ShapeShapestack_5*
T0*
out_type0
�
"gradients_1/Squeeze_1_grad/ReshapeReshape2gradients_1/mul_32_grad/tuple/control_dependency_1 gradients_1/Squeeze_1_grad/Shape*
T0*
Tshape0
n
 gradients_1/stack_5_grad/unstackUnpack"gradients_1/Squeeze_1_grad/Reshape*
T0*

axis *	
num
F
gradients_1/Sum_11_grad/ShapeShapesub_2*
T0*
out_type0
�
#gradients_1/Sum_11_grad/BroadcastToBroadcastTo gradients_1/stack_5_grad/unstackgradients_1/Sum_11_grad/Shape*
T0*

Tidx0
F
gradients_1/sub_2_grad/ShapeShapemul_31*
T0*
out_type0
Q
gradients_1/sub_2_grad/Shape_1Shapestrided_slice_9*
T0*
out_type0
�
,gradients_1/sub_2_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/sub_2_grad/Shapegradients_1/sub_2_grad/Shape_1*
T0
�
gradients_1/sub_2_grad/SumSum#gradients_1/Sum_11_grad/BroadcastTo,gradients_1/sub_2_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
z
gradients_1/sub_2_grad/ReshapeReshapegradients_1/sub_2_grad/Sumgradients_1/sub_2_grad/Shape*
T0*
Tshape0
O
gradients_1/sub_2_grad/NegNeg#gradients_1/Sum_11_grad/BroadcastTo*
T0
�
gradients_1/sub_2_grad/Sum_1Sumgradients_1/sub_2_grad/Neg.gradients_1/sub_2_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
 gradients_1/sub_2_grad/Reshape_1Reshapegradients_1/sub_2_grad/Sum_1gradients_1/sub_2_grad/Shape_1*
T0*
Tshape0
s
'gradients_1/sub_2_grad/tuple/group_depsNoOp^gradients_1/sub_2_grad/Reshape!^gradients_1/sub_2_grad/Reshape_1
�
/gradients_1/sub_2_grad/tuple/control_dependencyIdentitygradients_1/sub_2_grad/Reshape(^gradients_1/sub_2_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients_1/sub_2_grad/Reshape
�
1gradients_1/sub_2_grad/tuple/control_dependency_1Identity gradients_1/sub_2_grad/Reshape_1(^gradients_1/sub_2_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_1/sub_2_grad/Reshape_1
Q
gradients_1/mul_31_grad/ShapeShapestrided_slice_10*
T0*
out_type0
R
gradients_1/mul_31_grad/Shape_1Shapestrided_slice_8*
T0*
out_type0
�
-gradients_1/mul_31_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_31_grad/Shapegradients_1/mul_31_grad/Shape_1*
T0
m
gradients_1/mul_31_grad/MulMul/gradients_1/sub_2_grad/tuple/control_dependencystrided_slice_8*
T0
�
gradients_1/mul_31_grad/SumSumgradients_1/mul_31_grad/Mul-gradients_1/mul_31_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_1/mul_31_grad/ReshapeReshapegradients_1/mul_31_grad/Sumgradients_1/mul_31_grad/Shape*
T0*
Tshape0
p
gradients_1/mul_31_grad/Mul_1Mulstrided_slice_10/gradients_1/sub_2_grad/tuple/control_dependency*
T0
�
gradients_1/mul_31_grad/Sum_1Sumgradients_1/mul_31_grad/Mul_1/gradients_1/mul_31_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_1/mul_31_grad/Reshape_1Reshapegradients_1/mul_31_grad/Sum_1gradients_1/mul_31_grad/Shape_1*
T0*
Tshape0
v
(gradients_1/mul_31_grad/tuple/group_depsNoOp ^gradients_1/mul_31_grad/Reshape"^gradients_1/mul_31_grad/Reshape_1
�
0gradients_1/mul_31_grad/tuple/control_dependencyIdentitygradients_1/mul_31_grad/Reshape)^gradients_1/mul_31_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_31_grad/Reshape
�
2gradients_1/mul_31_grad/tuple/control_dependency_1Identity!gradients_1/mul_31_grad/Reshape_1)^gradients_1/mul_31_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_31_grad/Reshape_1
P
&gradients_1/strided_slice_9_grad/ShapeShapemul_30*
T0*
out_type0
l
7gradients_1/strided_slice_9_grad/StridedSliceGrad/beginConst*
dtype0*
valueB"        
j
5gradients_1/strided_slice_9_grad/StridedSliceGrad/endConst*
dtype0*
valueB"    @   
n
9gradients_1/strided_slice_9_grad/StridedSliceGrad/stridesConst*
dtype0*
valueB"      
�
1gradients_1/strided_slice_9_grad/StridedSliceGradStridedSliceGrad&gradients_1/strided_slice_9_grad/Shape7gradients_1/strided_slice_9_grad/StridedSliceGrad/begin5gradients_1/strided_slice_9_grad/StridedSliceGrad/end9gradients_1/strided_slice_9_grad/StridedSliceGrad/strides1gradients_1/sub_2_grad/tuple/control_dependency_1*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
O
&gradients_1/strided_slice_8_grad/ShapeShapemul_5*
T0*
out_type0
l
7gradients_1/strided_slice_8_grad/StridedSliceGrad/beginConst*
dtype0*
valueB"        
j
5gradients_1/strided_slice_8_grad/StridedSliceGrad/endConst*
dtype0*
valueB"    @   
n
9gradients_1/strided_slice_8_grad/StridedSliceGrad/stridesConst*
dtype0*
valueB"      
�
1gradients_1/strided_slice_8_grad/StridedSliceGradStridedSliceGrad&gradients_1/strided_slice_8_grad/Shape7gradients_1/strided_slice_8_grad/StridedSliceGrad/begin5gradients_1/strided_slice_8_grad/StridedSliceGrad/end9gradients_1/strided_slice_8_grad/StridedSliceGrad/strides2gradients_1/mul_31_grad/tuple/control_dependency_1*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
D
gradients_1/mul_30_grad/ShapeShapeExp*
T0*
out_type0
^
gradients_1/mul_30_grad/Shape_1Shapecritic/q/q1_encoding_1/Mean*
T0*
out_type0
�
-gradients_1/mul_30_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_30_grad/Shapegradients_1/mul_30_grad/Shape_1*
T0
{
gradients_1/mul_30_grad/MulMul1gradients_1/strided_slice_9_grad/StridedSliceGradcritic/q/q1_encoding_1/Mean*
T0
�
gradients_1/mul_30_grad/SumSumgradients_1/mul_30_grad/Mul-gradients_1/mul_30_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_1/mul_30_grad/ReshapeReshapegradients_1/mul_30_grad/Sumgradients_1/mul_30_grad/Shape*
T0*
Tshape0
e
gradients_1/mul_30_grad/Mul_1MulExp1gradients_1/strided_slice_9_grad/StridedSliceGrad*
T0
�
gradients_1/mul_30_grad/Sum_1Sumgradients_1/mul_30_grad/Mul_1/gradients_1/mul_30_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_1/mul_30_grad/Reshape_1Reshapegradients_1/mul_30_grad/Sum_1gradients_1/mul_30_grad/Shape_1*
T0*
Tshape0
v
(gradients_1/mul_30_grad/tuple/group_depsNoOp ^gradients_1/mul_30_grad/Reshape"^gradients_1/mul_30_grad/Reshape_1
�
0gradients_1/mul_30_grad/tuple/control_dependencyIdentitygradients_1/mul_30_grad/Reshape)^gradients_1/mul_30_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_30_grad/Reshape
�
2gradients_1/mul_30_grad/tuple/control_dependency_1Identity!gradients_1/mul_30_grad/Reshape_1)^gradients_1/mul_30_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_30_grad/Reshape_1
C
gradients_1/mul_5_grad/ShapeShapeExp*
T0*
out_type0
H
gradients_1/mul_5_grad/Shape_1Shapeaction*
T0*
out_type0
�
,gradients_1/mul_5_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_5_grad/Shapegradients_1/mul_5_grad/Shape_1*
T0
e
gradients_1/mul_5_grad/MulMul1gradients_1/strided_slice_8_grad/StridedSliceGradaction*
T0
�
gradients_1/mul_5_grad/SumSumgradients_1/mul_5_grad/Mul,gradients_1/mul_5_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
z
gradients_1/mul_5_grad/ReshapeReshapegradients_1/mul_5_grad/Sumgradients_1/mul_5_grad/Shape*
T0*
Tshape0
d
gradients_1/mul_5_grad/Mul_1MulExp1gradients_1/strided_slice_8_grad/StridedSliceGrad*
T0
�
gradients_1/mul_5_grad/Sum_1Sumgradients_1/mul_5_grad/Mul_1.gradients_1/mul_5_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
 gradients_1/mul_5_grad/Reshape_1Reshapegradients_1/mul_5_grad/Sum_1gradients_1/mul_5_grad/Shape_1*
T0*
Tshape0
s
'gradients_1/mul_5_grad/tuple/group_depsNoOp^gradients_1/mul_5_grad/Reshape!^gradients_1/mul_5_grad/Reshape_1
�
/gradients_1/mul_5_grad/tuple/control_dependencyIdentitygradients_1/mul_5_grad/Reshape(^gradients_1/mul_5_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients_1/mul_5_grad/Reshape
�
1gradients_1/mul_5_grad/tuple/control_dependency_1Identity gradients_1/mul_5_grad/Reshape_1(^gradients_1/mul_5_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_1/mul_5_grad/Reshape_1
�
gradients_1/AddNAddN0gradients_1/mul_30_grad/tuple/control_dependency/gradients_1/mul_5_grad/tuple/control_dependency*
N*
T0*2
_class(
&$loc:@gradients_1/mul_30_grad/Reshape
?
gradients_1/Exp_grad/mulMulgradients_1/AddNExp*
T0
�
gradients_1/AddN_1AddN1gradients_1/mul_5_grad/tuple/control_dependency_1gradients_1/Exp_grad/mul*
N*
T0*3
_class)
'%loc:@gradients_1/mul_5_grad/Reshape_1
f
*gradients_1/policy_1/Log_1_grad/Reciprocal
Reciprocalpolicy_1/add_2^gradients_1/AddN_1*
T0
s
#gradients_1/policy_1/Log_1_grad/mulMulgradients_1/AddN_1*gradients_1/policy_1/Log_1_grad/Reciprocal*
T0
Y
%gradients_1/policy_1/add_2_grad/ShapeShapepolicy_1/truediv*
T0*
out_type0
[
'gradients_1/policy_1/add_2_grad/Shape_1Shapepolicy_1/add_2/y*
T0*
out_type0
�
5gradients_1/policy_1/add_2_grad/BroadcastGradientArgsBroadcastGradientArgs%gradients_1/policy_1/add_2_grad/Shape'gradients_1/policy_1/add_2_grad/Shape_1*
T0
�
#gradients_1/policy_1/add_2_grad/SumSum#gradients_1/policy_1/Log_1_grad/mul5gradients_1/policy_1/add_2_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
'gradients_1/policy_1/add_2_grad/ReshapeReshape#gradients_1/policy_1/add_2_grad/Sum%gradients_1/policy_1/add_2_grad/Shape*
T0*
Tshape0
�
%gradients_1/policy_1/add_2_grad/Sum_1Sum#gradients_1/policy_1/Log_1_grad/mul7gradients_1/policy_1/add_2_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
)gradients_1/policy_1/add_2_grad/Reshape_1Reshape%gradients_1/policy_1/add_2_grad/Sum_1'gradients_1/policy_1/add_2_grad/Shape_1*
T0*
Tshape0
�
0gradients_1/policy_1/add_2_grad/tuple/group_depsNoOp(^gradients_1/policy_1/add_2_grad/Reshape*^gradients_1/policy_1/add_2_grad/Reshape_1
�
8gradients_1/policy_1/add_2_grad/tuple/control_dependencyIdentity'gradients_1/policy_1/add_2_grad/Reshape1^gradients_1/policy_1/add_2_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients_1/policy_1/add_2_grad/Reshape
�
:gradients_1/policy_1/add_2_grad/tuple/control_dependency_1Identity)gradients_1/policy_1/add_2_grad/Reshape_11^gradients_1/policy_1/add_2_grad/tuple/group_deps*
T0*<
_class2
0.loc:@gradients_1/policy_1/add_2_grad/Reshape_1
W
'gradients_1/policy_1/truediv_grad/ShapeShapepolicy_1/Mul*
T0*
out_type0
Y
)gradients_1/policy_1/truediv_grad/Shape_1Shapepolicy_1/Sum*
T0*
out_type0
�
7gradients_1/policy_1/truediv_grad/BroadcastGradientArgsBroadcastGradientArgs'gradients_1/policy_1/truediv_grad/Shape)gradients_1/policy_1/truediv_grad/Shape_1*
T0
�
)gradients_1/policy_1/truediv_grad/RealDivRealDiv8gradients_1/policy_1/add_2_grad/tuple/control_dependencypolicy_1/Sum*
T0
�
%gradients_1/policy_1/truediv_grad/SumSum)gradients_1/policy_1/truediv_grad/RealDiv7gradients_1/policy_1/truediv_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
)gradients_1/policy_1/truediv_grad/ReshapeReshape%gradients_1/policy_1/truediv_grad/Sum'gradients_1/policy_1/truediv_grad/Shape*
T0*
Tshape0
C
%gradients_1/policy_1/truediv_grad/NegNegpolicy_1/Mul*
T0
t
+gradients_1/policy_1/truediv_grad/RealDiv_1RealDiv%gradients_1/policy_1/truediv_grad/Negpolicy_1/Sum*
T0
z
+gradients_1/policy_1/truediv_grad/RealDiv_2RealDiv+gradients_1/policy_1/truediv_grad/RealDiv_1policy_1/Sum*
T0
�
%gradients_1/policy_1/truediv_grad/mulMul8gradients_1/policy_1/add_2_grad/tuple/control_dependency+gradients_1/policy_1/truediv_grad/RealDiv_2*
T0
�
'gradients_1/policy_1/truediv_grad/Sum_1Sum%gradients_1/policy_1/truediv_grad/mul9gradients_1/policy_1/truediv_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
+gradients_1/policy_1/truediv_grad/Reshape_1Reshape'gradients_1/policy_1/truediv_grad/Sum_1)gradients_1/policy_1/truediv_grad/Shape_1*
T0*
Tshape0
�
2gradients_1/policy_1/truediv_grad/tuple/group_depsNoOp*^gradients_1/policy_1/truediv_grad/Reshape,^gradients_1/policy_1/truediv_grad/Reshape_1
�
:gradients_1/policy_1/truediv_grad/tuple/control_dependencyIdentity)gradients_1/policy_1/truediv_grad/Reshape3^gradients_1/policy_1/truediv_grad/tuple/group_deps*
T0*<
_class2
0.loc:@gradients_1/policy_1/truediv_grad/Reshape
�
<gradients_1/policy_1/truediv_grad/tuple/control_dependency_1Identity+gradients_1/policy_1/truediv_grad/Reshape_13^gradients_1/policy_1/truediv_grad/tuple/group_deps*
T0*>
_class4
20loc:@gradients_1/policy_1/truediv_grad/Reshape_1
S
#gradients_1/policy_1/Sum_grad/ShapeShapepolicy_1/Mul*
T0*
out_type0
�
)gradients_1/policy_1/Sum_grad/BroadcastToBroadcastTo<gradients_1/policy_1/truediv_grad/tuple/control_dependency_1#gradients_1/policy_1/Sum_grad/Shape*
T0*

Tidx0
�
gradients_1/AddN_2AddN:gradients_1/policy_1/truediv_grad/tuple/control_dependency)gradients_1/policy_1/Sum_grad/BroadcastTo*
N*
T0*<
_class2
0.loc:@gradients_1/policy_1/truediv_grad/Reshape
S
#gradients_1/policy_1/Mul_grad/ShapeShapepolicy_1/add*
T0*
out_type0
_
%gradients_1/policy_1/Mul_grad/Shape_1Shapepolicy_1/strided_slice*
T0*
out_type0
�
3gradients_1/policy_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients_1/policy_1/Mul_grad/Shape%gradients_1/policy_1/Mul_grad/Shape_1*
T0
]
!gradients_1/policy_1/Mul_grad/MulMulgradients_1/AddN_2policy_1/strided_slice*
T0
�
!gradients_1/policy_1/Mul_grad/SumSum!gradients_1/policy_1/Mul_grad/Mul3gradients_1/policy_1/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
%gradients_1/policy_1/Mul_grad/ReshapeReshape!gradients_1/policy_1/Mul_grad/Sum#gradients_1/policy_1/Mul_grad/Shape*
T0*
Tshape0
U
#gradients_1/policy_1/Mul_grad/Mul_1Mulpolicy_1/addgradients_1/AddN_2*
T0
�
#gradients_1/policy_1/Mul_grad/Sum_1Sum#gradients_1/policy_1/Mul_grad/Mul_15gradients_1/policy_1/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
'gradients_1/policy_1/Mul_grad/Reshape_1Reshape#gradients_1/policy_1/Mul_grad/Sum_1%gradients_1/policy_1/Mul_grad/Shape_1*
T0*
Tshape0
�
.gradients_1/policy_1/Mul_grad/tuple/group_depsNoOp&^gradients_1/policy_1/Mul_grad/Reshape(^gradients_1/policy_1/Mul_grad/Reshape_1
�
6gradients_1/policy_1/Mul_grad/tuple/control_dependencyIdentity%gradients_1/policy_1/Mul_grad/Reshape/^gradients_1/policy_1/Mul_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/policy_1/Mul_grad/Reshape
�
8gradients_1/policy_1/Mul_grad/tuple/control_dependency_1Identity'gradients_1/policy_1/Mul_grad/Reshape_1/^gradients_1/policy_1/Mul_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients_1/policy_1/Mul_grad/Reshape_1
W
#gradients_1/policy_1/add_grad/ShapeShapepolicy_1/Softmax*
T0*
out_type0
W
%gradients_1/policy_1/add_grad/Shape_1Shapepolicy_1/add/y*
T0*
out_type0
�
3gradients_1/policy_1/add_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients_1/policy_1/add_grad/Shape%gradients_1/policy_1/add_grad/Shape_1*
T0
�
!gradients_1/policy_1/add_grad/SumSum6gradients_1/policy_1/Mul_grad/tuple/control_dependency3gradients_1/policy_1/add_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
%gradients_1/policy_1/add_grad/ReshapeReshape!gradients_1/policy_1/add_grad/Sum#gradients_1/policy_1/add_grad/Shape*
T0*
Tshape0
�
#gradients_1/policy_1/add_grad/Sum_1Sum6gradients_1/policy_1/Mul_grad/tuple/control_dependency5gradients_1/policy_1/add_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
'gradients_1/policy_1/add_grad/Reshape_1Reshape#gradients_1/policy_1/add_grad/Sum_1%gradients_1/policy_1/add_grad/Shape_1*
T0*
Tshape0
�
.gradients_1/policy_1/add_grad/tuple/group_depsNoOp&^gradients_1/policy_1/add_grad/Reshape(^gradients_1/policy_1/add_grad/Reshape_1
�
6gradients_1/policy_1/add_grad/tuple/control_dependencyIdentity%gradients_1/policy_1/add_grad/Reshape/^gradients_1/policy_1/add_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/policy_1/add_grad/Reshape
�
8gradients_1/policy_1/add_grad/tuple/control_dependency_1Identity'gradients_1/policy_1/add_grad/Reshape_1/^gradients_1/policy_1/add_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients_1/policy_1/add_grad/Reshape_1

%gradients_1/policy_1/Softmax_grad/mulMul6gradients_1/policy_1/add_grad/tuple/control_dependencypolicy_1/Softmax*
T0
j
7gradients_1/policy_1/Softmax_grad/Sum/reduction_indicesConst*
dtype0*
valueB :
���������
�
%gradients_1/policy_1/Softmax_grad/SumSum%gradients_1/policy_1/Softmax_grad/mul7gradients_1/policy_1/Softmax_grad/Sum/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
%gradients_1/policy_1/Softmax_grad/subSub6gradients_1/policy_1/add_grad/tuple/control_dependency%gradients_1/policy_1/Softmax_grad/Sum*
T0
p
'gradients_1/policy_1/Softmax_grad/mul_1Mul%gradients_1/policy_1/Softmax_grad/subpolicy_1/Softmax*
T0
�
-gradients_1/policy_1/dense/MatMul_grad/MatMulMatMul'gradients_1/policy_1/Softmax_grad/mul_1policy/dense/kernel/read*
T0*
transpose_a( *
transpose_b(
�
/gradients_1/policy_1/dense/MatMul_grad/MatMul_1MatMul policy/main_graph_0/hidden_3/Mul'gradients_1/policy_1/Softmax_grad/mul_1*
T0*
transpose_a(*
transpose_b( 
�
7gradients_1/policy_1/dense/MatMul_grad/tuple/group_depsNoOp.^gradients_1/policy_1/dense/MatMul_grad/MatMul0^gradients_1/policy_1/dense/MatMul_grad/MatMul_1
�
?gradients_1/policy_1/dense/MatMul_grad/tuple/control_dependencyIdentity-gradients_1/policy_1/dense/MatMul_grad/MatMul8^gradients_1/policy_1/dense/MatMul_grad/tuple/group_deps*
T0*@
_class6
42loc:@gradients_1/policy_1/dense/MatMul_grad/MatMul
�
Agradients_1/policy_1/dense/MatMul_grad/tuple/control_dependency_1Identity/gradients_1/policy_1/dense/MatMul_grad/MatMul_18^gradients_1/policy_1/dense/MatMul_grad/tuple/group_deps*
T0*B
_class8
64loc:@gradients_1/policy_1/dense/MatMul_grad/MatMul_1

7gradients_1/policy/main_graph_0/hidden_3/Mul_grad/ShapeShape$policy/main_graph_0/hidden_3/BiasAdd*
T0*
out_type0
�
9gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Shape_1Shape$policy/main_graph_0/hidden_3/Sigmoid*
T0*
out_type0
�
Ggradients_1/policy/main_graph_0/hidden_3/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Shape9gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Shape_1*
T0
�
5gradients_1/policy/main_graph_0/hidden_3/Mul_grad/MulMul?gradients_1/policy_1/dense/MatMul_grad/tuple/control_dependency$policy/main_graph_0/hidden_3/Sigmoid*
T0
�
5gradients_1/policy/main_graph_0/hidden_3/Mul_grad/SumSum5gradients_1/policy/main_graph_0/hidden_3/Mul_grad/MulGgradients_1/policy/main_graph_0/hidden_3/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
9gradients_1/policy/main_graph_0/hidden_3/Mul_grad/ReshapeReshape5gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Sum7gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Shape*
T0*
Tshape0
�
7gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Mul_1Mul$policy/main_graph_0/hidden_3/BiasAdd?gradients_1/policy_1/dense/MatMul_grad/tuple/control_dependency*
T0
�
7gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Sum_1Sum7gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Mul_1Igradients_1/policy/main_graph_0/hidden_3/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
;gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Reshape_1Reshape7gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Sum_19gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Shape_1*
T0*
Tshape0
�
Bgradients_1/policy/main_graph_0/hidden_3/Mul_grad/tuple/group_depsNoOp:^gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Reshape<^gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Reshape_1
�
Jgradients_1/policy/main_graph_0/hidden_3/Mul_grad/tuple/control_dependencyIdentity9gradients_1/policy/main_graph_0/hidden_3/Mul_grad/ReshapeC^gradients_1/policy/main_graph_0/hidden_3/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Reshape
�
Lgradients_1/policy/main_graph_0/hidden_3/Mul_grad/tuple/control_dependency_1Identity;gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Reshape_1C^gradients_1/policy/main_graph_0/hidden_3/Mul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Reshape_1
�
Agradients_1/policy/main_graph_0/hidden_3/Sigmoid_grad/SigmoidGradSigmoidGrad$policy/main_graph_0/hidden_3/SigmoidLgradients_1/policy/main_graph_0/hidden_3/Mul_grad/tuple/control_dependency_1*
T0
�
gradients_1/AddN_3AddNJgradients_1/policy/main_graph_0/hidden_3/Mul_grad/tuple/control_dependencyAgradients_1/policy/main_graph_0/hidden_3/Sigmoid_grad/SigmoidGrad*
N*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Reshape
�
Agradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_3*
T0*
data_formatNHWC
�
Fgradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/tuple/group_depsNoOp^gradients_1/AddN_3B^gradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/BiasAddGrad
�
Ngradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_3G^gradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_3/Mul_grad/Reshape
�
Pgradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/BiasAddGradG^gradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/BiasAddGrad
�
;gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/MatMulMatMulNgradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/tuple/control_dependency(policy/main_graph_0/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b(
�
=gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/MatMul_1MatMul policy/main_graph_0/hidden_2/MulNgradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Egradients_1/policy/main_graph_0/hidden_3/MatMul_grad/tuple/group_depsNoOp<^gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/MatMul>^gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/MatMul_1
�
Mgradients_1/policy/main_graph_0/hidden_3/MatMul_grad/tuple/control_dependencyIdentity;gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/MatMulF^gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/MatMul
�
Ogradients_1/policy/main_graph_0/hidden_3/MatMul_grad/tuple/control_dependency_1Identity=gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/MatMul_1F^gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_1/policy/main_graph_0/hidden_3/MatMul_grad/MatMul_1

7gradients_1/policy/main_graph_0/hidden_2/Mul_grad/ShapeShape$policy/main_graph_0/hidden_2/BiasAdd*
T0*
out_type0
�
9gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Shape_1Shape$policy/main_graph_0/hidden_2/Sigmoid*
T0*
out_type0
�
Ggradients_1/policy/main_graph_0/hidden_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Shape9gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Shape_1*
T0
�
5gradients_1/policy/main_graph_0/hidden_2/Mul_grad/MulMulMgradients_1/policy/main_graph_0/hidden_3/MatMul_grad/tuple/control_dependency$policy/main_graph_0/hidden_2/Sigmoid*
T0
�
5gradients_1/policy/main_graph_0/hidden_2/Mul_grad/SumSum5gradients_1/policy/main_graph_0/hidden_2/Mul_grad/MulGgradients_1/policy/main_graph_0/hidden_2/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
9gradients_1/policy/main_graph_0/hidden_2/Mul_grad/ReshapeReshape5gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Sum7gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Shape*
T0*
Tshape0
�
7gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Mul_1Mul$policy/main_graph_0/hidden_2/BiasAddMgradients_1/policy/main_graph_0/hidden_3/MatMul_grad/tuple/control_dependency*
T0
�
7gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Sum_1Sum7gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Mul_1Igradients_1/policy/main_graph_0/hidden_2/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
;gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Reshape_1Reshape7gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Sum_19gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Shape_1*
T0*
Tshape0
�
Bgradients_1/policy/main_graph_0/hidden_2/Mul_grad/tuple/group_depsNoOp:^gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Reshape<^gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Reshape_1
�
Jgradients_1/policy/main_graph_0/hidden_2/Mul_grad/tuple/control_dependencyIdentity9gradients_1/policy/main_graph_0/hidden_2/Mul_grad/ReshapeC^gradients_1/policy/main_graph_0/hidden_2/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Reshape
�
Lgradients_1/policy/main_graph_0/hidden_2/Mul_grad/tuple/control_dependency_1Identity;gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Reshape_1C^gradients_1/policy/main_graph_0/hidden_2/Mul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Reshape_1
�
Agradients_1/policy/main_graph_0/hidden_2/Sigmoid_grad/SigmoidGradSigmoidGrad$policy/main_graph_0/hidden_2/SigmoidLgradients_1/policy/main_graph_0/hidden_2/Mul_grad/tuple/control_dependency_1*
T0
�
gradients_1/AddN_4AddNJgradients_1/policy/main_graph_0/hidden_2/Mul_grad/tuple/control_dependencyAgradients_1/policy/main_graph_0/hidden_2/Sigmoid_grad/SigmoidGrad*
N*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Reshape
�
Agradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_4*
T0*
data_formatNHWC
�
Fgradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/tuple/group_depsNoOp^gradients_1/AddN_4B^gradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/BiasAddGrad
�
Ngradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_4G^gradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_2/Mul_grad/Reshape
�
Pgradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/BiasAddGradG^gradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/BiasAddGrad
�
;gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/MatMulMatMulNgradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/tuple/control_dependency(policy/main_graph_0/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b(
�
=gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/MatMul_1MatMul policy/main_graph_0/hidden_1/MulNgradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Egradients_1/policy/main_graph_0/hidden_2/MatMul_grad/tuple/group_depsNoOp<^gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/MatMul>^gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/MatMul_1
�
Mgradients_1/policy/main_graph_0/hidden_2/MatMul_grad/tuple/control_dependencyIdentity;gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/MatMulF^gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/MatMul
�
Ogradients_1/policy/main_graph_0/hidden_2/MatMul_grad/tuple/control_dependency_1Identity=gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/MatMul_1F^gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_1/policy/main_graph_0/hidden_2/MatMul_grad/MatMul_1

7gradients_1/policy/main_graph_0/hidden_1/Mul_grad/ShapeShape$policy/main_graph_0/hidden_1/BiasAdd*
T0*
out_type0
�
9gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Shape_1Shape$policy/main_graph_0/hidden_1/Sigmoid*
T0*
out_type0
�
Ggradients_1/policy/main_graph_0/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Shape9gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Shape_1*
T0
�
5gradients_1/policy/main_graph_0/hidden_1/Mul_grad/MulMulMgradients_1/policy/main_graph_0/hidden_2/MatMul_grad/tuple/control_dependency$policy/main_graph_0/hidden_1/Sigmoid*
T0
�
5gradients_1/policy/main_graph_0/hidden_1/Mul_grad/SumSum5gradients_1/policy/main_graph_0/hidden_1/Mul_grad/MulGgradients_1/policy/main_graph_0/hidden_1/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
9gradients_1/policy/main_graph_0/hidden_1/Mul_grad/ReshapeReshape5gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Sum7gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Shape*
T0*
Tshape0
�
7gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Mul_1Mul$policy/main_graph_0/hidden_1/BiasAddMgradients_1/policy/main_graph_0/hidden_2/MatMul_grad/tuple/control_dependency*
T0
�
7gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Sum_1Sum7gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Mul_1Igradients_1/policy/main_graph_0/hidden_1/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
;gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Reshape_1Reshape7gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Sum_19gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
�
Bgradients_1/policy/main_graph_0/hidden_1/Mul_grad/tuple/group_depsNoOp:^gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Reshape<^gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Reshape_1
�
Jgradients_1/policy/main_graph_0/hidden_1/Mul_grad/tuple/control_dependencyIdentity9gradients_1/policy/main_graph_0/hidden_1/Mul_grad/ReshapeC^gradients_1/policy/main_graph_0/hidden_1/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Reshape
�
Lgradients_1/policy/main_graph_0/hidden_1/Mul_grad/tuple/control_dependency_1Identity;gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Reshape_1C^gradients_1/policy/main_graph_0/hidden_1/Mul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Reshape_1
�
Agradients_1/policy/main_graph_0/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad$policy/main_graph_0/hidden_1/SigmoidLgradients_1/policy/main_graph_0/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
�
gradients_1/AddN_5AddNJgradients_1/policy/main_graph_0/hidden_1/Mul_grad/tuple/control_dependencyAgradients_1/policy/main_graph_0/hidden_1/Sigmoid_grad/SigmoidGrad*
N*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Reshape
�
Agradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_5*
T0*
data_formatNHWC
�
Fgradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients_1/AddN_5B^gradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/BiasAddGrad
�
Ngradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_5G^gradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_1/Mul_grad/Reshape
�
Pgradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/BiasAddGradG^gradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/BiasAddGrad
�
;gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/MatMulMatMulNgradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/tuple/control_dependency(policy/main_graph_0/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
=gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/MatMul_1MatMul policy/main_graph_0/hidden_0/MulNgradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Egradients_1/policy/main_graph_0/hidden_1/MatMul_grad/tuple/group_depsNoOp<^gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/MatMul>^gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/MatMul_1
�
Mgradients_1/policy/main_graph_0/hidden_1/MatMul_grad/tuple/control_dependencyIdentity;gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/MatMulF^gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/MatMul
�
Ogradients_1/policy/main_graph_0/hidden_1/MatMul_grad/tuple/control_dependency_1Identity=gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/MatMul_1F^gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_1/policy/main_graph_0/hidden_1/MatMul_grad/MatMul_1

7gradients_1/policy/main_graph_0/hidden_0/Mul_grad/ShapeShape$policy/main_graph_0/hidden_0/BiasAdd*
T0*
out_type0
�
9gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Shape_1Shape$policy/main_graph_0/hidden_0/Sigmoid*
T0*
out_type0
�
Ggradients_1/policy/main_graph_0/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Shape9gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Shape_1*
T0
�
5gradients_1/policy/main_graph_0/hidden_0/Mul_grad/MulMulMgradients_1/policy/main_graph_0/hidden_1/MatMul_grad/tuple/control_dependency$policy/main_graph_0/hidden_0/Sigmoid*
T0
�
5gradients_1/policy/main_graph_0/hidden_0/Mul_grad/SumSum5gradients_1/policy/main_graph_0/hidden_0/Mul_grad/MulGgradients_1/policy/main_graph_0/hidden_0/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
9gradients_1/policy/main_graph_0/hidden_0/Mul_grad/ReshapeReshape5gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Sum7gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Shape*
T0*
Tshape0
�
7gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Mul_1Mul$policy/main_graph_0/hidden_0/BiasAddMgradients_1/policy/main_graph_0/hidden_1/MatMul_grad/tuple/control_dependency*
T0
�
7gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Sum_1Sum7gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Mul_1Igradients_1/policy/main_graph_0/hidden_0/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
;gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Reshape_1Reshape7gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Sum_19gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
�
Bgradients_1/policy/main_graph_0/hidden_0/Mul_grad/tuple/group_depsNoOp:^gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Reshape<^gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Reshape_1
�
Jgradients_1/policy/main_graph_0/hidden_0/Mul_grad/tuple/control_dependencyIdentity9gradients_1/policy/main_graph_0/hidden_0/Mul_grad/ReshapeC^gradients_1/policy/main_graph_0/hidden_0/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Reshape
�
Lgradients_1/policy/main_graph_0/hidden_0/Mul_grad/tuple/control_dependency_1Identity;gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Reshape_1C^gradients_1/policy/main_graph_0/hidden_0/Mul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Reshape_1
�
Agradients_1/policy/main_graph_0/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad$policy/main_graph_0/hidden_0/SigmoidLgradients_1/policy/main_graph_0/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
�
gradients_1/AddN_6AddNJgradients_1/policy/main_graph_0/hidden_0/Mul_grad/tuple/control_dependencyAgradients_1/policy/main_graph_0/hidden_0/Sigmoid_grad/SigmoidGrad*
N*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Reshape
�
Agradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_6*
T0*
data_formatNHWC
�
Fgradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^gradients_1/AddN_6B^gradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/BiasAddGrad
�
Ngradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_6G^gradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_1/policy/main_graph_0/hidden_0/Mul_grad/Reshape
�
Pgradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/BiasAddGradG^gradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/BiasAddGrad
�
;gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/MatMulMatMulNgradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/tuple/control_dependency(policy/main_graph_0/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b(
�
=gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationNgradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Egradients_1/policy/main_graph_0/hidden_0/MatMul_grad/tuple/group_depsNoOp<^gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/MatMul>^gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/MatMul_1
�
Mgradients_1/policy/main_graph_0/hidden_0/MatMul_grad/tuple/control_dependencyIdentity;gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/MatMulF^gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/MatMul
�
Ogradients_1/policy/main_graph_0/hidden_0/MatMul_grad/tuple/control_dependency_1Identity=gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/MatMul_1F^gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_1/policy/main_graph_0/hidden_0/MatMul_grad/MatMul_1
p
beta1_power_1/initial_valueConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB
 *fff?
�
beta1_power_1
VariableV2*&
_class
loc:@policy/dense/kernel*
	container *
dtype0*
shape: *
shared_name 
�
beta1_power_1/AssignAssignbeta1_power_1beta1_power_1/initial_value*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
^
beta1_power_1/readIdentitybeta1_power_1*
T0*&
_class
loc:@policy/dense/kernel
p
beta2_power_1/initial_valueConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB
 *w�?
�
beta2_power_1
VariableV2*&
_class
loc:@policy/dense/kernel*
	container *
dtype0*
shape: *
shared_name 
�
beta2_power_1/AssignAssignbeta2_power_1beta2_power_1/initial_value*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
^
beta2_power_1/readIdentitybeta2_power_1*
T0*&
_class
loc:@policy/dense/kernel
�
Tpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorConst*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Jpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt/Initializer/zeros/ConstConst*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
dtype0*
valueB
 *    
�
Dpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt/Initializer/zerosFillTpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorJpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt/Initializer/zeros/Const*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*

index_type0
�
2policy/main_graph_0/hidden_0/kernel/sac_policy_opt
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
9policy/main_graph_0/hidden_0/kernel/sac_policy_opt/AssignAssign2policy/main_graph_0/hidden_0/kernel/sac_policy_optDpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt/Initializer/zeros*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
�
7policy/main_graph_0/hidden_0/kernel/sac_policy_opt/readIdentity2policy/main_graph_0/hidden_0/kernel/sac_policy_opt*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel
�
Vpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorConst*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Lpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt_1/Initializer/zeros/ConstConst*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
dtype0*
valueB
 *    
�
Fpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt_1/Initializer/zerosFillVpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorLpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt_1/Initializer/zeros/Const*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*

index_type0
�
4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
;policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1/AssignAssign4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1Fpolicy/main_graph_0/hidden_0/kernel/sac_policy_opt_1/Initializer/zeros*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
�
9policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1/readIdentity4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel
�
Bpolicy/main_graph_0/hidden_0/bias/sac_policy_opt/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
dtype0*
valueB@*    
�
0policy/main_graph_0/hidden_0/bias/sac_policy_opt
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
7policy/main_graph_0/hidden_0/bias/sac_policy_opt/AssignAssign0policy/main_graph_0/hidden_0/bias/sac_policy_optBpolicy/main_graph_0/hidden_0/bias/sac_policy_opt/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
�
5policy/main_graph_0/hidden_0/bias/sac_policy_opt/readIdentity0policy/main_graph_0/hidden_0/bias/sac_policy_opt*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias
�
Dpolicy/main_graph_0/hidden_0/bias/sac_policy_opt_1/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
dtype0*
valueB@*    
�
2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
9policy/main_graph_0/hidden_0/bias/sac_policy_opt_1/AssignAssign2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1Dpolicy/main_graph_0/hidden_0/bias/sac_policy_opt_1/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
�
7policy/main_graph_0/hidden_0/bias/sac_policy_opt_1/readIdentity2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias
�
Tpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorConst*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Jpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt/Initializer/zeros/ConstConst*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
dtype0*
valueB
 *    
�
Dpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt/Initializer/zerosFillTpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorJpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt/Initializer/zeros/Const*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*

index_type0
�
2policy/main_graph_0/hidden_1/kernel/sac_policy_opt
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
9policy/main_graph_0/hidden_1/kernel/sac_policy_opt/AssignAssign2policy/main_graph_0/hidden_1/kernel/sac_policy_optDpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt/Initializer/zeros*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
�
7policy/main_graph_0/hidden_1/kernel/sac_policy_opt/readIdentity2policy/main_graph_0/hidden_1/kernel/sac_policy_opt*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel
�
Vpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorConst*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Lpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt_1/Initializer/zeros/ConstConst*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
dtype0*
valueB
 *    
�
Fpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt_1/Initializer/zerosFillVpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorLpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt_1/Initializer/zeros/Const*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*

index_type0
�
4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
;policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1/AssignAssign4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1Fpolicy/main_graph_0/hidden_1/kernel/sac_policy_opt_1/Initializer/zeros*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
�
9policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1/readIdentity4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel
�
Bpolicy/main_graph_0/hidden_1/bias/sac_policy_opt/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
dtype0*
valueB@*    
�
0policy/main_graph_0/hidden_1/bias/sac_policy_opt
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
7policy/main_graph_0/hidden_1/bias/sac_policy_opt/AssignAssign0policy/main_graph_0/hidden_1/bias/sac_policy_optBpolicy/main_graph_0/hidden_1/bias/sac_policy_opt/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
�
5policy/main_graph_0/hidden_1/bias/sac_policy_opt/readIdentity0policy/main_graph_0/hidden_1/bias/sac_policy_opt*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias
�
Dpolicy/main_graph_0/hidden_1/bias/sac_policy_opt_1/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
dtype0*
valueB@*    
�
2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
9policy/main_graph_0/hidden_1/bias/sac_policy_opt_1/AssignAssign2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1Dpolicy/main_graph_0/hidden_1/bias/sac_policy_opt_1/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
�
7policy/main_graph_0/hidden_1/bias/sac_policy_opt_1/readIdentity2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias
�
Tpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorConst*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Jpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt/Initializer/zeros/ConstConst*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
dtype0*
valueB
 *    
�
Dpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt/Initializer/zerosFillTpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorJpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt/Initializer/zeros/Const*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*

index_type0
�
2policy/main_graph_0/hidden_2/kernel/sac_policy_opt
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
9policy/main_graph_0/hidden_2/kernel/sac_policy_opt/AssignAssign2policy/main_graph_0/hidden_2/kernel/sac_policy_optDpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt/Initializer/zeros*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
�
7policy/main_graph_0/hidden_2/kernel/sac_policy_opt/readIdentity2policy/main_graph_0/hidden_2/kernel/sac_policy_opt*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel
�
Vpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorConst*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Lpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt_1/Initializer/zeros/ConstConst*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
dtype0*
valueB
 *    
�
Fpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt_1/Initializer/zerosFillVpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorLpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt_1/Initializer/zeros/Const*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*

index_type0
�
4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
;policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1/AssignAssign4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1Fpolicy/main_graph_0/hidden_2/kernel/sac_policy_opt_1/Initializer/zeros*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
�
9policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1/readIdentity4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel
�
Bpolicy/main_graph_0/hidden_2/bias/sac_policy_opt/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
dtype0*
valueB@*    
�
0policy/main_graph_0/hidden_2/bias/sac_policy_opt
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
7policy/main_graph_0/hidden_2/bias/sac_policy_opt/AssignAssign0policy/main_graph_0/hidden_2/bias/sac_policy_optBpolicy/main_graph_0/hidden_2/bias/sac_policy_opt/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
�
5policy/main_graph_0/hidden_2/bias/sac_policy_opt/readIdentity0policy/main_graph_0/hidden_2/bias/sac_policy_opt*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias
�
Dpolicy/main_graph_0/hidden_2/bias/sac_policy_opt_1/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
dtype0*
valueB@*    
�
2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
9policy/main_graph_0/hidden_2/bias/sac_policy_opt_1/AssignAssign2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1Dpolicy/main_graph_0/hidden_2/bias/sac_policy_opt_1/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
�
7policy/main_graph_0/hidden_2/bias/sac_policy_opt_1/readIdentity2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias
�
Tpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorConst*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Jpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt/Initializer/zeros/ConstConst*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
dtype0*
valueB
 *    
�
Dpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt/Initializer/zerosFillTpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorJpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt/Initializer/zeros/Const*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*

index_type0
�
2policy/main_graph_0/hidden_3/kernel/sac_policy_opt
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
9policy/main_graph_0/hidden_3/kernel/sac_policy_opt/AssignAssign2policy/main_graph_0/hidden_3/kernel/sac_policy_optDpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt/Initializer/zeros*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
�
7policy/main_graph_0/hidden_3/kernel/sac_policy_opt/readIdentity2policy/main_graph_0/hidden_3/kernel/sac_policy_opt*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel
�
Vpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorConst*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Lpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt_1/Initializer/zeros/ConstConst*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
dtype0*
valueB
 *    
�
Fpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt_1/Initializer/zerosFillVpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorLpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt_1/Initializer/zeros/Const*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*

index_type0
�
4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1
VariableV2*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
;policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1/AssignAssign4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1Fpolicy/main_graph_0/hidden_3/kernel/sac_policy_opt_1/Initializer/zeros*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
�
9policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1/readIdentity4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel
�
Bpolicy/main_graph_0/hidden_3/bias/sac_policy_opt/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
dtype0*
valueB@*    
�
0policy/main_graph_0/hidden_3/bias/sac_policy_opt
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
7policy/main_graph_0/hidden_3/bias/sac_policy_opt/AssignAssign0policy/main_graph_0/hidden_3/bias/sac_policy_optBpolicy/main_graph_0/hidden_3/bias/sac_policy_opt/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
�
5policy/main_graph_0/hidden_3/bias/sac_policy_opt/readIdentity0policy/main_graph_0/hidden_3/bias/sac_policy_opt*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias
�
Dpolicy/main_graph_0/hidden_3/bias/sac_policy_opt_1/Initializer/zerosConst*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
dtype0*
valueB@*    
�
2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1
VariableV2*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
9policy/main_graph_0/hidden_3/bias/sac_policy_opt_1/AssignAssign2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1Dpolicy/main_graph_0/hidden_3/bias/sac_policy_opt_1/Initializer/zeros*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
�
7policy/main_graph_0/hidden_3/bias/sac_policy_opt_1/readIdentity2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias
�
Dpolicy/dense/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensorConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB"@   @   
�
:policy/dense/kernel/sac_policy_opt/Initializer/zeros/ConstConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB
 *    
�
4policy/dense/kernel/sac_policy_opt/Initializer/zerosFillDpolicy/dense/kernel/sac_policy_opt/Initializer/zeros/shape_as_tensor:policy/dense/kernel/sac_policy_opt/Initializer/zeros/Const*
T0*&
_class
loc:@policy/dense/kernel*

index_type0
�
"policy/dense/kernel/sac_policy_opt
VariableV2*&
_class
loc:@policy/dense/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
)policy/dense/kernel/sac_policy_opt/AssignAssign"policy/dense/kernel/sac_policy_opt4policy/dense/kernel/sac_policy_opt/Initializer/zeros*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
�
'policy/dense/kernel/sac_policy_opt/readIdentity"policy/dense/kernel/sac_policy_opt*
T0*&
_class
loc:@policy/dense/kernel
�
Fpolicy/dense/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensorConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB"@   @   
�
<policy/dense/kernel/sac_policy_opt_1/Initializer/zeros/ConstConst*&
_class
loc:@policy/dense/kernel*
dtype0*
valueB
 *    
�
6policy/dense/kernel/sac_policy_opt_1/Initializer/zerosFillFpolicy/dense/kernel/sac_policy_opt_1/Initializer/zeros/shape_as_tensor<policy/dense/kernel/sac_policy_opt_1/Initializer/zeros/Const*
T0*&
_class
loc:@policy/dense/kernel*

index_type0
�
$policy/dense/kernel/sac_policy_opt_1
VariableV2*&
_class
loc:@policy/dense/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
+policy/dense/kernel/sac_policy_opt_1/AssignAssign$policy/dense/kernel/sac_policy_opt_16policy/dense/kernel/sac_policy_opt_1/Initializer/zeros*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
�
)policy/dense/kernel/sac_policy_opt_1/readIdentity$policy/dense/kernel/sac_policy_opt_1*
T0*&
_class
loc:@policy/dense/kernel
A
sac_policy_opt/beta1Const*
dtype0*
valueB
 *fff?
A
sac_policy_opt/beta2Const*
dtype0*
valueB
 *w�?
C
sac_policy_opt/epsilonConst*
dtype0*
valueB
 *w�+2
�
Csac_policy_opt/update_policy/main_graph_0/hidden_0/kernel/ApplyAdam	ApplyAdam#policy/main_graph_0/hidden_0/kernel2policy/main_graph_0/hidden_0/kernel/sac_policy_opt4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonOgradients_1/policy/main_graph_0/hidden_0/MatMul_grad/tuple/control_dependency_1*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking( *
use_nesterov( 
�
Asac_policy_opt/update_policy/main_graph_0/hidden_0/bias/ApplyAdam	ApplyAdam!policy/main_graph_0/hidden_0/bias0policy/main_graph_0/hidden_0/bias/sac_policy_opt2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonPgradients_1/policy/main_graph_0/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking( *
use_nesterov( 
�
Csac_policy_opt/update_policy/main_graph_0/hidden_1/kernel/ApplyAdam	ApplyAdam#policy/main_graph_0/hidden_1/kernel2policy/main_graph_0/hidden_1/kernel/sac_policy_opt4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonOgradients_1/policy/main_graph_0/hidden_1/MatMul_grad/tuple/control_dependency_1*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking( *
use_nesterov( 
�
Asac_policy_opt/update_policy/main_graph_0/hidden_1/bias/ApplyAdam	ApplyAdam!policy/main_graph_0/hidden_1/bias0policy/main_graph_0/hidden_1/bias/sac_policy_opt2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonPgradients_1/policy/main_graph_0/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking( *
use_nesterov( 
�
Csac_policy_opt/update_policy/main_graph_0/hidden_2/kernel/ApplyAdam	ApplyAdam#policy/main_graph_0/hidden_2/kernel2policy/main_graph_0/hidden_2/kernel/sac_policy_opt4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonOgradients_1/policy/main_graph_0/hidden_2/MatMul_grad/tuple/control_dependency_1*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking( *
use_nesterov( 
�
Asac_policy_opt/update_policy/main_graph_0/hidden_2/bias/ApplyAdam	ApplyAdam!policy/main_graph_0/hidden_2/bias0policy/main_graph_0/hidden_2/bias/sac_policy_opt2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonPgradients_1/policy/main_graph_0/hidden_2/BiasAdd_grad/tuple/control_dependency_1*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking( *
use_nesterov( 
�
Csac_policy_opt/update_policy/main_graph_0/hidden_3/kernel/ApplyAdam	ApplyAdam#policy/main_graph_0/hidden_3/kernel2policy/main_graph_0/hidden_3/kernel/sac_policy_opt4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonOgradients_1/policy/main_graph_0/hidden_3/MatMul_grad/tuple/control_dependency_1*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking( *
use_nesterov( 
�
Asac_policy_opt/update_policy/main_graph_0/hidden_3/bias/ApplyAdam	ApplyAdam!policy/main_graph_0/hidden_3/bias0policy/main_graph_0/hidden_3/bias/sac_policy_opt2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonPgradients_1/policy/main_graph_0/hidden_3/BiasAdd_grad/tuple/control_dependency_1*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking( *
use_nesterov( 
�
3sac_policy_opt/update_policy/dense/kernel/ApplyAdam	ApplyAdampolicy/dense/kernel"policy/dense/kernel/sac_policy_opt$policy/dense/kernel/sac_policy_opt_1beta1_power_1/readbeta2_power_1/readVariable_2/readsac_policy_opt/beta1sac_policy_opt/beta2sac_policy_opt/epsilonAgradients_1/policy_1/dense/MatMul_grad/tuple/control_dependency_1*
T0*&
_class
loc:@policy/dense/kernel*
use_locking( *
use_nesterov( 
�
sac_policy_opt/mulMulbeta1_power_1/readsac_policy_opt/beta14^sac_policy_opt/update_policy/dense/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_0/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_0/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_1/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_1/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_2/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_2/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_3/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_3/kernel/ApplyAdam*
T0*&
_class
loc:@policy/dense/kernel
�
sac_policy_opt/AssignAssignbeta1_power_1sac_policy_opt/mul*
T0*&
_class
loc:@policy/dense/kernel*
use_locking( *
validate_shape(
�
sac_policy_opt/mul_1Mulbeta2_power_1/readsac_policy_opt/beta24^sac_policy_opt/update_policy/dense/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_0/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_0/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_1/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_1/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_2/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_2/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_3/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_3/kernel/ApplyAdam*
T0*&
_class
loc:@policy/dense/kernel
�
sac_policy_opt/Assign_1Assignbeta2_power_1sac_policy_opt/mul_1*
T0*&
_class
loc:@policy/dense/kernel*
use_locking( *
validate_shape(
�
sac_policy_optNoOp^sac_policy_opt/Assign^sac_policy_opt/Assign_14^sac_policy_opt/update_policy/dense/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_0/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_0/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_1/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_1/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_2/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_2/kernel/ApplyAdamB^sac_policy_opt/update_policy/main_graph_0/hidden_3/bias/ApplyAdamD^sac_policy_opt/update_policy/main_graph_0/hidden_3/kernel/ApplyAdam
K
gradients_2/ShapeConst^sac_policy_opt*
dtype0*
valueB 
S
gradients_2/grad_ys_0Const^sac_policy_opt*
dtype0*
valueB
 *  �?
]
gradients_2/FillFillgradients_2/Shapegradients_2/grad_ys_0*
T0*

index_type0
S
'gradients_2/add_7_grad/tuple/group_depsNoOp^gradients_2/Fill^sac_policy_opt
�
/gradients_2/add_7_grad/tuple/control_dependencyIdentitygradients_2/Fill(^gradients_2/add_7_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_2/Fill
�
1gradients_2/add_7_grad/tuple/control_dependency_1Identitygradients_2/Fill(^gradients_2/add_7_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_2/Fill
r
'gradients_2/add_6_grad/tuple/group_depsNoOp0^gradients_2/add_7_grad/tuple/control_dependency^sac_policy_opt
�
/gradients_2/add_6_grad/tuple/control_dependencyIdentity/gradients_2/add_7_grad/tuple/control_dependency(^gradients_2/add_6_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_2/Fill
�
1gradients_2/add_6_grad/tuple/control_dependency_1Identity/gradients_2/add_7_grad/tuple/control_dependency(^gradients_2/add_6_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_2/Fill
e
&gradients_2/Mean_23_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB:
�
 gradients_2/Mean_23_grad/ReshapeReshape1gradients_2/add_7_grad/tuple/control_dependency_1&gradients_2/Mean_23_grad/Reshape/shape*
T0*
Tshape0
]
gradients_2/Mean_23_grad/ConstConst^sac_policy_opt*
dtype0*
valueB:
�
gradients_2/Mean_23_grad/TileTile gradients_2/Mean_23_grad/Reshapegradients_2/Mean_23_grad/Const*
T0*

Tmultiples0
^
 gradients_2/Mean_23_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB
 *   @
u
 gradients_2/Mean_23_grad/truedivRealDivgradients_2/Mean_23_grad/Tile gradients_2/Mean_23_grad/Const_1*
T0
e
&gradients_2/Mean_14_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB:
�
 gradients_2/Mean_14_grad/ReshapeReshape/gradients_2/add_6_grad/tuple/control_dependency&gradients_2/Mean_14_grad/Reshape/shape*
T0*
Tshape0
]
gradients_2/Mean_14_grad/ConstConst^sac_policy_opt*
dtype0*
valueB:
�
gradients_2/Mean_14_grad/TileTile gradients_2/Mean_14_grad/Reshapegradients_2/Mean_14_grad/Const*
T0*

Tmultiples0
^
 gradients_2/Mean_14_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB
 *   @
u
 gradients_2/Mean_14_grad/truedivRealDivgradients_2/Mean_14_grad/Tile gradients_2/Mean_14_grad/Const_1*
T0
e
&gradients_2/Mean_15_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB:
�
 gradients_2/Mean_15_grad/ReshapeReshape1gradients_2/add_6_grad/tuple/control_dependency_1&gradients_2/Mean_15_grad/Reshape/shape*
T0*
Tshape0
]
gradients_2/Mean_15_grad/ConstConst^sac_policy_opt*
dtype0*
valueB:
�
gradients_2/Mean_15_grad/TileTile gradients_2/Mean_15_grad/Reshapegradients_2/Mean_15_grad/Const*
T0*

Tmultiples0
^
 gradients_2/Mean_15_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB
 *   @
u
 gradients_2/Mean_15_grad/truedivRealDivgradients_2/Mean_15_grad/Tile gradients_2/Mean_15_grad/Const_1*
T0
r
&gradients_2/Mean_23/input_grad/unstackUnpack gradients_2/Mean_23_grad/truediv*
T0*

axis *	
num
q
/gradients_2/Mean_23/input_grad/tuple/group_depsNoOp'^gradients_2/Mean_23/input_grad/unstack^sac_policy_opt
�
7gradients_2/Mean_23/input_grad/tuple/control_dependencyIdentity&gradients_2/Mean_23/input_grad/unstack0^gradients_2/Mean_23/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_2/Mean_23/input_grad/unstack
�
9gradients_2/Mean_23/input_grad/tuple/control_dependency_1Identity(gradients_2/Mean_23/input_grad/unstack:10^gradients_2/Mean_23/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_2/Mean_23/input_grad/unstack
r
&gradients_2/Mean_14/input_grad/unstackUnpack gradients_2/Mean_14_grad/truediv*
T0*

axis *	
num
q
/gradients_2/Mean_14/input_grad/tuple/group_depsNoOp'^gradients_2/Mean_14/input_grad/unstack^sac_policy_opt
�
7gradients_2/Mean_14/input_grad/tuple/control_dependencyIdentity&gradients_2/Mean_14/input_grad/unstack0^gradients_2/Mean_14/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_2/Mean_14/input_grad/unstack
�
9gradients_2/Mean_14/input_grad/tuple/control_dependency_1Identity(gradients_2/Mean_14/input_grad/unstack:10^gradients_2/Mean_14/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_2/Mean_14/input_grad/unstack
r
&gradients_2/Mean_15/input_grad/unstackUnpack gradients_2/Mean_15_grad/truediv*
T0*

axis *	
num
q
/gradients_2/Mean_15/input_grad/tuple/group_depsNoOp'^gradients_2/Mean_15/input_grad/unstack^sac_policy_opt
�
7gradients_2/Mean_15/input_grad/tuple/control_dependencyIdentity&gradients_2/Mean_15/input_grad/unstack0^gradients_2/Mean_15/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_2/Mean_15/input_grad/unstack
�
9gradients_2/Mean_15/input_grad/tuple/control_dependency_1Identity(gradients_2/Mean_15/input_grad/unstack:10^gradients_2/Mean_15/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_2/Mean_15/input_grad/unstack
m
gradients_2/mul_35_grad/MulMul7gradients_2/Mean_23/input_grad/tuple/control_dependencyMean_20*
T0
p
gradients_2/mul_35_grad/Mul_1Mul7gradients_2/Mean_23/input_grad/tuple/control_dependencymul_35/x*
T0

(gradients_2/mul_35_grad/tuple/group_depsNoOp^gradients_2/mul_35_grad/Mul^gradients_2/mul_35_grad/Mul_1^sac_policy_opt
�
0gradients_2/mul_35_grad/tuple/control_dependencyIdentitygradients_2/mul_35_grad/Mul)^gradients_2/mul_35_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_2/mul_35_grad/Mul
�
2gradients_2/mul_35_grad/tuple/control_dependency_1Identitygradients_2/mul_35_grad/Mul_1)^gradients_2/mul_35_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_2/mul_35_grad/Mul_1
o
gradients_2/mul_37_grad/MulMul9gradients_2/Mean_23/input_grad/tuple/control_dependency_1Mean_22*
T0
r
gradients_2/mul_37_grad/Mul_1Mul9gradients_2/Mean_23/input_grad/tuple/control_dependency_1mul_37/x*
T0

(gradients_2/mul_37_grad/tuple/group_depsNoOp^gradients_2/mul_37_grad/Mul^gradients_2/mul_37_grad/Mul_1^sac_policy_opt
�
0gradients_2/mul_37_grad/tuple/control_dependencyIdentitygradients_2/mul_37_grad/Mul)^gradients_2/mul_37_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_2/mul_37_grad/Mul
�
2gradients_2/mul_37_grad/tuple/control_dependency_1Identitygradients_2/mul_37_grad/Mul_1)^gradients_2/mul_37_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_2/mul_37_grad/Mul_1
l
gradients_2/mul_16_grad/MulMul7gradients_2/Mean_14/input_grad/tuple/control_dependencyMean_8*
T0
p
gradients_2/mul_16_grad/Mul_1Mul7gradients_2/Mean_14/input_grad/tuple/control_dependencymul_16/x*
T0

(gradients_2/mul_16_grad/tuple/group_depsNoOp^gradients_2/mul_16_grad/Mul^gradients_2/mul_16_grad/Mul_1^sac_policy_opt
�
0gradients_2/mul_16_grad/tuple/control_dependencyIdentitygradients_2/mul_16_grad/Mul)^gradients_2/mul_16_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_2/mul_16_grad/Mul
�
2gradients_2/mul_16_grad/tuple/control_dependency_1Identitygradients_2/mul_16_grad/Mul_1)^gradients_2/mul_16_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_2/mul_16_grad/Mul_1
o
gradients_2/mul_25_grad/MulMul9gradients_2/Mean_14/input_grad/tuple/control_dependency_1Mean_12*
T0
r
gradients_2/mul_25_grad/Mul_1Mul9gradients_2/Mean_14/input_grad/tuple/control_dependency_1mul_25/x*
T0

(gradients_2/mul_25_grad/tuple/group_depsNoOp^gradients_2/mul_25_grad/Mul^gradients_2/mul_25_grad/Mul_1^sac_policy_opt
�
0gradients_2/mul_25_grad/tuple/control_dependencyIdentitygradients_2/mul_25_grad/Mul)^gradients_2/mul_25_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_2/mul_25_grad/Mul
�
2gradients_2/mul_25_grad/tuple/control_dependency_1Identitygradients_2/mul_25_grad/Mul_1)^gradients_2/mul_25_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_2/mul_25_grad/Mul_1
l
gradients_2/mul_18_grad/MulMul7gradients_2/Mean_15/input_grad/tuple/control_dependencyMean_9*
T0
p
gradients_2/mul_18_grad/Mul_1Mul7gradients_2/Mean_15/input_grad/tuple/control_dependencymul_18/x*
T0

(gradients_2/mul_18_grad/tuple/group_depsNoOp^gradients_2/mul_18_grad/Mul^gradients_2/mul_18_grad/Mul_1^sac_policy_opt
�
0gradients_2/mul_18_grad/tuple/control_dependencyIdentitygradients_2/mul_18_grad/Mul)^gradients_2/mul_18_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_2/mul_18_grad/Mul
�
2gradients_2/mul_18_grad/tuple/control_dependency_1Identitygradients_2/mul_18_grad/Mul_1)^gradients_2/mul_18_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_2/mul_18_grad/Mul_1
o
gradients_2/mul_27_grad/MulMul9gradients_2/Mean_15/input_grad/tuple/control_dependency_1Mean_13*
T0
r
gradients_2/mul_27_grad/Mul_1Mul9gradients_2/Mean_15/input_grad/tuple/control_dependency_1mul_27/x*
T0

(gradients_2/mul_27_grad/tuple/group_depsNoOp^gradients_2/mul_27_grad/Mul^gradients_2/mul_27_grad/Mul_1^sac_policy_opt
�
0gradients_2/mul_27_grad/tuple/control_dependencyIdentitygradients_2/mul_27_grad/Mul)^gradients_2/mul_27_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_2/mul_27_grad/Mul
�
2gradients_2/mul_27_grad/tuple/control_dependency_1Identitygradients_2/mul_27_grad/Mul_1)^gradients_2/mul_27_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_2/mul_27_grad/Mul_1
l
&gradients_2/Mean_20_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB"      
�
 gradients_2/Mean_20_grad/ReshapeReshape2gradients_2/mul_35_grad/tuple/control_dependency_1&gradients_2/Mean_20_grad/Reshape/shape*
T0*
Tshape0
Y
gradients_2/Mean_20_grad/ShapeShapemul_34^sac_policy_opt*
T0*
out_type0
�
gradients_2/Mean_20_grad/TileTile gradients_2/Mean_20_grad/Reshapegradients_2/Mean_20_grad/Shape*
T0*

Tmultiples0
[
 gradients_2/Mean_20_grad/Shape_1Shapemul_34^sac_policy_opt*
T0*
out_type0
Z
 gradients_2/Mean_20_grad/Shape_2Const^sac_policy_opt*
dtype0*
valueB 
]
gradients_2/Mean_20_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_20_grad/ProdProd gradients_2/Mean_20_grad/Shape_1gradients_2/Mean_20_grad/Const*
T0*

Tidx0*
	keep_dims( 
_
 gradients_2/Mean_20_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_20_grad/Prod_1Prod gradients_2/Mean_20_grad/Shape_2 gradients_2/Mean_20_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
]
"gradients_2/Mean_20_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
y
 gradients_2/Mean_20_grad/MaximumMaximumgradients_2/Mean_20_grad/Prod_1"gradients_2/Mean_20_grad/Maximum/y*
T0
w
!gradients_2/Mean_20_grad/floordivFloorDivgradients_2/Mean_20_grad/Prod gradients_2/Mean_20_grad/Maximum*
T0
p
gradients_2/Mean_20_grad/CastCast!gradients_2/Mean_20_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
r
 gradients_2/Mean_20_grad/truedivRealDivgradients_2/Mean_20_grad/Tilegradients_2/Mean_20_grad/Cast*
T0
l
&gradients_2/Mean_22_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB"      
�
 gradients_2/Mean_22_grad/ReshapeReshape2gradients_2/mul_37_grad/tuple/control_dependency_1&gradients_2/Mean_22_grad/Reshape/shape*
T0*
Tshape0
Y
gradients_2/Mean_22_grad/ShapeShapemul_36^sac_policy_opt*
T0*
out_type0
�
gradients_2/Mean_22_grad/TileTile gradients_2/Mean_22_grad/Reshapegradients_2/Mean_22_grad/Shape*
T0*

Tmultiples0
[
 gradients_2/Mean_22_grad/Shape_1Shapemul_36^sac_policy_opt*
T0*
out_type0
Z
 gradients_2/Mean_22_grad/Shape_2Const^sac_policy_opt*
dtype0*
valueB 
]
gradients_2/Mean_22_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_22_grad/ProdProd gradients_2/Mean_22_grad/Shape_1gradients_2/Mean_22_grad/Const*
T0*

Tidx0*
	keep_dims( 
_
 gradients_2/Mean_22_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_22_grad/Prod_1Prod gradients_2/Mean_22_grad/Shape_2 gradients_2/Mean_22_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
]
"gradients_2/Mean_22_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
y
 gradients_2/Mean_22_grad/MaximumMaximumgradients_2/Mean_22_grad/Prod_1"gradients_2/Mean_22_grad/Maximum/y*
T0
w
!gradients_2/Mean_22_grad/floordivFloorDivgradients_2/Mean_22_grad/Prod gradients_2/Mean_22_grad/Maximum*
T0
p
gradients_2/Mean_22_grad/CastCast!gradients_2/Mean_22_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
r
 gradients_2/Mean_22_grad/truedivRealDivgradients_2/Mean_22_grad/Tilegradients_2/Mean_22_grad/Cast*
T0
k
%gradients_2/Mean_8_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB"      
�
gradients_2/Mean_8_grad/ReshapeReshape2gradients_2/mul_16_grad/tuple/control_dependency_1%gradients_2/Mean_8_grad/Reshape/shape*
T0*
Tshape0
X
gradients_2/Mean_8_grad/ShapeShapemul_15^sac_policy_opt*
T0*
out_type0

gradients_2/Mean_8_grad/TileTilegradients_2/Mean_8_grad/Reshapegradients_2/Mean_8_grad/Shape*
T0*

Tmultiples0
Z
gradients_2/Mean_8_grad/Shape_1Shapemul_15^sac_policy_opt*
T0*
out_type0
Y
gradients_2/Mean_8_grad/Shape_2Const^sac_policy_opt*
dtype0*
valueB 
\
gradients_2/Mean_8_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_8_grad/ProdProdgradients_2/Mean_8_grad/Shape_1gradients_2/Mean_8_grad/Const*
T0*

Tidx0*
	keep_dims( 
^
gradients_2/Mean_8_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_8_grad/Prod_1Prodgradients_2/Mean_8_grad/Shape_2gradients_2/Mean_8_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
\
!gradients_2/Mean_8_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
v
gradients_2/Mean_8_grad/MaximumMaximumgradients_2/Mean_8_grad/Prod_1!gradients_2/Mean_8_grad/Maximum/y*
T0
t
 gradients_2/Mean_8_grad/floordivFloorDivgradients_2/Mean_8_grad/Prodgradients_2/Mean_8_grad/Maximum*
T0
n
gradients_2/Mean_8_grad/CastCast gradients_2/Mean_8_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
o
gradients_2/Mean_8_grad/truedivRealDivgradients_2/Mean_8_grad/Tilegradients_2/Mean_8_grad/Cast*
T0
l
&gradients_2/Mean_12_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB"      
�
 gradients_2/Mean_12_grad/ReshapeReshape2gradients_2/mul_25_grad/tuple/control_dependency_1&gradients_2/Mean_12_grad/Reshape/shape*
T0*
Tshape0
Y
gradients_2/Mean_12_grad/ShapeShapemul_24^sac_policy_opt*
T0*
out_type0
�
gradients_2/Mean_12_grad/TileTile gradients_2/Mean_12_grad/Reshapegradients_2/Mean_12_grad/Shape*
T0*

Tmultiples0
[
 gradients_2/Mean_12_grad/Shape_1Shapemul_24^sac_policy_opt*
T0*
out_type0
Z
 gradients_2/Mean_12_grad/Shape_2Const^sac_policy_opt*
dtype0*
valueB 
]
gradients_2/Mean_12_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_12_grad/ProdProd gradients_2/Mean_12_grad/Shape_1gradients_2/Mean_12_grad/Const*
T0*

Tidx0*
	keep_dims( 
_
 gradients_2/Mean_12_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_12_grad/Prod_1Prod gradients_2/Mean_12_grad/Shape_2 gradients_2/Mean_12_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
]
"gradients_2/Mean_12_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
y
 gradients_2/Mean_12_grad/MaximumMaximumgradients_2/Mean_12_grad/Prod_1"gradients_2/Mean_12_grad/Maximum/y*
T0
w
!gradients_2/Mean_12_grad/floordivFloorDivgradients_2/Mean_12_grad/Prod gradients_2/Mean_12_grad/Maximum*
T0
p
gradients_2/Mean_12_grad/CastCast!gradients_2/Mean_12_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
r
 gradients_2/Mean_12_grad/truedivRealDivgradients_2/Mean_12_grad/Tilegradients_2/Mean_12_grad/Cast*
T0
k
%gradients_2/Mean_9_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB"      
�
gradients_2/Mean_9_grad/ReshapeReshape2gradients_2/mul_18_grad/tuple/control_dependency_1%gradients_2/Mean_9_grad/Reshape/shape*
T0*
Tshape0
X
gradients_2/Mean_9_grad/ShapeShapemul_17^sac_policy_opt*
T0*
out_type0

gradients_2/Mean_9_grad/TileTilegradients_2/Mean_9_grad/Reshapegradients_2/Mean_9_grad/Shape*
T0*

Tmultiples0
Z
gradients_2/Mean_9_grad/Shape_1Shapemul_17^sac_policy_opt*
T0*
out_type0
Y
gradients_2/Mean_9_grad/Shape_2Const^sac_policy_opt*
dtype0*
valueB 
\
gradients_2/Mean_9_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_9_grad/ProdProdgradients_2/Mean_9_grad/Shape_1gradients_2/Mean_9_grad/Const*
T0*

Tidx0*
	keep_dims( 
^
gradients_2/Mean_9_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_9_grad/Prod_1Prodgradients_2/Mean_9_grad/Shape_2gradients_2/Mean_9_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
\
!gradients_2/Mean_9_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
v
gradients_2/Mean_9_grad/MaximumMaximumgradients_2/Mean_9_grad/Prod_1!gradients_2/Mean_9_grad/Maximum/y*
T0
t
 gradients_2/Mean_9_grad/floordivFloorDivgradients_2/Mean_9_grad/Prodgradients_2/Mean_9_grad/Maximum*
T0
n
gradients_2/Mean_9_grad/CastCast gradients_2/Mean_9_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
o
gradients_2/Mean_9_grad/truedivRealDivgradients_2/Mean_9_grad/Tilegradients_2/Mean_9_grad/Cast*
T0
l
&gradients_2/Mean_13_grad/Reshape/shapeConst^sac_policy_opt*
dtype0*
valueB"      
�
 gradients_2/Mean_13_grad/ReshapeReshape2gradients_2/mul_27_grad/tuple/control_dependency_1&gradients_2/Mean_13_grad/Reshape/shape*
T0*
Tshape0
Y
gradients_2/Mean_13_grad/ShapeShapemul_26^sac_policy_opt*
T0*
out_type0
�
gradients_2/Mean_13_grad/TileTile gradients_2/Mean_13_grad/Reshapegradients_2/Mean_13_grad/Shape*
T0*

Tmultiples0
[
 gradients_2/Mean_13_grad/Shape_1Shapemul_26^sac_policy_opt*
T0*
out_type0
Z
 gradients_2/Mean_13_grad/Shape_2Const^sac_policy_opt*
dtype0*
valueB 
]
gradients_2/Mean_13_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_13_grad/ProdProd gradients_2/Mean_13_grad/Shape_1gradients_2/Mean_13_grad/Const*
T0*

Tidx0*
	keep_dims( 
_
 gradients_2/Mean_13_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_13_grad/Prod_1Prod gradients_2/Mean_13_grad/Shape_2 gradients_2/Mean_13_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
]
"gradients_2/Mean_13_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
y
 gradients_2/Mean_13_grad/MaximumMaximumgradients_2/Mean_13_grad/Prod_1"gradients_2/Mean_13_grad/Maximum/y*
T0
w
!gradients_2/Mean_13_grad/floordivFloorDivgradients_2/Mean_13_grad/Prod gradients_2/Mean_13_grad/Maximum*
T0
p
gradients_2/Mean_13_grad/CastCast!gradients_2/Mean_13_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
r
 gradients_2/Mean_13_grad/truedivRealDivgradients_2/Mean_13_grad/Tilegradients_2/Mean_13_grad/Cast*
T0
[
gradients_2/mul_34_grad/ShapeShape	ToFloat_6^sac_policy_opt*
T0*
out_type0
g
gradients_2/mul_34_grad/Shape_1ShapeSquaredDifference_5^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_34_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_34_grad/Shapegradients_2/mul_34_grad/Shape_1*
T0
b
gradients_2/mul_34_grad/MulMul gradients_2/Mean_20_grad/truedivSquaredDifference_5*
T0
�
gradients_2/mul_34_grad/SumSumgradients_2/mul_34_grad/Mul-gradients_2/mul_34_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_34_grad/ReshapeReshapegradients_2/mul_34_grad/Sumgradients_2/mul_34_grad/Shape*
T0*
Tshape0
Z
gradients_2/mul_34_grad/Mul_1Mul	ToFloat_6 gradients_2/Mean_20_grad/truediv*
T0
�
gradients_2/mul_34_grad/Sum_1Sumgradients_2/mul_34_grad/Mul_1/gradients_2/mul_34_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_34_grad/Reshape_1Reshapegradients_2/mul_34_grad/Sum_1gradients_2/mul_34_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_34_grad/tuple/group_depsNoOp ^gradients_2/mul_34_grad/Reshape"^gradients_2/mul_34_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_34_grad/tuple/control_dependencyIdentitygradients_2/mul_34_grad/Reshape)^gradients_2/mul_34_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_34_grad/Reshape
�
2gradients_2/mul_34_grad/tuple/control_dependency_1Identity!gradients_2/mul_34_grad/Reshape_1)^gradients_2/mul_34_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_34_grad/Reshape_1
[
gradients_2/mul_36_grad/ShapeShape	ToFloat_7^sac_policy_opt*
T0*
out_type0
g
gradients_2/mul_36_grad/Shape_1ShapeSquaredDifference_6^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_36_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_36_grad/Shapegradients_2/mul_36_grad/Shape_1*
T0
b
gradients_2/mul_36_grad/MulMul gradients_2/Mean_22_grad/truedivSquaredDifference_6*
T0
�
gradients_2/mul_36_grad/SumSumgradients_2/mul_36_grad/Mul-gradients_2/mul_36_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_36_grad/ReshapeReshapegradients_2/mul_36_grad/Sumgradients_2/mul_36_grad/Shape*
T0*
Tshape0
Z
gradients_2/mul_36_grad/Mul_1Mul	ToFloat_7 gradients_2/Mean_22_grad/truediv*
T0
�
gradients_2/mul_36_grad/Sum_1Sumgradients_2/mul_36_grad/Mul_1/gradients_2/mul_36_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_36_grad/Reshape_1Reshapegradients_2/mul_36_grad/Sum_1gradients_2/mul_36_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_36_grad/tuple/group_depsNoOp ^gradients_2/mul_36_grad/Reshape"^gradients_2/mul_36_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_36_grad/tuple/control_dependencyIdentitygradients_2/mul_36_grad/Reshape)^gradients_2/mul_36_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_36_grad/Reshape
�
2gradients_2/mul_36_grad/tuple/control_dependency_1Identity!gradients_2/mul_36_grad/Reshape_1)^gradients_2/mul_36_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_36_grad/Reshape_1
Y
gradients_2/mul_15_grad/ShapeShapeToFloat^sac_policy_opt*
T0*
out_type0
g
gradients_2/mul_15_grad/Shape_1ShapeSquaredDifference_1^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_15_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_15_grad/Shapegradients_2/mul_15_grad/Shape_1*
T0
a
gradients_2/mul_15_grad/MulMulgradients_2/Mean_8_grad/truedivSquaredDifference_1*
T0
�
gradients_2/mul_15_grad/SumSumgradients_2/mul_15_grad/Mul-gradients_2/mul_15_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_15_grad/ReshapeReshapegradients_2/mul_15_grad/Sumgradients_2/mul_15_grad/Shape*
T0*
Tshape0
W
gradients_2/mul_15_grad/Mul_1MulToFloatgradients_2/Mean_8_grad/truediv*
T0
�
gradients_2/mul_15_grad/Sum_1Sumgradients_2/mul_15_grad/Mul_1/gradients_2/mul_15_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_15_grad/Reshape_1Reshapegradients_2/mul_15_grad/Sum_1gradients_2/mul_15_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_15_grad/tuple/group_depsNoOp ^gradients_2/mul_15_grad/Reshape"^gradients_2/mul_15_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_15_grad/tuple/control_dependencyIdentitygradients_2/mul_15_grad/Reshape)^gradients_2/mul_15_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_15_grad/Reshape
�
2gradients_2/mul_15_grad/tuple/control_dependency_1Identity!gradients_2/mul_15_grad/Reshape_1)^gradients_2/mul_15_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_15_grad/Reshape_1
[
gradients_2/mul_24_grad/ShapeShape	ToFloat_2^sac_policy_opt*
T0*
out_type0
g
gradients_2/mul_24_grad/Shape_1ShapeSquaredDifference_3^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_24_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_24_grad/Shapegradients_2/mul_24_grad/Shape_1*
T0
b
gradients_2/mul_24_grad/MulMul gradients_2/Mean_12_grad/truedivSquaredDifference_3*
T0
�
gradients_2/mul_24_grad/SumSumgradients_2/mul_24_grad/Mul-gradients_2/mul_24_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_24_grad/ReshapeReshapegradients_2/mul_24_grad/Sumgradients_2/mul_24_grad/Shape*
T0*
Tshape0
Z
gradients_2/mul_24_grad/Mul_1Mul	ToFloat_2 gradients_2/Mean_12_grad/truediv*
T0
�
gradients_2/mul_24_grad/Sum_1Sumgradients_2/mul_24_grad/Mul_1/gradients_2/mul_24_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_24_grad/Reshape_1Reshapegradients_2/mul_24_grad/Sum_1gradients_2/mul_24_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_24_grad/tuple/group_depsNoOp ^gradients_2/mul_24_grad/Reshape"^gradients_2/mul_24_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_24_grad/tuple/control_dependencyIdentitygradients_2/mul_24_grad/Reshape)^gradients_2/mul_24_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_24_grad/Reshape
�
2gradients_2/mul_24_grad/tuple/control_dependency_1Identity!gradients_2/mul_24_grad/Reshape_1)^gradients_2/mul_24_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_24_grad/Reshape_1
[
gradients_2/mul_17_grad/ShapeShape	ToFloat_1^sac_policy_opt*
T0*
out_type0
g
gradients_2/mul_17_grad/Shape_1ShapeSquaredDifference_2^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_17_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_17_grad/Shapegradients_2/mul_17_grad/Shape_1*
T0
a
gradients_2/mul_17_grad/MulMulgradients_2/Mean_9_grad/truedivSquaredDifference_2*
T0
�
gradients_2/mul_17_grad/SumSumgradients_2/mul_17_grad/Mul-gradients_2/mul_17_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_17_grad/ReshapeReshapegradients_2/mul_17_grad/Sumgradients_2/mul_17_grad/Shape*
T0*
Tshape0
Y
gradients_2/mul_17_grad/Mul_1Mul	ToFloat_1gradients_2/Mean_9_grad/truediv*
T0
�
gradients_2/mul_17_grad/Sum_1Sumgradients_2/mul_17_grad/Mul_1/gradients_2/mul_17_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_17_grad/Reshape_1Reshapegradients_2/mul_17_grad/Sum_1gradients_2/mul_17_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_17_grad/tuple/group_depsNoOp ^gradients_2/mul_17_grad/Reshape"^gradients_2/mul_17_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_17_grad/tuple/control_dependencyIdentitygradients_2/mul_17_grad/Reshape)^gradients_2/mul_17_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_17_grad/Reshape
�
2gradients_2/mul_17_grad/tuple/control_dependency_1Identity!gradients_2/mul_17_grad/Reshape_1)^gradients_2/mul_17_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_17_grad/Reshape_1
[
gradients_2/mul_26_grad/ShapeShape	ToFloat_3^sac_policy_opt*
T0*
out_type0
g
gradients_2/mul_26_grad/Shape_1ShapeSquaredDifference_4^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_26_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_26_grad/Shapegradients_2/mul_26_grad/Shape_1*
T0
b
gradients_2/mul_26_grad/MulMul gradients_2/Mean_13_grad/truedivSquaredDifference_4*
T0
�
gradients_2/mul_26_grad/SumSumgradients_2/mul_26_grad/Mul-gradients_2/mul_26_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_26_grad/ReshapeReshapegradients_2/mul_26_grad/Sumgradients_2/mul_26_grad/Shape*
T0*
Tshape0
Z
gradients_2/mul_26_grad/Mul_1Mul	ToFloat_3 gradients_2/Mean_13_grad/truediv*
T0
�
gradients_2/mul_26_grad/Sum_1Sumgradients_2/mul_26_grad/Mul_1/gradients_2/mul_26_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_26_grad/Reshape_1Reshapegradients_2/mul_26_grad/Sum_1gradients_2/mul_26_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_26_grad/tuple/group_depsNoOp ^gradients_2/mul_26_grad/Reshape"^gradients_2/mul_26_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_26_grad/tuple/control_dependencyIdentitygradients_2/mul_26_grad/Reshape)^gradients_2/mul_26_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_26_grad/Reshape
�
2gradients_2/mul_26_grad/tuple/control_dependency_1Identity!gradients_2/mul_26_grad/Reshape_1)^gradients_2/mul_26_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_26_grad/Reshape_1
�
+gradients_2/SquaredDifference_5_grad/scalarConst3^gradients_2/mul_34_grad/tuple/control_dependency_1^sac_policy_opt*
dtype0*
valueB
 *   @
�
(gradients_2/SquaredDifference_5_grad/MulMul+gradients_2/SquaredDifference_5_grad/scalar2gradients_2/mul_34_grad/tuple/control_dependency_1*
T0
�
(gradients_2/SquaredDifference_5_grad/subSub$critic/value/extrinsic_value/BiasAddStopGradient_43^gradients_2/mul_34_grad/tuple/control_dependency_1^sac_policy_opt*
T0
�
*gradients_2/SquaredDifference_5_grad/mul_1Mul(gradients_2/SquaredDifference_5_grad/Mul(gradients_2/SquaredDifference_5_grad/sub*
T0
�
*gradients_2/SquaredDifference_5_grad/ShapeShape$critic/value/extrinsic_value/BiasAdd^sac_policy_opt*
T0*
out_type0
o
,gradients_2/SquaredDifference_5_grad/Shape_1ShapeStopGradient_4^sac_policy_opt*
T0*
out_type0
�
:gradients_2/SquaredDifference_5_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_2/SquaredDifference_5_grad/Shape,gradients_2/SquaredDifference_5_grad/Shape_1*
T0
�
(gradients_2/SquaredDifference_5_grad/SumSum*gradients_2/SquaredDifference_5_grad/mul_1:gradients_2/SquaredDifference_5_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
,gradients_2/SquaredDifference_5_grad/ReshapeReshape(gradients_2/SquaredDifference_5_grad/Sum*gradients_2/SquaredDifference_5_grad/Shape*
T0*
Tshape0
�
*gradients_2/SquaredDifference_5_grad/Sum_1Sum*gradients_2/SquaredDifference_5_grad/mul_1<gradients_2/SquaredDifference_5_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
.gradients_2/SquaredDifference_5_grad/Reshape_1Reshape*gradients_2/SquaredDifference_5_grad/Sum_1,gradients_2/SquaredDifference_5_grad/Shape_1*
T0*
Tshape0
h
(gradients_2/SquaredDifference_5_grad/NegNeg.gradients_2/SquaredDifference_5_grad/Reshape_1*
T0
�
5gradients_2/SquaredDifference_5_grad/tuple/group_depsNoOp)^gradients_2/SquaredDifference_5_grad/Neg-^gradients_2/SquaredDifference_5_grad/Reshape^sac_policy_opt
�
=gradients_2/SquaredDifference_5_grad/tuple/control_dependencyIdentity,gradients_2/SquaredDifference_5_grad/Reshape6^gradients_2/SquaredDifference_5_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_2/SquaredDifference_5_grad/Reshape
�
?gradients_2/SquaredDifference_5_grad/tuple/control_dependency_1Identity(gradients_2/SquaredDifference_5_grad/Neg6^gradients_2/SquaredDifference_5_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_2/SquaredDifference_5_grad/Neg
�
+gradients_2/SquaredDifference_6_grad/scalarConst3^gradients_2/mul_36_grad/tuple/control_dependency_1^sac_policy_opt*
dtype0*
valueB
 *   @
�
(gradients_2/SquaredDifference_6_grad/MulMul+gradients_2/SquaredDifference_6_grad/scalar2gradients_2/mul_36_grad/tuple/control_dependency_1*
T0
�
(gradients_2/SquaredDifference_6_grad/subSub$critic/value/curiosity_value/BiasAddStopGradient_53^gradients_2/mul_36_grad/tuple/control_dependency_1^sac_policy_opt*
T0
�
*gradients_2/SquaredDifference_6_grad/mul_1Mul(gradients_2/SquaredDifference_6_grad/Mul(gradients_2/SquaredDifference_6_grad/sub*
T0
�
*gradients_2/SquaredDifference_6_grad/ShapeShape$critic/value/curiosity_value/BiasAdd^sac_policy_opt*
T0*
out_type0
o
,gradients_2/SquaredDifference_6_grad/Shape_1ShapeStopGradient_5^sac_policy_opt*
T0*
out_type0
�
:gradients_2/SquaredDifference_6_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_2/SquaredDifference_6_grad/Shape,gradients_2/SquaredDifference_6_grad/Shape_1*
T0
�
(gradients_2/SquaredDifference_6_grad/SumSum*gradients_2/SquaredDifference_6_grad/mul_1:gradients_2/SquaredDifference_6_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
,gradients_2/SquaredDifference_6_grad/ReshapeReshape(gradients_2/SquaredDifference_6_grad/Sum*gradients_2/SquaredDifference_6_grad/Shape*
T0*
Tshape0
�
*gradients_2/SquaredDifference_6_grad/Sum_1Sum*gradients_2/SquaredDifference_6_grad/mul_1<gradients_2/SquaredDifference_6_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
.gradients_2/SquaredDifference_6_grad/Reshape_1Reshape*gradients_2/SquaredDifference_6_grad/Sum_1,gradients_2/SquaredDifference_6_grad/Shape_1*
T0*
Tshape0
h
(gradients_2/SquaredDifference_6_grad/NegNeg.gradients_2/SquaredDifference_6_grad/Reshape_1*
T0
�
5gradients_2/SquaredDifference_6_grad/tuple/group_depsNoOp)^gradients_2/SquaredDifference_6_grad/Neg-^gradients_2/SquaredDifference_6_grad/Reshape^sac_policy_opt
�
=gradients_2/SquaredDifference_6_grad/tuple/control_dependencyIdentity,gradients_2/SquaredDifference_6_grad/Reshape6^gradients_2/SquaredDifference_6_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_2/SquaredDifference_6_grad/Reshape
�
?gradients_2/SquaredDifference_6_grad/tuple/control_dependency_1Identity(gradients_2/SquaredDifference_6_grad/Neg6^gradients_2/SquaredDifference_6_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_2/SquaredDifference_6_grad/Neg
�
+gradients_2/SquaredDifference_1_grad/scalarConst3^gradients_2/mul_15_grad/tuple/control_dependency_1^sac_policy_opt*
dtype0*
valueB
 *   @
�
(gradients_2/SquaredDifference_1_grad/MulMul+gradients_2/SquaredDifference_1_grad/scalar2gradients_2/mul_15_grad/tuple/control_dependency_1*
T0
�
(gradients_2/SquaredDifference_1_grad/subSubStopGradient_1Mean_63^gradients_2/mul_15_grad/tuple/control_dependency_1^sac_policy_opt*
T0
�
*gradients_2/SquaredDifference_1_grad/mul_1Mul(gradients_2/SquaredDifference_1_grad/Mul(gradients_2/SquaredDifference_1_grad/sub*
T0
m
*gradients_2/SquaredDifference_1_grad/ShapeShapeStopGradient_1^sac_policy_opt*
T0*
out_type0
g
,gradients_2/SquaredDifference_1_grad/Shape_1ShapeMean_6^sac_policy_opt*
T0*
out_type0
�
:gradients_2/SquaredDifference_1_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_2/SquaredDifference_1_grad/Shape,gradients_2/SquaredDifference_1_grad/Shape_1*
T0
�
(gradients_2/SquaredDifference_1_grad/SumSum*gradients_2/SquaredDifference_1_grad/mul_1:gradients_2/SquaredDifference_1_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
,gradients_2/SquaredDifference_1_grad/ReshapeReshape(gradients_2/SquaredDifference_1_grad/Sum*gradients_2/SquaredDifference_1_grad/Shape*
T0*
Tshape0
�
*gradients_2/SquaredDifference_1_grad/Sum_1Sum*gradients_2/SquaredDifference_1_grad/mul_1<gradients_2/SquaredDifference_1_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
.gradients_2/SquaredDifference_1_grad/Reshape_1Reshape*gradients_2/SquaredDifference_1_grad/Sum_1,gradients_2/SquaredDifference_1_grad/Shape_1*
T0*
Tshape0
h
(gradients_2/SquaredDifference_1_grad/NegNeg.gradients_2/SquaredDifference_1_grad/Reshape_1*
T0
�
5gradients_2/SquaredDifference_1_grad/tuple/group_depsNoOp)^gradients_2/SquaredDifference_1_grad/Neg-^gradients_2/SquaredDifference_1_grad/Reshape^sac_policy_opt
�
=gradients_2/SquaredDifference_1_grad/tuple/control_dependencyIdentity,gradients_2/SquaredDifference_1_grad/Reshape6^gradients_2/SquaredDifference_1_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_2/SquaredDifference_1_grad/Reshape
�
?gradients_2/SquaredDifference_1_grad/tuple/control_dependency_1Identity(gradients_2/SquaredDifference_1_grad/Neg6^gradients_2/SquaredDifference_1_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_2/SquaredDifference_1_grad/Neg
�
+gradients_2/SquaredDifference_3_grad/scalarConst3^gradients_2/mul_24_grad/tuple/control_dependency_1^sac_policy_opt*
dtype0*
valueB
 *   @
�
(gradients_2/SquaredDifference_3_grad/MulMul+gradients_2/SquaredDifference_3_grad/scalar2gradients_2/mul_24_grad/tuple/control_dependency_1*
T0
�
(gradients_2/SquaredDifference_3_grad/subSubStopGradient_2Mean_103^gradients_2/mul_24_grad/tuple/control_dependency_1^sac_policy_opt*
T0
�
*gradients_2/SquaredDifference_3_grad/mul_1Mul(gradients_2/SquaredDifference_3_grad/Mul(gradients_2/SquaredDifference_3_grad/sub*
T0
m
*gradients_2/SquaredDifference_3_grad/ShapeShapeStopGradient_2^sac_policy_opt*
T0*
out_type0
h
,gradients_2/SquaredDifference_3_grad/Shape_1ShapeMean_10^sac_policy_opt*
T0*
out_type0
�
:gradients_2/SquaredDifference_3_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_2/SquaredDifference_3_grad/Shape,gradients_2/SquaredDifference_3_grad/Shape_1*
T0
�
(gradients_2/SquaredDifference_3_grad/SumSum*gradients_2/SquaredDifference_3_grad/mul_1:gradients_2/SquaredDifference_3_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
,gradients_2/SquaredDifference_3_grad/ReshapeReshape(gradients_2/SquaredDifference_3_grad/Sum*gradients_2/SquaredDifference_3_grad/Shape*
T0*
Tshape0
�
*gradients_2/SquaredDifference_3_grad/Sum_1Sum*gradients_2/SquaredDifference_3_grad/mul_1<gradients_2/SquaredDifference_3_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
.gradients_2/SquaredDifference_3_grad/Reshape_1Reshape*gradients_2/SquaredDifference_3_grad/Sum_1,gradients_2/SquaredDifference_3_grad/Shape_1*
T0*
Tshape0
h
(gradients_2/SquaredDifference_3_grad/NegNeg.gradients_2/SquaredDifference_3_grad/Reshape_1*
T0
�
5gradients_2/SquaredDifference_3_grad/tuple/group_depsNoOp)^gradients_2/SquaredDifference_3_grad/Neg-^gradients_2/SquaredDifference_3_grad/Reshape^sac_policy_opt
�
=gradients_2/SquaredDifference_3_grad/tuple/control_dependencyIdentity,gradients_2/SquaredDifference_3_grad/Reshape6^gradients_2/SquaredDifference_3_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_2/SquaredDifference_3_grad/Reshape
�
?gradients_2/SquaredDifference_3_grad/tuple/control_dependency_1Identity(gradients_2/SquaredDifference_3_grad/Neg6^gradients_2/SquaredDifference_3_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_2/SquaredDifference_3_grad/Neg
�
+gradients_2/SquaredDifference_2_grad/scalarConst3^gradients_2/mul_17_grad/tuple/control_dependency_1^sac_policy_opt*
dtype0*
valueB
 *   @
�
(gradients_2/SquaredDifference_2_grad/MulMul+gradients_2/SquaredDifference_2_grad/scalar2gradients_2/mul_17_grad/tuple/control_dependency_1*
T0
�
(gradients_2/SquaredDifference_2_grad/subSubStopGradient_1Mean_73^gradients_2/mul_17_grad/tuple/control_dependency_1^sac_policy_opt*
T0
�
*gradients_2/SquaredDifference_2_grad/mul_1Mul(gradients_2/SquaredDifference_2_grad/Mul(gradients_2/SquaredDifference_2_grad/sub*
T0
m
*gradients_2/SquaredDifference_2_grad/ShapeShapeStopGradient_1^sac_policy_opt*
T0*
out_type0
g
,gradients_2/SquaredDifference_2_grad/Shape_1ShapeMean_7^sac_policy_opt*
T0*
out_type0
�
:gradients_2/SquaredDifference_2_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_2/SquaredDifference_2_grad/Shape,gradients_2/SquaredDifference_2_grad/Shape_1*
T0
�
(gradients_2/SquaredDifference_2_grad/SumSum*gradients_2/SquaredDifference_2_grad/mul_1:gradients_2/SquaredDifference_2_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
,gradients_2/SquaredDifference_2_grad/ReshapeReshape(gradients_2/SquaredDifference_2_grad/Sum*gradients_2/SquaredDifference_2_grad/Shape*
T0*
Tshape0
�
*gradients_2/SquaredDifference_2_grad/Sum_1Sum*gradients_2/SquaredDifference_2_grad/mul_1<gradients_2/SquaredDifference_2_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
.gradients_2/SquaredDifference_2_grad/Reshape_1Reshape*gradients_2/SquaredDifference_2_grad/Sum_1,gradients_2/SquaredDifference_2_grad/Shape_1*
T0*
Tshape0
h
(gradients_2/SquaredDifference_2_grad/NegNeg.gradients_2/SquaredDifference_2_grad/Reshape_1*
T0
�
5gradients_2/SquaredDifference_2_grad/tuple/group_depsNoOp)^gradients_2/SquaredDifference_2_grad/Neg-^gradients_2/SquaredDifference_2_grad/Reshape^sac_policy_opt
�
=gradients_2/SquaredDifference_2_grad/tuple/control_dependencyIdentity,gradients_2/SquaredDifference_2_grad/Reshape6^gradients_2/SquaredDifference_2_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_2/SquaredDifference_2_grad/Reshape
�
?gradients_2/SquaredDifference_2_grad/tuple/control_dependency_1Identity(gradients_2/SquaredDifference_2_grad/Neg6^gradients_2/SquaredDifference_2_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_2/SquaredDifference_2_grad/Neg
�
+gradients_2/SquaredDifference_4_grad/scalarConst3^gradients_2/mul_26_grad/tuple/control_dependency_1^sac_policy_opt*
dtype0*
valueB
 *   @
�
(gradients_2/SquaredDifference_4_grad/MulMul+gradients_2/SquaredDifference_4_grad/scalar2gradients_2/mul_26_grad/tuple/control_dependency_1*
T0
�
(gradients_2/SquaredDifference_4_grad/subSubStopGradient_2Mean_113^gradients_2/mul_26_grad/tuple/control_dependency_1^sac_policy_opt*
T0
�
*gradients_2/SquaredDifference_4_grad/mul_1Mul(gradients_2/SquaredDifference_4_grad/Mul(gradients_2/SquaredDifference_4_grad/sub*
T0
m
*gradients_2/SquaredDifference_4_grad/ShapeShapeStopGradient_2^sac_policy_opt*
T0*
out_type0
h
,gradients_2/SquaredDifference_4_grad/Shape_1ShapeMean_11^sac_policy_opt*
T0*
out_type0
�
:gradients_2/SquaredDifference_4_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_2/SquaredDifference_4_grad/Shape,gradients_2/SquaredDifference_4_grad/Shape_1*
T0
�
(gradients_2/SquaredDifference_4_grad/SumSum*gradients_2/SquaredDifference_4_grad/mul_1:gradients_2/SquaredDifference_4_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
,gradients_2/SquaredDifference_4_grad/ReshapeReshape(gradients_2/SquaredDifference_4_grad/Sum*gradients_2/SquaredDifference_4_grad/Shape*
T0*
Tshape0
�
*gradients_2/SquaredDifference_4_grad/Sum_1Sum*gradients_2/SquaredDifference_4_grad/mul_1<gradients_2/SquaredDifference_4_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
.gradients_2/SquaredDifference_4_grad/Reshape_1Reshape*gradients_2/SquaredDifference_4_grad/Sum_1,gradients_2/SquaredDifference_4_grad/Shape_1*
T0*
Tshape0
h
(gradients_2/SquaredDifference_4_grad/NegNeg.gradients_2/SquaredDifference_4_grad/Reshape_1*
T0
�
5gradients_2/SquaredDifference_4_grad/tuple/group_depsNoOp)^gradients_2/SquaredDifference_4_grad/Neg-^gradients_2/SquaredDifference_4_grad/Reshape^sac_policy_opt
�
=gradients_2/SquaredDifference_4_grad/tuple/control_dependencyIdentity,gradients_2/SquaredDifference_4_grad/Reshape6^gradients_2/SquaredDifference_4_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_2/SquaredDifference_4_grad/Reshape
�
?gradients_2/SquaredDifference_4_grad/tuple/control_dependency_1Identity(gradients_2/SquaredDifference_4_grad/Neg6^gradients_2/SquaredDifference_4_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_2/SquaredDifference_4_grad/Neg
�
Agradients_2/critic/value/extrinsic_value/BiasAdd_grad/BiasAddGradBiasAddGrad=gradients_2/SquaredDifference_5_grad/tuple/control_dependency*
T0*
data_formatNHWC
�
Fgradients_2/critic/value/extrinsic_value/BiasAdd_grad/tuple/group_depsNoOp>^gradients_2/SquaredDifference_5_grad/tuple/control_dependencyB^gradients_2/critic/value/extrinsic_value/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Ngradients_2/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependencyIdentity=gradients_2/SquaredDifference_5_grad/tuple/control_dependencyG^gradients_2/critic/value/extrinsic_value/BiasAdd_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_2/SquaredDifference_5_grad/Reshape
�
Pgradients_2/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_2/critic/value/extrinsic_value/BiasAdd_grad/BiasAddGradG^gradients_2/critic/value/extrinsic_value/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_2/critic/value/extrinsic_value/BiasAdd_grad/BiasAddGrad
�
Agradients_2/critic/value/curiosity_value/BiasAdd_grad/BiasAddGradBiasAddGrad=gradients_2/SquaredDifference_6_grad/tuple/control_dependency*
T0*
data_formatNHWC
�
Fgradients_2/critic/value/curiosity_value/BiasAdd_grad/tuple/group_depsNoOp>^gradients_2/SquaredDifference_6_grad/tuple/control_dependencyB^gradients_2/critic/value/curiosity_value/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Ngradients_2/critic/value/curiosity_value/BiasAdd_grad/tuple/control_dependencyIdentity=gradients_2/SquaredDifference_6_grad/tuple/control_dependencyG^gradients_2/critic/value/curiosity_value/BiasAdd_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_2/SquaredDifference_6_grad/Reshape
�
Pgradients_2/critic/value/curiosity_value/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_2/critic/value/curiosity_value/BiasAdd_grad/BiasAddGradG^gradients_2/critic/value/curiosity_value/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_2/critic/value/curiosity_value/BiasAdd_grad/BiasAddGrad
^
gradients_2/Mean_6_grad/ShapeShapeMean_6/input^sac_policy_opt*
T0*
out_type0
�
gradients_2/Mean_6_grad/SizeConst^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_6_grad/addAddV2Mean_6/reduction_indicesgradients_2/Mean_6_grad/Size*
T0*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape
�
gradients_2/Mean_6_grad/modFloorModgradients_2/Mean_6_grad/addgradients_2/Mean_6_grad/Size*
T0*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape
�
gradients_2/Mean_6_grad/Shape_1Const^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape*
dtype0*
valueB 
�
#gradients_2/Mean_6_grad/range/startConst^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape*
dtype0*
value	B : 
�
#gradients_2/Mean_6_grad/range/deltaConst^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_6_grad/rangeRange#gradients_2/Mean_6_grad/range/startgradients_2/Mean_6_grad/Size#gradients_2/Mean_6_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape
�
"gradients_2/Mean_6_grad/Fill/valueConst^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_6_grad/FillFillgradients_2/Mean_6_grad/Shape_1"gradients_2/Mean_6_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape*

index_type0
�
%gradients_2/Mean_6_grad/DynamicStitchDynamicStitchgradients_2/Mean_6_grad/rangegradients_2/Mean_6_grad/modgradients_2/Mean_6_grad/Shapegradients_2/Mean_6_grad/Fill*
N*
T0*0
_class&
$"loc:@gradients_2/Mean_6_grad/Shape
�
gradients_2/Mean_6_grad/ReshapeReshape?gradients_2/SquaredDifference_1_grad/tuple/control_dependency_1%gradients_2/Mean_6_grad/DynamicStitch*
T0*
Tshape0
�
#gradients_2/Mean_6_grad/BroadcastToBroadcastTogradients_2/Mean_6_grad/Reshapegradients_2/Mean_6_grad/Shape*
T0*

Tidx0
`
gradients_2/Mean_6_grad/Shape_2ShapeMean_6/input^sac_policy_opt*
T0*
out_type0
Z
gradients_2/Mean_6_grad/Shape_3ShapeMean_6^sac_policy_opt*
T0*
out_type0
\
gradients_2/Mean_6_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_6_grad/ProdProdgradients_2/Mean_6_grad/Shape_2gradients_2/Mean_6_grad/Const*
T0*

Tidx0*
	keep_dims( 
^
gradients_2/Mean_6_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_6_grad/Prod_1Prodgradients_2/Mean_6_grad/Shape_3gradients_2/Mean_6_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
\
!gradients_2/Mean_6_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
v
gradients_2/Mean_6_grad/MaximumMaximumgradients_2/Mean_6_grad/Prod_1!gradients_2/Mean_6_grad/Maximum/y*
T0
t
 gradients_2/Mean_6_grad/floordivFloorDivgradients_2/Mean_6_grad/Prodgradients_2/Mean_6_grad/Maximum*
T0
n
gradients_2/Mean_6_grad/CastCast gradients_2/Mean_6_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
v
gradients_2/Mean_6_grad/truedivRealDiv#gradients_2/Mean_6_grad/BroadcastTogradients_2/Mean_6_grad/Cast*
T0
`
gradients_2/Mean_10_grad/ShapeShapeMean_10/input^sac_policy_opt*
T0*
out_type0
�
gradients_2/Mean_10_grad/SizeConst^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_10_grad/addAddV2Mean_10/reduction_indicesgradients_2/Mean_10_grad/Size*
T0*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape
�
gradients_2/Mean_10_grad/modFloorModgradients_2/Mean_10_grad/addgradients_2/Mean_10_grad/Size*
T0*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape
�
 gradients_2/Mean_10_grad/Shape_1Const^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape*
dtype0*
valueB 
�
$gradients_2/Mean_10_grad/range/startConst^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape*
dtype0*
value	B : 
�
$gradients_2/Mean_10_grad/range/deltaConst^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_10_grad/rangeRange$gradients_2/Mean_10_grad/range/startgradients_2/Mean_10_grad/Size$gradients_2/Mean_10_grad/range/delta*

Tidx0*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape
�
#gradients_2/Mean_10_grad/Fill/valueConst^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_10_grad/FillFill gradients_2/Mean_10_grad/Shape_1#gradients_2/Mean_10_grad/Fill/value*
T0*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape*

index_type0
�
&gradients_2/Mean_10_grad/DynamicStitchDynamicStitchgradients_2/Mean_10_grad/rangegradients_2/Mean_10_grad/modgradients_2/Mean_10_grad/Shapegradients_2/Mean_10_grad/Fill*
N*
T0*1
_class'
%#loc:@gradients_2/Mean_10_grad/Shape
�
 gradients_2/Mean_10_grad/ReshapeReshape?gradients_2/SquaredDifference_3_grad/tuple/control_dependency_1&gradients_2/Mean_10_grad/DynamicStitch*
T0*
Tshape0
�
$gradients_2/Mean_10_grad/BroadcastToBroadcastTo gradients_2/Mean_10_grad/Reshapegradients_2/Mean_10_grad/Shape*
T0*

Tidx0
b
 gradients_2/Mean_10_grad/Shape_2ShapeMean_10/input^sac_policy_opt*
T0*
out_type0
\
 gradients_2/Mean_10_grad/Shape_3ShapeMean_10^sac_policy_opt*
T0*
out_type0
]
gradients_2/Mean_10_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_10_grad/ProdProd gradients_2/Mean_10_grad/Shape_2gradients_2/Mean_10_grad/Const*
T0*

Tidx0*
	keep_dims( 
_
 gradients_2/Mean_10_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_10_grad/Prod_1Prod gradients_2/Mean_10_grad/Shape_3 gradients_2/Mean_10_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
]
"gradients_2/Mean_10_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
y
 gradients_2/Mean_10_grad/MaximumMaximumgradients_2/Mean_10_grad/Prod_1"gradients_2/Mean_10_grad/Maximum/y*
T0
w
!gradients_2/Mean_10_grad/floordivFloorDivgradients_2/Mean_10_grad/Prod gradients_2/Mean_10_grad/Maximum*
T0
p
gradients_2/Mean_10_grad/CastCast!gradients_2/Mean_10_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
y
 gradients_2/Mean_10_grad/truedivRealDiv$gradients_2/Mean_10_grad/BroadcastTogradients_2/Mean_10_grad/Cast*
T0
^
gradients_2/Mean_7_grad/ShapeShapeMean_7/input^sac_policy_opt*
T0*
out_type0
�
gradients_2/Mean_7_grad/SizeConst^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_7_grad/addAddV2Mean_7/reduction_indicesgradients_2/Mean_7_grad/Size*
T0*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape
�
gradients_2/Mean_7_grad/modFloorModgradients_2/Mean_7_grad/addgradients_2/Mean_7_grad/Size*
T0*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape
�
gradients_2/Mean_7_grad/Shape_1Const^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape*
dtype0*
valueB 
�
#gradients_2/Mean_7_grad/range/startConst^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape*
dtype0*
value	B : 
�
#gradients_2/Mean_7_grad/range/deltaConst^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_7_grad/rangeRange#gradients_2/Mean_7_grad/range/startgradients_2/Mean_7_grad/Size#gradients_2/Mean_7_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape
�
"gradients_2/Mean_7_grad/Fill/valueConst^sac_policy_opt*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_7_grad/FillFillgradients_2/Mean_7_grad/Shape_1"gradients_2/Mean_7_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape*

index_type0
�
%gradients_2/Mean_7_grad/DynamicStitchDynamicStitchgradients_2/Mean_7_grad/rangegradients_2/Mean_7_grad/modgradients_2/Mean_7_grad/Shapegradients_2/Mean_7_grad/Fill*
N*
T0*0
_class&
$"loc:@gradients_2/Mean_7_grad/Shape
�
gradients_2/Mean_7_grad/ReshapeReshape?gradients_2/SquaredDifference_2_grad/tuple/control_dependency_1%gradients_2/Mean_7_grad/DynamicStitch*
T0*
Tshape0
�
#gradients_2/Mean_7_grad/BroadcastToBroadcastTogradients_2/Mean_7_grad/Reshapegradients_2/Mean_7_grad/Shape*
T0*

Tidx0
`
gradients_2/Mean_7_grad/Shape_2ShapeMean_7/input^sac_policy_opt*
T0*
out_type0
Z
gradients_2/Mean_7_grad/Shape_3ShapeMean_7^sac_policy_opt*
T0*
out_type0
\
gradients_2/Mean_7_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_7_grad/ProdProdgradients_2/Mean_7_grad/Shape_2gradients_2/Mean_7_grad/Const*
T0*

Tidx0*
	keep_dims( 
^
gradients_2/Mean_7_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_7_grad/Prod_1Prodgradients_2/Mean_7_grad/Shape_3gradients_2/Mean_7_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
\
!gradients_2/Mean_7_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
v
gradients_2/Mean_7_grad/MaximumMaximumgradients_2/Mean_7_grad/Prod_1!gradients_2/Mean_7_grad/Maximum/y*
T0
t
 gradients_2/Mean_7_grad/floordivFloorDivgradients_2/Mean_7_grad/Prodgradients_2/Mean_7_grad/Maximum*
T0
n
gradients_2/Mean_7_grad/CastCast gradients_2/Mean_7_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
v
gradients_2/Mean_7_grad/truedivRealDiv#gradients_2/Mean_7_grad/BroadcastTogradients_2/Mean_7_grad/Cast*
T0
`
gradients_2/Mean_11_grad/ShapeShapeMean_11/input^sac_policy_opt*
T0*
out_type0
�
gradients_2/Mean_11_grad/SizeConst^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_11_grad/addAddV2Mean_11/reduction_indicesgradients_2/Mean_11_grad/Size*
T0*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape
�
gradients_2/Mean_11_grad/modFloorModgradients_2/Mean_11_grad/addgradients_2/Mean_11_grad/Size*
T0*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape
�
 gradients_2/Mean_11_grad/Shape_1Const^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape*
dtype0*
valueB 
�
$gradients_2/Mean_11_grad/range/startConst^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape*
dtype0*
value	B : 
�
$gradients_2/Mean_11_grad/range/deltaConst^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_11_grad/rangeRange$gradients_2/Mean_11_grad/range/startgradients_2/Mean_11_grad/Size$gradients_2/Mean_11_grad/range/delta*

Tidx0*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape
�
#gradients_2/Mean_11_grad/Fill/valueConst^sac_policy_opt*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape*
dtype0*
value	B :
�
gradients_2/Mean_11_grad/FillFill gradients_2/Mean_11_grad/Shape_1#gradients_2/Mean_11_grad/Fill/value*
T0*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape*

index_type0
�
&gradients_2/Mean_11_grad/DynamicStitchDynamicStitchgradients_2/Mean_11_grad/rangegradients_2/Mean_11_grad/modgradients_2/Mean_11_grad/Shapegradients_2/Mean_11_grad/Fill*
N*
T0*1
_class'
%#loc:@gradients_2/Mean_11_grad/Shape
�
 gradients_2/Mean_11_grad/ReshapeReshape?gradients_2/SquaredDifference_4_grad/tuple/control_dependency_1&gradients_2/Mean_11_grad/DynamicStitch*
T0*
Tshape0
�
$gradients_2/Mean_11_grad/BroadcastToBroadcastTo gradients_2/Mean_11_grad/Reshapegradients_2/Mean_11_grad/Shape*
T0*

Tidx0
b
 gradients_2/Mean_11_grad/Shape_2ShapeMean_11/input^sac_policy_opt*
T0*
out_type0
\
 gradients_2/Mean_11_grad/Shape_3ShapeMean_11^sac_policy_opt*
T0*
out_type0
]
gradients_2/Mean_11_grad/ConstConst^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_11_grad/ProdProd gradients_2/Mean_11_grad/Shape_2gradients_2/Mean_11_grad/Const*
T0*

Tidx0*
	keep_dims( 
_
 gradients_2/Mean_11_grad/Const_1Const^sac_policy_opt*
dtype0*
valueB: 
�
gradients_2/Mean_11_grad/Prod_1Prod gradients_2/Mean_11_grad/Shape_3 gradients_2/Mean_11_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
]
"gradients_2/Mean_11_grad/Maximum/yConst^sac_policy_opt*
dtype0*
value	B :
y
 gradients_2/Mean_11_grad/MaximumMaximumgradients_2/Mean_11_grad/Prod_1"gradients_2/Mean_11_grad/Maximum/y*
T0
w
!gradients_2/Mean_11_grad/floordivFloorDivgradients_2/Mean_11_grad/Prod gradients_2/Mean_11_grad/Maximum*
T0
p
gradients_2/Mean_11_grad/CastCast!gradients_2/Mean_11_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
y
 gradients_2/Mean_11_grad/truedivRealDiv$gradients_2/Mean_11_grad/BroadcastTogradients_2/Mean_11_grad/Cast*
T0
�
;gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMulMatMulNgradients_2/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependency(critic/value/extrinsic_value/kernel/read*
T0*
transpose_a( *
transpose_b(
�
=gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMul_1MatMul!critic/value/encoder/hidden_3/MulNgradients_2/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Egradients_2/critic/value/extrinsic_value/MatMul_grad/tuple/group_depsNoOp<^gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMul>^gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMul_1^sac_policy_opt
�
Mgradients_2/critic/value/extrinsic_value/MatMul_grad/tuple/control_dependencyIdentity;gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMulF^gradients_2/critic/value/extrinsic_value/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMul
�
Ogradients_2/critic/value/extrinsic_value/MatMul_grad/tuple/control_dependency_1Identity=gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMul_1F^gradients_2/critic/value/extrinsic_value/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMul_1
�
;gradients_2/critic/value/curiosity_value/MatMul_grad/MatMulMatMulNgradients_2/critic/value/curiosity_value/BiasAdd_grad/tuple/control_dependency(critic/value/curiosity_value/kernel/read*
T0*
transpose_a( *
transpose_b(
�
=gradients_2/critic/value/curiosity_value/MatMul_grad/MatMul_1MatMul!critic/value/encoder/hidden_3/MulNgradients_2/critic/value/curiosity_value/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Egradients_2/critic/value/curiosity_value/MatMul_grad/tuple/group_depsNoOp<^gradients_2/critic/value/curiosity_value/MatMul_grad/MatMul>^gradients_2/critic/value/curiosity_value/MatMul_grad/MatMul_1^sac_policy_opt
�
Mgradients_2/critic/value/curiosity_value/MatMul_grad/tuple/control_dependencyIdentity;gradients_2/critic/value/curiosity_value/MatMul_grad/MatMulF^gradients_2/critic/value/curiosity_value/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_2/critic/value/curiosity_value/MatMul_grad/MatMul
�
Ogradients_2/critic/value/curiosity_value/MatMul_grad/tuple/control_dependency_1Identity=gradients_2/critic/value/curiosity_value/MatMul_grad/MatMul_1F^gradients_2/critic/value/curiosity_value/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_2/critic/value/curiosity_value/MatMul_grad/MatMul_1
p
%gradients_2/Mean_6/input_grad/unstackUnpackgradients_2/Mean_6_grad/truediv*
T0*

axis *	
num
r
&gradients_2/Mean_10/input_grad/unstackUnpack gradients_2/Mean_10_grad/truediv*
T0*

axis *	
num
p
%gradients_2/Mean_7/input_grad/unstackUnpackgradients_2/Mean_7_grad/truediv*
T0*

axis *	
num
r
&gradients_2/Mean_11/input_grad/unstackUnpack gradients_2/Mean_11_grad/truediv*
T0*

axis *	
num
�
gradients_2/AddNAddNMgradients_2/critic/value/extrinsic_value/MatMul_grad/tuple/control_dependencyMgradients_2/critic/value/curiosity_value/MatMul_grad/tuple/control_dependency*
N*
T0*N
_classD
B@loc:@gradients_2/critic/value/extrinsic_value/MatMul_grad/MatMul
�
8gradients_2/critic/value/encoder/hidden_3/Mul_grad/ShapeShape%critic/value/encoder/hidden_3/BiasAdd^sac_policy_opt*
T0*
out_type0
�
:gradients_2/critic/value/encoder/hidden_3/Mul_grad/Shape_1Shape%critic/value/encoder/hidden_3/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Hgradients_2/critic/value/encoder/hidden_3/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs8gradients_2/critic/value/encoder/hidden_3/Mul_grad/Shape:gradients_2/critic/value/encoder/hidden_3/Mul_grad/Shape_1*
T0

6gradients_2/critic/value/encoder/hidden_3/Mul_grad/MulMulgradients_2/AddN%critic/value/encoder/hidden_3/Sigmoid*
T0
�
6gradients_2/critic/value/encoder/hidden_3/Mul_grad/SumSum6gradients_2/critic/value/encoder/hidden_3/Mul_grad/MulHgradients_2/critic/value/encoder/hidden_3/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
:gradients_2/critic/value/encoder/hidden_3/Mul_grad/ReshapeReshape6gradients_2/critic/value/encoder/hidden_3/Mul_grad/Sum8gradients_2/critic/value/encoder/hidden_3/Mul_grad/Shape*
T0*
Tshape0
�
8gradients_2/critic/value/encoder/hidden_3/Mul_grad/Mul_1Mul%critic/value/encoder/hidden_3/BiasAddgradients_2/AddN*
T0
�
8gradients_2/critic/value/encoder/hidden_3/Mul_grad/Sum_1Sum8gradients_2/critic/value/encoder/hidden_3/Mul_grad/Mul_1Jgradients_2/critic/value/encoder/hidden_3/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
<gradients_2/critic/value/encoder/hidden_3/Mul_grad/Reshape_1Reshape8gradients_2/critic/value/encoder/hidden_3/Mul_grad/Sum_1:gradients_2/critic/value/encoder/hidden_3/Mul_grad/Shape_1*
T0*
Tshape0
�
Cgradients_2/critic/value/encoder/hidden_3/Mul_grad/tuple/group_depsNoOp;^gradients_2/critic/value/encoder/hidden_3/Mul_grad/Reshape=^gradients_2/critic/value/encoder/hidden_3/Mul_grad/Reshape_1^sac_policy_opt
�
Kgradients_2/critic/value/encoder/hidden_3/Mul_grad/tuple/control_dependencyIdentity:gradients_2/critic/value/encoder/hidden_3/Mul_grad/ReshapeD^gradients_2/critic/value/encoder/hidden_3/Mul_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_3/Mul_grad/Reshape
�
Mgradients_2/critic/value/encoder/hidden_3/Mul_grad/tuple/control_dependency_1Identity<gradients_2/critic/value/encoder/hidden_3/Mul_grad/Reshape_1D^gradients_2/critic/value/encoder/hidden_3/Mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_2/critic/value/encoder/hidden_3/Mul_grad/Reshape_1
`
gradients_2/Sum_6_grad/ShapeShapestrided_slice_4^sac_policy_opt*
T0*
out_type0
�
"gradients_2/Sum_6_grad/BroadcastToBroadcastTo%gradients_2/Mean_6/input_grad/unstackgradients_2/Sum_6_grad/Shape*
T0*

Tidx0
`
gradients_2/Sum_8_grad/ShapeShapestrided_slice_6^sac_policy_opt*
T0*
out_type0
�
"gradients_2/Sum_8_grad/BroadcastToBroadcastTo&gradients_2/Mean_10/input_grad/unstackgradients_2/Sum_8_grad/Shape*
T0*

Tidx0
`
gradients_2/Sum_7_grad/ShapeShapestrided_slice_5^sac_policy_opt*
T0*
out_type0
�
"gradients_2/Sum_7_grad/BroadcastToBroadcastTo%gradients_2/Mean_7/input_grad/unstackgradients_2/Sum_7_grad/Shape*
T0*

Tidx0
`
gradients_2/Sum_9_grad/ShapeShapestrided_slice_7^sac_policy_opt*
T0*
out_type0
�
"gradients_2/Sum_9_grad/BroadcastToBroadcastTo&gradients_2/Mean_11/input_grad/unstackgradients_2/Sum_9_grad/Shape*
T0*

Tidx0
�
Bgradients_2/critic/value/encoder/hidden_3/Sigmoid_grad/SigmoidGradSigmoidGrad%critic/value/encoder/hidden_3/SigmoidMgradients_2/critic/value/encoder/hidden_3/Mul_grad/tuple/control_dependency_1*
T0
a
&gradients_2/strided_slice_4_grad/ShapeShapemul_13^sac_policy_opt*
T0*
out_type0
}
7gradients_2/strided_slice_4_grad/StridedSliceGrad/beginConst^sac_policy_opt*
dtype0*
valueB"        
{
5gradients_2/strided_slice_4_grad/StridedSliceGrad/endConst^sac_policy_opt*
dtype0*
valueB"    @   

9gradients_2/strided_slice_4_grad/StridedSliceGrad/stridesConst^sac_policy_opt*
dtype0*
valueB"      
�
1gradients_2/strided_slice_4_grad/StridedSliceGradStridedSliceGrad&gradients_2/strided_slice_4_grad/Shape7gradients_2/strided_slice_4_grad/StridedSliceGrad/begin5gradients_2/strided_slice_4_grad/StridedSliceGrad/end9gradients_2/strided_slice_4_grad/StridedSliceGrad/strides"gradients_2/Sum_6_grad/BroadcastTo*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
a
&gradients_2/strided_slice_6_grad/ShapeShapemul_22^sac_policy_opt*
T0*
out_type0
}
7gradients_2/strided_slice_6_grad/StridedSliceGrad/beginConst^sac_policy_opt*
dtype0*
valueB"        
{
5gradients_2/strided_slice_6_grad/StridedSliceGrad/endConst^sac_policy_opt*
dtype0*
valueB"    @   

9gradients_2/strided_slice_6_grad/StridedSliceGrad/stridesConst^sac_policy_opt*
dtype0*
valueB"      
�
1gradients_2/strided_slice_6_grad/StridedSliceGradStridedSliceGrad&gradients_2/strided_slice_6_grad/Shape7gradients_2/strided_slice_6_grad/StridedSliceGrad/begin5gradients_2/strided_slice_6_grad/StridedSliceGrad/end9gradients_2/strided_slice_6_grad/StridedSliceGrad/strides"gradients_2/Sum_8_grad/BroadcastTo*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
a
&gradients_2/strided_slice_5_grad/ShapeShapemul_14^sac_policy_opt*
T0*
out_type0
}
7gradients_2/strided_slice_5_grad/StridedSliceGrad/beginConst^sac_policy_opt*
dtype0*
valueB"        
{
5gradients_2/strided_slice_5_grad/StridedSliceGrad/endConst^sac_policy_opt*
dtype0*
valueB"    @   

9gradients_2/strided_slice_5_grad/StridedSliceGrad/stridesConst^sac_policy_opt*
dtype0*
valueB"      
�
1gradients_2/strided_slice_5_grad/StridedSliceGradStridedSliceGrad&gradients_2/strided_slice_5_grad/Shape7gradients_2/strided_slice_5_grad/StridedSliceGrad/begin5gradients_2/strided_slice_5_grad/StridedSliceGrad/end9gradients_2/strided_slice_5_grad/StridedSliceGrad/strides"gradients_2/Sum_7_grad/BroadcastTo*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
a
&gradients_2/strided_slice_7_grad/ShapeShapemul_23^sac_policy_opt*
T0*
out_type0
}
7gradients_2/strided_slice_7_grad/StridedSliceGrad/beginConst^sac_policy_opt*
dtype0*
valueB"        
{
5gradients_2/strided_slice_7_grad/StridedSliceGrad/endConst^sac_policy_opt*
dtype0*
valueB"    @   

9gradients_2/strided_slice_7_grad/StridedSliceGrad/stridesConst^sac_policy_opt*
dtype0*
valueB"      
�
1gradients_2/strided_slice_7_grad/StridedSliceGradStridedSliceGrad&gradients_2/strided_slice_7_grad/Shape7gradients_2/strided_slice_7_grad/StridedSliceGrad/begin5gradients_2/strided_slice_7_grad/StridedSliceGrad/end9gradients_2/strided_slice_7_grad/StridedSliceGrad/strides"gradients_2/Sum_9_grad/BroadcastTo*
Index0*
T0*

begin_mask*
ellipsis_mask *
end_mask*
new_axis_mask *
shrink_axis_mask 
�
gradients_2/AddN_1AddNKgradients_2/critic/value/encoder/hidden_3/Mul_grad/tuple/control_dependencyBgradients_2/critic/value/encoder/hidden_3/Sigmoid_grad/SigmoidGrad*
N*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_3/Mul_grad/Reshape
�
Bgradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_1*
T0*
data_formatNHWC
�
Ggradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_1C^gradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Ogradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_1H^gradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_3/Mul_grad/Reshape
�
Qgradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/tuple/control_dependency_1IdentityBgradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/BiasAddGradH^gradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/BiasAddGrad
^
gradients_2/mul_13_grad/ShapeShapeStopGradient^sac_policy_opt*
T0*
out_type0
}
gradients_2/mul_13_grad/Shape_1Shape)critic/q/q1_encoding/extrinsic_q1/BiasAdd^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_13_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_13_grad/Shapegradients_2/mul_13_grad/Shape_1*
T0
�
gradients_2/mul_13_grad/MulMul1gradients_2/strided_slice_4_grad/StridedSliceGrad)critic/q/q1_encoding/extrinsic_q1/BiasAdd*
T0
�
gradients_2/mul_13_grad/SumSumgradients_2/mul_13_grad/Mul-gradients_2/mul_13_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_13_grad/ReshapeReshapegradients_2/mul_13_grad/Sumgradients_2/mul_13_grad/Shape*
T0*
Tshape0
n
gradients_2/mul_13_grad/Mul_1MulStopGradient1gradients_2/strided_slice_4_grad/StridedSliceGrad*
T0
�
gradients_2/mul_13_grad/Sum_1Sumgradients_2/mul_13_grad/Mul_1/gradients_2/mul_13_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_13_grad/Reshape_1Reshapegradients_2/mul_13_grad/Sum_1gradients_2/mul_13_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_13_grad/tuple/group_depsNoOp ^gradients_2/mul_13_grad/Reshape"^gradients_2/mul_13_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_13_grad/tuple/control_dependencyIdentitygradients_2/mul_13_grad/Reshape)^gradients_2/mul_13_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_13_grad/Reshape
�
2gradients_2/mul_13_grad/tuple/control_dependency_1Identity!gradients_2/mul_13_grad/Reshape_1)^gradients_2/mul_13_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_13_grad/Reshape_1
^
gradients_2/mul_22_grad/ShapeShapeStopGradient^sac_policy_opt*
T0*
out_type0
}
gradients_2/mul_22_grad/Shape_1Shape)critic/q/q1_encoding/curiosity_q1/BiasAdd^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_22_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_22_grad/Shapegradients_2/mul_22_grad/Shape_1*
T0
�
gradients_2/mul_22_grad/MulMul1gradients_2/strided_slice_6_grad/StridedSliceGrad)critic/q/q1_encoding/curiosity_q1/BiasAdd*
T0
�
gradients_2/mul_22_grad/SumSumgradients_2/mul_22_grad/Mul-gradients_2/mul_22_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_22_grad/ReshapeReshapegradients_2/mul_22_grad/Sumgradients_2/mul_22_grad/Shape*
T0*
Tshape0
n
gradients_2/mul_22_grad/Mul_1MulStopGradient1gradients_2/strided_slice_6_grad/StridedSliceGrad*
T0
�
gradients_2/mul_22_grad/Sum_1Sumgradients_2/mul_22_grad/Mul_1/gradients_2/mul_22_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_22_grad/Reshape_1Reshapegradients_2/mul_22_grad/Sum_1gradients_2/mul_22_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_22_grad/tuple/group_depsNoOp ^gradients_2/mul_22_grad/Reshape"^gradients_2/mul_22_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_22_grad/tuple/control_dependencyIdentitygradients_2/mul_22_grad/Reshape)^gradients_2/mul_22_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_22_grad/Reshape
�
2gradients_2/mul_22_grad/tuple/control_dependency_1Identity!gradients_2/mul_22_grad/Reshape_1)^gradients_2/mul_22_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_22_grad/Reshape_1
^
gradients_2/mul_14_grad/ShapeShapeStopGradient^sac_policy_opt*
T0*
out_type0
}
gradients_2/mul_14_grad/Shape_1Shape)critic/q/q2_encoding/extrinsic_q2/BiasAdd^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_14_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_14_grad/Shapegradients_2/mul_14_grad/Shape_1*
T0
�
gradients_2/mul_14_grad/MulMul1gradients_2/strided_slice_5_grad/StridedSliceGrad)critic/q/q2_encoding/extrinsic_q2/BiasAdd*
T0
�
gradients_2/mul_14_grad/SumSumgradients_2/mul_14_grad/Mul-gradients_2/mul_14_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_14_grad/ReshapeReshapegradients_2/mul_14_grad/Sumgradients_2/mul_14_grad/Shape*
T0*
Tshape0
n
gradients_2/mul_14_grad/Mul_1MulStopGradient1gradients_2/strided_slice_5_grad/StridedSliceGrad*
T0
�
gradients_2/mul_14_grad/Sum_1Sumgradients_2/mul_14_grad/Mul_1/gradients_2/mul_14_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_14_grad/Reshape_1Reshapegradients_2/mul_14_grad/Sum_1gradients_2/mul_14_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_14_grad/tuple/group_depsNoOp ^gradients_2/mul_14_grad/Reshape"^gradients_2/mul_14_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_14_grad/tuple/control_dependencyIdentitygradients_2/mul_14_grad/Reshape)^gradients_2/mul_14_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_14_grad/Reshape
�
2gradients_2/mul_14_grad/tuple/control_dependency_1Identity!gradients_2/mul_14_grad/Reshape_1)^gradients_2/mul_14_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_14_grad/Reshape_1
^
gradients_2/mul_23_grad/ShapeShapeStopGradient^sac_policy_opt*
T0*
out_type0
}
gradients_2/mul_23_grad/Shape_1Shape)critic/q/q2_encoding/curiosity_q2/BiasAdd^sac_policy_opt*
T0*
out_type0
�
-gradients_2/mul_23_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_23_grad/Shapegradients_2/mul_23_grad/Shape_1*
T0
�
gradients_2/mul_23_grad/MulMul1gradients_2/strided_slice_7_grad/StridedSliceGrad)critic/q/q2_encoding/curiosity_q2/BiasAdd*
T0
�
gradients_2/mul_23_grad/SumSumgradients_2/mul_23_grad/Mul-gradients_2/mul_23_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_2/mul_23_grad/ReshapeReshapegradients_2/mul_23_grad/Sumgradients_2/mul_23_grad/Shape*
T0*
Tshape0
n
gradients_2/mul_23_grad/Mul_1MulStopGradient1gradients_2/strided_slice_7_grad/StridedSliceGrad*
T0
�
gradients_2/mul_23_grad/Sum_1Sumgradients_2/mul_23_grad/Mul_1/gradients_2/mul_23_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_2/mul_23_grad/Reshape_1Reshapegradients_2/mul_23_grad/Sum_1gradients_2/mul_23_grad/Shape_1*
T0*
Tshape0
�
(gradients_2/mul_23_grad/tuple/group_depsNoOp ^gradients_2/mul_23_grad/Reshape"^gradients_2/mul_23_grad/Reshape_1^sac_policy_opt
�
0gradients_2/mul_23_grad/tuple/control_dependencyIdentitygradients_2/mul_23_grad/Reshape)^gradients_2/mul_23_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_23_grad/Reshape
�
2gradients_2/mul_23_grad/tuple/control_dependency_1Identity!gradients_2/mul_23_grad/Reshape_1)^gradients_2/mul_23_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_23_grad/Reshape_1
�
<gradients_2/critic/value/encoder/hidden_3/MatMul_grad/MatMulMatMulOgradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/tuple/control_dependency)critic/value/encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b(
�
>gradients_2/critic/value/encoder/hidden_3/MatMul_grad/MatMul_1MatMul!critic/value/encoder/hidden_2/MulOgradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Fgradients_2/critic/value/encoder/hidden_3/MatMul_grad/tuple/group_depsNoOp=^gradients_2/critic/value/encoder/hidden_3/MatMul_grad/MatMul?^gradients_2/critic/value/encoder/hidden_3/MatMul_grad/MatMul_1^sac_policy_opt
�
Ngradients_2/critic/value/encoder/hidden_3/MatMul_grad/tuple/control_dependencyIdentity<gradients_2/critic/value/encoder/hidden_3/MatMul_grad/MatMulG^gradients_2/critic/value/encoder/hidden_3/MatMul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_2/critic/value/encoder/hidden_3/MatMul_grad/MatMul
�
Pgradients_2/critic/value/encoder/hidden_3/MatMul_grad/tuple/control_dependency_1Identity>gradients_2/critic/value/encoder/hidden_3/MatMul_grad/MatMul_1G^gradients_2/critic/value/encoder/hidden_3/MatMul_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@gradients_2/critic/value/encoder/hidden_3/MatMul_grad/MatMul_1
�
Fgradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/BiasAddGradBiasAddGrad2gradients_2/mul_13_grad/tuple/control_dependency_1*
T0*
data_formatNHWC
�
Kgradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/group_depsNoOpG^gradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/BiasAddGrad3^gradients_2/mul_13_grad/tuple/control_dependency_1^sac_policy_opt
�
Sgradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependencyIdentity2gradients_2/mul_13_grad/tuple/control_dependency_1L^gradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_13_grad/Reshape_1
�
Ugradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependency_1IdentityFgradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/BiasAddGradL^gradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/group_deps*
T0*Y
_classO
MKloc:@gradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/BiasAddGrad
�
Fgradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/BiasAddGradBiasAddGrad2gradients_2/mul_22_grad/tuple/control_dependency_1*
T0*
data_formatNHWC
�
Kgradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/tuple/group_depsNoOpG^gradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/BiasAddGrad3^gradients_2/mul_22_grad/tuple/control_dependency_1^sac_policy_opt
�
Sgradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/tuple/control_dependencyIdentity2gradients_2/mul_22_grad/tuple/control_dependency_1L^gradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_22_grad/Reshape_1
�
Ugradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/tuple/control_dependency_1IdentityFgradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/BiasAddGradL^gradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/tuple/group_deps*
T0*Y
_classO
MKloc:@gradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/BiasAddGrad
�
Fgradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/BiasAddGradBiasAddGrad2gradients_2/mul_14_grad/tuple/control_dependency_1*
T0*
data_formatNHWC
�
Kgradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/group_depsNoOpG^gradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/BiasAddGrad3^gradients_2/mul_14_grad/tuple/control_dependency_1^sac_policy_opt
�
Sgradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependencyIdentity2gradients_2/mul_14_grad/tuple/control_dependency_1L^gradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_14_grad/Reshape_1
�
Ugradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependency_1IdentityFgradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/BiasAddGradL^gradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/group_deps*
T0*Y
_classO
MKloc:@gradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/BiasAddGrad
�
Fgradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/BiasAddGradBiasAddGrad2gradients_2/mul_23_grad/tuple/control_dependency_1*
T0*
data_formatNHWC
�
Kgradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/tuple/group_depsNoOpG^gradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/BiasAddGrad3^gradients_2/mul_23_grad/tuple/control_dependency_1^sac_policy_opt
�
Sgradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/tuple/control_dependencyIdentity2gradients_2/mul_23_grad/tuple/control_dependency_1L^gradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_23_grad/Reshape_1
�
Ugradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/tuple/control_dependency_1IdentityFgradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/BiasAddGradL^gradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/tuple/group_deps*
T0*Y
_classO
MKloc:@gradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/BiasAddGrad
�
8gradients_2/critic/value/encoder/hidden_2/Mul_grad/ShapeShape%critic/value/encoder/hidden_2/BiasAdd^sac_policy_opt*
T0*
out_type0
�
:gradients_2/critic/value/encoder/hidden_2/Mul_grad/Shape_1Shape%critic/value/encoder/hidden_2/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Hgradients_2/critic/value/encoder/hidden_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs8gradients_2/critic/value/encoder/hidden_2/Mul_grad/Shape:gradients_2/critic/value/encoder/hidden_2/Mul_grad/Shape_1*
T0
�
6gradients_2/critic/value/encoder/hidden_2/Mul_grad/MulMulNgradients_2/critic/value/encoder/hidden_3/MatMul_grad/tuple/control_dependency%critic/value/encoder/hidden_2/Sigmoid*
T0
�
6gradients_2/critic/value/encoder/hidden_2/Mul_grad/SumSum6gradients_2/critic/value/encoder/hidden_2/Mul_grad/MulHgradients_2/critic/value/encoder/hidden_2/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
:gradients_2/critic/value/encoder/hidden_2/Mul_grad/ReshapeReshape6gradients_2/critic/value/encoder/hidden_2/Mul_grad/Sum8gradients_2/critic/value/encoder/hidden_2/Mul_grad/Shape*
T0*
Tshape0
�
8gradients_2/critic/value/encoder/hidden_2/Mul_grad/Mul_1Mul%critic/value/encoder/hidden_2/BiasAddNgradients_2/critic/value/encoder/hidden_3/MatMul_grad/tuple/control_dependency*
T0
�
8gradients_2/critic/value/encoder/hidden_2/Mul_grad/Sum_1Sum8gradients_2/critic/value/encoder/hidden_2/Mul_grad/Mul_1Jgradients_2/critic/value/encoder/hidden_2/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
<gradients_2/critic/value/encoder/hidden_2/Mul_grad/Reshape_1Reshape8gradients_2/critic/value/encoder/hidden_2/Mul_grad/Sum_1:gradients_2/critic/value/encoder/hidden_2/Mul_grad/Shape_1*
T0*
Tshape0
�
Cgradients_2/critic/value/encoder/hidden_2/Mul_grad/tuple/group_depsNoOp;^gradients_2/critic/value/encoder/hidden_2/Mul_grad/Reshape=^gradients_2/critic/value/encoder/hidden_2/Mul_grad/Reshape_1^sac_policy_opt
�
Kgradients_2/critic/value/encoder/hidden_2/Mul_grad/tuple/control_dependencyIdentity:gradients_2/critic/value/encoder/hidden_2/Mul_grad/ReshapeD^gradients_2/critic/value/encoder/hidden_2/Mul_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_2/Mul_grad/Reshape
�
Mgradients_2/critic/value/encoder/hidden_2/Mul_grad/tuple/control_dependency_1Identity<gradients_2/critic/value/encoder/hidden_2/Mul_grad/Reshape_1D^gradients_2/critic/value/encoder/hidden_2/Mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_2/critic/value/encoder/hidden_2/Mul_grad/Reshape_1
�
@gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMulMatMulSgradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependency-critic/q/q1_encoding/extrinsic_q1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Bgradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul_1MatMul,critic/q/q1_encoding/q1_encoder/hidden_3/MulSgradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Jgradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/group_depsNoOpA^gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMulC^gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul_1^sac_policy_opt
�
Rgradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/control_dependencyIdentity@gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMulK^gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul
�
Tgradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/control_dependency_1IdentityBgradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul_1K^gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul_1
�
@gradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/MatMulMatMulSgradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/tuple/control_dependency-critic/q/q1_encoding/curiosity_q1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Bgradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/MatMul_1MatMul,critic/q/q1_encoding/q1_encoder/hidden_3/MulSgradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Jgradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/tuple/group_depsNoOpA^gradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/MatMulC^gradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/MatMul_1^sac_policy_opt
�
Rgradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/tuple/control_dependencyIdentity@gradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/MatMulK^gradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/MatMul
�
Tgradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/tuple/control_dependency_1IdentityBgradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/MatMul_1K^gradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/MatMul_1
�
@gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMulMatMulSgradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependency-critic/q/q2_encoding/extrinsic_q2/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Bgradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul_1MatMul,critic/q/q2_encoding/q2_encoder/hidden_3/MulSgradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Jgradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/group_depsNoOpA^gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMulC^gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul_1^sac_policy_opt
�
Rgradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/control_dependencyIdentity@gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMulK^gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul
�
Tgradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/control_dependency_1IdentityBgradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul_1K^gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul_1
�
@gradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/MatMulMatMulSgradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/tuple/control_dependency-critic/q/q2_encoding/curiosity_q2/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Bgradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/MatMul_1MatMul,critic/q/q2_encoding/q2_encoder/hidden_3/MulSgradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Jgradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/tuple/group_depsNoOpA^gradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/MatMulC^gradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/MatMul_1^sac_policy_opt
�
Rgradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/tuple/control_dependencyIdentity@gradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/MatMulK^gradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/MatMul
�
Tgradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/tuple/control_dependency_1IdentityBgradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/MatMul_1K^gradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/MatMul_1
�
Bgradients_2/critic/value/encoder/hidden_2/Sigmoid_grad/SigmoidGradSigmoidGrad%critic/value/encoder/hidden_2/SigmoidMgradients_2/critic/value/encoder/hidden_2/Mul_grad/tuple/control_dependency_1*
T0
�
gradients_2/AddN_2AddNRgradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/control_dependencyRgradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/tuple/control_dependency*
N*
T0*S
_classI
GEloc:@gradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/ShapeShape0critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd^sac_policy_opt*
T0*
out_type0
�
Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Shape_1Shape0critic/q/q1_encoding/q1_encoder/hidden_3/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Sgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/ShapeEgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Shape_1*
T0
�
Agradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/MulMulgradients_2/AddN_20critic/q/q1_encoding/q1_encoder/hidden_3/Sigmoid*
T0
�
Agradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/SumSumAgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/MulSgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/ReshapeReshapeAgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/SumCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Shape*
T0*
Tshape0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Mul_1Mul0critic/q/q1_encoding/q1_encoder/hidden_3/BiasAddgradients_2/AddN_2*
T0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Sum_1SumCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Mul_1Ugradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Ggradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Reshape_1ReshapeCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Sum_1Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Shape_1*
T0*
Tshape0
�
Ngradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/tuple/group_depsNoOpF^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/ReshapeH^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Reshape_1^sac_policy_opt
�
Vgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/tuple/control_dependencyIdentityEgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/ReshapeO^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Reshape
�
Xgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/tuple/control_dependency_1IdentityGgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Reshape_1O^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Reshape_1
�
gradients_2/AddN_3AddNRgradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/control_dependencyRgradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/tuple/control_dependency*
N*
T0*S
_classI
GEloc:@gradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/ShapeShape0critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd^sac_policy_opt*
T0*
out_type0
�
Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Shape_1Shape0critic/q/q2_encoding/q2_encoder/hidden_3/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Sgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/ShapeEgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Shape_1*
T0
�
Agradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/MulMulgradients_2/AddN_30critic/q/q2_encoding/q2_encoder/hidden_3/Sigmoid*
T0
�
Agradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/SumSumAgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/MulSgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/ReshapeReshapeAgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/SumCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Shape*
T0*
Tshape0
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Mul_1Mul0critic/q/q2_encoding/q2_encoder/hidden_3/BiasAddgradients_2/AddN_3*
T0
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Sum_1SumCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Mul_1Ugradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Ggradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Reshape_1ReshapeCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Sum_1Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Shape_1*
T0*
Tshape0
�
Ngradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/tuple/group_depsNoOpF^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/ReshapeH^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Reshape_1^sac_policy_opt
�
Vgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/tuple/control_dependencyIdentityEgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/ReshapeO^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Reshape
�
Xgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/tuple/control_dependency_1IdentityGgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Reshape_1O^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Reshape_1
�
gradients_2/AddN_4AddNKgradients_2/critic/value/encoder/hidden_2/Mul_grad/tuple/control_dependencyBgradients_2/critic/value/encoder/hidden_2/Sigmoid_grad/SigmoidGrad*
N*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_2/Mul_grad/Reshape
�
Bgradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_4*
T0*
data_formatNHWC
�
Ggradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_4C^gradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Ogradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_4H^gradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_2/Mul_grad/Reshape
�
Qgradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/tuple/control_dependency_1IdentityBgradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/BiasAddGradH^gradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/BiasAddGrad
�
Mgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q1_encoding/q1_encoder/hidden_3/SigmoidXgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/tuple/control_dependency_1*
T0
�
Mgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q2_encoding/q2_encoder/hidden_3/SigmoidXgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/tuple/control_dependency_1*
T0
�
<gradients_2/critic/value/encoder/hidden_2/MatMul_grad/MatMulMatMulOgradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/tuple/control_dependency)critic/value/encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b(
�
>gradients_2/critic/value/encoder/hidden_2/MatMul_grad/MatMul_1MatMul!critic/value/encoder/hidden_1/MulOgradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Fgradients_2/critic/value/encoder/hidden_2/MatMul_grad/tuple/group_depsNoOp=^gradients_2/critic/value/encoder/hidden_2/MatMul_grad/MatMul?^gradients_2/critic/value/encoder/hidden_2/MatMul_grad/MatMul_1^sac_policy_opt
�
Ngradients_2/critic/value/encoder/hidden_2/MatMul_grad/tuple/control_dependencyIdentity<gradients_2/critic/value/encoder/hidden_2/MatMul_grad/MatMulG^gradients_2/critic/value/encoder/hidden_2/MatMul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_2/critic/value/encoder/hidden_2/MatMul_grad/MatMul
�
Pgradients_2/critic/value/encoder/hidden_2/MatMul_grad/tuple/control_dependency_1Identity>gradients_2/critic/value/encoder/hidden_2/MatMul_grad/MatMul_1G^gradients_2/critic/value/encoder/hidden_2/MatMul_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@gradients_2/critic/value/encoder/hidden_2/MatMul_grad/MatMul_1
�
gradients_2/AddN_5AddNVgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/tuple/control_dependencyMgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Sigmoid_grad/SigmoidGrad*
N*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Reshape
�
Mgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_5*
T0*
data_formatNHWC
�
Rgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_5N^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Zgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_5S^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/Mul_grad/Reshape
�
\gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/BiasAddGradS^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/BiasAddGrad
�
gradients_2/AddN_6AddNVgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/tuple/control_dependencyMgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Sigmoid_grad/SigmoidGrad*
N*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Reshape
�
Mgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_6*
T0*
data_formatNHWC
�
Rgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_6N^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Zgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_6S^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/Mul_grad/Reshape
�
\gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/BiasAddGradS^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/BiasAddGrad
�
8gradients_2/critic/value/encoder/hidden_1/Mul_grad/ShapeShape%critic/value/encoder/hidden_1/BiasAdd^sac_policy_opt*
T0*
out_type0
�
:gradients_2/critic/value/encoder/hidden_1/Mul_grad/Shape_1Shape%critic/value/encoder/hidden_1/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Hgradients_2/critic/value/encoder/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs8gradients_2/critic/value/encoder/hidden_1/Mul_grad/Shape:gradients_2/critic/value/encoder/hidden_1/Mul_grad/Shape_1*
T0
�
6gradients_2/critic/value/encoder/hidden_1/Mul_grad/MulMulNgradients_2/critic/value/encoder/hidden_2/MatMul_grad/tuple/control_dependency%critic/value/encoder/hidden_1/Sigmoid*
T0
�
6gradients_2/critic/value/encoder/hidden_1/Mul_grad/SumSum6gradients_2/critic/value/encoder/hidden_1/Mul_grad/MulHgradients_2/critic/value/encoder/hidden_1/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
:gradients_2/critic/value/encoder/hidden_1/Mul_grad/ReshapeReshape6gradients_2/critic/value/encoder/hidden_1/Mul_grad/Sum8gradients_2/critic/value/encoder/hidden_1/Mul_grad/Shape*
T0*
Tshape0
�
8gradients_2/critic/value/encoder/hidden_1/Mul_grad/Mul_1Mul%critic/value/encoder/hidden_1/BiasAddNgradients_2/critic/value/encoder/hidden_2/MatMul_grad/tuple/control_dependency*
T0
�
8gradients_2/critic/value/encoder/hidden_1/Mul_grad/Sum_1Sum8gradients_2/critic/value/encoder/hidden_1/Mul_grad/Mul_1Jgradients_2/critic/value/encoder/hidden_1/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
<gradients_2/critic/value/encoder/hidden_1/Mul_grad/Reshape_1Reshape8gradients_2/critic/value/encoder/hidden_1/Mul_grad/Sum_1:gradients_2/critic/value/encoder/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
�
Cgradients_2/critic/value/encoder/hidden_1/Mul_grad/tuple/group_depsNoOp;^gradients_2/critic/value/encoder/hidden_1/Mul_grad/Reshape=^gradients_2/critic/value/encoder/hidden_1/Mul_grad/Reshape_1^sac_policy_opt
�
Kgradients_2/critic/value/encoder/hidden_1/Mul_grad/tuple/control_dependencyIdentity:gradients_2/critic/value/encoder/hidden_1/Mul_grad/ReshapeD^gradients_2/critic/value/encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_1/Mul_grad/Reshape
�
Mgradients_2/critic/value/encoder/hidden_1/Mul_grad/tuple/control_dependency_1Identity<gradients_2/critic/value/encoder/hidden_1/Mul_grad/Reshape_1D^gradients_2/critic/value/encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_2/critic/value/encoder/hidden_1/Mul_grad/Reshape_1
�
Ggradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/MatMulMatMulZgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/tuple/control_dependency4critic/q/q1_encoding/q1_encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Igradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/MatMul_1MatMul,critic/q/q1_encoding/q1_encoder/hidden_2/MulZgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Qgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/tuple/group_depsNoOpH^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/MatMulJ^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/MatMul_1^sac_policy_opt
�
Ygradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/tuple/control_dependencyIdentityGgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/MatMulR^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/MatMul
�
[gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/tuple/control_dependency_1IdentityIgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/MatMul_1R^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/MatMul_1
�
Ggradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/MatMulMatMulZgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/tuple/control_dependency4critic/q/q2_encoding/q2_encoder/hidden_3/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Igradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/MatMul_1MatMul,critic/q/q2_encoding/q2_encoder/hidden_2/MulZgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Qgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/tuple/group_depsNoOpH^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/MatMulJ^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/MatMul_1^sac_policy_opt
�
Ygradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/tuple/control_dependencyIdentityGgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/MatMulR^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/MatMul
�
[gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/tuple/control_dependency_1IdentityIgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/MatMul_1R^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/MatMul_1
�
Bgradients_2/critic/value/encoder/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad%critic/value/encoder/hidden_1/SigmoidMgradients_2/critic/value/encoder/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/ShapeShape0critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd^sac_policy_opt*
T0*
out_type0
�
Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Shape_1Shape0critic/q/q1_encoding/q1_encoder/hidden_2/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Sgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/ShapeEgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Shape_1*
T0
�
Agradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/MulMulYgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/tuple/control_dependency0critic/q/q1_encoding/q1_encoder/hidden_2/Sigmoid*
T0
�
Agradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/SumSumAgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/MulSgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/ReshapeReshapeAgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/SumCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Shape*
T0*
Tshape0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Mul_1Mul0critic/q/q1_encoding/q1_encoder/hidden_2/BiasAddYgradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/tuple/control_dependency*
T0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Sum_1SumCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Mul_1Ugradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Ggradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Reshape_1ReshapeCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Sum_1Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Shape_1*
T0*
Tshape0
�
Ngradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/tuple/group_depsNoOpF^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/ReshapeH^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Reshape_1^sac_policy_opt
�
Vgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/tuple/control_dependencyIdentityEgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/ReshapeO^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Reshape
�
Xgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/tuple/control_dependency_1IdentityGgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Reshape_1O^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Reshape_1
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/ShapeShape0critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd^sac_policy_opt*
T0*
out_type0
�
Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Shape_1Shape0critic/q/q2_encoding/q2_encoder/hidden_2/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Sgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/ShapeEgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Shape_1*
T0
�
Agradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/MulMulYgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/tuple/control_dependency0critic/q/q2_encoding/q2_encoder/hidden_2/Sigmoid*
T0
�
Agradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/SumSumAgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/MulSgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/ReshapeReshapeAgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/SumCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Shape*
T0*
Tshape0
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Mul_1Mul0critic/q/q2_encoding/q2_encoder/hidden_2/BiasAddYgradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/tuple/control_dependency*
T0
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Sum_1SumCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Mul_1Ugradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Ggradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Reshape_1ReshapeCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Sum_1Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Shape_1*
T0*
Tshape0
�
Ngradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/tuple/group_depsNoOpF^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/ReshapeH^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Reshape_1^sac_policy_opt
�
Vgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/tuple/control_dependencyIdentityEgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/ReshapeO^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Reshape
�
Xgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/tuple/control_dependency_1IdentityGgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Reshape_1O^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Reshape_1
�
gradients_2/AddN_7AddNKgradients_2/critic/value/encoder/hidden_1/Mul_grad/tuple/control_dependencyBgradients_2/critic/value/encoder/hidden_1/Sigmoid_grad/SigmoidGrad*
N*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_1/Mul_grad/Reshape
�
Bgradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_7*
T0*
data_formatNHWC
�
Ggradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_7C^gradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Ogradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_7H^gradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_1/Mul_grad/Reshape
�
Qgradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityBgradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/BiasAddGradH^gradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/BiasAddGrad
�
Mgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q1_encoding/q1_encoder/hidden_2/SigmoidXgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/tuple/control_dependency_1*
T0
�
Mgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q2_encoding/q2_encoder/hidden_2/SigmoidXgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/tuple/control_dependency_1*
T0
�
<gradients_2/critic/value/encoder/hidden_1/MatMul_grad/MatMulMatMulOgradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency)critic/value/encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
>gradients_2/critic/value/encoder/hidden_1/MatMul_grad/MatMul_1MatMul!critic/value/encoder/hidden_0/MulOgradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Fgradients_2/critic/value/encoder/hidden_1/MatMul_grad/tuple/group_depsNoOp=^gradients_2/critic/value/encoder/hidden_1/MatMul_grad/MatMul?^gradients_2/critic/value/encoder/hidden_1/MatMul_grad/MatMul_1^sac_policy_opt
�
Ngradients_2/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependencyIdentity<gradients_2/critic/value/encoder/hidden_1/MatMul_grad/MatMulG^gradients_2/critic/value/encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_2/critic/value/encoder/hidden_1/MatMul_grad/MatMul
�
Pgradients_2/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependency_1Identity>gradients_2/critic/value/encoder/hidden_1/MatMul_grad/MatMul_1G^gradients_2/critic/value/encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@gradients_2/critic/value/encoder/hidden_1/MatMul_grad/MatMul_1
�
gradients_2/AddN_8AddNVgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/tuple/control_dependencyMgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Sigmoid_grad/SigmoidGrad*
N*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Reshape
�
Mgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_8*
T0*
data_formatNHWC
�
Rgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_8N^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Zgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_8S^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/Mul_grad/Reshape
�
\gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/BiasAddGradS^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/BiasAddGrad
�
gradients_2/AddN_9AddNVgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/tuple/control_dependencyMgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Sigmoid_grad/SigmoidGrad*
N*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Reshape
�
Mgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_9*
T0*
data_formatNHWC
�
Rgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_9N^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Zgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_9S^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/Mul_grad/Reshape
�
\gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/BiasAddGradS^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/BiasAddGrad
�
8gradients_2/critic/value/encoder/hidden_0/Mul_grad/ShapeShape%critic/value/encoder/hidden_0/BiasAdd^sac_policy_opt*
T0*
out_type0
�
:gradients_2/critic/value/encoder/hidden_0/Mul_grad/Shape_1Shape%critic/value/encoder/hidden_0/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Hgradients_2/critic/value/encoder/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs8gradients_2/critic/value/encoder/hidden_0/Mul_grad/Shape:gradients_2/critic/value/encoder/hidden_0/Mul_grad/Shape_1*
T0
�
6gradients_2/critic/value/encoder/hidden_0/Mul_grad/MulMulNgradients_2/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependency%critic/value/encoder/hidden_0/Sigmoid*
T0
�
6gradients_2/critic/value/encoder/hidden_0/Mul_grad/SumSum6gradients_2/critic/value/encoder/hidden_0/Mul_grad/MulHgradients_2/critic/value/encoder/hidden_0/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
:gradients_2/critic/value/encoder/hidden_0/Mul_grad/ReshapeReshape6gradients_2/critic/value/encoder/hidden_0/Mul_grad/Sum8gradients_2/critic/value/encoder/hidden_0/Mul_grad/Shape*
T0*
Tshape0
�
8gradients_2/critic/value/encoder/hidden_0/Mul_grad/Mul_1Mul%critic/value/encoder/hidden_0/BiasAddNgradients_2/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependency*
T0
�
8gradients_2/critic/value/encoder/hidden_0/Mul_grad/Sum_1Sum8gradients_2/critic/value/encoder/hidden_0/Mul_grad/Mul_1Jgradients_2/critic/value/encoder/hidden_0/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
<gradients_2/critic/value/encoder/hidden_0/Mul_grad/Reshape_1Reshape8gradients_2/critic/value/encoder/hidden_0/Mul_grad/Sum_1:gradients_2/critic/value/encoder/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
�
Cgradients_2/critic/value/encoder/hidden_0/Mul_grad/tuple/group_depsNoOp;^gradients_2/critic/value/encoder/hidden_0/Mul_grad/Reshape=^gradients_2/critic/value/encoder/hidden_0/Mul_grad/Reshape_1^sac_policy_opt
�
Kgradients_2/critic/value/encoder/hidden_0/Mul_grad/tuple/control_dependencyIdentity:gradients_2/critic/value/encoder/hidden_0/Mul_grad/ReshapeD^gradients_2/critic/value/encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_0/Mul_grad/Reshape
�
Mgradients_2/critic/value/encoder/hidden_0/Mul_grad/tuple/control_dependency_1Identity<gradients_2/critic/value/encoder/hidden_0/Mul_grad/Reshape_1D^gradients_2/critic/value/encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_2/critic/value/encoder/hidden_0/Mul_grad/Reshape_1
�
Ggradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/MatMulMatMulZgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/tuple/control_dependency4critic/q/q1_encoding/q1_encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Igradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/MatMul_1MatMul,critic/q/q1_encoding/q1_encoder/hidden_1/MulZgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Qgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/tuple/group_depsNoOpH^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/MatMulJ^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/MatMul_1^sac_policy_opt
�
Ygradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/tuple/control_dependencyIdentityGgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/MatMulR^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/MatMul
�
[gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/tuple/control_dependency_1IdentityIgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/MatMul_1R^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/MatMul_1
�
Ggradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/MatMulMatMulZgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/tuple/control_dependency4critic/q/q2_encoding/q2_encoder/hidden_2/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Igradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/MatMul_1MatMul,critic/q/q2_encoding/q2_encoder/hidden_1/MulZgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Qgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/tuple/group_depsNoOpH^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/MatMulJ^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/MatMul_1^sac_policy_opt
�
Ygradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/tuple/control_dependencyIdentityGgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/MatMulR^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/MatMul
�
[gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/tuple/control_dependency_1IdentityIgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/MatMul_1R^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/MatMul_1
�
Bgradients_2/critic/value/encoder/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad%critic/value/encoder/hidden_0/SigmoidMgradients_2/critic/value/encoder/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ShapeShape0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd^sac_policy_opt*
T0*
out_type0
�
Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Shape_1Shape0critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Sgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ShapeEgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Shape_1*
T0
�
Agradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/MulMulYgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/tuple/control_dependency0critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid*
T0
�
Agradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/SumSumAgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/MulSgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ReshapeReshapeAgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/SumCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Shape*
T0*
Tshape0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Mul_1Mul0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAddYgradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/tuple/control_dependency*
T0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Sum_1SumCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Mul_1Ugradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Ggradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape_1ReshapeCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Sum_1Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
�
Ngradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/group_depsNoOpF^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ReshapeH^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape_1^sac_policy_opt
�
Vgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/control_dependencyIdentityEgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ReshapeO^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape
�
Xgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/control_dependency_1IdentityGgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape_1O^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape_1
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ShapeShape0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd^sac_policy_opt*
T0*
out_type0
�
Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Shape_1Shape0critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Sgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ShapeEgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Shape_1*
T0
�
Agradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/MulMulYgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/tuple/control_dependency0critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid*
T0
�
Agradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/SumSumAgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/MulSgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ReshapeReshapeAgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/SumCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Shape*
T0*
Tshape0
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Mul_1Mul0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAddYgradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/tuple/control_dependency*
T0
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Sum_1SumCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Mul_1Ugradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Ggradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape_1ReshapeCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Sum_1Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
�
Ngradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/group_depsNoOpF^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ReshapeH^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape_1^sac_policy_opt
�
Vgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/control_dependencyIdentityEgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ReshapeO^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape
�
Xgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/control_dependency_1IdentityGgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape_1O^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape_1
�
gradients_2/AddN_10AddNKgradients_2/critic/value/encoder/hidden_0/Mul_grad/tuple/control_dependencyBgradients_2/critic/value/encoder/hidden_0/Sigmoid_grad/SigmoidGrad*
N*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_0/Mul_grad/Reshape
�
Bgradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_10*
T0*
data_formatNHWC
�
Ggradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_10C^gradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Ogradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_10H^gradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_2/critic/value/encoder/hidden_0/Mul_grad/Reshape
�
Qgradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityBgradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/BiasAddGradH^gradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/BiasAddGrad
�
Mgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q1_encoding/q1_encoder/hidden_1/SigmoidXgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
�
Mgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q2_encoding/q2_encoder/hidden_1/SigmoidXgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
�
<gradients_2/critic/value/encoder/hidden_0/MatMul_grad/MatMulMatMulOgradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency)critic/value/encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b(
�
>gradients_2/critic/value/encoder/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationOgradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Fgradients_2/critic/value/encoder/hidden_0/MatMul_grad/tuple/group_depsNoOp=^gradients_2/critic/value/encoder/hidden_0/MatMul_grad/MatMul?^gradients_2/critic/value/encoder/hidden_0/MatMul_grad/MatMul_1^sac_policy_opt
�
Ngradients_2/critic/value/encoder/hidden_0/MatMul_grad/tuple/control_dependencyIdentity<gradients_2/critic/value/encoder/hidden_0/MatMul_grad/MatMulG^gradients_2/critic/value/encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_2/critic/value/encoder/hidden_0/MatMul_grad/MatMul
�
Pgradients_2/critic/value/encoder/hidden_0/MatMul_grad/tuple/control_dependency_1Identity>gradients_2/critic/value/encoder/hidden_0/MatMul_grad/MatMul_1G^gradients_2/critic/value/encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@gradients_2/critic/value/encoder/hidden_0/MatMul_grad/MatMul_1
�
gradients_2/AddN_11AddNVgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/control_dependencyMgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid_grad/SigmoidGrad*
N*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape
�
Mgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_11*
T0*
data_formatNHWC
�
Rgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_11N^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Zgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_11S^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape
�
\gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/BiasAddGradS^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/BiasAddGrad
�
gradients_2/AddN_12AddNVgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/control_dependencyMgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid_grad/SigmoidGrad*
N*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape
�
Mgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_12*
T0*
data_formatNHWC
�
Rgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_12N^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Zgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_12S^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape
�
\gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/BiasAddGradS^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/BiasAddGrad
�
Ggradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMulMatMulZgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Igradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul_1MatMul,critic/q/q1_encoding/q1_encoder/hidden_0/MulZgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Qgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/group_depsNoOpH^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMulJ^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul_1^sac_policy_opt
�
Ygradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependencyIdentityGgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMulR^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul
�
[gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1IdentityIgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul_1R^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul_1
�
Ggradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMulMatMulZgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Igradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul_1MatMul,critic/q/q2_encoding/q2_encoder/hidden_0/MulZgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Qgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/group_depsNoOpH^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMulJ^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul_1^sac_policy_opt
�
Ygradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependencyIdentityGgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMulR^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul
�
[gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1IdentityIgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul_1R^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul_1
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ShapeShape0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd^sac_policy_opt*
T0*
out_type0
�
Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Shape_1Shape0critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Sgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ShapeEgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Shape_1*
T0
�
Agradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/MulMulYgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependency0critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid*
T0
�
Agradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/SumSumAgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/MulSgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ReshapeReshapeAgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/SumCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Shape*
T0*
Tshape0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Mul_1Mul0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAddYgradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependency*
T0
�
Cgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Sum_1SumCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Mul_1Ugradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Ggradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape_1ReshapeCgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Sum_1Egradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
�
Ngradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/group_depsNoOpF^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ReshapeH^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape_1^sac_policy_opt
�
Vgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/control_dependencyIdentityEgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ReshapeO^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape
�
Xgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/control_dependency_1IdentityGgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape_1O^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape_1
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ShapeShape0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd^sac_policy_opt*
T0*
out_type0
�
Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Shape_1Shape0critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid^sac_policy_opt*
T0*
out_type0
�
Sgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ShapeEgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Shape_1*
T0
�
Agradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/MulMulYgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependency0critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid*
T0
�
Agradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/SumSumAgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/MulSgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
�
Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ReshapeReshapeAgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/SumCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Shape*
T0*
Tshape0
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Mul_1Mul0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAddYgradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependency*
T0
�
Cgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Sum_1SumCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Mul_1Ugradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
Ggradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape_1ReshapeCgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Sum_1Egradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
�
Ngradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/group_depsNoOpF^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ReshapeH^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape_1^sac_policy_opt
�
Vgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/control_dependencyIdentityEgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ReshapeO^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape
�
Xgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/control_dependency_1IdentityGgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape_1O^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape_1
�
Mgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q1_encoding/q1_encoder/hidden_0/SigmoidXgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
�
Mgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q2_encoding/q2_encoder/hidden_0/SigmoidXgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
�
gradients_2/AddN_13AddNVgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/control_dependencyMgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid_grad/SigmoidGrad*
N*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape
�
Mgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_13*
T0*
data_formatNHWC
�
Rgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_13N^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Zgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_13S^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape
�
\gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/BiasAddGradS^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/BiasAddGrad
�
gradients_2/AddN_14AddNVgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/control_dependencyMgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid_grad/SigmoidGrad*
N*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape
�
Mgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients_2/AddN_14*
T0*
data_formatNHWC
�
Rgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^gradients_2/AddN_14N^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/BiasAddGrad^sac_policy_opt
�
Zgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients_2/AddN_14S^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape
�
\gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/BiasAddGradS^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/BiasAddGrad
�
Ggradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMulMatMulZgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Igradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationZgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Qgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/group_depsNoOpH^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMulJ^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul_1^sac_policy_opt
�
Ygradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/control_dependencyIdentityGgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMulR^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul
�
[gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1IdentityIgradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul_1R^gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul_1
�
Ggradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMulMatMulZgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/read*
T0*
transpose_a( *
transpose_b(
�
Igradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationZgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
transpose_b( 
�
Qgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/group_depsNoOpH^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMulJ^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul_1^sac_policy_opt
�
Ygradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/control_dependencyIdentityGgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMulR^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul
�
[gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1IdentityIgradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul_1R^gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul_1
�
beta1_power_2/initial_valueConst*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
dtype0*
valueB
 *fff?
�
beta1_power_2
VariableV2*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
	container *
dtype0*
shape: *
shared_name 
�
beta1_power_2/AssignAssignbeta1_power_2beta1_power_2/initial_value*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
q
beta1_power_2/readIdentitybeta1_power_2*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias
�
beta2_power_2/initial_valueConst*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
dtype0*
valueB
 *w�?
�
beta2_power_2
VariableV2*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
	container *
dtype0*
shape: *
shared_name 
�
beta2_power_2/AssignAssignbeta2_power_2beta2_power_2/initial_value*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
q
beta2_power_2/readIdentitybeta2_power_2*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias
�
Tcritic/value/encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Jcritic/value/encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/ConstConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Dcritic/value/encoder/hidden_0/kernel/sac_value_opt/Initializer/zerosFillTcritic/value/encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorJcritic/value/encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/Const*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*

index_type0
�
2critic/value/encoder/hidden_0/kernel/sac_value_opt
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
9critic/value/encoder/hidden_0/kernel/sac_value_opt/AssignAssign2critic/value/encoder/hidden_0/kernel/sac_value_optDcritic/value/encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
7critic/value/encoder/hidden_0/kernel/sac_value_opt/readIdentity2critic/value/encoder/hidden_0/kernel/sac_value_opt*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
�
Vcritic/value/encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Lcritic/value/encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Fcritic/value/encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zerosFillVcritic/value/encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorLcritic/value/encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*

index_type0
�
4critic/value/encoder/hidden_0/kernel/sac_value_opt_1
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
;critic/value/encoder/hidden_0/kernel/sac_value_opt_1/AssignAssign4critic/value/encoder/hidden_0/kernel/sac_value_opt_1Fcritic/value/encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
9critic/value/encoder/hidden_0/kernel/sac_value_opt_1/readIdentity4critic/value/encoder/hidden_0/kernel/sac_value_opt_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
�
Bcritic/value/encoder/hidden_0/bias/sac_value_opt/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
dtype0*
valueB@*    
�
0critic/value/encoder/hidden_0/bias/sac_value_opt
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
7critic/value/encoder/hidden_0/bias/sac_value_opt/AssignAssign0critic/value/encoder/hidden_0/bias/sac_value_optBcritic/value/encoder/hidden_0/bias/sac_value_opt/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
5critic/value/encoder/hidden_0/bias/sac_value_opt/readIdentity0critic/value/encoder/hidden_0/bias/sac_value_opt*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias
�
Dcritic/value/encoder/hidden_0/bias/sac_value_opt_1/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
dtype0*
valueB@*    
�
2critic/value/encoder/hidden_0/bias/sac_value_opt_1
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
9critic/value/encoder/hidden_0/bias/sac_value_opt_1/AssignAssign2critic/value/encoder/hidden_0/bias/sac_value_opt_1Dcritic/value/encoder/hidden_0/bias/sac_value_opt_1/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
7critic/value/encoder/hidden_0/bias/sac_value_opt_1/readIdentity2critic/value/encoder/hidden_0/bias/sac_value_opt_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias
�
Tcritic/value/encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Jcritic/value/encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/ConstConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Dcritic/value/encoder/hidden_1/kernel/sac_value_opt/Initializer/zerosFillTcritic/value/encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorJcritic/value/encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/Const*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*

index_type0
�
2critic/value/encoder/hidden_1/kernel/sac_value_opt
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
9critic/value/encoder/hidden_1/kernel/sac_value_opt/AssignAssign2critic/value/encoder/hidden_1/kernel/sac_value_optDcritic/value/encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
7critic/value/encoder/hidden_1/kernel/sac_value_opt/readIdentity2critic/value/encoder/hidden_1/kernel/sac_value_opt*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
�
Vcritic/value/encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Lcritic/value/encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Fcritic/value/encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zerosFillVcritic/value/encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorLcritic/value/encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*

index_type0
�
4critic/value/encoder/hidden_1/kernel/sac_value_opt_1
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
;critic/value/encoder/hidden_1/kernel/sac_value_opt_1/AssignAssign4critic/value/encoder/hidden_1/kernel/sac_value_opt_1Fcritic/value/encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
9critic/value/encoder/hidden_1/kernel/sac_value_opt_1/readIdentity4critic/value/encoder/hidden_1/kernel/sac_value_opt_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
�
Bcritic/value/encoder/hidden_1/bias/sac_value_opt/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
dtype0*
valueB@*    
�
0critic/value/encoder/hidden_1/bias/sac_value_opt
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
7critic/value/encoder/hidden_1/bias/sac_value_opt/AssignAssign0critic/value/encoder/hidden_1/bias/sac_value_optBcritic/value/encoder/hidden_1/bias/sac_value_opt/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
5critic/value/encoder/hidden_1/bias/sac_value_opt/readIdentity0critic/value/encoder/hidden_1/bias/sac_value_opt*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias
�
Dcritic/value/encoder/hidden_1/bias/sac_value_opt_1/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
dtype0*
valueB@*    
�
2critic/value/encoder/hidden_1/bias/sac_value_opt_1
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
9critic/value/encoder/hidden_1/bias/sac_value_opt_1/AssignAssign2critic/value/encoder/hidden_1/bias/sac_value_opt_1Dcritic/value/encoder/hidden_1/bias/sac_value_opt_1/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
7critic/value/encoder/hidden_1/bias/sac_value_opt_1/readIdentity2critic/value/encoder/hidden_1/bias/sac_value_opt_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias
�
Tcritic/value/encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Jcritic/value/encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/ConstConst*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Dcritic/value/encoder/hidden_2/kernel/sac_value_opt/Initializer/zerosFillTcritic/value/encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorJcritic/value/encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/Const*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*

index_type0
�
2critic/value/encoder/hidden_2/kernel/sac_value_opt
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
9critic/value/encoder/hidden_2/kernel/sac_value_opt/AssignAssign2critic/value/encoder/hidden_2/kernel/sac_value_optDcritic/value/encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
7critic/value/encoder/hidden_2/kernel/sac_value_opt/readIdentity2critic/value/encoder/hidden_2/kernel/sac_value_opt*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel
�
Vcritic/value/encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Lcritic/value/encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Fcritic/value/encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zerosFillVcritic/value/encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorLcritic/value/encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*

index_type0
�
4critic/value/encoder/hidden_2/kernel/sac_value_opt_1
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
;critic/value/encoder/hidden_2/kernel/sac_value_opt_1/AssignAssign4critic/value/encoder/hidden_2/kernel/sac_value_opt_1Fcritic/value/encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
9critic/value/encoder/hidden_2/kernel/sac_value_opt_1/readIdentity4critic/value/encoder/hidden_2/kernel/sac_value_opt_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel
�
Bcritic/value/encoder/hidden_2/bias/sac_value_opt/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
dtype0*
valueB@*    
�
0critic/value/encoder/hidden_2/bias/sac_value_opt
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
7critic/value/encoder/hidden_2/bias/sac_value_opt/AssignAssign0critic/value/encoder/hidden_2/bias/sac_value_optBcritic/value/encoder/hidden_2/bias/sac_value_opt/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
5critic/value/encoder/hidden_2/bias/sac_value_opt/readIdentity0critic/value/encoder/hidden_2/bias/sac_value_opt*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias
�
Dcritic/value/encoder/hidden_2/bias/sac_value_opt_1/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
dtype0*
valueB@*    
�
2critic/value/encoder/hidden_2/bias/sac_value_opt_1
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
9critic/value/encoder/hidden_2/bias/sac_value_opt_1/AssignAssign2critic/value/encoder/hidden_2/bias/sac_value_opt_1Dcritic/value/encoder/hidden_2/bias/sac_value_opt_1/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
7critic/value/encoder/hidden_2/bias/sac_value_opt_1/readIdentity2critic/value/encoder/hidden_2/bias/sac_value_opt_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias
�
Tcritic/value/encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Jcritic/value/encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/ConstConst*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Dcritic/value/encoder/hidden_3/kernel/sac_value_opt/Initializer/zerosFillTcritic/value/encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorJcritic/value/encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/Const*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*

index_type0
�
2critic/value/encoder/hidden_3/kernel/sac_value_opt
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
9critic/value/encoder/hidden_3/kernel/sac_value_opt/AssignAssign2critic/value/encoder/hidden_3/kernel/sac_value_optDcritic/value/encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
7critic/value/encoder/hidden_3/kernel/sac_value_opt/readIdentity2critic/value/encoder/hidden_3/kernel/sac_value_opt*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel
�
Vcritic/value/encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Lcritic/value/encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Fcritic/value/encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zerosFillVcritic/value/encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorLcritic/value/encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*

index_type0
�
4critic/value/encoder/hidden_3/kernel/sac_value_opt_1
VariableV2*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
;critic/value/encoder/hidden_3/kernel/sac_value_opt_1/AssignAssign4critic/value/encoder/hidden_3/kernel/sac_value_opt_1Fcritic/value/encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
9critic/value/encoder/hidden_3/kernel/sac_value_opt_1/readIdentity4critic/value/encoder/hidden_3/kernel/sac_value_opt_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel
�
Bcritic/value/encoder/hidden_3/bias/sac_value_opt/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
dtype0*
valueB@*    
�
0critic/value/encoder/hidden_3/bias/sac_value_opt
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
7critic/value/encoder/hidden_3/bias/sac_value_opt/AssignAssign0critic/value/encoder/hidden_3/bias/sac_value_optBcritic/value/encoder/hidden_3/bias/sac_value_opt/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
5critic/value/encoder/hidden_3/bias/sac_value_opt/readIdentity0critic/value/encoder/hidden_3/bias/sac_value_opt*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias
�
Dcritic/value/encoder/hidden_3/bias/sac_value_opt_1/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
dtype0*
valueB@*    
�
2critic/value/encoder/hidden_3/bias/sac_value_opt_1
VariableV2*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
9critic/value/encoder/hidden_3/bias/sac_value_opt_1/AssignAssign2critic/value/encoder/hidden_3/bias/sac_value_opt_1Dcritic/value/encoder/hidden_3/bias/sac_value_opt_1/Initializer/zeros*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
7critic/value/encoder/hidden_3/bias/sac_value_opt_1/readIdentity2critic/value/encoder/hidden_3/bias/sac_value_opt_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias
�
Ccritic/value/extrinsic_value/kernel/sac_value_opt/Initializer/zerosConst*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
valueB@*    
�
1critic/value/extrinsic_value/kernel/sac_value_opt
VariableV2*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
	container *
dtype0*
shape
:@*
shared_name 
�
8critic/value/extrinsic_value/kernel/sac_value_opt/AssignAssign1critic/value/extrinsic_value/kernel/sac_value_optCcritic/value/extrinsic_value/kernel/sac_value_opt/Initializer/zeros*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
6critic/value/extrinsic_value/kernel/sac_value_opt/readIdentity1critic/value/extrinsic_value/kernel/sac_value_opt*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel
�
Ecritic/value/extrinsic_value/kernel/sac_value_opt_1/Initializer/zerosConst*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
valueB@*    
�
3critic/value/extrinsic_value/kernel/sac_value_opt_1
VariableV2*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
	container *
dtype0*
shape
:@*
shared_name 
�
:critic/value/extrinsic_value/kernel/sac_value_opt_1/AssignAssign3critic/value/extrinsic_value/kernel/sac_value_opt_1Ecritic/value/extrinsic_value/kernel/sac_value_opt_1/Initializer/zeros*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
8critic/value/extrinsic_value/kernel/sac_value_opt_1/readIdentity3critic/value/extrinsic_value/kernel/sac_value_opt_1*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel
�
Acritic/value/extrinsic_value/bias/sac_value_opt/Initializer/zerosConst*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
dtype0*
valueB*    
�
/critic/value/extrinsic_value/bias/sac_value_opt
VariableV2*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
	container *
dtype0*
shape:*
shared_name 
�
6critic/value/extrinsic_value/bias/sac_value_opt/AssignAssign/critic/value/extrinsic_value/bias/sac_value_optAcritic/value/extrinsic_value/bias/sac_value_opt/Initializer/zeros*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
4critic/value/extrinsic_value/bias/sac_value_opt/readIdentity/critic/value/extrinsic_value/bias/sac_value_opt*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias
�
Ccritic/value/extrinsic_value/bias/sac_value_opt_1/Initializer/zerosConst*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
dtype0*
valueB*    
�
1critic/value/extrinsic_value/bias/sac_value_opt_1
VariableV2*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
	container *
dtype0*
shape:*
shared_name 
�
8critic/value/extrinsic_value/bias/sac_value_opt_1/AssignAssign1critic/value/extrinsic_value/bias/sac_value_opt_1Ccritic/value/extrinsic_value/bias/sac_value_opt_1/Initializer/zeros*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
6critic/value/extrinsic_value/bias/sac_value_opt_1/readIdentity1critic/value/extrinsic_value/bias/sac_value_opt_1*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias
�
Ccritic/value/curiosity_value/kernel/sac_value_opt/Initializer/zerosConst*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
dtype0*
valueB@*    
�
1critic/value/curiosity_value/kernel/sac_value_opt
VariableV2*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
	container *
dtype0*
shape
:@*
shared_name 
�
8critic/value/curiosity_value/kernel/sac_value_opt/AssignAssign1critic/value/curiosity_value/kernel/sac_value_optCcritic/value/curiosity_value/kernel/sac_value_opt/Initializer/zeros*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
6critic/value/curiosity_value/kernel/sac_value_opt/readIdentity1critic/value/curiosity_value/kernel/sac_value_opt*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel
�
Ecritic/value/curiosity_value/kernel/sac_value_opt_1/Initializer/zerosConst*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
dtype0*
valueB@*    
�
3critic/value/curiosity_value/kernel/sac_value_opt_1
VariableV2*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
	container *
dtype0*
shape
:@*
shared_name 
�
:critic/value/curiosity_value/kernel/sac_value_opt_1/AssignAssign3critic/value/curiosity_value/kernel/sac_value_opt_1Ecritic/value/curiosity_value/kernel/sac_value_opt_1/Initializer/zeros*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
8critic/value/curiosity_value/kernel/sac_value_opt_1/readIdentity3critic/value/curiosity_value/kernel/sac_value_opt_1*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel
�
Acritic/value/curiosity_value/bias/sac_value_opt/Initializer/zerosConst*4
_class*
(&loc:@critic/value/curiosity_value/bias*
dtype0*
valueB*    
�
/critic/value/curiosity_value/bias/sac_value_opt
VariableV2*4
_class*
(&loc:@critic/value/curiosity_value/bias*
	container *
dtype0*
shape:*
shared_name 
�
6critic/value/curiosity_value/bias/sac_value_opt/AssignAssign/critic/value/curiosity_value/bias/sac_value_optAcritic/value/curiosity_value/bias/sac_value_opt/Initializer/zeros*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
4critic/value/curiosity_value/bias/sac_value_opt/readIdentity/critic/value/curiosity_value/bias/sac_value_opt*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias
�
Ccritic/value/curiosity_value/bias/sac_value_opt_1/Initializer/zerosConst*4
_class*
(&loc:@critic/value/curiosity_value/bias*
dtype0*
valueB*    
�
1critic/value/curiosity_value/bias/sac_value_opt_1
VariableV2*4
_class*
(&loc:@critic/value/curiosity_value/bias*
	container *
dtype0*
shape:*
shared_name 
�
8critic/value/curiosity_value/bias/sac_value_opt_1/AssignAssign1critic/value/curiosity_value/bias/sac_value_opt_1Ccritic/value/curiosity_value/bias/sac_value_opt_1/Initializer/zeros*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
6critic/value/curiosity_value/bias/sac_value_opt_1/readIdentity1critic/value/curiosity_value/bias/sac_value_opt_1*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias
�
_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Ucritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Ocritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt/Initializer/zerosFill_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorUcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*

index_type0
�
=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Dcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt/AssignAssign=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_optOcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
Bcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt/readIdentity=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
�
acritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Wcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Qcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zerosFillacritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorWcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*

index_type0
�
?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Fcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1/AssignAssign?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1Qcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
Dcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1/readIdentity?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
�
Mcritic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
dtype0*
valueB@*    
�
;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Bcritic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt/AssignAssign;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_optMcritic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
@critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt/readIdentity;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias
�
Ocritic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
dtype0*
valueB@*    
�
=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Dcritic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1/AssignAssign=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1Ocritic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
Bcritic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1/readIdentity=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias
�
_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Ucritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Ocritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt/Initializer/zerosFill_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorUcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*

index_type0
�
=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Dcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt/AssignAssign=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_optOcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
Bcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt/readIdentity=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
�
acritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Wcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Qcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zerosFillacritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorWcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*

index_type0
�
?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Fcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1/AssignAssign?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1Qcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
Dcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1/readIdentity?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
�
Mcritic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
dtype0*
valueB@*    
�
;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Bcritic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt/AssignAssign;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_optMcritic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
@critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt/readIdentity;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias
�
Ocritic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
dtype0*
valueB@*    
�
=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Dcritic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1/AssignAssign=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1Ocritic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
Bcritic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1/readIdentity=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias
�
_critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Ucritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Ocritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt/Initializer/zerosFill_critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorUcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*

index_type0
�
=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Dcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt/AssignAssign=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_optOcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
Bcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt/readIdentity=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel
�
acritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Wcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Qcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zerosFillacritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorWcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*

index_type0
�
?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Fcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1/AssignAssign?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1Qcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
Dcritic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1/readIdentity?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel
�
Mcritic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
dtype0*
valueB@*    
�
;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Bcritic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt/AssignAssign;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_optMcritic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
@critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt/readIdentity;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias
�
Ocritic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
dtype0*
valueB@*    
�
=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Dcritic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1/AssignAssign=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1Ocritic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
Bcritic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1/readIdentity=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias
�
_critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Ucritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Ocritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt/Initializer/zerosFill_critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorUcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*

index_type0
�
=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Dcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt/AssignAssign=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_optOcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
Bcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt/readIdentity=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel
�
acritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Wcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Qcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zerosFillacritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorWcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*

index_type0
�
?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1
VariableV2*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Fcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1/AssignAssign?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1Qcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
Dcritic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1/readIdentity?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel
�
Mcritic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
dtype0*
valueB@*    
�
;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Bcritic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt/AssignAssign;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_optMcritic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
@critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt/readIdentity;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias
�
Ocritic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
dtype0*
valueB@*    
�
=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1
VariableV2*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Dcritic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1/AssignAssign=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1Ocritic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
Bcritic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1/readIdentity=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias
�
Xcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
valueB"@   @   
�
Ncritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt/Initializer/zeros/ConstConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
valueB
 *    
�
Hcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt/Initializer/zerosFillXcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorNcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt/Initializer/zeros/Const*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*

index_type0
�
6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt
VariableV2*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
=critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt/AssignAssign6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_optHcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt/Initializer/zeros*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
�
;critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt/readIdentity6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
�
Zcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
valueB"@   @   
�
Pcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
valueB
 *    
�
Jcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1/Initializer/zerosFillZcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorPcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*

index_type0
�
8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1
VariableV2*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
?critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1/AssignAssign8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1Jcritic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1/Initializer/zeros*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
�
=critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1/readIdentity8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
�
Fcritic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0*
valueB@*    
�
4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt
VariableV2*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
;critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt/AssignAssign4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_optFcritic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
�
9critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt/readIdentity4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias
�
Hcritic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0*
valueB@*    
�
6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1
VariableV2*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
=critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1/AssignAssign6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1Hcritic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
�
;critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1/readIdentity6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias
�
Xcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
dtype0*
valueB"@   @   
�
Ncritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt/Initializer/zeros/ConstConst*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
dtype0*
valueB
 *    
�
Hcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt/Initializer/zerosFillXcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorNcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt/Initializer/zeros/Const*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*

index_type0
�
6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt
VariableV2*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
=critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt/AssignAssign6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_optHcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt/Initializer/zeros*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
�
;critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt/readIdentity6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel
�
Zcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
dtype0*
valueB"@   @   
�
Pcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
dtype0*
valueB
 *    
�
Jcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1/Initializer/zerosFillZcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorPcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*

index_type0
�
8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1
VariableV2*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
?critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1/AssignAssign8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1Jcritic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1/Initializer/zeros*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
�
=critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1/readIdentity8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel
�
Fcritic/q/q1_encoding/curiosity_q1/bias/sac_value_opt/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
dtype0*
valueB@*    
�
4critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt
VariableV2*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
;critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt/AssignAssign4critic/q/q1_encoding/curiosity_q1/bias/sac_value_optFcritic/q/q1_encoding/curiosity_q1/bias/sac_value_opt/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
�
9critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt/readIdentity4critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias
�
Hcritic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
dtype0*
valueB@*    
�
6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1
VariableV2*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
=critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1/AssignAssign6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1Hcritic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
�
;critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1/readIdentity6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias
�
_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Ucritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Ocritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt/Initializer/zerosFill_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorUcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*

index_type0
�
=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Dcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt/AssignAssign=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_optOcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
Bcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt/readIdentity=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
�
acritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
valueB"@   @   
�
Wcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
valueB
 *    
�
Qcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zerosFillacritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorWcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*

index_type0
�
?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Fcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1/AssignAssign?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1Qcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
Dcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1/readIdentity?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
�
Mcritic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
dtype0*
valueB@*    
�
;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Bcritic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt/AssignAssign;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_optMcritic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
@critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt/readIdentity;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias
�
Ocritic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
dtype0*
valueB@*    
�
=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Dcritic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1/AssignAssign=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1Ocritic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
Bcritic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1/readIdentity=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias
�
_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Ucritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Ocritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt/Initializer/zerosFill_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorUcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*

index_type0
�
=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Dcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt/AssignAssign=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_optOcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
Bcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt/readIdentity=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
�
acritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
valueB"@   @   
�
Wcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
valueB
 *    
�
Qcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zerosFillacritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorWcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*

index_type0
�
?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Fcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1/AssignAssign?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1Qcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
Dcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1/readIdentity?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
�
Mcritic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
dtype0*
valueB@*    
�
;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Bcritic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt/AssignAssign;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_optMcritic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
@critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt/readIdentity;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias
�
Ocritic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
dtype0*
valueB@*    
�
=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Dcritic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1/AssignAssign=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1Ocritic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
Bcritic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1/readIdentity=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias
�
_critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Ucritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Ocritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt/Initializer/zerosFill_critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorUcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*

index_type0
�
=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Dcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt/AssignAssign=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_optOcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
Bcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt/readIdentity=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel
�
acritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
dtype0*
valueB"@   @   
�
Wcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
dtype0*
valueB
 *    
�
Qcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zerosFillacritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorWcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*

index_type0
�
?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Fcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1/AssignAssign?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1Qcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
Dcritic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1/readIdentity?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel
�
Mcritic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
dtype0*
valueB@*    
�
;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Bcritic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt/AssignAssign;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_optMcritic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
@critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt/readIdentity;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias
�
Ocritic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
dtype0*
valueB@*    
�
=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Dcritic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1/AssignAssign=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1Ocritic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
Bcritic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1/readIdentity=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias
�
_critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Ucritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Ocritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt/Initializer/zerosFill_critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorUcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*

index_type0
�
=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Dcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt/AssignAssign=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_optOcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
Bcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt/readIdentity=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel
�
acritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
dtype0*
valueB"@   @   
�
Wcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
dtype0*
valueB
 *    
�
Qcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zerosFillacritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorWcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*

index_type0
�
?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1
VariableV2*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
Fcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1/AssignAssign?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1Qcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1/Initializer/zeros*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
Dcritic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1/readIdentity?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel
�
Mcritic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
dtype0*
valueB@*    
�
;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Bcritic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt/AssignAssign;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_optMcritic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
@critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt/readIdentity;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias
�
Ocritic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
dtype0*
valueB@*    
�
=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1
VariableV2*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
	container *
dtype0*
shape:@*
shared_name 
�
Dcritic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1/AssignAssign=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1Ocritic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1/Initializer/zeros*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
Bcritic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1/readIdentity=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias
�
Xcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
valueB"@   @   
�
Ncritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt/Initializer/zeros/ConstConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
valueB
 *    
�
Hcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt/Initializer/zerosFillXcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorNcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt/Initializer/zeros/Const*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*

index_type0
�
6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt
VariableV2*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
=critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt/AssignAssign6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_optHcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt/Initializer/zeros*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
�
;critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt/readIdentity6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
�
Zcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
valueB"@   @   
�
Pcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
valueB
 *    
�
Jcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1/Initializer/zerosFillZcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorPcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*

index_type0
�
8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1
VariableV2*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
?critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1/AssignAssign8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1Jcritic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1/Initializer/zeros*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
�
=critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1/readIdentity8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
�
Fcritic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
dtype0*
valueB@*    
�
4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt
VariableV2*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
;critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt/AssignAssign4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_optFcritic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
�
9critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt/readIdentity4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias
�
Hcritic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
dtype0*
valueB@*    
�
6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1
VariableV2*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
=critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1/AssignAssign6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1Hcritic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
�
;critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1/readIdentity6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias
�
Xcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorConst*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
dtype0*
valueB"@   @   
�
Ncritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt/Initializer/zeros/ConstConst*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
dtype0*
valueB
 *    
�
Hcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt/Initializer/zerosFillXcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt/Initializer/zeros/shape_as_tensorNcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt/Initializer/zeros/Const*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*

index_type0
�
6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt
VariableV2*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
=critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt/AssignAssign6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_optHcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt/Initializer/zeros*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
�
;critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt/readIdentity6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel
�
Zcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorConst*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
dtype0*
valueB"@   @   
�
Pcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1/Initializer/zeros/ConstConst*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
dtype0*
valueB
 *    
�
Jcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1/Initializer/zerosFillZcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1/Initializer/zeros/shape_as_tensorPcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1/Initializer/zeros/Const*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*

index_type0
�
8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1
VariableV2*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
	container *
dtype0*
shape
:@@*
shared_name 
�
?critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1/AssignAssign8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1Jcritic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1/Initializer/zeros*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
�
=critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1/readIdentity8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel
�
Fcritic/q/q2_encoding/curiosity_q2/bias/sac_value_opt/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
dtype0*
valueB@*    
�
4critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt
VariableV2*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
;critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt/AssignAssign4critic/q/q2_encoding/curiosity_q2/bias/sac_value_optFcritic/q/q2_encoding/curiosity_q2/bias/sac_value_opt/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
�
9critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt/readIdentity4critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias
�
Hcritic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
dtype0*
valueB@*    
�
6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1
VariableV2*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
	container *
dtype0*
shape:@*
shared_name 
�
=critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1/AssignAssign6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1Hcritic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1/Initializer/zeros*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
�
;critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1/readIdentity6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias
Q
sac_value_opt/beta1Const^sac_policy_opt*
dtype0*
valueB
 *fff?
Q
sac_value_opt/beta2Const^sac_policy_opt*
dtype0*
valueB
 *w�?
S
sac_value_opt/epsilonConst^sac_policy_opt*
dtype0*
valueB
 *w�+2
�
Csac_value_opt/update_critic/value/encoder/hidden_0/kernel/ApplyAdam	ApplyAdam$critic/value/encoder/hidden_0/kernel2critic/value/encoder/hidden_0/kernel/sac_value_opt4critic/value/encoder/hidden_0/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonPgradients_2/critic/value/encoder/hidden_0/MatMul_grad/tuple/control_dependency_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking( *
use_nesterov( 
�
Asac_value_opt/update_critic/value/encoder/hidden_0/bias/ApplyAdam	ApplyAdam"critic/value/encoder/hidden_0/bias0critic/value/encoder/hidden_0/bias/sac_value_opt2critic/value/encoder/hidden_0/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonQgradients_2/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking( *
use_nesterov( 
�
Csac_value_opt/update_critic/value/encoder/hidden_1/kernel/ApplyAdam	ApplyAdam$critic/value/encoder/hidden_1/kernel2critic/value/encoder/hidden_1/kernel/sac_value_opt4critic/value/encoder/hidden_1/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonPgradients_2/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependency_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking( *
use_nesterov( 
�
Asac_value_opt/update_critic/value/encoder/hidden_1/bias/ApplyAdam	ApplyAdam"critic/value/encoder/hidden_1/bias0critic/value/encoder/hidden_1/bias/sac_value_opt2critic/value/encoder/hidden_1/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonQgradients_2/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking( *
use_nesterov( 
�
Csac_value_opt/update_critic/value/encoder/hidden_2/kernel/ApplyAdam	ApplyAdam$critic/value/encoder/hidden_2/kernel2critic/value/encoder/hidden_2/kernel/sac_value_opt4critic/value/encoder/hidden_2/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonPgradients_2/critic/value/encoder/hidden_2/MatMul_grad/tuple/control_dependency_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking( *
use_nesterov( 
�
Asac_value_opt/update_critic/value/encoder/hidden_2/bias/ApplyAdam	ApplyAdam"critic/value/encoder/hidden_2/bias0critic/value/encoder/hidden_2/bias/sac_value_opt2critic/value/encoder/hidden_2/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonQgradients_2/critic/value/encoder/hidden_2/BiasAdd_grad/tuple/control_dependency_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking( *
use_nesterov( 
�
Csac_value_opt/update_critic/value/encoder/hidden_3/kernel/ApplyAdam	ApplyAdam$critic/value/encoder/hidden_3/kernel2critic/value/encoder/hidden_3/kernel/sac_value_opt4critic/value/encoder/hidden_3/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonPgradients_2/critic/value/encoder/hidden_3/MatMul_grad/tuple/control_dependency_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking( *
use_nesterov( 
�
Asac_value_opt/update_critic/value/encoder/hidden_3/bias/ApplyAdam	ApplyAdam"critic/value/encoder/hidden_3/bias0critic/value/encoder/hidden_3/bias/sac_value_opt2critic/value/encoder/hidden_3/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonQgradients_2/critic/value/encoder/hidden_3/BiasAdd_grad/tuple/control_dependency_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking( *
use_nesterov( 
�
Bsac_value_opt/update_critic/value/extrinsic_value/kernel/ApplyAdam	ApplyAdam#critic/value/extrinsic_value/kernel1critic/value/extrinsic_value/kernel/sac_value_opt3critic/value/extrinsic_value/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonOgradients_2/critic/value/extrinsic_value/MatMul_grad/tuple/control_dependency_1*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking( *
use_nesterov( 
�
@sac_value_opt/update_critic/value/extrinsic_value/bias/ApplyAdam	ApplyAdam!critic/value/extrinsic_value/bias/critic/value/extrinsic_value/bias/sac_value_opt1critic/value/extrinsic_value/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonPgradients_2/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependency_1*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking( *
use_nesterov( 
�
Bsac_value_opt/update_critic/value/curiosity_value/kernel/ApplyAdam	ApplyAdam#critic/value/curiosity_value/kernel1critic/value/curiosity_value/kernel/sac_value_opt3critic/value/curiosity_value/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonOgradients_2/critic/value/curiosity_value/MatMul_grad/tuple/control_dependency_1*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking( *
use_nesterov( 
�
@sac_value_opt/update_critic/value/curiosity_value/bias/ApplyAdam	ApplyAdam!critic/value/curiosity_value/bias/critic/value/curiosity_value/bias/sac_value_opt1critic/value/curiosity_value/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonPgradients_2/critic/value/curiosity_value/BiasAdd_grad/tuple/control_dependency_1*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking( *
use_nesterov( 
�
Nsac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/ApplyAdam	ApplyAdam/critic/q/q1_encoding/q1_encoder/hidden_0/kernel=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon[gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking( *
use_nesterov( 
�
Lsac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_0/bias/ApplyAdam	ApplyAdam-critic/q/q1_encoding/q1_encoder/hidden_0/bias;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon\gradients_2/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking( *
use_nesterov( 
�
Nsac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/ApplyAdam	ApplyAdam/critic/q/q1_encoding/q1_encoder/hidden_1/kernel=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon[gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking( *
use_nesterov( 
�
Lsac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_1/bias/ApplyAdam	ApplyAdam-critic/q/q1_encoding/q1_encoder/hidden_1/bias;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon\gradients_2/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking( *
use_nesterov( 
�
Nsac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_2/kernel/ApplyAdam	ApplyAdam/critic/q/q1_encoding/q1_encoder/hidden_2/kernel=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon[gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/MatMul_grad/tuple/control_dependency_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking( *
use_nesterov( 
�
Lsac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_2/bias/ApplyAdam	ApplyAdam-critic/q/q1_encoding/q1_encoder/hidden_2/bias;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon\gradients_2/critic/q/q1_encoding/q1_encoder/hidden_2/BiasAdd_grad/tuple/control_dependency_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking( *
use_nesterov( 
�
Nsac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_3/kernel/ApplyAdam	ApplyAdam/critic/q/q1_encoding/q1_encoder/hidden_3/kernel=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon[gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/MatMul_grad/tuple/control_dependency_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking( *
use_nesterov( 
�
Lsac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_3/bias/ApplyAdam	ApplyAdam-critic/q/q1_encoding/q1_encoder/hidden_3/bias;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon\gradients_2/critic/q/q1_encoding/q1_encoder/hidden_3/BiasAdd_grad/tuple/control_dependency_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking( *
use_nesterov( 
�
Gsac_value_opt/update_critic/q/q1_encoding/extrinsic_q1/kernel/ApplyAdam	ApplyAdam(critic/q/q1_encoding/extrinsic_q1/kernel6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonTgradients_2/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/control_dependency_1*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking( *
use_nesterov( 
�
Esac_value_opt/update_critic/q/q1_encoding/extrinsic_q1/bias/ApplyAdam	ApplyAdam&critic/q/q1_encoding/extrinsic_q1/bias4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonUgradients_2/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependency_1*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking( *
use_nesterov( 
�
Gsac_value_opt/update_critic/q/q1_encoding/curiosity_q1/kernel/ApplyAdam	ApplyAdam(critic/q/q1_encoding/curiosity_q1/kernel6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonTgradients_2/critic/q/q1_encoding/curiosity_q1/MatMul_grad/tuple/control_dependency_1*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking( *
use_nesterov( 
�
Esac_value_opt/update_critic/q/q1_encoding/curiosity_q1/bias/ApplyAdam	ApplyAdam&critic/q/q1_encoding/curiosity_q1/bias4critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonUgradients_2/critic/q/q1_encoding/curiosity_q1/BiasAdd_grad/tuple/control_dependency_1*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking( *
use_nesterov( 
�
Nsac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/ApplyAdam	ApplyAdam/critic/q/q2_encoding/q2_encoder/hidden_0/kernel=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon[gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking( *
use_nesterov( 
�
Lsac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_0/bias/ApplyAdam	ApplyAdam-critic/q/q2_encoding/q2_encoder/hidden_0/bias;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon\gradients_2/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking( *
use_nesterov( 
�
Nsac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/ApplyAdam	ApplyAdam/critic/q/q2_encoding/q2_encoder/hidden_1/kernel=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon[gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking( *
use_nesterov( 
�
Lsac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_1/bias/ApplyAdam	ApplyAdam-critic/q/q2_encoding/q2_encoder/hidden_1/bias;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon\gradients_2/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking( *
use_nesterov( 
�
Nsac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_2/kernel/ApplyAdam	ApplyAdam/critic/q/q2_encoding/q2_encoder/hidden_2/kernel=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon[gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/MatMul_grad/tuple/control_dependency_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking( *
use_nesterov( 
�
Lsac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_2/bias/ApplyAdam	ApplyAdam-critic/q/q2_encoding/q2_encoder/hidden_2/bias;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon\gradients_2/critic/q/q2_encoding/q2_encoder/hidden_2/BiasAdd_grad/tuple/control_dependency_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking( *
use_nesterov( 
�
Nsac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_3/kernel/ApplyAdam	ApplyAdam/critic/q/q2_encoding/q2_encoder/hidden_3/kernel=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon[gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/MatMul_grad/tuple/control_dependency_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking( *
use_nesterov( 
�
Lsac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_3/bias/ApplyAdam	ApplyAdam-critic/q/q2_encoding/q2_encoder/hidden_3/bias;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilon\gradients_2/critic/q/q2_encoding/q2_encoder/hidden_3/BiasAdd_grad/tuple/control_dependency_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking( *
use_nesterov( 
�
Gsac_value_opt/update_critic/q/q2_encoding/extrinsic_q2/kernel/ApplyAdam	ApplyAdam(critic/q/q2_encoding/extrinsic_q2/kernel6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonTgradients_2/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/control_dependency_1*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking( *
use_nesterov( 
�
Esac_value_opt/update_critic/q/q2_encoding/extrinsic_q2/bias/ApplyAdam	ApplyAdam&critic/q/q2_encoding/extrinsic_q2/bias4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonUgradients_2/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependency_1*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking( *
use_nesterov( 
�
Gsac_value_opt/update_critic/q/q2_encoding/curiosity_q2/kernel/ApplyAdam	ApplyAdam(critic/q/q2_encoding/curiosity_q2/kernel6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonTgradients_2/critic/q/q2_encoding/curiosity_q2/MatMul_grad/tuple/control_dependency_1*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking( *
use_nesterov( 
�
Esac_value_opt/update_critic/q/q2_encoding/curiosity_q2/bias/ApplyAdam	ApplyAdam&critic/q/q2_encoding/curiosity_q2/bias4critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1beta1_power_2/readbeta2_power_2/readVariable_2/readsac_value_opt/beta1sac_value_opt/beta2sac_value_opt/epsilonUgradients_2/critic/q/q2_encoding/curiosity_q2/BiasAdd_grad/tuple/control_dependency_1*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking( *
use_nesterov( 
�
sac_value_opt/mulMulbeta1_power_2/readsac_value_opt/beta1F^sac_value_opt/update_critic/q/q1_encoding/curiosity_q1/bias/ApplyAdamH^sac_value_opt/update_critic/q/q1_encoding/curiosity_q1/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q1_encoding/extrinsic_q1/bias/ApplyAdamH^sac_value_opt/update_critic/q/q1_encoding/extrinsic_q1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_0/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_1/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_2/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_3/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_3/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q2_encoding/curiosity_q2/bias/ApplyAdamH^sac_value_opt/update_critic/q/q2_encoding/curiosity_q2/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q2_encoding/extrinsic_q2/bias/ApplyAdamH^sac_value_opt/update_critic/q/q2_encoding/extrinsic_q2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_0/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_1/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_2/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_3/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_3/kernel/ApplyAdamA^sac_value_opt/update_critic/value/curiosity_value/bias/ApplyAdamC^sac_value_opt/update_critic/value/curiosity_value/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_0/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_0/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_1/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_1/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_2/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_2/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_3/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_3/kernel/ApplyAdamA^sac_value_opt/update_critic/value/extrinsic_value/bias/ApplyAdamC^sac_value_opt/update_critic/value/extrinsic_value/kernel/ApplyAdam*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias
�
sac_value_opt/AssignAssignbeta1_power_2sac_value_opt/mul*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking( *
validate_shape(
�
sac_value_opt/mul_1Mulbeta2_power_2/readsac_value_opt/beta2F^sac_value_opt/update_critic/q/q1_encoding/curiosity_q1/bias/ApplyAdamH^sac_value_opt/update_critic/q/q1_encoding/curiosity_q1/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q1_encoding/extrinsic_q1/bias/ApplyAdamH^sac_value_opt/update_critic/q/q1_encoding/extrinsic_q1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_0/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_1/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_2/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_3/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_3/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q2_encoding/curiosity_q2/bias/ApplyAdamH^sac_value_opt/update_critic/q/q2_encoding/curiosity_q2/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q2_encoding/extrinsic_q2/bias/ApplyAdamH^sac_value_opt/update_critic/q/q2_encoding/extrinsic_q2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_0/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_1/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_2/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_3/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_3/kernel/ApplyAdamA^sac_value_opt/update_critic/value/curiosity_value/bias/ApplyAdamC^sac_value_opt/update_critic/value/curiosity_value/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_0/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_0/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_1/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_1/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_2/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_2/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_3/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_3/kernel/ApplyAdamA^sac_value_opt/update_critic/value/extrinsic_value/bias/ApplyAdamC^sac_value_opt/update_critic/value/extrinsic_value/kernel/ApplyAdam*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias
�
sac_value_opt/Assign_1Assignbeta2_power_2sac_value_opt/mul_1*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking( *
validate_shape(
�
sac_value_optNoOp^sac_policy_opt^sac_value_opt/Assign^sac_value_opt/Assign_1F^sac_value_opt/update_critic/q/q1_encoding/curiosity_q1/bias/ApplyAdamH^sac_value_opt/update_critic/q/q1_encoding/curiosity_q1/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q1_encoding/extrinsic_q1/bias/ApplyAdamH^sac_value_opt/update_critic/q/q1_encoding/extrinsic_q1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_0/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_1/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_2/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_3/bias/ApplyAdamO^sac_value_opt/update_critic/q/q1_encoding/q1_encoder/hidden_3/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q2_encoding/curiosity_q2/bias/ApplyAdamH^sac_value_opt/update_critic/q/q2_encoding/curiosity_q2/kernel/ApplyAdamF^sac_value_opt/update_critic/q/q2_encoding/extrinsic_q2/bias/ApplyAdamH^sac_value_opt/update_critic/q/q2_encoding/extrinsic_q2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_0/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_1/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_2/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_2/kernel/ApplyAdamM^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_3/bias/ApplyAdamO^sac_value_opt/update_critic/q/q2_encoding/q2_encoder/hidden_3/kernel/ApplyAdamA^sac_value_opt/update_critic/value/curiosity_value/bias/ApplyAdamC^sac_value_opt/update_critic/value/curiosity_value/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_0/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_0/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_1/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_1/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_2/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_2/kernel/ApplyAdamB^sac_value_opt/update_critic/value/encoder/hidden_3/bias/ApplyAdamD^sac_value_opt/update_critic/value/encoder/hidden_3/kernel/ApplyAdamA^sac_value_opt/update_critic/value/extrinsic_value/bias/ApplyAdamC^sac_value_opt/update_critic/value/extrinsic_value/kernel/ApplyAdam
[
gradients_3/ShapeConst^sac_policy_opt^sac_value_opt*
dtype0*
valueB 
c
gradients_3/grad_ys_0Const^sac_policy_opt^sac_value_opt*
dtype0*
valueB
 *  �?
]
gradients_3/FillFillgradients_3/Shapegradients_3/grad_ys_0*
T0*

index_type0
<
gradients_3/Neg_1_grad/NegNeggradients_3/Fill*
T0
u
&gradients_3/Mean_17_grad/Reshape/shapeConst^sac_policy_opt^sac_value_opt*
dtype0*
valueB:
�
 gradients_3/Mean_17_grad/ReshapeReshapegradients_3/Neg_1_grad/Neg&gradients_3/Mean_17_grad/Reshape/shape*
T0*
Tshape0
i
gradients_3/Mean_17_grad/ShapeShapemul_29^sac_policy_opt^sac_value_opt*
T0*
out_type0
�
gradients_3/Mean_17_grad/TileTile gradients_3/Mean_17_grad/Reshapegradients_3/Mean_17_grad/Shape*
T0*

Tmultiples0
k
 gradients_3/Mean_17_grad/Shape_1Shapemul_29^sac_policy_opt^sac_value_opt*
T0*
out_type0
j
 gradients_3/Mean_17_grad/Shape_2Const^sac_policy_opt^sac_value_opt*
dtype0*
valueB 
m
gradients_3/Mean_17_grad/ConstConst^sac_policy_opt^sac_value_opt*
dtype0*
valueB: 
�
gradients_3/Mean_17_grad/ProdProd gradients_3/Mean_17_grad/Shape_1gradients_3/Mean_17_grad/Const*
T0*

Tidx0*
	keep_dims( 
o
 gradients_3/Mean_17_grad/Const_1Const^sac_policy_opt^sac_value_opt*
dtype0*
valueB: 
�
gradients_3/Mean_17_grad/Prod_1Prod gradients_3/Mean_17_grad/Shape_2 gradients_3/Mean_17_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
m
"gradients_3/Mean_17_grad/Maximum/yConst^sac_policy_opt^sac_value_opt*
dtype0*
value	B :
y
 gradients_3/Mean_17_grad/MaximumMaximumgradients_3/Mean_17_grad/Prod_1"gradients_3/Mean_17_grad/Maximum/y*
T0
w
!gradients_3/Mean_17_grad/floordivFloorDivgradients_3/Mean_17_grad/Prod gradients_3/Mean_17_grad/Maximum*
T0
p
gradients_3/Mean_17_grad/CastCast!gradients_3/Mean_17_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
r
 gradients_3/Mean_17_grad/truedivRealDivgradients_3/Mean_17_grad/Tilegradients_3/Mean_17_grad/Cast*
T0
k
gradients_3/mul_29_grad/ShapeShape	ToFloat_4^sac_policy_opt^sac_value_opt*
T0*
out_type0
k
gradients_3/mul_29_grad/Shape_1ShapeMean_16^sac_policy_opt^sac_value_opt*
T0*
out_type0
�
-gradients_3/mul_29_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_3/mul_29_grad/Shapegradients_3/mul_29_grad/Shape_1*
T0
V
gradients_3/mul_29_grad/MulMul gradients_3/Mean_17_grad/truedivMean_16*
T0
�
gradients_3/mul_29_grad/SumSumgradients_3/mul_29_grad/Mul-gradients_3/mul_29_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_3/mul_29_grad/ReshapeReshapegradients_3/mul_29_grad/Sumgradients_3/mul_29_grad/Shape*
T0*
Tshape0
Z
gradients_3/mul_29_grad/Mul_1Mul	ToFloat_4 gradients_3/Mean_17_grad/truediv*
T0
�
gradients_3/mul_29_grad/Sum_1Sumgradients_3/mul_29_grad/Mul_1/gradients_3/mul_29_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_3/mul_29_grad/Reshape_1Reshapegradients_3/mul_29_grad/Sum_1gradients_3/mul_29_grad/Shape_1*
T0*
Tshape0
�
(gradients_3/mul_29_grad/tuple/group_depsNoOp ^gradients_3/mul_29_grad/Reshape"^gradients_3/mul_29_grad/Reshape_1^sac_policy_opt^sac_value_opt
�
0gradients_3/mul_29_grad/tuple/control_dependencyIdentitygradients_3/mul_29_grad/Reshape)^gradients_3/mul_29_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_3/mul_29_grad/Reshape
�
2gradients_3/mul_29_grad/tuple/control_dependency_1Identity!gradients_3/mul_29_grad/Reshape_1)^gradients_3/mul_29_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_3/mul_29_grad/Reshape_1
i
gradients_3/Mean_16_grad/ShapeShapemul_28^sac_policy_opt^sac_value_opt*
T0*
out_type0
�
gradients_3/Mean_16_grad/SizeConst^sac_policy_opt^sac_value_opt*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape*
dtype0*
value	B :
�
gradients_3/Mean_16_grad/addAddV2Mean_16/reduction_indicesgradients_3/Mean_16_grad/Size*
T0*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape
�
gradients_3/Mean_16_grad/modFloorModgradients_3/Mean_16_grad/addgradients_3/Mean_16_grad/Size*
T0*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape
�
 gradients_3/Mean_16_grad/Shape_1Const^sac_policy_opt^sac_value_opt*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape*
dtype0*
valueB 
�
$gradients_3/Mean_16_grad/range/startConst^sac_policy_opt^sac_value_opt*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape*
dtype0*
value	B : 
�
$gradients_3/Mean_16_grad/range/deltaConst^sac_policy_opt^sac_value_opt*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape*
dtype0*
value	B :
�
gradients_3/Mean_16_grad/rangeRange$gradients_3/Mean_16_grad/range/startgradients_3/Mean_16_grad/Size$gradients_3/Mean_16_grad/range/delta*

Tidx0*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape
�
#gradients_3/Mean_16_grad/Fill/valueConst^sac_policy_opt^sac_value_opt*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape*
dtype0*
value	B :
�
gradients_3/Mean_16_grad/FillFill gradients_3/Mean_16_grad/Shape_1#gradients_3/Mean_16_grad/Fill/value*
T0*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape*

index_type0
�
&gradients_3/Mean_16_grad/DynamicStitchDynamicStitchgradients_3/Mean_16_grad/rangegradients_3/Mean_16_grad/modgradients_3/Mean_16_grad/Shapegradients_3/Mean_16_grad/Fill*
N*
T0*1
_class'
%#loc:@gradients_3/Mean_16_grad/Shape
�
 gradients_3/Mean_16_grad/ReshapeReshape2gradients_3/mul_29_grad/tuple/control_dependency_1&gradients_3/Mean_16_grad/DynamicStitch*
T0*
Tshape0
�
$gradients_3/Mean_16_grad/BroadcastToBroadcastTo gradients_3/Mean_16_grad/Reshapegradients_3/Mean_16_grad/Shape*
T0*

Tidx0
k
 gradients_3/Mean_16_grad/Shape_2Shapemul_28^sac_policy_opt^sac_value_opt*
T0*
out_type0
l
 gradients_3/Mean_16_grad/Shape_3ShapeMean_16^sac_policy_opt^sac_value_opt*
T0*
out_type0
m
gradients_3/Mean_16_grad/ConstConst^sac_policy_opt^sac_value_opt*
dtype0*
valueB: 
�
gradients_3/Mean_16_grad/ProdProd gradients_3/Mean_16_grad/Shape_2gradients_3/Mean_16_grad/Const*
T0*

Tidx0*
	keep_dims( 
o
 gradients_3/Mean_16_grad/Const_1Const^sac_policy_opt^sac_value_opt*
dtype0*
valueB: 
�
gradients_3/Mean_16_grad/Prod_1Prod gradients_3/Mean_16_grad/Shape_3 gradients_3/Mean_16_grad/Const_1*
T0*

Tidx0*
	keep_dims( 
m
"gradients_3/Mean_16_grad/Maximum/yConst^sac_policy_opt^sac_value_opt*
dtype0*
value	B :
y
 gradients_3/Mean_16_grad/MaximumMaximumgradients_3/Mean_16_grad/Prod_1"gradients_3/Mean_16_grad/Maximum/y*
T0
w
!gradients_3/Mean_16_grad/floordivFloorDivgradients_3/Mean_16_grad/Prod gradients_3/Mean_16_grad/Maximum*
T0
p
gradients_3/Mean_16_grad/CastCast!gradients_3/Mean_16_grad/floordiv*

DstT0*

SrcT0*
Truncate( 
y
 gradients_3/Mean_16_grad/truedivRealDiv$gradients_3/Mean_16_grad/BroadcastTogradients_3/Mean_16_grad/Cast*
T0
s
gradients_3/mul_28_grad/ShapeShapelog_ent_coef/read^sac_policy_opt^sac_value_opt*
T0*
out_type0
k
gradients_3/mul_28_grad/Shape_1ShapeSqueeze^sac_policy_opt^sac_value_opt*
T0*
out_type0
�
-gradients_3/mul_28_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_3/mul_28_grad/Shapegradients_3/mul_28_grad/Shape_1*
T0
V
gradients_3/mul_28_grad/MulMul gradients_3/Mean_16_grad/truedivSqueeze*
T0
�
gradients_3/mul_28_grad/SumSumgradients_3/mul_28_grad/Mul-gradients_3/mul_28_grad/BroadcastGradientArgs*
T0*

Tidx0*
	keep_dims( 
}
gradients_3/mul_28_grad/ReshapeReshapegradients_3/mul_28_grad/Sumgradients_3/mul_28_grad/Shape*
T0*
Tshape0
b
gradients_3/mul_28_grad/Mul_1Mullog_ent_coef/read gradients_3/Mean_16_grad/truediv*
T0
�
gradients_3/mul_28_grad/Sum_1Sumgradients_3/mul_28_grad/Mul_1/gradients_3/mul_28_grad/BroadcastGradientArgs:1*
T0*

Tidx0*
	keep_dims( 
�
!gradients_3/mul_28_grad/Reshape_1Reshapegradients_3/mul_28_grad/Sum_1gradients_3/mul_28_grad/Shape_1*
T0*
Tshape0
�
(gradients_3/mul_28_grad/tuple/group_depsNoOp ^gradients_3/mul_28_grad/Reshape"^gradients_3/mul_28_grad/Reshape_1^sac_policy_opt^sac_value_opt
�
0gradients_3/mul_28_grad/tuple/control_dependencyIdentitygradients_3/mul_28_grad/Reshape)^gradients_3/mul_28_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_3/mul_28_grad/Reshape
�
2gradients_3/mul_28_grad/tuple/control_dependency_1Identity!gradients_3/mul_28_grad/Reshape_1)^gradients_3/mul_28_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_3/mul_28_grad/Reshape_1
i
beta1_power_3/initial_valueConst*
_class
loc:@log_ent_coef*
dtype0*
valueB
 *fff?
z
beta1_power_3
VariableV2*
_class
loc:@log_ent_coef*
	container *
dtype0*
shape: *
shared_name 
�
beta1_power_3/AssignAssignbeta1_power_3beta1_power_3/initial_value*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
W
beta1_power_3/readIdentitybeta1_power_3*
T0*
_class
loc:@log_ent_coef
i
beta2_power_3/initial_valueConst*
_class
loc:@log_ent_coef*
dtype0*
valueB
 *w�?
z
beta2_power_3
VariableV2*
_class
loc:@log_ent_coef*
	container *
dtype0*
shape: *
shared_name 
�
beta2_power_3/AssignAssignbeta2_power_3beta2_power_3/initial_value*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
W
beta2_power_3/readIdentitybeta2_power_3*
T0*
_class
loc:@log_ent_coef
�
.log_ent_coef/sac_entropy_opt/Initializer/zerosConst*
_class
loc:@log_ent_coef*
dtype0*
valueB*    
�
log_ent_coef/sac_entropy_opt
VariableV2*
_class
loc:@log_ent_coef*
	container *
dtype0*
shape:*
shared_name 
�
#log_ent_coef/sac_entropy_opt/AssignAssignlog_ent_coef/sac_entropy_opt.log_ent_coef/sac_entropy_opt/Initializer/zeros*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
u
!log_ent_coef/sac_entropy_opt/readIdentitylog_ent_coef/sac_entropy_opt*
T0*
_class
loc:@log_ent_coef
�
0log_ent_coef/sac_entropy_opt_1/Initializer/zerosConst*
_class
loc:@log_ent_coef*
dtype0*
valueB*    
�
log_ent_coef/sac_entropy_opt_1
VariableV2*
_class
loc:@log_ent_coef*
	container *
dtype0*
shape:*
shared_name 
�
%log_ent_coef/sac_entropy_opt_1/AssignAssignlog_ent_coef/sac_entropy_opt_10log_ent_coef/sac_entropy_opt_1/Initializer/zeros*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
y
#log_ent_coef/sac_entropy_opt_1/readIdentitylog_ent_coef/sac_entropy_opt_1*
T0*
_class
loc:@log_ent_coef
c
sac_entropy_opt/beta1Const^sac_policy_opt^sac_value_opt*
dtype0*
valueB
 *fff?
c
sac_entropy_opt/beta2Const^sac_policy_opt^sac_value_opt*
dtype0*
valueB
 *w�?
e
sac_entropy_opt/epsilonConst^sac_policy_opt^sac_value_opt*
dtype0*
valueB
 *w�+2
�
-sac_entropy_opt/update_log_ent_coef/ApplyAdam	ApplyAdamlog_ent_coeflog_ent_coef/sac_entropy_optlog_ent_coef/sac_entropy_opt_1beta1_power_3/readbeta2_power_3/readVariable_2/readsac_entropy_opt/beta1sac_entropy_opt/beta2sac_entropy_opt/epsilon0gradients_3/mul_28_grad/tuple/control_dependency*
T0*
_class
loc:@log_ent_coef*
use_locking( *
use_nesterov( 
�
sac_entropy_opt/mulMulbeta1_power_3/readsac_entropy_opt/beta1.^sac_entropy_opt/update_log_ent_coef/ApplyAdam*
T0*
_class
loc:@log_ent_coef
�
sac_entropy_opt/AssignAssignbeta1_power_3sac_entropy_opt/mul*
T0*
_class
loc:@log_ent_coef*
use_locking( *
validate_shape(
�
sac_entropy_opt/mul_1Mulbeta2_power_3/readsac_entropy_opt/beta2.^sac_entropy_opt/update_log_ent_coef/ApplyAdam*
T0*
_class
loc:@log_ent_coef
�
sac_entropy_opt/Assign_1Assignbeta2_power_3sac_entropy_opt/mul_1*
T0*
_class
loc:@log_ent_coef*
use_locking( *
validate_shape(
�
sac_entropy_optNoOp^sac_entropy_opt/Assign^sac_entropy_opt/Assign_1.^sac_entropy_opt/update_log_ent_coef/ApplyAdam^sac_policy_opt^sac_value_opt
C
save_1/filename/inputConst*
dtype0*
valueB Bmodel
Z
save_1/filenamePlaceholderWithDefaultsave_1/filename/input*
dtype0*
shape: 
Q
save_1/ConstPlaceholderWithDefaultsave_1/filename*
dtype0*
shape: 
�F
save_1/SaveV2/tensor_namesConst*
dtype0*�E
value�EB�E�BVariableB
Variable_1B
Variable_2Baction_output_shapeBbeta1_powerBbeta1_power_1Bbeta1_power_2Bbeta1_power_3Bbeta2_powerBbeta2_power_1Bbeta2_power_2Bbeta2_power_3B&critic/q/q1_encoding/curiosity_q1/biasB4critic/q/q1_encoding/curiosity_q1/bias/sac_value_optB6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1B(critic/q/q1_encoding/curiosity_q1/kernelB6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_optB8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1B&critic/q/q1_encoding/extrinsic_q1/biasB4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_optB6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1B(critic/q/q1_encoding/extrinsic_q1/kernelB6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_optB8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1B-critic/q/q1_encoding/q1_encoder/hidden_0/biasB;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_optB=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1B/critic/q/q1_encoding/q1_encoder/hidden_0/kernelB=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_optB?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1B-critic/q/q1_encoding/q1_encoder/hidden_1/biasB;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_optB=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1B/critic/q/q1_encoding/q1_encoder/hidden_1/kernelB=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_optB?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1B-critic/q/q1_encoding/q1_encoder/hidden_2/biasB;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_optB=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1B/critic/q/q1_encoding/q1_encoder/hidden_2/kernelB=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_optB?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1B-critic/q/q1_encoding/q1_encoder/hidden_3/biasB;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_optB=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1B/critic/q/q1_encoding/q1_encoder/hidden_3/kernelB=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_optB?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1B&critic/q/q2_encoding/curiosity_q2/biasB4critic/q/q2_encoding/curiosity_q2/bias/sac_value_optB6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1B(critic/q/q2_encoding/curiosity_q2/kernelB6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_optB8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1B&critic/q/q2_encoding/extrinsic_q2/biasB4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_optB6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1B(critic/q/q2_encoding/extrinsic_q2/kernelB6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_optB8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1B-critic/q/q2_encoding/q2_encoder/hidden_0/biasB;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_optB=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1B/critic/q/q2_encoding/q2_encoder/hidden_0/kernelB=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_optB?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1B-critic/q/q2_encoding/q2_encoder/hidden_1/biasB;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_optB=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1B/critic/q/q2_encoding/q2_encoder/hidden_1/kernelB=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_optB?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1B-critic/q/q2_encoding/q2_encoder/hidden_2/biasB;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_optB=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1B/critic/q/q2_encoding/q2_encoder/hidden_2/kernelB=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_optB?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1B-critic/q/q2_encoding/q2_encoder/hidden_3/biasB;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_optB=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1B/critic/q/q2_encoding/q2_encoder/hidden_3/kernelB=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_optB?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1B!critic/value/curiosity_value/biasB/critic/value/curiosity_value/bias/sac_value_optB1critic/value/curiosity_value/bias/sac_value_opt_1B#critic/value/curiosity_value/kernelB1critic/value/curiosity_value/kernel/sac_value_optB3critic/value/curiosity_value/kernel/sac_value_opt_1B"critic/value/encoder/hidden_0/biasB0critic/value/encoder/hidden_0/bias/sac_value_optB2critic/value/encoder/hidden_0/bias/sac_value_opt_1B$critic/value/encoder/hidden_0/kernelB2critic/value/encoder/hidden_0/kernel/sac_value_optB4critic/value/encoder/hidden_0/kernel/sac_value_opt_1B"critic/value/encoder/hidden_1/biasB0critic/value/encoder/hidden_1/bias/sac_value_optB2critic/value/encoder/hidden_1/bias/sac_value_opt_1B$critic/value/encoder/hidden_1/kernelB2critic/value/encoder/hidden_1/kernel/sac_value_optB4critic/value/encoder/hidden_1/kernel/sac_value_opt_1B"critic/value/encoder/hidden_2/biasB0critic/value/encoder/hidden_2/bias/sac_value_optB2critic/value/encoder/hidden_2/bias/sac_value_opt_1B$critic/value/encoder/hidden_2/kernelB2critic/value/encoder/hidden_2/kernel/sac_value_optB4critic/value/encoder/hidden_2/kernel/sac_value_opt_1B"critic/value/encoder/hidden_3/biasB0critic/value/encoder/hidden_3/bias/sac_value_optB2critic/value/encoder/hidden_3/bias/sac_value_opt_1B$critic/value/encoder/hidden_3/kernelB2critic/value/encoder/hidden_3/kernel/sac_value_optB4critic/value/encoder/hidden_3/kernel/sac_value_opt_1B!critic/value/extrinsic_value/biasB/critic/value/extrinsic_value/bias/sac_value_optB1critic/value/extrinsic_value/bias/sac_value_opt_1B#critic/value/extrinsic_value/kernelB1critic/value/extrinsic_value/kernel/sac_value_optB3critic/value/extrinsic_value/kernel/sac_value_opt_1B*curiosity_vector_obs_encoder/hidden_0/biasB/curiosity_vector_obs_encoder/hidden_0/bias/AdamB1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1B,curiosity_vector_obs_encoder/hidden_0/kernelB1curiosity_vector_obs_encoder/hidden_0/kernel/AdamB3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1B*curiosity_vector_obs_encoder/hidden_1/biasB/curiosity_vector_obs_encoder/hidden_1/bias/AdamB1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1B,curiosity_vector_obs_encoder/hidden_1/kernelB1curiosity_vector_obs_encoder/hidden_1/kernel/AdamB3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1B
dense/biasBdense/bias/AdamBdense/bias/Adam_1Bdense/kernelBdense/kernel/AdamBdense/kernel/Adam_1Bdense_1/biasBdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_1/kernelBdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_2/biasBdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_2/kernelBdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_3/biasBdense_3/bias/AdamBdense_3/bias/Adam_1Bdense_3/kernelBdense_3/kernel/AdamBdense_3/kernel/Adam_1Bglobal_stepBis_continuous_controlBlog_ent_coefBlog_ent_coef/sac_entropy_optBlog_ent_coef/sac_entropy_opt_1Bmemory_sizeBpolicy/dense/kernelB"policy/dense/kernel/sac_policy_optB$policy/dense/kernel/sac_policy_opt_1B!policy/main_graph_0/hidden_0/biasB0policy/main_graph_0/hidden_0/bias/sac_policy_optB2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1B#policy/main_graph_0/hidden_0/kernelB2policy/main_graph_0/hidden_0/kernel/sac_policy_optB4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1B!policy/main_graph_0/hidden_1/biasB0policy/main_graph_0/hidden_1/bias/sac_policy_optB2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1B#policy/main_graph_0/hidden_1/kernelB2policy/main_graph_0/hidden_1/kernel/sac_policy_optB4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1B!policy/main_graph_0/hidden_2/biasB0policy/main_graph_0/hidden_2/bias/sac_policy_optB2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1B#policy/main_graph_0/hidden_2/kernelB2policy/main_graph_0/hidden_2/kernel/sac_policy_optB4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1B!policy/main_graph_0/hidden_3/biasB0policy/main_graph_0/hidden_3/bias/sac_policy_optB2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1B#policy/main_graph_0/hidden_3/kernelB2policy/main_graph_0/hidden_3/kernel/sac_policy_optB4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1B0target_network/critic/value/curiosity_value/biasB2target_network/critic/value/curiosity_value/kernelB1target_network/critic/value/encoder/hidden_0/biasB3target_network/critic/value/encoder/hidden_0/kernelB1target_network/critic/value/encoder/hidden_1/biasB3target_network/critic/value/encoder/hidden_1/kernelB1target_network/critic/value/encoder/hidden_2/biasB3target_network/critic/value/encoder/hidden_2/kernelB1target_network/critic/value/encoder/hidden_3/biasB3target_network/critic/value/encoder/hidden_3/kernelB0target_network/critic/value/extrinsic_value/biasB2target_network/critic/value/extrinsic_value/kernelBtrainer_major_versionBtrainer_minor_versionBtrainer_patch_versionBversion_number
�
save_1/SaveV2/shape_and_slicesConst*
dtype0*�
value�B��B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 
�H
save_1/SaveV2SaveV2save_1/Constsave_1/SaveV2/tensor_namessave_1/SaveV2/shape_and_slicesVariable
Variable_1
Variable_2action_output_shapebeta1_powerbeta1_power_1beta1_power_2beta1_power_3beta2_powerbeta2_power_1beta2_power_2beta2_power_3&critic/q/q1_encoding/curiosity_q1/bias4critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1(critic/q/q1_encoding/curiosity_q1/kernel6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1&critic/q/q1_encoding/extrinsic_q1/bias4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1(critic/q/q1_encoding/extrinsic_q1/kernel6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1-critic/q/q1_encoding/q1_encoder/hidden_0/bias;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1/critic/q/q1_encoding/q1_encoder/hidden_0/kernel=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1-critic/q/q1_encoding/q1_encoder/hidden_1/bias;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1/critic/q/q1_encoding/q1_encoder/hidden_1/kernel=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1-critic/q/q1_encoding/q1_encoder/hidden_2/bias;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1/critic/q/q1_encoding/q1_encoder/hidden_2/kernel=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1-critic/q/q1_encoding/q1_encoder/hidden_3/bias;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1/critic/q/q1_encoding/q1_encoder/hidden_3/kernel=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1&critic/q/q2_encoding/curiosity_q2/bias4critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1(critic/q/q2_encoding/curiosity_q2/kernel6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1&critic/q/q2_encoding/extrinsic_q2/bias4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1(critic/q/q2_encoding/extrinsic_q2/kernel6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1-critic/q/q2_encoding/q2_encoder/hidden_0/bias;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1/critic/q/q2_encoding/q2_encoder/hidden_0/kernel=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1-critic/q/q2_encoding/q2_encoder/hidden_1/bias;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1/critic/q/q2_encoding/q2_encoder/hidden_1/kernel=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1-critic/q/q2_encoding/q2_encoder/hidden_2/bias;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1/critic/q/q2_encoding/q2_encoder/hidden_2/kernel=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1-critic/q/q2_encoding/q2_encoder/hidden_3/bias;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1/critic/q/q2_encoding/q2_encoder/hidden_3/kernel=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1!critic/value/curiosity_value/bias/critic/value/curiosity_value/bias/sac_value_opt1critic/value/curiosity_value/bias/sac_value_opt_1#critic/value/curiosity_value/kernel1critic/value/curiosity_value/kernel/sac_value_opt3critic/value/curiosity_value/kernel/sac_value_opt_1"critic/value/encoder/hidden_0/bias0critic/value/encoder/hidden_0/bias/sac_value_opt2critic/value/encoder/hidden_0/bias/sac_value_opt_1$critic/value/encoder/hidden_0/kernel2critic/value/encoder/hidden_0/kernel/sac_value_opt4critic/value/encoder/hidden_0/kernel/sac_value_opt_1"critic/value/encoder/hidden_1/bias0critic/value/encoder/hidden_1/bias/sac_value_opt2critic/value/encoder/hidden_1/bias/sac_value_opt_1$critic/value/encoder/hidden_1/kernel2critic/value/encoder/hidden_1/kernel/sac_value_opt4critic/value/encoder/hidden_1/kernel/sac_value_opt_1"critic/value/encoder/hidden_2/bias0critic/value/encoder/hidden_2/bias/sac_value_opt2critic/value/encoder/hidden_2/bias/sac_value_opt_1$critic/value/encoder/hidden_2/kernel2critic/value/encoder/hidden_2/kernel/sac_value_opt4critic/value/encoder/hidden_2/kernel/sac_value_opt_1"critic/value/encoder/hidden_3/bias0critic/value/encoder/hidden_3/bias/sac_value_opt2critic/value/encoder/hidden_3/bias/sac_value_opt_1$critic/value/encoder/hidden_3/kernel2critic/value/encoder/hidden_3/kernel/sac_value_opt4critic/value/encoder/hidden_3/kernel/sac_value_opt_1!critic/value/extrinsic_value/bias/critic/value/extrinsic_value/bias/sac_value_opt1critic/value/extrinsic_value/bias/sac_value_opt_1#critic/value/extrinsic_value/kernel1critic/value/extrinsic_value/kernel/sac_value_opt3critic/value/extrinsic_value/kernel/sac_value_opt_1*curiosity_vector_obs_encoder/hidden_0/bias/curiosity_vector_obs_encoder/hidden_0/bias/Adam1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1,curiosity_vector_obs_encoder/hidden_0/kernel1curiosity_vector_obs_encoder/hidden_0/kernel/Adam3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1*curiosity_vector_obs_encoder/hidden_1/bias/curiosity_vector_obs_encoder/hidden_1/bias/Adam1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1,curiosity_vector_obs_encoder/hidden_1/kernel1curiosity_vector_obs_encoder/hidden_1/kernel/Adam3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1
dense/biasdense/bias/Adamdense/bias/Adam_1dense/kerneldense/kernel/Adamdense/kernel/Adam_1dense_1/biasdense_1/bias/Adamdense_1/bias/Adam_1dense_1/kerneldense_1/kernel/Adamdense_1/kernel/Adam_1dense_2/biasdense_2/bias/Adamdense_2/bias/Adam_1dense_2/kerneldense_2/kernel/Adamdense_2/kernel/Adam_1dense_3/biasdense_3/bias/Adamdense_3/bias/Adam_1dense_3/kerneldense_3/kernel/Adamdense_3/kernel/Adam_1global_stepis_continuous_controllog_ent_coeflog_ent_coef/sac_entropy_optlog_ent_coef/sac_entropy_opt_1memory_sizepolicy/dense/kernel"policy/dense/kernel/sac_policy_opt$policy/dense/kernel/sac_policy_opt_1!policy/main_graph_0/hidden_0/bias0policy/main_graph_0/hidden_0/bias/sac_policy_opt2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1#policy/main_graph_0/hidden_0/kernel2policy/main_graph_0/hidden_0/kernel/sac_policy_opt4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1!policy/main_graph_0/hidden_1/bias0policy/main_graph_0/hidden_1/bias/sac_policy_opt2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1#policy/main_graph_0/hidden_1/kernel2policy/main_graph_0/hidden_1/kernel/sac_policy_opt4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1!policy/main_graph_0/hidden_2/bias0policy/main_graph_0/hidden_2/bias/sac_policy_opt2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1#policy/main_graph_0/hidden_2/kernel2policy/main_graph_0/hidden_2/kernel/sac_policy_opt4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1!policy/main_graph_0/hidden_3/bias0policy/main_graph_0/hidden_3/bias/sac_policy_opt2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1#policy/main_graph_0/hidden_3/kernel2policy/main_graph_0/hidden_3/kernel/sac_policy_opt4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_10target_network/critic/value/curiosity_value/bias2target_network/critic/value/curiosity_value/kernel1target_network/critic/value/encoder/hidden_0/bias3target_network/critic/value/encoder/hidden_0/kernel1target_network/critic/value/encoder/hidden_1/bias3target_network/critic/value/encoder/hidden_1/kernel1target_network/critic/value/encoder/hidden_2/bias3target_network/critic/value/encoder/hidden_2/kernel1target_network/critic/value/encoder/hidden_3/bias3target_network/critic/value/encoder/hidden_3/kernel0target_network/critic/value/extrinsic_value/bias2target_network/critic/value/extrinsic_value/kerneltrainer_major_versiontrainer_minor_versiontrainer_patch_versionversion_number*�
dtypes�
�2�
m
save_1/control_dependencyIdentitysave_1/Const^save_1/SaveV2*
T0*
_class
loc:@save_1/Const
�F
save_1/RestoreV2/tensor_namesConst"/device:CPU:0*
dtype0*�E
value�EB�E�BVariableB
Variable_1B
Variable_2Baction_output_shapeBbeta1_powerBbeta1_power_1Bbeta1_power_2Bbeta1_power_3Bbeta2_powerBbeta2_power_1Bbeta2_power_2Bbeta2_power_3B&critic/q/q1_encoding/curiosity_q1/biasB4critic/q/q1_encoding/curiosity_q1/bias/sac_value_optB6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1B(critic/q/q1_encoding/curiosity_q1/kernelB6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_optB8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1B&critic/q/q1_encoding/extrinsic_q1/biasB4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_optB6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1B(critic/q/q1_encoding/extrinsic_q1/kernelB6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_optB8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1B-critic/q/q1_encoding/q1_encoder/hidden_0/biasB;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_optB=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1B/critic/q/q1_encoding/q1_encoder/hidden_0/kernelB=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_optB?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1B-critic/q/q1_encoding/q1_encoder/hidden_1/biasB;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_optB=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1B/critic/q/q1_encoding/q1_encoder/hidden_1/kernelB=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_optB?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1B-critic/q/q1_encoding/q1_encoder/hidden_2/biasB;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_optB=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1B/critic/q/q1_encoding/q1_encoder/hidden_2/kernelB=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_optB?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1B-critic/q/q1_encoding/q1_encoder/hidden_3/biasB;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_optB=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1B/critic/q/q1_encoding/q1_encoder/hidden_3/kernelB=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_optB?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1B&critic/q/q2_encoding/curiosity_q2/biasB4critic/q/q2_encoding/curiosity_q2/bias/sac_value_optB6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1B(critic/q/q2_encoding/curiosity_q2/kernelB6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_optB8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1B&critic/q/q2_encoding/extrinsic_q2/biasB4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_optB6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1B(critic/q/q2_encoding/extrinsic_q2/kernelB6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_optB8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1B-critic/q/q2_encoding/q2_encoder/hidden_0/biasB;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_optB=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1B/critic/q/q2_encoding/q2_encoder/hidden_0/kernelB=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_optB?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1B-critic/q/q2_encoding/q2_encoder/hidden_1/biasB;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_optB=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1B/critic/q/q2_encoding/q2_encoder/hidden_1/kernelB=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_optB?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1B-critic/q/q2_encoding/q2_encoder/hidden_2/biasB;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_optB=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1B/critic/q/q2_encoding/q2_encoder/hidden_2/kernelB=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_optB?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1B-critic/q/q2_encoding/q2_encoder/hidden_3/biasB;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_optB=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1B/critic/q/q2_encoding/q2_encoder/hidden_3/kernelB=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_optB?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1B!critic/value/curiosity_value/biasB/critic/value/curiosity_value/bias/sac_value_optB1critic/value/curiosity_value/bias/sac_value_opt_1B#critic/value/curiosity_value/kernelB1critic/value/curiosity_value/kernel/sac_value_optB3critic/value/curiosity_value/kernel/sac_value_opt_1B"critic/value/encoder/hidden_0/biasB0critic/value/encoder/hidden_0/bias/sac_value_optB2critic/value/encoder/hidden_0/bias/sac_value_opt_1B$critic/value/encoder/hidden_0/kernelB2critic/value/encoder/hidden_0/kernel/sac_value_optB4critic/value/encoder/hidden_0/kernel/sac_value_opt_1B"critic/value/encoder/hidden_1/biasB0critic/value/encoder/hidden_1/bias/sac_value_optB2critic/value/encoder/hidden_1/bias/sac_value_opt_1B$critic/value/encoder/hidden_1/kernelB2critic/value/encoder/hidden_1/kernel/sac_value_optB4critic/value/encoder/hidden_1/kernel/sac_value_opt_1B"critic/value/encoder/hidden_2/biasB0critic/value/encoder/hidden_2/bias/sac_value_optB2critic/value/encoder/hidden_2/bias/sac_value_opt_1B$critic/value/encoder/hidden_2/kernelB2critic/value/encoder/hidden_2/kernel/sac_value_optB4critic/value/encoder/hidden_2/kernel/sac_value_opt_1B"critic/value/encoder/hidden_3/biasB0critic/value/encoder/hidden_3/bias/sac_value_optB2critic/value/encoder/hidden_3/bias/sac_value_opt_1B$critic/value/encoder/hidden_3/kernelB2critic/value/encoder/hidden_3/kernel/sac_value_optB4critic/value/encoder/hidden_3/kernel/sac_value_opt_1B!critic/value/extrinsic_value/biasB/critic/value/extrinsic_value/bias/sac_value_optB1critic/value/extrinsic_value/bias/sac_value_opt_1B#critic/value/extrinsic_value/kernelB1critic/value/extrinsic_value/kernel/sac_value_optB3critic/value/extrinsic_value/kernel/sac_value_opt_1B*curiosity_vector_obs_encoder/hidden_0/biasB/curiosity_vector_obs_encoder/hidden_0/bias/AdamB1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1B,curiosity_vector_obs_encoder/hidden_0/kernelB1curiosity_vector_obs_encoder/hidden_0/kernel/AdamB3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1B*curiosity_vector_obs_encoder/hidden_1/biasB/curiosity_vector_obs_encoder/hidden_1/bias/AdamB1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1B,curiosity_vector_obs_encoder/hidden_1/kernelB1curiosity_vector_obs_encoder/hidden_1/kernel/AdamB3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1B
dense/biasBdense/bias/AdamBdense/bias/Adam_1Bdense/kernelBdense/kernel/AdamBdense/kernel/Adam_1Bdense_1/biasBdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_1/kernelBdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_2/biasBdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_2/kernelBdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_3/biasBdense_3/bias/AdamBdense_3/bias/Adam_1Bdense_3/kernelBdense_3/kernel/AdamBdense_3/kernel/Adam_1Bglobal_stepBis_continuous_controlBlog_ent_coefBlog_ent_coef/sac_entropy_optBlog_ent_coef/sac_entropy_opt_1Bmemory_sizeBpolicy/dense/kernelB"policy/dense/kernel/sac_policy_optB$policy/dense/kernel/sac_policy_opt_1B!policy/main_graph_0/hidden_0/biasB0policy/main_graph_0/hidden_0/bias/sac_policy_optB2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1B#policy/main_graph_0/hidden_0/kernelB2policy/main_graph_0/hidden_0/kernel/sac_policy_optB4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1B!policy/main_graph_0/hidden_1/biasB0policy/main_graph_0/hidden_1/bias/sac_policy_optB2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1B#policy/main_graph_0/hidden_1/kernelB2policy/main_graph_0/hidden_1/kernel/sac_policy_optB4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1B!policy/main_graph_0/hidden_2/biasB0policy/main_graph_0/hidden_2/bias/sac_policy_optB2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1B#policy/main_graph_0/hidden_2/kernelB2policy/main_graph_0/hidden_2/kernel/sac_policy_optB4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1B!policy/main_graph_0/hidden_3/biasB0policy/main_graph_0/hidden_3/bias/sac_policy_optB2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1B#policy/main_graph_0/hidden_3/kernelB2policy/main_graph_0/hidden_3/kernel/sac_policy_optB4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1B0target_network/critic/value/curiosity_value/biasB2target_network/critic/value/curiosity_value/kernelB1target_network/critic/value/encoder/hidden_0/biasB3target_network/critic/value/encoder/hidden_0/kernelB1target_network/critic/value/encoder/hidden_1/biasB3target_network/critic/value/encoder/hidden_1/kernelB1target_network/critic/value/encoder/hidden_2/biasB3target_network/critic/value/encoder/hidden_2/kernelB1target_network/critic/value/encoder/hidden_3/biasB3target_network/critic/value/encoder/hidden_3/kernelB0target_network/critic/value/extrinsic_value/biasB2target_network/critic/value/extrinsic_value/kernelBtrainer_major_versionBtrainer_minor_versionBtrainer_patch_versionBversion_number
�
!save_1/RestoreV2/shape_and_slicesConst"/device:CPU:0*
dtype0*�
value�B��B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 
�
save_1/RestoreV2	RestoreV2save_1/Constsave_1/RestoreV2/tensor_names!save_1/RestoreV2/shape_and_slices"/device:CPU:0*�
dtypes�
�2�
�
save_1/AssignAssignVariablesave_1/RestoreV2*
T0*
_class
loc:@Variable*
use_locking(*
validate_shape(
�
save_1/Assign_1Assign
Variable_1save_1/RestoreV2:1*
T0*
_class
loc:@Variable_1*
use_locking(*
validate_shape(
�
save_1/Assign_2Assign
Variable_2save_1/RestoreV2:2*
T0*
_class
loc:@Variable_2*
use_locking(*
validate_shape(
�
save_1/Assign_3Assignaction_output_shapesave_1/RestoreV2:3*
T0*&
_class
loc:@action_output_shape*
use_locking(*
validate_shape(
�
save_1/Assign_4Assignbeta1_powersave_1/RestoreV2:4*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_5Assignbeta1_power_1save_1/RestoreV2:5*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_6Assignbeta1_power_2save_1/RestoreV2:6*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_7Assignbeta1_power_3save_1/RestoreV2:7*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
�
save_1/Assign_8Assignbeta2_powersave_1/RestoreV2:8*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_9Assignbeta2_power_1save_1/RestoreV2:9*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_10Assignbeta2_power_2save_1/RestoreV2:10*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_11Assignbeta2_power_3save_1/RestoreV2:11*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
�
save_1/Assign_12Assign&critic/q/q1_encoding/curiosity_q1/biassave_1/RestoreV2:12*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_13Assign4critic/q/q1_encoding/curiosity_q1/bias/sac_value_optsave_1/RestoreV2:13*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_14Assign6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1save_1/RestoreV2:14*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_15Assign(critic/q/q1_encoding/curiosity_q1/kernelsave_1/RestoreV2:15*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_16Assign6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_optsave_1/RestoreV2:16*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_17Assign8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1save_1/RestoreV2:17*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_18Assign&critic/q/q1_encoding/extrinsic_q1/biassave_1/RestoreV2:18*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_19Assign4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_optsave_1/RestoreV2:19*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_20Assign6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1save_1/RestoreV2:20*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_21Assign(critic/q/q1_encoding/extrinsic_q1/kernelsave_1/RestoreV2:21*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_22Assign6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_optsave_1/RestoreV2:22*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_23Assign8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1save_1/RestoreV2:23*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_24Assign-critic/q/q1_encoding/q1_encoder/hidden_0/biassave_1/RestoreV2:24*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_25Assign;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_optsave_1/RestoreV2:25*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_26Assign=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1save_1/RestoreV2:26*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_27Assign/critic/q/q1_encoding/q1_encoder/hidden_0/kernelsave_1/RestoreV2:27*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_28Assign=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_optsave_1/RestoreV2:28*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_29Assign?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1save_1/RestoreV2:29*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_30Assign-critic/q/q1_encoding/q1_encoder/hidden_1/biassave_1/RestoreV2:30*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_31Assign;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_optsave_1/RestoreV2:31*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_32Assign=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1save_1/RestoreV2:32*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_33Assign/critic/q/q1_encoding/q1_encoder/hidden_1/kernelsave_1/RestoreV2:33*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_34Assign=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_optsave_1/RestoreV2:34*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_35Assign?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1save_1/RestoreV2:35*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_36Assign-critic/q/q1_encoding/q1_encoder/hidden_2/biassave_1/RestoreV2:36*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_37Assign;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_optsave_1/RestoreV2:37*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_38Assign=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1save_1/RestoreV2:38*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_39Assign/critic/q/q1_encoding/q1_encoder/hidden_2/kernelsave_1/RestoreV2:39*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_40Assign=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_optsave_1/RestoreV2:40*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_41Assign?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1save_1/RestoreV2:41*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_42Assign-critic/q/q1_encoding/q1_encoder/hidden_3/biassave_1/RestoreV2:42*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_43Assign;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_optsave_1/RestoreV2:43*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_44Assign=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1save_1/RestoreV2:44*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_45Assign/critic/q/q1_encoding/q1_encoder/hidden_3/kernelsave_1/RestoreV2:45*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_46Assign=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_optsave_1/RestoreV2:46*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_47Assign?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1save_1/RestoreV2:47*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_48Assign&critic/q/q2_encoding/curiosity_q2/biassave_1/RestoreV2:48*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_49Assign4critic/q/q2_encoding/curiosity_q2/bias/sac_value_optsave_1/RestoreV2:49*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_50Assign6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1save_1/RestoreV2:50*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_51Assign(critic/q/q2_encoding/curiosity_q2/kernelsave_1/RestoreV2:51*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_52Assign6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_optsave_1/RestoreV2:52*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_53Assign8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1save_1/RestoreV2:53*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_54Assign&critic/q/q2_encoding/extrinsic_q2/biassave_1/RestoreV2:54*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_55Assign4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_optsave_1/RestoreV2:55*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_56Assign6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1save_1/RestoreV2:56*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_57Assign(critic/q/q2_encoding/extrinsic_q2/kernelsave_1/RestoreV2:57*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_58Assign6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_optsave_1/RestoreV2:58*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_59Assign8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1save_1/RestoreV2:59*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_60Assign-critic/q/q2_encoding/q2_encoder/hidden_0/biassave_1/RestoreV2:60*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_61Assign;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_optsave_1/RestoreV2:61*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_62Assign=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1save_1/RestoreV2:62*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_63Assign/critic/q/q2_encoding/q2_encoder/hidden_0/kernelsave_1/RestoreV2:63*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_64Assign=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_optsave_1/RestoreV2:64*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_65Assign?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1save_1/RestoreV2:65*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_66Assign-critic/q/q2_encoding/q2_encoder/hidden_1/biassave_1/RestoreV2:66*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_67Assign;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_optsave_1/RestoreV2:67*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_68Assign=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1save_1/RestoreV2:68*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_69Assign/critic/q/q2_encoding/q2_encoder/hidden_1/kernelsave_1/RestoreV2:69*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_70Assign=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_optsave_1/RestoreV2:70*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_71Assign?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1save_1/RestoreV2:71*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_72Assign-critic/q/q2_encoding/q2_encoder/hidden_2/biassave_1/RestoreV2:72*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_73Assign;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_optsave_1/RestoreV2:73*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_74Assign=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1save_1/RestoreV2:74*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_75Assign/critic/q/q2_encoding/q2_encoder/hidden_2/kernelsave_1/RestoreV2:75*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_76Assign=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_optsave_1/RestoreV2:76*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_77Assign?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1save_1/RestoreV2:77*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_78Assign-critic/q/q2_encoding/q2_encoder/hidden_3/biassave_1/RestoreV2:78*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_79Assign;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_optsave_1/RestoreV2:79*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_80Assign=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1save_1/RestoreV2:80*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_81Assign/critic/q/q2_encoding/q2_encoder/hidden_3/kernelsave_1/RestoreV2:81*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_82Assign=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_optsave_1/RestoreV2:82*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_83Assign?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1save_1/RestoreV2:83*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_84Assign!critic/value/curiosity_value/biassave_1/RestoreV2:84*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
save_1/Assign_85Assign/critic/value/curiosity_value/bias/sac_value_optsave_1/RestoreV2:85*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
save_1/Assign_86Assign1critic/value/curiosity_value/bias/sac_value_opt_1save_1/RestoreV2:86*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
save_1/Assign_87Assign#critic/value/curiosity_value/kernelsave_1/RestoreV2:87*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_88Assign1critic/value/curiosity_value/kernel/sac_value_optsave_1/RestoreV2:88*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_89Assign3critic/value/curiosity_value/kernel/sac_value_opt_1save_1/RestoreV2:89*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_90Assign"critic/value/encoder/hidden_0/biassave_1/RestoreV2:90*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_91Assign0critic/value/encoder/hidden_0/bias/sac_value_optsave_1/RestoreV2:91*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_92Assign2critic/value/encoder/hidden_0/bias/sac_value_opt_1save_1/RestoreV2:92*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_93Assign$critic/value/encoder/hidden_0/kernelsave_1/RestoreV2:93*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_94Assign2critic/value/encoder/hidden_0/kernel/sac_value_optsave_1/RestoreV2:94*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_95Assign4critic/value/encoder/hidden_0/kernel/sac_value_opt_1save_1/RestoreV2:95*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_96Assign"critic/value/encoder/hidden_1/biassave_1/RestoreV2:96*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_97Assign0critic/value/encoder/hidden_1/bias/sac_value_optsave_1/RestoreV2:97*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_98Assign2critic/value/encoder/hidden_1/bias/sac_value_opt_1save_1/RestoreV2:98*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_99Assign$critic/value/encoder/hidden_1/kernelsave_1/RestoreV2:99*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_100Assign2critic/value/encoder/hidden_1/kernel/sac_value_optsave_1/RestoreV2:100*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_101Assign4critic/value/encoder/hidden_1/kernel/sac_value_opt_1save_1/RestoreV2:101*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_102Assign"critic/value/encoder/hidden_2/biassave_1/RestoreV2:102*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_103Assign0critic/value/encoder/hidden_2/bias/sac_value_optsave_1/RestoreV2:103*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_104Assign2critic/value/encoder/hidden_2/bias/sac_value_opt_1save_1/RestoreV2:104*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_105Assign$critic/value/encoder/hidden_2/kernelsave_1/RestoreV2:105*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_106Assign2critic/value/encoder/hidden_2/kernel/sac_value_optsave_1/RestoreV2:106*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_107Assign4critic/value/encoder/hidden_2/kernel/sac_value_opt_1save_1/RestoreV2:107*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_108Assign"critic/value/encoder/hidden_3/biassave_1/RestoreV2:108*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_109Assign0critic/value/encoder/hidden_3/bias/sac_value_optsave_1/RestoreV2:109*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_110Assign2critic/value/encoder/hidden_3/bias/sac_value_opt_1save_1/RestoreV2:110*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_111Assign$critic/value/encoder/hidden_3/kernelsave_1/RestoreV2:111*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_112Assign2critic/value/encoder/hidden_3/kernel/sac_value_optsave_1/RestoreV2:112*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_113Assign4critic/value/encoder/hidden_3/kernel/sac_value_opt_1save_1/RestoreV2:113*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_114Assign!critic/value/extrinsic_value/biassave_1/RestoreV2:114*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
save_1/Assign_115Assign/critic/value/extrinsic_value/bias/sac_value_optsave_1/RestoreV2:115*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
save_1/Assign_116Assign1critic/value/extrinsic_value/bias/sac_value_opt_1save_1/RestoreV2:116*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
save_1/Assign_117Assign#critic/value/extrinsic_value/kernelsave_1/RestoreV2:117*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_118Assign1critic/value/extrinsic_value/kernel/sac_value_optsave_1/RestoreV2:118*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_119Assign3critic/value/extrinsic_value/kernel/sac_value_opt_1save_1/RestoreV2:119*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_120Assign*curiosity_vector_obs_encoder/hidden_0/biassave_1/RestoreV2:120*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_121Assign/curiosity_vector_obs_encoder/hidden_0/bias/Adamsave_1/RestoreV2:121*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_122Assign1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1save_1/RestoreV2:122*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_123Assign,curiosity_vector_obs_encoder/hidden_0/kernelsave_1/RestoreV2:123*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_124Assign1curiosity_vector_obs_encoder/hidden_0/kernel/Adamsave_1/RestoreV2:124*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_125Assign3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1save_1/RestoreV2:125*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_126Assign*curiosity_vector_obs_encoder/hidden_1/biassave_1/RestoreV2:126*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_127Assign/curiosity_vector_obs_encoder/hidden_1/bias/Adamsave_1/RestoreV2:127*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_128Assign1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1save_1/RestoreV2:128*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_129Assign,curiosity_vector_obs_encoder/hidden_1/kernelsave_1/RestoreV2:129*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_130Assign1curiosity_vector_obs_encoder/hidden_1/kernel/Adamsave_1/RestoreV2:130*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_131Assign3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1save_1/RestoreV2:131*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_132Assign
dense/biassave_1/RestoreV2:132*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
�
save_1/Assign_133Assigndense/bias/Adamsave_1/RestoreV2:133*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
�
save_1/Assign_134Assigndense/bias/Adam_1save_1/RestoreV2:134*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
�
save_1/Assign_135Assigndense/kernelsave_1/RestoreV2:135*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_136Assigndense/kernel/Adamsave_1/RestoreV2:136*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_137Assigndense/kernel/Adam_1save_1/RestoreV2:137*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_138Assigndense_1/biassave_1/RestoreV2:138*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_139Assigndense_1/bias/Adamsave_1/RestoreV2:139*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_140Assigndense_1/bias/Adam_1save_1/RestoreV2:140*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_141Assigndense_1/kernelsave_1/RestoreV2:141*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_142Assigndense_1/kernel/Adamsave_1/RestoreV2:142*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_143Assigndense_1/kernel/Adam_1save_1/RestoreV2:143*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_144Assigndense_2/biassave_1/RestoreV2:144*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_145Assigndense_2/bias/Adamsave_1/RestoreV2:145*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_146Assigndense_2/bias/Adam_1save_1/RestoreV2:146*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_147Assigndense_2/kernelsave_1/RestoreV2:147*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_148Assigndense_2/kernel/Adamsave_1/RestoreV2:148*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_149Assigndense_2/kernel/Adam_1save_1/RestoreV2:149*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_150Assigndense_3/biassave_1/RestoreV2:150*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_151Assigndense_3/bias/Adamsave_1/RestoreV2:151*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_152Assigndense_3/bias/Adam_1save_1/RestoreV2:152*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_153Assigndense_3/kernelsave_1/RestoreV2:153*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_154Assigndense_3/kernel/Adamsave_1/RestoreV2:154*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_155Assigndense_3/kernel/Adam_1save_1/RestoreV2:155*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_156Assignglobal_stepsave_1/RestoreV2:156*
T0*
_class
loc:@global_step*
use_locking(*
validate_shape(
�
save_1/Assign_157Assignis_continuous_controlsave_1/RestoreV2:157*
T0*(
_class
loc:@is_continuous_control*
use_locking(*
validate_shape(
�
save_1/Assign_158Assignlog_ent_coefsave_1/RestoreV2:158*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
�
save_1/Assign_159Assignlog_ent_coef/sac_entropy_optsave_1/RestoreV2:159*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
�
save_1/Assign_160Assignlog_ent_coef/sac_entropy_opt_1save_1/RestoreV2:160*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
�
save_1/Assign_161Assignmemory_sizesave_1/RestoreV2:161*
T0*
_class
loc:@memory_size*
use_locking(*
validate_shape(
�
save_1/Assign_162Assignpolicy/dense/kernelsave_1/RestoreV2:162*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_163Assign"policy/dense/kernel/sac_policy_optsave_1/RestoreV2:163*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_164Assign$policy/dense/kernel/sac_policy_opt_1save_1/RestoreV2:164*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_165Assign!policy/main_graph_0/hidden_0/biassave_1/RestoreV2:165*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_166Assign0policy/main_graph_0/hidden_0/bias/sac_policy_optsave_1/RestoreV2:166*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_167Assign2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1save_1/RestoreV2:167*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_168Assign#policy/main_graph_0/hidden_0/kernelsave_1/RestoreV2:168*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_169Assign2policy/main_graph_0/hidden_0/kernel/sac_policy_optsave_1/RestoreV2:169*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_170Assign4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1save_1/RestoreV2:170*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_171Assign!policy/main_graph_0/hidden_1/biassave_1/RestoreV2:171*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_172Assign0policy/main_graph_0/hidden_1/bias/sac_policy_optsave_1/RestoreV2:172*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_173Assign2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1save_1/RestoreV2:173*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_174Assign#policy/main_graph_0/hidden_1/kernelsave_1/RestoreV2:174*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_175Assign2policy/main_graph_0/hidden_1/kernel/sac_policy_optsave_1/RestoreV2:175*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_176Assign4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1save_1/RestoreV2:176*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_177Assign!policy/main_graph_0/hidden_2/biassave_1/RestoreV2:177*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_178Assign0policy/main_graph_0/hidden_2/bias/sac_policy_optsave_1/RestoreV2:178*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_179Assign2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1save_1/RestoreV2:179*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_180Assign#policy/main_graph_0/hidden_2/kernelsave_1/RestoreV2:180*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_181Assign2policy/main_graph_0/hidden_2/kernel/sac_policy_optsave_1/RestoreV2:181*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_182Assign4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1save_1/RestoreV2:182*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_183Assign!policy/main_graph_0/hidden_3/biassave_1/RestoreV2:183*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_184Assign0policy/main_graph_0/hidden_3/bias/sac_policy_optsave_1/RestoreV2:184*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_185Assign2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1save_1/RestoreV2:185*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_186Assign#policy/main_graph_0/hidden_3/kernelsave_1/RestoreV2:186*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_187Assign2policy/main_graph_0/hidden_3/kernel/sac_policy_optsave_1/RestoreV2:187*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_188Assign4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1save_1/RestoreV2:188*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_189Assign0target_network/critic/value/curiosity_value/biassave_1/RestoreV2:189*
T0*C
_class9
75loc:@target_network/critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
�
save_1/Assign_190Assign2target_network/critic/value/curiosity_value/kernelsave_1/RestoreV2:190*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_191Assign1target_network/critic/value/encoder/hidden_0/biassave_1/RestoreV2:191*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
�
save_1/Assign_192Assign3target_network/critic/value/encoder/hidden_0/kernelsave_1/RestoreV2:192*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_193Assign1target_network/critic/value/encoder/hidden_1/biassave_1/RestoreV2:193*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
�
save_1/Assign_194Assign3target_network/critic/value/encoder/hidden_1/kernelsave_1/RestoreV2:194*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_195Assign1target_network/critic/value/encoder/hidden_2/biassave_1/RestoreV2:195*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
�
save_1/Assign_196Assign3target_network/critic/value/encoder/hidden_2/kernelsave_1/RestoreV2:196*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_197Assign1target_network/critic/value/encoder/hidden_3/biassave_1/RestoreV2:197*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
�
save_1/Assign_198Assign3target_network/critic/value/encoder/hidden_3/kernelsave_1/RestoreV2:198*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_199Assign0target_network/critic/value/extrinsic_value/biassave_1/RestoreV2:199*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
�
save_1/Assign_200Assign2target_network/critic/value/extrinsic_value/kernelsave_1/RestoreV2:200*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
�
save_1/Assign_201Assigntrainer_major_versionsave_1/RestoreV2:201*
T0*(
_class
loc:@trainer_major_version*
use_locking(*
validate_shape(
�
save_1/Assign_202Assigntrainer_minor_versionsave_1/RestoreV2:202*
T0*(
_class
loc:@trainer_minor_version*
use_locking(*
validate_shape(
�
save_1/Assign_203Assigntrainer_patch_versionsave_1/RestoreV2:203*
T0*(
_class
loc:@trainer_patch_version*
use_locking(*
validate_shape(
�
save_1/Assign_204Assignversion_numbersave_1/RestoreV2:204*
T0*!
_class
loc:@version_number*
use_locking(*
validate_shape(
�
save_1/restore_allNoOp^save_1/Assign^save_1/Assign_1^save_1/Assign_10^save_1/Assign_100^save_1/Assign_101^save_1/Assign_102^save_1/Assign_103^save_1/Assign_104^save_1/Assign_105^save_1/Assign_106^save_1/Assign_107^save_1/Assign_108^save_1/Assign_109^save_1/Assign_11^save_1/Assign_110^save_1/Assign_111^save_1/Assign_112^save_1/Assign_113^save_1/Assign_114^save_1/Assign_115^save_1/Assign_116^save_1/Assign_117^save_1/Assign_118^save_1/Assign_119^save_1/Assign_12^save_1/Assign_120^save_1/Assign_121^save_1/Assign_122^save_1/Assign_123^save_1/Assign_124^save_1/Assign_125^save_1/Assign_126^save_1/Assign_127^save_1/Assign_128^save_1/Assign_129^save_1/Assign_13^save_1/Assign_130^save_1/Assign_131^save_1/Assign_132^save_1/Assign_133^save_1/Assign_134^save_1/Assign_135^save_1/Assign_136^save_1/Assign_137^save_1/Assign_138^save_1/Assign_139^save_1/Assign_14^save_1/Assign_140^save_1/Assign_141^save_1/Assign_142^save_1/Assign_143^save_1/Assign_144^save_1/Assign_145^save_1/Assign_146^save_1/Assign_147^save_1/Assign_148^save_1/Assign_149^save_1/Assign_15^save_1/Assign_150^save_1/Assign_151^save_1/Assign_152^save_1/Assign_153^save_1/Assign_154^save_1/Assign_155^save_1/Assign_156^save_1/Assign_157^save_1/Assign_158^save_1/Assign_159^save_1/Assign_16^save_1/Assign_160^save_1/Assign_161^save_1/Assign_162^save_1/Assign_163^save_1/Assign_164^save_1/Assign_165^save_1/Assign_166^save_1/Assign_167^save_1/Assign_168^save_1/Assign_169^save_1/Assign_17^save_1/Assign_170^save_1/Assign_171^save_1/Assign_172^save_1/Assign_173^save_1/Assign_174^save_1/Assign_175^save_1/Assign_176^save_1/Assign_177^save_1/Assign_178^save_1/Assign_179^save_1/Assign_18^save_1/Assign_180^save_1/Assign_181^save_1/Assign_182^save_1/Assign_183^save_1/Assign_184^save_1/Assign_185^save_1/Assign_186^save_1/Assign_187^save_1/Assign_188^save_1/Assign_189^save_1/Assign_19^save_1/Assign_190^save_1/Assign_191^save_1/Assign_192^save_1/Assign_193^save_1/Assign_194^save_1/Assign_195^save_1/Assign_196^save_1/Assign_197^save_1/Assign_198^save_1/Assign_199^save_1/Assign_2^save_1/Assign_20^save_1/Assign_200^save_1/Assign_201^save_1/Assign_202^save_1/Assign_203^save_1/Assign_204^save_1/Assign_21^save_1/Assign_22^save_1/Assign_23^save_1/Assign_24^save_1/Assign_25^save_1/Assign_26^save_1/Assign_27^save_1/Assign_28^save_1/Assign_29^save_1/Assign_3^save_1/Assign_30^save_1/Assign_31^save_1/Assign_32^save_1/Assign_33^save_1/Assign_34^save_1/Assign_35^save_1/Assign_36^save_1/Assign_37^save_1/Assign_38^save_1/Assign_39^save_1/Assign_4^save_1/Assign_40^save_1/Assign_41^save_1/Assign_42^save_1/Assign_43^save_1/Assign_44^save_1/Assign_45^save_1/Assign_46^save_1/Assign_47^save_1/Assign_48^save_1/Assign_49^save_1/Assign_5^save_1/Assign_50^save_1/Assign_51^save_1/Assign_52^save_1/Assign_53^save_1/Assign_54^save_1/Assign_55^save_1/Assign_56^save_1/Assign_57^save_1/Assign_58^save_1/Assign_59^save_1/Assign_6^save_1/Assign_60^save_1/Assign_61^save_1/Assign_62^save_1/Assign_63^save_1/Assign_64^save_1/Assign_65^save_1/Assign_66^save_1/Assign_67^save_1/Assign_68^save_1/Assign_69^save_1/Assign_7^save_1/Assign_70^save_1/Assign_71^save_1/Assign_72^save_1/Assign_73^save_1/Assign_74^save_1/Assign_75^save_1/Assign_76^save_1/Assign_77^save_1/Assign_78^save_1/Assign_79^save_1/Assign_8^save_1/Assign_80^save_1/Assign_81^save_1/Assign_82^save_1/Assign_83^save_1/Assign_84^save_1/Assign_85^save_1/Assign_86^save_1/Assign_87^save_1/Assign_88^save_1/Assign_89^save_1/Assign_9^save_1/Assign_90^save_1/Assign_91^save_1/Assign_92^save_1/Assign_93^save_1/Assign_94^save_1/Assign_95^save_1/Assign_96^save_1/Assign_97^save_1/Assign_98^save_1/Assign_99
4
PlaceholderPlaceholder*
dtype0*
shape: 

	Assign_27Assignglobal_stepPlaceholder*
T0*
_class
loc:@global_step*
use_locking(*
validate_shape(
6
Placeholder_1Placeholder*
dtype0*
shape: 
�
	Assign_28Assignis_continuous_controlPlaceholder_1*
T0*(
_class
loc:@is_continuous_control*
use_locking(*
validate_shape(
6
Placeholder_2Placeholder*
dtype0*
shape: 
�
	Assign_29Assigntrainer_major_versionPlaceholder_2*
T0*(
_class
loc:@trainer_major_version*
use_locking(*
validate_shape(
6
Placeholder_3Placeholder*
dtype0*
shape: 
�
	Assign_30Assigntrainer_minor_versionPlaceholder_3*
T0*(
_class
loc:@trainer_minor_version*
use_locking(*
validate_shape(
6
Placeholder_4Placeholder*
dtype0*
shape: 
�
	Assign_31Assigntrainer_patch_versionPlaceholder_4*
T0*(
_class
loc:@trainer_patch_version*
use_locking(*
validate_shape(
6
Placeholder_5Placeholder*
dtype0*
shape: 
�
	Assign_32Assignversion_numberPlaceholder_5*
T0*!
_class
loc:@version_number*
use_locking(*
validate_shape(
6
Placeholder_6Placeholder*
dtype0*
shape: 
�
	Assign_33Assignmemory_sizePlaceholder_6*
T0*
_class
loc:@memory_size*
use_locking(*
validate_shape(
6
Placeholder_7Placeholder*
dtype0*
shape: 
�
	Assign_34Assignaction_output_shapePlaceholder_7*
T0*&
_class
loc:@action_output_shape*
use_locking(*
validate_shape(
>
Placeholder_8Placeholder*
dtype0*
shape
:@@
�
	Assign_35Assign#policy/main_graph_0/hidden_0/kernelPlaceholder_8*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
:
Placeholder_9Placeholder*
dtype0*
shape:@
�
	Assign_36Assign!policy/main_graph_0/hidden_0/biasPlaceholder_9*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
?
Placeholder_10Placeholder*
dtype0*
shape
:@@
�
	Assign_37Assign#policy/main_graph_0/hidden_1/kernelPlaceholder_10*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
;
Placeholder_11Placeholder*
dtype0*
shape:@
�
	Assign_38Assign!policy/main_graph_0/hidden_1/biasPlaceholder_11*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
?
Placeholder_12Placeholder*
dtype0*
shape
:@@
�
	Assign_39Assign#policy/main_graph_0/hidden_2/kernelPlaceholder_12*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
;
Placeholder_13Placeholder*
dtype0*
shape:@
�
	Assign_40Assign!policy/main_graph_0/hidden_2/biasPlaceholder_13*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
?
Placeholder_14Placeholder*
dtype0*
shape
:@@
�
	Assign_41Assign#policy/main_graph_0/hidden_3/kernelPlaceholder_14*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
;
Placeholder_15Placeholder*
dtype0*
shape:@
�
	Assign_42Assign!policy/main_graph_0/hidden_3/biasPlaceholder_15*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
?
Placeholder_16Placeholder*
dtype0*
shape
:@@
�
	Assign_43Assignpolicy/dense/kernelPlaceholder_16*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
@
Placeholder_17Placeholder*
dtype0*
shape:	@�
�
	Assign_44Assign,curiosity_vector_obs_encoder/hidden_0/kernelPlaceholder_17*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
<
Placeholder_18Placeholder*
dtype0*
shape:�
�
	Assign_45Assign*curiosity_vector_obs_encoder/hidden_0/biasPlaceholder_18*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
A
Placeholder_19Placeholder*
dtype0*
shape:
��
�
	Assign_46Assign,curiosity_vector_obs_encoder/hidden_1/kernelPlaceholder_19*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
<
Placeholder_20Placeholder*
dtype0*
shape:�
�
	Assign_47Assign*curiosity_vector_obs_encoder/hidden_1/biasPlaceholder_20*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
A
Placeholder_21Placeholder*
dtype0*
shape:
��
�
	Assign_48Assigndense/kernelPlaceholder_21*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
<
Placeholder_22Placeholder*
dtype0*
shape:�
�
	Assign_49Assign
dense/biasPlaceholder_22*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
@
Placeholder_23Placeholder*
dtype0*
shape:	�@
�
	Assign_50Assigndense_1/kernelPlaceholder_23*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
;
Placeholder_24Placeholder*
dtype0*
shape:@
�
	Assign_51Assigndense_1/biasPlaceholder_24*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
A
Placeholder_25Placeholder*
dtype0*
shape:
��
�
	Assign_52Assigndense_2/kernelPlaceholder_25*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
<
Placeholder_26Placeholder*
dtype0*
shape:�
�
	Assign_53Assigndense_2/biasPlaceholder_26*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
A
Placeholder_27Placeholder*
dtype0*
shape:
��
�
	Assign_54Assigndense_3/kernelPlaceholder_27*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
<
Placeholder_28Placeholder*
dtype0*
shape:�
�
	Assign_55Assigndense_3/biasPlaceholder_28*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
7
Placeholder_29Placeholder*
dtype0*
shape: 
�
	Assign_56Assignbeta1_powerPlaceholder_29*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
7
Placeholder_30Placeholder*
dtype0*
shape: 
�
	Assign_57Assignbeta2_powerPlaceholder_30*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
@
Placeholder_31Placeholder*
dtype0*
shape:	@�
�
	Assign_58Assign1curiosity_vector_obs_encoder/hidden_0/kernel/AdamPlaceholder_31*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
@
Placeholder_32Placeholder*
dtype0*
shape:	@�
�
	Assign_59Assign3curiosity_vector_obs_encoder/hidden_0/kernel/Adam_1Placeholder_32*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
<
Placeholder_33Placeholder*
dtype0*
shape:�
�
	Assign_60Assign/curiosity_vector_obs_encoder/hidden_0/bias/AdamPlaceholder_33*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
<
Placeholder_34Placeholder*
dtype0*
shape:�
�
	Assign_61Assign1curiosity_vector_obs_encoder/hidden_0/bias/Adam_1Placeholder_34*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_0/bias*
use_locking(*
validate_shape(
A
Placeholder_35Placeholder*
dtype0*
shape:
��
�
	Assign_62Assign1curiosity_vector_obs_encoder/hidden_1/kernel/AdamPlaceholder_35*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
A
Placeholder_36Placeholder*
dtype0*
shape:
��
�
	Assign_63Assign3curiosity_vector_obs_encoder/hidden_1/kernel/Adam_1Placeholder_36*
T0*?
_class5
31loc:@curiosity_vector_obs_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
<
Placeholder_37Placeholder*
dtype0*
shape:�
�
	Assign_64Assign/curiosity_vector_obs_encoder/hidden_1/bias/AdamPlaceholder_37*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
<
Placeholder_38Placeholder*
dtype0*
shape:�
�
	Assign_65Assign1curiosity_vector_obs_encoder/hidden_1/bias/Adam_1Placeholder_38*
T0*=
_class3
1/loc:@curiosity_vector_obs_encoder/hidden_1/bias*
use_locking(*
validate_shape(
A
Placeholder_39Placeholder*
dtype0*
shape:
��
�
	Assign_66Assigndense/kernel/AdamPlaceholder_39*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
A
Placeholder_40Placeholder*
dtype0*
shape:
��
�
	Assign_67Assigndense/kernel/Adam_1Placeholder_40*
T0*
_class
loc:@dense/kernel*
use_locking(*
validate_shape(
<
Placeholder_41Placeholder*
dtype0*
shape:�
�
	Assign_68Assigndense/bias/AdamPlaceholder_41*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
<
Placeholder_42Placeholder*
dtype0*
shape:�
�
	Assign_69Assigndense/bias/Adam_1Placeholder_42*
T0*
_class
loc:@dense/bias*
use_locking(*
validate_shape(
@
Placeholder_43Placeholder*
dtype0*
shape:	�@
�
	Assign_70Assigndense_1/kernel/AdamPlaceholder_43*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
@
Placeholder_44Placeholder*
dtype0*
shape:	�@
�
	Assign_71Assigndense_1/kernel/Adam_1Placeholder_44*
T0*!
_class
loc:@dense_1/kernel*
use_locking(*
validate_shape(
;
Placeholder_45Placeholder*
dtype0*
shape:@
�
	Assign_72Assigndense_1/bias/AdamPlaceholder_45*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
;
Placeholder_46Placeholder*
dtype0*
shape:@
�
	Assign_73Assigndense_1/bias/Adam_1Placeholder_46*
T0*
_class
loc:@dense_1/bias*
use_locking(*
validate_shape(
A
Placeholder_47Placeholder*
dtype0*
shape:
��
�
	Assign_74Assigndense_2/kernel/AdamPlaceholder_47*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
A
Placeholder_48Placeholder*
dtype0*
shape:
��
�
	Assign_75Assigndense_2/kernel/Adam_1Placeholder_48*
T0*!
_class
loc:@dense_2/kernel*
use_locking(*
validate_shape(
<
Placeholder_49Placeholder*
dtype0*
shape:�
�
	Assign_76Assigndense_2/bias/AdamPlaceholder_49*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
<
Placeholder_50Placeholder*
dtype0*
shape:�
�
	Assign_77Assigndense_2/bias/Adam_1Placeholder_50*
T0*
_class
loc:@dense_2/bias*
use_locking(*
validate_shape(
A
Placeholder_51Placeholder*
dtype0*
shape:
��
�
	Assign_78Assigndense_3/kernel/AdamPlaceholder_51*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
A
Placeholder_52Placeholder*
dtype0*
shape:
��
�
	Assign_79Assigndense_3/kernel/Adam_1Placeholder_52*
T0*!
_class
loc:@dense_3/kernel*
use_locking(*
validate_shape(
<
Placeholder_53Placeholder*
dtype0*
shape:�
�
	Assign_80Assigndense_3/bias/AdamPlaceholder_53*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
<
Placeholder_54Placeholder*
dtype0*
shape:�
�
	Assign_81Assigndense_3/bias/Adam_1Placeholder_54*
T0*
_class
loc:@dense_3/bias*
use_locking(*
validate_shape(
7
Placeholder_55Placeholder*
dtype0*
shape: 
|
	Assign_82AssignVariablePlaceholder_55*
T0*
_class
loc:@Variable*
use_locking(*
validate_shape(
7
Placeholder_56Placeholder*
dtype0*
shape: 
�
	Assign_83Assign
Variable_1Placeholder_56*
T0*
_class
loc:@Variable_1*
use_locking(*
validate_shape(
?
Placeholder_57Placeholder*
dtype0*
shape
:@@
�
	Assign_84Assign$critic/value/encoder/hidden_0/kernelPlaceholder_57*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
;
Placeholder_58Placeholder*
dtype0*
shape:@
�
	Assign_85Assign"critic/value/encoder/hidden_0/biasPlaceholder_58*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
?
Placeholder_59Placeholder*
dtype0*
shape
:@@
�
	Assign_86Assign$critic/value/encoder/hidden_1/kernelPlaceholder_59*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
;
Placeholder_60Placeholder*
dtype0*
shape:@
�
	Assign_87Assign"critic/value/encoder/hidden_1/biasPlaceholder_60*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
?
Placeholder_61Placeholder*
dtype0*
shape
:@@
�
	Assign_88Assign$critic/value/encoder/hidden_2/kernelPlaceholder_61*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
;
Placeholder_62Placeholder*
dtype0*
shape:@
�
	Assign_89Assign"critic/value/encoder/hidden_2/biasPlaceholder_62*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
?
Placeholder_63Placeholder*
dtype0*
shape
:@@
�
	Assign_90Assign$critic/value/encoder/hidden_3/kernelPlaceholder_63*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
;
Placeholder_64Placeholder*
dtype0*
shape:@
�
	Assign_91Assign"critic/value/encoder/hidden_3/biasPlaceholder_64*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
?
Placeholder_65Placeholder*
dtype0*
shape
:@
�
	Assign_92Assign#critic/value/extrinsic_value/kernelPlaceholder_65*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
;
Placeholder_66Placeholder*
dtype0*
shape:
�
	Assign_93Assign!critic/value/extrinsic_value/biasPlaceholder_66*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
?
Placeholder_67Placeholder*
dtype0*
shape
:@
�
	Assign_94Assign#critic/value/curiosity_value/kernelPlaceholder_67*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
;
Placeholder_68Placeholder*
dtype0*
shape:
�
	Assign_95Assign!critic/value/curiosity_value/biasPlaceholder_68*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
?
Placeholder_69Placeholder*
dtype0*
shape
:@@
�
	Assign_96Assign/critic/q/q1_encoding/q1_encoder/hidden_0/kernelPlaceholder_69*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
;
Placeholder_70Placeholder*
dtype0*
shape:@
�
	Assign_97Assign-critic/q/q1_encoding/q1_encoder/hidden_0/biasPlaceholder_70*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
?
Placeholder_71Placeholder*
dtype0*
shape
:@@
�
	Assign_98Assign/critic/q/q1_encoding/q1_encoder/hidden_1/kernelPlaceholder_71*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
;
Placeholder_72Placeholder*
dtype0*
shape:@
�
	Assign_99Assign-critic/q/q1_encoding/q1_encoder/hidden_1/biasPlaceholder_72*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
?
Placeholder_73Placeholder*
dtype0*
shape
:@@
�

Assign_100Assign/critic/q/q1_encoding/q1_encoder/hidden_2/kernelPlaceholder_73*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
;
Placeholder_74Placeholder*
dtype0*
shape:@
�

Assign_101Assign-critic/q/q1_encoding/q1_encoder/hidden_2/biasPlaceholder_74*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
?
Placeholder_75Placeholder*
dtype0*
shape
:@@
�

Assign_102Assign/critic/q/q1_encoding/q1_encoder/hidden_3/kernelPlaceholder_75*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
;
Placeholder_76Placeholder*
dtype0*
shape:@
�

Assign_103Assign-critic/q/q1_encoding/q1_encoder/hidden_3/biasPlaceholder_76*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
?
Placeholder_77Placeholder*
dtype0*
shape
:@@
�

Assign_104Assign(critic/q/q1_encoding/extrinsic_q1/kernelPlaceholder_77*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
;
Placeholder_78Placeholder*
dtype0*
shape:@
�

Assign_105Assign&critic/q/q1_encoding/extrinsic_q1/biasPlaceholder_78*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
?
Placeholder_79Placeholder*
dtype0*
shape
:@@
�

Assign_106Assign(critic/q/q1_encoding/curiosity_q1/kernelPlaceholder_79*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
;
Placeholder_80Placeholder*
dtype0*
shape:@
�

Assign_107Assign&critic/q/q1_encoding/curiosity_q1/biasPlaceholder_80*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
?
Placeholder_81Placeholder*
dtype0*
shape
:@@
�

Assign_108Assign/critic/q/q2_encoding/q2_encoder/hidden_0/kernelPlaceholder_81*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
;
Placeholder_82Placeholder*
dtype0*
shape:@
�

Assign_109Assign-critic/q/q2_encoding/q2_encoder/hidden_0/biasPlaceholder_82*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
?
Placeholder_83Placeholder*
dtype0*
shape
:@@
�

Assign_110Assign/critic/q/q2_encoding/q2_encoder/hidden_1/kernelPlaceholder_83*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
;
Placeholder_84Placeholder*
dtype0*
shape:@
�

Assign_111Assign-critic/q/q2_encoding/q2_encoder/hidden_1/biasPlaceholder_84*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
?
Placeholder_85Placeholder*
dtype0*
shape
:@@
�

Assign_112Assign/critic/q/q2_encoding/q2_encoder/hidden_2/kernelPlaceholder_85*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
;
Placeholder_86Placeholder*
dtype0*
shape:@
�

Assign_113Assign-critic/q/q2_encoding/q2_encoder/hidden_2/biasPlaceholder_86*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
?
Placeholder_87Placeholder*
dtype0*
shape
:@@
�

Assign_114Assign/critic/q/q2_encoding/q2_encoder/hidden_3/kernelPlaceholder_87*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
;
Placeholder_88Placeholder*
dtype0*
shape:@
�

Assign_115Assign-critic/q/q2_encoding/q2_encoder/hidden_3/biasPlaceholder_88*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
?
Placeholder_89Placeholder*
dtype0*
shape
:@@
�

Assign_116Assign(critic/q/q2_encoding/extrinsic_q2/kernelPlaceholder_89*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
;
Placeholder_90Placeholder*
dtype0*
shape:@
�

Assign_117Assign&critic/q/q2_encoding/extrinsic_q2/biasPlaceholder_90*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
?
Placeholder_91Placeholder*
dtype0*
shape
:@@
�

Assign_118Assign(critic/q/q2_encoding/curiosity_q2/kernelPlaceholder_91*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
;
Placeholder_92Placeholder*
dtype0*
shape:@
�

Assign_119Assign&critic/q/q2_encoding/curiosity_q2/biasPlaceholder_92*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
?
Placeholder_93Placeholder*
dtype0*
shape
:@@
�

Assign_120Assign3target_network/critic/value/encoder/hidden_0/kernelPlaceholder_93*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
;
Placeholder_94Placeholder*
dtype0*
shape:@
�

Assign_121Assign1target_network/critic/value/encoder/hidden_0/biasPlaceholder_94*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
?
Placeholder_95Placeholder*
dtype0*
shape
:@@
�

Assign_122Assign3target_network/critic/value/encoder/hidden_1/kernelPlaceholder_95*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
;
Placeholder_96Placeholder*
dtype0*
shape:@
�

Assign_123Assign1target_network/critic/value/encoder/hidden_1/biasPlaceholder_96*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
?
Placeholder_97Placeholder*
dtype0*
shape
:@@
�

Assign_124Assign3target_network/critic/value/encoder/hidden_2/kernelPlaceholder_97*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
;
Placeholder_98Placeholder*
dtype0*
shape:@
�

Assign_125Assign1target_network/critic/value/encoder/hidden_2/biasPlaceholder_98*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
?
Placeholder_99Placeholder*
dtype0*
shape
:@@
�

Assign_126Assign3target_network/critic/value/encoder/hidden_3/kernelPlaceholder_99*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
<
Placeholder_100Placeholder*
dtype0*
shape:@
�

Assign_127Assign1target_network/critic/value/encoder/hidden_3/biasPlaceholder_100*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
@
Placeholder_101Placeholder*
dtype0*
shape
:@
�

Assign_128Assign2target_network/critic/value/extrinsic_value/kernelPlaceholder_101*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
<
Placeholder_102Placeholder*
dtype0*
shape:
�

Assign_129Assign0target_network/critic/value/extrinsic_value/biasPlaceholder_102*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
@
Placeholder_103Placeholder*
dtype0*
shape
:@
�

Assign_130Assign2target_network/critic/value/curiosity_value/kernelPlaceholder_103*
T0*E
_class;
97loc:@target_network/critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
<
Placeholder_104Placeholder*
dtype0*
shape:
�

Assign_131Assign0target_network/critic/value/curiosity_value/biasPlaceholder_104*
T0*C
_class9
75loc:@target_network/critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
8
Placeholder_105Placeholder*
dtype0*
shape: 
�

Assign_132Assign
Variable_2Placeholder_105*
T0*
_class
loc:@Variable_2*
use_locking(*
validate_shape(
<
Placeholder_106Placeholder*
dtype0*
shape:
�

Assign_133Assignlog_ent_coefPlaceholder_106*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
8
Placeholder_107Placeholder*
dtype0*
shape: 
�

Assign_134Assignbeta1_power_1Placeholder_107*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
8
Placeholder_108Placeholder*
dtype0*
shape: 
�

Assign_135Assignbeta2_power_1Placeholder_108*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
@
Placeholder_109Placeholder*
dtype0*
shape
:@@
�

Assign_136Assign2policy/main_graph_0/hidden_0/kernel/sac_policy_optPlaceholder_109*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
@
Placeholder_110Placeholder*
dtype0*
shape
:@@
�

Assign_137Assign4policy/main_graph_0/hidden_0/kernel/sac_policy_opt_1Placeholder_110*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_0/kernel*
use_locking(*
validate_shape(
<
Placeholder_111Placeholder*
dtype0*
shape:@
�

Assign_138Assign0policy/main_graph_0/hidden_0/bias/sac_policy_optPlaceholder_111*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
<
Placeholder_112Placeholder*
dtype0*
shape:@
�

Assign_139Assign2policy/main_graph_0/hidden_0/bias/sac_policy_opt_1Placeholder_112*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_0/bias*
use_locking(*
validate_shape(
@
Placeholder_113Placeholder*
dtype0*
shape
:@@
�

Assign_140Assign2policy/main_graph_0/hidden_1/kernel/sac_policy_optPlaceholder_113*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
@
Placeholder_114Placeholder*
dtype0*
shape
:@@
�

Assign_141Assign4policy/main_graph_0/hidden_1/kernel/sac_policy_opt_1Placeholder_114*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_1/kernel*
use_locking(*
validate_shape(
<
Placeholder_115Placeholder*
dtype0*
shape:@
�

Assign_142Assign0policy/main_graph_0/hidden_1/bias/sac_policy_optPlaceholder_115*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
<
Placeholder_116Placeholder*
dtype0*
shape:@
�

Assign_143Assign2policy/main_graph_0/hidden_1/bias/sac_policy_opt_1Placeholder_116*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_1/bias*
use_locking(*
validate_shape(
@
Placeholder_117Placeholder*
dtype0*
shape
:@@
�

Assign_144Assign2policy/main_graph_0/hidden_2/kernel/sac_policy_optPlaceholder_117*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
@
Placeholder_118Placeholder*
dtype0*
shape
:@@
�

Assign_145Assign4policy/main_graph_0/hidden_2/kernel/sac_policy_opt_1Placeholder_118*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_2/kernel*
use_locking(*
validate_shape(
<
Placeholder_119Placeholder*
dtype0*
shape:@
�

Assign_146Assign0policy/main_graph_0/hidden_2/bias/sac_policy_optPlaceholder_119*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
<
Placeholder_120Placeholder*
dtype0*
shape:@
�

Assign_147Assign2policy/main_graph_0/hidden_2/bias/sac_policy_opt_1Placeholder_120*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_2/bias*
use_locking(*
validate_shape(
@
Placeholder_121Placeholder*
dtype0*
shape
:@@
�

Assign_148Assign2policy/main_graph_0/hidden_3/kernel/sac_policy_optPlaceholder_121*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
@
Placeholder_122Placeholder*
dtype0*
shape
:@@
�

Assign_149Assign4policy/main_graph_0/hidden_3/kernel/sac_policy_opt_1Placeholder_122*
T0*6
_class,
*(loc:@policy/main_graph_0/hidden_3/kernel*
use_locking(*
validate_shape(
<
Placeholder_123Placeholder*
dtype0*
shape:@
�

Assign_150Assign0policy/main_graph_0/hidden_3/bias/sac_policy_optPlaceholder_123*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
<
Placeholder_124Placeholder*
dtype0*
shape:@
�

Assign_151Assign2policy/main_graph_0/hidden_3/bias/sac_policy_opt_1Placeholder_124*
T0*4
_class*
(&loc:@policy/main_graph_0/hidden_3/bias*
use_locking(*
validate_shape(
@
Placeholder_125Placeholder*
dtype0*
shape
:@@
�

Assign_152Assign"policy/dense/kernel/sac_policy_optPlaceholder_125*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
@
Placeholder_126Placeholder*
dtype0*
shape
:@@
�

Assign_153Assign$policy/dense/kernel/sac_policy_opt_1Placeholder_126*
T0*&
_class
loc:@policy/dense/kernel*
use_locking(*
validate_shape(
8
Placeholder_127Placeholder*
dtype0*
shape: 
�

Assign_154Assignbeta1_power_2Placeholder_127*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
8
Placeholder_128Placeholder*
dtype0*
shape: 
�

Assign_155Assignbeta2_power_2Placeholder_128*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
@
Placeholder_129Placeholder*
dtype0*
shape
:@@
�

Assign_156Assign2critic/value/encoder/hidden_0/kernel/sac_value_optPlaceholder_129*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
@
Placeholder_130Placeholder*
dtype0*
shape
:@@
�

Assign_157Assign4critic/value/encoder/hidden_0/kernel/sac_value_opt_1Placeholder_130*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_locking(*
validate_shape(
<
Placeholder_131Placeholder*
dtype0*
shape:@
�

Assign_158Assign0critic/value/encoder/hidden_0/bias/sac_value_optPlaceholder_131*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
<
Placeholder_132Placeholder*
dtype0*
shape:@
�

Assign_159Assign2critic/value/encoder/hidden_0/bias/sac_value_opt_1Placeholder_132*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_locking(*
validate_shape(
@
Placeholder_133Placeholder*
dtype0*
shape
:@@
�

Assign_160Assign2critic/value/encoder/hidden_1/kernel/sac_value_optPlaceholder_133*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
@
Placeholder_134Placeholder*
dtype0*
shape
:@@
�

Assign_161Assign4critic/value/encoder/hidden_1/kernel/sac_value_opt_1Placeholder_134*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_locking(*
validate_shape(
<
Placeholder_135Placeholder*
dtype0*
shape:@
�

Assign_162Assign0critic/value/encoder/hidden_1/bias/sac_value_optPlaceholder_135*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
<
Placeholder_136Placeholder*
dtype0*
shape:@
�

Assign_163Assign2critic/value/encoder/hidden_1/bias/sac_value_opt_1Placeholder_136*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_locking(*
validate_shape(
@
Placeholder_137Placeholder*
dtype0*
shape
:@@
�

Assign_164Assign2critic/value/encoder/hidden_2/kernel/sac_value_optPlaceholder_137*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
@
Placeholder_138Placeholder*
dtype0*
shape
:@@
�

Assign_165Assign4critic/value/encoder/hidden_2/kernel/sac_value_opt_1Placeholder_138*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_2/kernel*
use_locking(*
validate_shape(
<
Placeholder_139Placeholder*
dtype0*
shape:@
�

Assign_166Assign0critic/value/encoder/hidden_2/bias/sac_value_optPlaceholder_139*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
<
Placeholder_140Placeholder*
dtype0*
shape:@
�

Assign_167Assign2critic/value/encoder/hidden_2/bias/sac_value_opt_1Placeholder_140*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_2/bias*
use_locking(*
validate_shape(
@
Placeholder_141Placeholder*
dtype0*
shape
:@@
�

Assign_168Assign2critic/value/encoder/hidden_3/kernel/sac_value_optPlaceholder_141*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
@
Placeholder_142Placeholder*
dtype0*
shape
:@@
�

Assign_169Assign4critic/value/encoder/hidden_3/kernel/sac_value_opt_1Placeholder_142*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_3/kernel*
use_locking(*
validate_shape(
<
Placeholder_143Placeholder*
dtype0*
shape:@
�

Assign_170Assign0critic/value/encoder/hidden_3/bias/sac_value_optPlaceholder_143*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
<
Placeholder_144Placeholder*
dtype0*
shape:@
�

Assign_171Assign2critic/value/encoder/hidden_3/bias/sac_value_opt_1Placeholder_144*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_3/bias*
use_locking(*
validate_shape(
@
Placeholder_145Placeholder*
dtype0*
shape
:@
�

Assign_172Assign1critic/value/extrinsic_value/kernel/sac_value_optPlaceholder_145*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
@
Placeholder_146Placeholder*
dtype0*
shape
:@
�

Assign_173Assign3critic/value/extrinsic_value/kernel/sac_value_opt_1Placeholder_146*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_locking(*
validate_shape(
<
Placeholder_147Placeholder*
dtype0*
shape:
�

Assign_174Assign/critic/value/extrinsic_value/bias/sac_value_optPlaceholder_147*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
<
Placeholder_148Placeholder*
dtype0*
shape:
�

Assign_175Assign1critic/value/extrinsic_value/bias/sac_value_opt_1Placeholder_148*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_locking(*
validate_shape(
@
Placeholder_149Placeholder*
dtype0*
shape
:@
�

Assign_176Assign1critic/value/curiosity_value/kernel/sac_value_optPlaceholder_149*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
@
Placeholder_150Placeholder*
dtype0*
shape
:@
�

Assign_177Assign3critic/value/curiosity_value/kernel/sac_value_opt_1Placeholder_150*
T0*6
_class,
*(loc:@critic/value/curiosity_value/kernel*
use_locking(*
validate_shape(
<
Placeholder_151Placeholder*
dtype0*
shape:
�

Assign_178Assign/critic/value/curiosity_value/bias/sac_value_optPlaceholder_151*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
<
Placeholder_152Placeholder*
dtype0*
shape:
�

Assign_179Assign1critic/value/curiosity_value/bias/sac_value_opt_1Placeholder_152*
T0*4
_class*
(&loc:@critic/value/curiosity_value/bias*
use_locking(*
validate_shape(
@
Placeholder_153Placeholder*
dtype0*
shape
:@@
�

Assign_180Assign=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_optPlaceholder_153*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
@
Placeholder_154Placeholder*
dtype0*
shape
:@@
�

Assign_181Assign?critic/q/q1_encoding/q1_encoder/hidden_0/kernel/sac_value_opt_1Placeholder_154*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
<
Placeholder_155Placeholder*
dtype0*
shape:@
�

Assign_182Assign;critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_optPlaceholder_155*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
<
Placeholder_156Placeholder*
dtype0*
shape:@
�

Assign_183Assign=critic/q/q1_encoding/q1_encoder/hidden_0/bias/sac_value_opt_1Placeholder_156*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_locking(*
validate_shape(
@
Placeholder_157Placeholder*
dtype0*
shape
:@@
�

Assign_184Assign=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_optPlaceholder_157*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
@
Placeholder_158Placeholder*
dtype0*
shape
:@@
�

Assign_185Assign?critic/q/q1_encoding/q1_encoder/hidden_1/kernel/sac_value_opt_1Placeholder_158*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
<
Placeholder_159Placeholder*
dtype0*
shape:@
�

Assign_186Assign;critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_optPlaceholder_159*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
<
Placeholder_160Placeholder*
dtype0*
shape:@
�

Assign_187Assign=critic/q/q1_encoding/q1_encoder/hidden_1/bias/sac_value_opt_1Placeholder_160*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_locking(*
validate_shape(
@
Placeholder_161Placeholder*
dtype0*
shape
:@@
�

Assign_188Assign=critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_optPlaceholder_161*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
@
Placeholder_162Placeholder*
dtype0*
shape
:@@
�

Assign_189Assign?critic/q/q1_encoding/q1_encoder/hidden_2/kernel/sac_value_opt_1Placeholder_162*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
<
Placeholder_163Placeholder*
dtype0*
shape:@
�

Assign_190Assign;critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_optPlaceholder_163*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
<
Placeholder_164Placeholder*
dtype0*
shape:@
�

Assign_191Assign=critic/q/q1_encoding/q1_encoder/hidden_2/bias/sac_value_opt_1Placeholder_164*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_2/bias*
use_locking(*
validate_shape(
@
Placeholder_165Placeholder*
dtype0*
shape
:@@
�

Assign_192Assign=critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_optPlaceholder_165*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
@
Placeholder_166Placeholder*
dtype0*
shape
:@@
�

Assign_193Assign?critic/q/q1_encoding/q1_encoder/hidden_3/kernel/sac_value_opt_1Placeholder_166*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
<
Placeholder_167Placeholder*
dtype0*
shape:@
�

Assign_194Assign;critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_optPlaceholder_167*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
<
Placeholder_168Placeholder*
dtype0*
shape:@
�

Assign_195Assign=critic/q/q1_encoding/q1_encoder/hidden_3/bias/sac_value_opt_1Placeholder_168*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_3/bias*
use_locking(*
validate_shape(
@
Placeholder_169Placeholder*
dtype0*
shape
:@@
�

Assign_196Assign6critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_optPlaceholder_169*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
@
Placeholder_170Placeholder*
dtype0*
shape
:@@
�

Assign_197Assign8critic/q/q1_encoding/extrinsic_q1/kernel/sac_value_opt_1Placeholder_170*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_locking(*
validate_shape(
<
Placeholder_171Placeholder*
dtype0*
shape:@
�

Assign_198Assign4critic/q/q1_encoding/extrinsic_q1/bias/sac_value_optPlaceholder_171*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
<
Placeholder_172Placeholder*
dtype0*
shape:@
�

Assign_199Assign6critic/q/q1_encoding/extrinsic_q1/bias/sac_value_opt_1Placeholder_172*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_locking(*
validate_shape(
@
Placeholder_173Placeholder*
dtype0*
shape
:@@
�

Assign_200Assign6critic/q/q1_encoding/curiosity_q1/kernel/sac_value_optPlaceholder_173*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
@
Placeholder_174Placeholder*
dtype0*
shape
:@@
�

Assign_201Assign8critic/q/q1_encoding/curiosity_q1/kernel/sac_value_opt_1Placeholder_174*
T0*;
_class1
/-loc:@critic/q/q1_encoding/curiosity_q1/kernel*
use_locking(*
validate_shape(
<
Placeholder_175Placeholder*
dtype0*
shape:@
�

Assign_202Assign4critic/q/q1_encoding/curiosity_q1/bias/sac_value_optPlaceholder_175*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
<
Placeholder_176Placeholder*
dtype0*
shape:@
�

Assign_203Assign6critic/q/q1_encoding/curiosity_q1/bias/sac_value_opt_1Placeholder_176*
T0*9
_class/
-+loc:@critic/q/q1_encoding/curiosity_q1/bias*
use_locking(*
validate_shape(
@
Placeholder_177Placeholder*
dtype0*
shape
:@@
�

Assign_204Assign=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_optPlaceholder_177*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
@
Placeholder_178Placeholder*
dtype0*
shape
:@@
�

Assign_205Assign?critic/q/q2_encoding/q2_encoder/hidden_0/kernel/sac_value_opt_1Placeholder_178*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_locking(*
validate_shape(
<
Placeholder_179Placeholder*
dtype0*
shape:@
�

Assign_206Assign;critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_optPlaceholder_179*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
<
Placeholder_180Placeholder*
dtype0*
shape:@
�

Assign_207Assign=critic/q/q2_encoding/q2_encoder/hidden_0/bias/sac_value_opt_1Placeholder_180*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_locking(*
validate_shape(
@
Placeholder_181Placeholder*
dtype0*
shape
:@@
�

Assign_208Assign=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_optPlaceholder_181*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
@
Placeholder_182Placeholder*
dtype0*
shape
:@@
�

Assign_209Assign?critic/q/q2_encoding/q2_encoder/hidden_1/kernel/sac_value_opt_1Placeholder_182*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_locking(*
validate_shape(
<
Placeholder_183Placeholder*
dtype0*
shape:@
�

Assign_210Assign;critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_optPlaceholder_183*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
<
Placeholder_184Placeholder*
dtype0*
shape:@
�

Assign_211Assign=critic/q/q2_encoding/q2_encoder/hidden_1/bias/sac_value_opt_1Placeholder_184*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_locking(*
validate_shape(
@
Placeholder_185Placeholder*
dtype0*
shape
:@@
�

Assign_212Assign=critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_optPlaceholder_185*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
@
Placeholder_186Placeholder*
dtype0*
shape
:@@
�

Assign_213Assign?critic/q/q2_encoding/q2_encoder/hidden_2/kernel/sac_value_opt_1Placeholder_186*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_2/kernel*
use_locking(*
validate_shape(
<
Placeholder_187Placeholder*
dtype0*
shape:@
�

Assign_214Assign;critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_optPlaceholder_187*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
<
Placeholder_188Placeholder*
dtype0*
shape:@
�

Assign_215Assign=critic/q/q2_encoding/q2_encoder/hidden_2/bias/sac_value_opt_1Placeholder_188*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_2/bias*
use_locking(*
validate_shape(
@
Placeholder_189Placeholder*
dtype0*
shape
:@@
�

Assign_216Assign=critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_optPlaceholder_189*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
@
Placeholder_190Placeholder*
dtype0*
shape
:@@
�

Assign_217Assign?critic/q/q2_encoding/q2_encoder/hidden_3/kernel/sac_value_opt_1Placeholder_190*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_3/kernel*
use_locking(*
validate_shape(
<
Placeholder_191Placeholder*
dtype0*
shape:@
�

Assign_218Assign;critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_optPlaceholder_191*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
<
Placeholder_192Placeholder*
dtype0*
shape:@
�

Assign_219Assign=critic/q/q2_encoding/q2_encoder/hidden_3/bias/sac_value_opt_1Placeholder_192*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_3/bias*
use_locking(*
validate_shape(
@
Placeholder_193Placeholder*
dtype0*
shape
:@@
�

Assign_220Assign6critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_optPlaceholder_193*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
@
Placeholder_194Placeholder*
dtype0*
shape
:@@
�

Assign_221Assign8critic/q/q2_encoding/extrinsic_q2/kernel/sac_value_opt_1Placeholder_194*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_locking(*
validate_shape(
<
Placeholder_195Placeholder*
dtype0*
shape:@
�

Assign_222Assign4critic/q/q2_encoding/extrinsic_q2/bias/sac_value_optPlaceholder_195*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
<
Placeholder_196Placeholder*
dtype0*
shape:@
�

Assign_223Assign6critic/q/q2_encoding/extrinsic_q2/bias/sac_value_opt_1Placeholder_196*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_locking(*
validate_shape(
@
Placeholder_197Placeholder*
dtype0*
shape
:@@
�

Assign_224Assign6critic/q/q2_encoding/curiosity_q2/kernel/sac_value_optPlaceholder_197*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
@
Placeholder_198Placeholder*
dtype0*
shape
:@@
�

Assign_225Assign8critic/q/q2_encoding/curiosity_q2/kernel/sac_value_opt_1Placeholder_198*
T0*;
_class1
/-loc:@critic/q/q2_encoding/curiosity_q2/kernel*
use_locking(*
validate_shape(
<
Placeholder_199Placeholder*
dtype0*
shape:@
�

Assign_226Assign4critic/q/q2_encoding/curiosity_q2/bias/sac_value_optPlaceholder_199*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
<
Placeholder_200Placeholder*
dtype0*
shape:@
�

Assign_227Assign6critic/q/q2_encoding/curiosity_q2/bias/sac_value_opt_1Placeholder_200*
T0*9
_class/
-+loc:@critic/q/q2_encoding/curiosity_q2/bias*
use_locking(*
validate_shape(
8
Placeholder_201Placeholder*
dtype0*
shape: 
�

Assign_228Assignbeta1_power_3Placeholder_201*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
8
Placeholder_202Placeholder*
dtype0*
shape: 
�

Assign_229Assignbeta2_power_3Placeholder_202*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
<
Placeholder_203Placeholder*
dtype0*
shape:
�

Assign_230Assignlog_ent_coef/sac_entropy_optPlaceholder_203*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape(
<
Placeholder_204Placeholder*
dtype0*
shape:
�

Assign_231Assignlog_ent_coef/sac_entropy_opt_1Placeholder_204*
T0*
_class
loc:@log_ent_coef*
use_locking(*
validate_shape("�