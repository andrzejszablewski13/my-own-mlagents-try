﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace KiK
{
    public class Clicked : MonoBehaviour
    {
        // Start is called before the first frame update
        public UnityEvent happend;
        private void OnMouseUpAsButton()
        {
            happend.Invoke();
            Debug.Log(this.gameObject.name);
        }
    }
}