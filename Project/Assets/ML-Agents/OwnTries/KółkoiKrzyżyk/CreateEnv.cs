﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace KiK
{
    public class CreateEnv : MonoBehaviour
    {
        // Start is called before the first frame update
        public GameObject _platePreFab, _darkPreFab, _lightPreFab, _assPreFab;
        public Material _posMove, _unPosMove;
        public int sizeOfMap = 3;
        public Figure[,] _dataMap;
        private GameObject[,] _visDataMap, _assDataMap;
        public Stack<GameObject> _darkStack, _lightStack;
        public UnityEvent happend;
        public bool notOnlyData = true;
        public enum Figure
        {
            Nothing=0,Black=1,White=2
        }
        private void Awake()
        {
            CreateVisEnv();
            /* Debug.Log(PossibleMoveStack(true).Pop());
             MadeMove(PossibleMoveStack(true).Pop(), true);
             Debug.Log(PossibleMoveStack(false).Pop());*/
        }

        public void CreateVisEnv()
        {
            _darkStack = new Stack<GameObject>();
            _lightStack = new Stack<GameObject>();
            if (notOnlyData)
            {
                _visDataMap = new GameObject[sizeOfMap, sizeOfMap];
                _assDataMap = new GameObject[sizeOfMap, sizeOfMap];
                string _tempString;

                for (int i = 0; i < sizeOfMap; i++)
                {
                    for (int j = 0; j < sizeOfMap; j++)
                    {
                        _visDataMap[i, j] = Instantiate(_platePreFab, this.transform);

                        _tempString = i.ToString() + "i" + j.ToString();
                        _visDataMap[i, j].name = _tempString;
                        _visDataMap[i, j].AddComponent<Clicked>().happend = happend;
                        _visDataMap[i, j].transform.localPosition = new Vector3(_visDataMap[i, j].transform.localPosition.x + (i - sizeOfMap / 2),
                            0,
                             _visDataMap[i, j].transform.localPosition.z + (j - sizeOfMap / 2));
                        _assDataMap[i, j] = Instantiate(_assPreFab, this.transform);
                        _assDataMap[i, j].GetComponent<MeshRenderer>().material = _unPosMove;
                        _assDataMap[i, j].transform.localPosition = new Vector3(_visDataMap[i, j].transform.localPosition.x,
                            0.5f,
                             _visDataMap[i, j].transform.localPosition.z);
                    }
                }
            }

            /*        RestartEnv();*/
        }
        public void RestartEnv()
        {
            _dataMap = new Figure[sizeOfMap, sizeOfMap];
            for (int i = 0; i < sizeOfMap; i++)
            {
                for (int j = 0; j < sizeOfMap; j++)
                {
                    _dataMap[i, j] = Figure.Nothing;
                }
            }
            if (notOnlyData)
            {
                for (int i = 0; i < _darkStack.Count; i++)
                {
                    Destroy(_darkStack.Pop());
                }
                for (int i = 0; i < _lightStack.Count; i++)
                {
                    Destroy(_lightStack.Pop());
                }
            }

        }
        public bool InsertFigure(bool isBlack, int x, int y)
        {

            if (isBlack && _dataMap[x, y] != Figure.Black)
            {
                _dataMap[x, y] = Figure.Black;
                if (notOnlyData)
                {
                    for (int i = 0; i < _visDataMap[x, y].transform.childCount; i++)
                    {
                        Destroy(_visDataMap[x, y].transform.GetChild(0).gameObject);
                    }
                    GameObject _temp = Instantiate(_darkPreFab, _visDataMap[x, y].transform);
                    _darkStack.Push(_temp);
                }
            }
            else if (_dataMap[x, y] != Figure.White)
            {
                _dataMap[x, y] = Figure.White;
                if (notOnlyData)
                {
                    for (int i = 0; i < _visDataMap[x, y].transform.childCount; i++)
                    {
                        Destroy(_visDataMap[x, y].transform.GetChild(0).gameObject);
                    }

                    GameObject _temp = Instantiate(_lightPreFab, _visDataMap[x, y].transform);
                    _lightStack.Push(_temp);

                }
            }
            else
            {
                return false;
            }
            return true;
        }
        private Vector2Int _tempCord;
        private Figure startvalue;
        private Vector2Int[] _winCheck = new Vector2Int[24] {new Vector2Int(-1,0),new Vector2Int(1,0),new Vector2Int(-1,0),
                                new Vector2Int(-2,0),new Vector2Int(1,0),new Vector2Int(2,0),
                                new Vector2Int(0,-1),new Vector2Int(0,1),new Vector2Int(0,-1),new Vector2Int(0,-2),new Vector2Int(0,1),new Vector2Int(0,2),
                                new Vector2Int(1,1),new Vector2Int(-1,-1),new Vector2Int(-1,-1),new Vector2Int(-2,-2),new Vector2Int(1,1),new Vector2Int(2,2),
                                new Vector2Int(-1,1),new Vector2Int(1,-1),new Vector2Int(1,-1),new Vector2Int(2,-2),new Vector2Int(-1,1),new Vector2Int(-2,2), };
        public bool MadeMove(Vector2Int startPos, bool isBlack)
        {
            InsertFigure(isBlack, startPos.x, startPos.y);

            startvalue = isBlack ? Figure.Black : Figure.White;
            for (int i = 0; i < _winCheck.Length; i+=2)
            {
                if(startPos.x+_winCheck[i].x>=0 && startPos.x + _winCheck[i].x<sizeOfMap && startPos.y + _winCheck[i].y >= 0 && startPos.y + _winCheck[i].y < sizeOfMap
                    && _dataMap[startPos.x + _winCheck[i].x, startPos.y + _winCheck[i].y]==startvalue)
                {
                    int j = i + 1;
                    if (startPos.x + _winCheck[j].x >= 0 && startPos.x + _winCheck[j].x < sizeOfMap && startPos.y + _winCheck[j].y >= 0 && startPos.y + _winCheck[j].y < sizeOfMap
                    && _dataMap[startPos.x + _winCheck[j].x, startPos.y + _winCheck[j].y] == startvalue)
                    {
                        return true;
                    }
                }
            }
            return false;

        }
        public Stack<Vector2Int> PossibleMoveStack(bool isBlack)
        {
            if (notOnlyData)
            {
                for (int i = 0; i < sizeOfMap; i++)
                {
                    for (int j = 0; j < sizeOfMap; j++)
                    {
                        _assDataMap[i, j].GetComponent<MeshRenderer>().material = _unPosMove;
                    }
                }
            }
            Stack<Vector2Int> moveStak = new Stack<Vector2Int>();


            for (int i = 0; i < sizeOfMap; i++)
            {
                for (int j = 0; j < sizeOfMap; j++)
                {
                    if (_dataMap[i, j] == Figure.Nothing)
                    {
                        _tempCord.x = i;
                        _tempCord.y = j;
                        moveStak.Push(_tempCord);
                        if(notOnlyData)
                        _assDataMap[i, j].GetComponent<MeshRenderer>().material = _posMove;
                    }
                }
            }


            return moveStak;
        }

       
    }
}