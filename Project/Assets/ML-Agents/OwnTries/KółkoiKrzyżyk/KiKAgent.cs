﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Unity.MLAgents;
using Unity.MLAgents.Sensors.Reflection;
using Unity.MLAgents.Policies;
using Unity.MLAgents.Sensors;

namespace KiK
{
    public class KiKAgent : Agent
    {

       /* [Observable] public CreateEnv.Figure[,] dataMap;*/
        public CreateEnv _visEnv;
        public KiKAgent _enemy;
        public bool isBlack;
        private Stack<Vector2Int> _moveStack;
        private int heuInt0 = -1;
        private BehaviorParameters _behPar;
        public override void Initialize()
        {
            _behPar = this.gameObject.GetComponent<BehaviorParameters>();
            Debug.Log(_behPar.BehaviorType);
            if (_behPar.BehaviorType == BehaviorType.HeuristicOnly)
            {
                Academy.Instance.AutomaticSteppingEnabled = false;
                _timeMem = _timeUp;
            }
            if (!isBlack)
            {
                _visEnv.CreateVisEnv();
            }
            if (_behPar.BehaviorType == BehaviorType.HeuristicOnly)
            {
                /* Academy.Instance.EnvironmentStep();*/
                this.OnEpisodeBegin();

            }

        }
        private float _timeMem = 0, _timeUp = 0.1f;
        private Ray ray;
        private RaycastHit hit;
        public void GetClick()
        {

            _timeMem = _timeUp;
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                string _temp;
                if (hit.collider.gameObject.transform.localPosition.y == 0)
                {
                    _temp = hit.collider.gameObject.name;
                    heuInt0 = int.Parse(_temp.Substring(0, _temp.IndexOf('i'))) * _visEnv.sizeOfMap + int.Parse(_temp.Substring(_temp.IndexOf('i') + 1));
                    Debug.Log(int.Parse(_temp.Substring(0, _temp.IndexOf('i'))) * _visEnv.sizeOfMap + int.Parse(_temp.Substring(_temp.IndexOf('i') + 1)) + " " + _temp);

                }
                //Debug.Log(hit);
                this.RequestDecision();
            }

        }
        private void FixedUpdate()
        {
            if (_behPar.BehaviorType == BehaviorType.HeuristicOnly)
            {
                if (heuInt0 < 0 && Input.GetMouseButtonDown(0))
                {
                    GetClick();
                }
                if (_timeMem > 0)
                {
                    _timeMem -= Time.deltaTime;
                    _visEnv.PossibleMoveStack(isBlack);
                    Academy.Instance.EnvironmentStep();
                }
            }
        }

        public override void OnEpisodeBegin()
        {
            /*        Debug.Log("test");*/

            if (!isBlack)
            {


                _visEnv.RestartEnv();
                this.RequestDecision();
            }

        }

        public override void CollectObservations(VectorSensor sensor)
        {

            for (int i = 0; i < _visEnv.sizeOfMap; i++)
            {
                for (int j = 0; j < _visEnv.sizeOfMap; j++)
                {
                    
                    sensor.AddOneHotObservation((int)(_visEnv._dataMap[i, j]), 3);
                }
            }
        }

        private int[] _unPossibleMoves;
        private List<int> _possibleMoves;
        private Vector2Int jk;
        private Vector2Int[] _tempVecTab;
        private void ListOfPosMooves()
        {
            _moveStack = _visEnv.PossibleMoveStack(isBlack);
            _tempVecTab = _moveStack.ToArray();

            if (_moveStack.Count > 0)
            {
                /*            Debug.Log(_moveStack.Count + _temp.Length);*/
                pass = false;
                _possibleMoves = new List<int>();
                for (int i = 0; i < _tempVecTab.Length; i++)
                {

                    jk = _tempVecTab[i];
                    _possibleMoves.Add(jk.x * _visEnv.sizeOfMap + jk.y);
                    /*                Debug.Log((jk.x * _visEnv.sizeOfMap + jk.y) + " " + jk);*/
                }
                _possibleMoves.Sort();
            }
        }
        public override void CollectDiscreteActionMasks(DiscreteActionMasker actionMasker)
        {
            ListOfPosMooves();


            int j = 0;
            if (_possibleMoves.Count > 0)
            {
                _unPossibleMoves = new int[(int)Mathf.Pow(_visEnv.sizeOfMap, 2) - _possibleMoves.Count];
                for (int i = 0; i < Mathf.Pow(_visEnv.sizeOfMap, 2); i++)
                {
                    if (j >= _possibleMoves.Count || i != _possibleMoves[j])
                    {
                        /* Debug.Log(i + " " + j + " " + _unPossibleMoves.Length + " " + _possibleMoves.Count + " " + d + " " +
                             ((int)Mathf.Pow(_visEnv.sizeOfMap, 2) - _moveStack.Count));*/

                        _unPossibleMoves[i - j] = i;
                    }
                    else
                    {
                        j++;
                    }
                }
                actionMasker.SetMask(0, _unPossibleMoves);
            }

        }

        public bool pass = false,win=false;
        public override void OnActionReceived(float[] vectorAction)
        {
            pass = true;
            ListOfPosMooves();
            if (_possibleMoves.Count > 0)
            {
                int i = Mathf.FloorToInt(vectorAction[0]);

                if (_possibleMoves.Contains(i))
                {

                    jk = new Vector2Int(i / _visEnv.sizeOfMap, i % _visEnv.sizeOfMap);
                    win=_visEnv.MadeMove(jk, isBlack);
                    AddReward(0.002f);
                }
                else
                {
                    AddReward(-0.01f);
                    if (_behPar.BehaviorType != BehaviorType.HeuristicOnly)
                    {
                        this.RequestDecision();
                    }
                    else
                    {
                        Debug.Log("test " + i);
                    }


                    return;
                }
            }
            
            if (win || (this.pass))
            {

                if (_behPar.BehaviorType == BehaviorType.InferenceOnly && _enemy._behPar.BehaviorType == BehaviorType.InferenceOnly)
                {
                    using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"C:\Users\Andruplay\Desktop\ml-agents-release_6\config\matches.txt", true))
                    {
                        string _tempstring;
                        if (!isBlack)
                        {
                            _tempstring = _behPar.Model.name + " " + _enemy._behPar.Model.name + " " + win + " " + isBlack;
                        }
                        else
                        {
                            _tempstring = _enemy._behPar.Model.name + " " + _behPar.Model.name + " " + win + " " + isBlack;
                        }
                        file.WriteLine(_tempstring);
                    }
                }
                Debug.Log("test " + Academy.Instance.StepCount + "Is win: " + win + "By black: " + isBlack);
                if (win)
                {

                        AddReward(1);
                        _enemy.AddReward(-1);
                        _enemy.EndEpisode();
                        EndEpisode();
                    
                }
                
                else
                {
                    AddReward(0.2f);
                    _enemy.AddReward(0.2f);
                    _enemy.EndEpisode();
                    EndEpisode();
                }
            }
            else
            {
                /*            Debug.Log("test"+ _behPar.BehaviorType +" "+heuInt0);*/
                if (_behPar.BehaviorType != BehaviorType.HeuristicOnly)
                {

                }
                if (_enemy._behPar.BehaviorType != BehaviorType.HeuristicOnly)
                {
                    _enemy.RequestDecision();
                }

            }

        }

        public override void Heuristic(float[] actionsOut)
        {

            actionsOut[0] = heuInt0;
            heuInt0 = -1;
        }
    }
}